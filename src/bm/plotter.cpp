/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "plotter.h"
#include "bmmisc.h"
#include <QGraphicsScene>
#include <QGraphicsSimpleTextItem>
#include <QPainter>
#include <QDebug>
#include <qmath.h>

// ### Note: There is a potential for refactoring in order to avoid code duplication
// in this file.


// Template method.
QImage Plotter::createImage(QString *error, QList<PointInfo> *pointInfos) const
{
    const QRectF sceneRect_ = sceneRect();

    QGraphicsScene scene_far(sceneRect_); // Background scene (rendered first)
    QGraphicsScene scene_mid_aa(sceneRect_); // Middle scene (antialiased)
    QGraphicsScene scene_near(sceneRect_); // Foreground scene (rendered last)

    if (!drawScenes(&scene_far, &scene_mid_aa, &scene_near, error, pointInfos))
        return QImage();

    Q_ASSERT(scene_far.sceneRect() == scene_mid_aa.sceneRect());
    Q_ASSERT(scene_far.sceneRect() == scene_near.sceneRect());

    QImage image(scene_far.width(), scene_far.height(), QImage::Format_ARGB32);
    {
        QPainter painter(&image);
        scene_far.render(&painter);
    }
    {
        QPainter painter(&image);
        painter.setRenderHint(QPainter::Antialiasing);
        scene_mid_aa.render(&painter);
    }
    {
        QPainter painter(&image);
        scene_near.render(&painter);
    }

    return image;
}

ResultHistoryPlotter::ResultHistoryPlotter(
    const QList<int> &timestamps, const QList<qreal> &values, const int resultId, const int basePos)
    : timestamps(timestamps), values(values), resultId(resultId), basePos(basePos)
    , width(1100), height(450)
{
}

QRectF ResultHistoryPlotter::sceneRect() const
{
    return QRectF(0, 0, width, height);
}

// ### 2 B DOCUMENTED!
// ### 2 B DONE: Maybe factor out parts that this function has in common with
//     other *::drawScenes() implementations
bool ResultHistoryPlotter::drawScenes(
    QGraphicsScene *scene_far, QGraphicsScene *scene_mid_aa, QGraphicsScene *scene_near,
    QString *error, QList<PointInfo> *pointInfos) const
{
    Q_UNUSED(pointInfos);

    if (timestamps.isEmpty()) {
        if (error)
            *error = "no data to plot (no values)";
        return false;
    }
    Q_ASSERT(timestamps.size() == values.size());
    for (int i = 1; i < timestamps.size(); ++i) {
        Q_ASSERT(timestamps.at(i - 1) <= timestamps.at(i));
    }
    Q_ASSERT(basePos < timestamps.size());

    const qreal pad_left = 150;
    const qreal pad_right = 150;
    const qreal pad_top = 50;
    const qreal pad_bottom = 50;
    const qreal xmin = pad_left;
    const qreal xmax = width - pad_right;
    const qreal xdefault = 0.5 * (xmin + xmax);
    const qreal ymin = pad_top;
    const qreal ymax = height - pad_bottom;
    const qreal ydefault = 0.5 * (ymin + ymax);

    qreal loTimestamp = timestamps.first();
    qreal hiTimestamp = timestamps.last();

    qreal xfact = xdefault;
    if (loTimestamp < hiTimestamp) {
        const qreal tfact = 1.0 / (hiTimestamp - loTimestamp);
        xfact = tfact * (xmax - xmin);
    }

    qreal vmin = -1;
    qreal vmax = -1;
    vmin = vmax = values.first();
    for (int i = 1; i < values.size(); ++i) {
        vmin = qMin(vmin, values.at(i));
        vmax = qMax(vmax, values.at(i));
    }

    const qreal vfact = 1 / (vmax - vmin); // zero division handled elsewhere
    const qreal yfact = vfact * (ymax - ymin);

    // Compute scene coordinates of history curve ...
    QList<qreal> x;
    QList<qreal> y;
    for (int i = 0; i < values.size(); ++i) {
        const qreal t = timestamps.at(i);
        const qreal v = values.at(i);
        x.append(((timestamps.size() > 1) || (loTimestamp < hiTimestamp))
                 ? (xmin + (t - loTimestamp) * xfact)
                 : xdefault);
        y.append(BMMisc::v2y(v, ymax, vmin, yfact, ydefault));
    }

    // Draw background ...
    scene_far->setBackgroundBrush(QBrush(Qt::white));

    // Draw axis indicators ...
    const qreal indicatorSize = 5;
    const QColor indicatorColor(119, 119, 255, 255);
    // ... x-axis (all values) ...
    {
        QPainterPath path;
        for (int i = 0; i < x.size(); ++i) {
            path.moveTo(x.at(i), ymax);
            path.lineTo(x.at(i), ymax + indicatorSize);
        }
        scene_far->addPath(path, QPen(indicatorColor));
    }
    // ... y-axises (min and max value only) ...
    {
        QPainterPath path;
        path.moveTo(xmin, ymin);
        path.lineTo(xmin - indicatorSize, ymin);
        path.moveTo(xmin, ymax);
        path.lineTo(xmin - indicatorSize, ymax);
        path.moveTo(xmax, ymin);
        path.lineTo(xmax + indicatorSize, ymin);
        path.moveTo(xmax, ymax);
        path.lineTo(xmax + indicatorSize, ymax);
        scene_far->addPath(path, QPen(indicatorColor));
    }

    // Draw border rectangle  ...
    scene_far->addRect(xmin, ymin, xmax - xmin, ymax - ymin, QPen(QColor(220, 220, 220, 255)));

    if (basePos >= 0) {
        // Draw line indicating the base position ...
        QColor color(0, 0, 255);
        color.setAlpha(100);
        scene_far->addLine(xmin, y.at(basePos), xmax, y.at(basePos), QPen(color, 2));
    }

    const qreal dpSize = 4;

    // Draw history curve between data points ...
    {
        QPainterPath path(QPointF(x.first(), y.first()));
        for (int i = 0; i < x.size(); ++i)
            path.lineTo(x.at(i), y.at(i));
        scene_mid_aa->addPath(path, QPen(QColor(128, 128, 128, 255)));
    }

    // Draw data points (except the one representing the base position if any) ...
    {
        QPainterPath path;
        path.setFillRule(Qt::WindingFill);
        const qreal dpSize_2 = 0.5 * dpSize;
        for (int i = 0; i < x.size(); ++i)
            if ((basePos < 0) || (i != basePos))
                path.addRect(x.at(i) - dpSize_2, y.at(i) - dpSize_2, dpSize, dpSize);

        const QColor color(0, 0, 0, 255);
        scene_near->addPath(path, QPen(color), QBrush(color));
    }

    if (basePos >= 0) {
        // Draw the data point representing the base position ...
        QPainterPath path;
        path.addRect(x.at(basePos) - dpSize / 2, y.at(basePos) - dpSize / 2, dpSize, dpSize);
        const QColor color(0, 0, 0, 255);
        scene_near->addPath(path, QPen(color), QBrush(Qt::yellow)); // ### 4 NOW
    }


    // Draw labels ...
    const qreal labelPad = 10;
    const qreal captionPad = 4;
    qreal captionHeight;

    // ... left y axis ...
    {
        const QString yCaption = "value"; // ### 4 NOW (could e.g. be metric name)
        QFont font;
        font.setPointSize(14);
        QGraphicsSimpleTextItem *text = scene_near->addSimpleText(yCaption, font);
        const qreal x_ = xmin - text->boundingRect().height() - captionPad;
        const qreal y_ = (ymin + ymax) / 2 + text->boundingRect().width() / 2;
        text->setTransform(QTransform().translate(x_, y_).rotate(-90));
        captionHeight = text->boundingRect().height();
    }
    {
        QGraphicsSimpleTextItem *text =
            scene_near->addSimpleText(QString().setNum(vmin));
        text->setPos(
            xmin - captionPad - captionHeight - labelPad - text->boundingRect().width(),
            ymax - text->boundingRect().height() / 2);
    }
    {
        QGraphicsSimpleTextItem *text =
            scene_near->addSimpleText(QString().setNum(vmax));
        text->setPos(
            xmin - captionPad - captionHeight - labelPad - text->boundingRect().width(),
            ymin - text->boundingRect().height() / 2);
    }

    if (basePos >= 0) {
        QGraphicsSimpleTextItem *text =
            scene_near->addSimpleText(QString().setNum(values.at(basePos)));
        text->setPos(
            xmin - captionPad - captionHeight - labelPad - text->boundingRect().width(),
            y.at(basePos) - text->boundingRect().height() / 2);

        // Avoid overlap
        if (!scene_near->collidingItems(text).isEmpty()) {
            scene_near->removeItem(text);
            delete text;
        }
    }

    const bool showNormDiff = true; // ### 4 NOW

    if ((basePos >= 0) && showNormDiff) {
        // ... right y axis ...
        {
            QFont font;
            font.setPointSize(14);
            QGraphicsSimpleTextItem *text =
                scene_near->addSimpleText("norm. diff. between baseline value and value", font);
            const qreal x_ = xmax + captionPad;
            const qreal y_ = (ymin + ymax) / 2 + text->boundingRect().width() / 2;
            text->setTransform(QTransform().translate(x_, y_).rotate(-90));
            captionHeight = text->boundingRect().height();
        }
        {
            QGraphicsSimpleTextItem *text =
                scene_near->addSimpleText(
                    QString().setNum(
                        BMMisc::normalizedDifference(values.at(basePos), vmin), 'f', 4));
            text->setPos(
                xmax + captionPad + captionHeight + labelPad,
                ymax - text->boundingRect().height() / 2);
        }
        {
            QGraphicsSimpleTextItem *text =
                scene_near->addSimpleText(
                    QString().setNum(
                        BMMisc::normalizedDifference(values.at(basePos), vmax), 'f', 4));
            text->setPos(
                xmax + captionPad + captionHeight + labelPad,
                ymin - text->boundingRect().height() / 2);
        }
        {
            QGraphicsSimpleTextItem *text =
                scene_near->addSimpleText(QString().setNum(0.0, 'f', 4));
            text->setPos(
                xmax + captionPad + captionHeight + labelPad,
                y.last() - text->boundingRect().height() / 2);

            // Avoid overlap
            if (!scene_near->collidingItems(text).isEmpty()) {
                scene_near->removeItem(text);
                delete text;
            }
        }
    }

    // ... top x axis ...
    {
        QFont font;
        font.setPointSize(16);
        const QString title = QString("Result history containing baseline result %1").arg(resultId);
        QGraphicsSimpleTextItem *text = scene_near->addSimpleText(title, font);
        const qreal x_ = (xmin + xmax) / 2 - text->boundingRect().width() / 2;
        const qreal y_ = ymin - labelPad - text->boundingRect().height();
        text->translate(x_, y_);
    }

    // ... bottom x axis ...
    QDateTime dateTime;
    dateTime.setTime_t(loTimestamp);
    const QString xLabel_lo = dateTime.toString();
    dateTime.setTime_t(hiTimestamp);
    const QString xLabel_hi = dateTime.toString();
    {
        QFont font;
        font.setPointSize(14);
        QGraphicsSimpleTextItem *text = scene_near->addSimpleText("time", font);
        const qreal x_ = (xmin + xmax) / 2 - text->boundingRect().width() / 2;
        const qreal y_ = ymax + labelPad;
        text->translate(x_, y_);
    }
    {
        QGraphicsSimpleTextItem *text = scene_near->addSimpleText(xLabel_lo);
        text->setPos(
            xmin - text->boundingRect().width() / 2,
            ymax + labelPad);
    }
    {
        QGraphicsSimpleTextItem *text = scene_near->addSimpleText(xLabel_hi);
        text->setPos(
            xmax - text->boundingRect().width() / 2,
            ymax + labelPad);
    }

    return true;
}


IndexPlotter::IndexPlotter(
    const QList<int> &timestamps, const QList<qreal> &values, const QList<int> &contributions,
    const int baseTimestamp, const int baseValuePos)
    : timestamps(timestamps), values(values), contributions(contributions)
    , baseTimestamp(baseTimestamp), baseValuePos(baseValuePos)
{
    width = 1100;
    height = 450;
}

QRectF IndexPlotter::sceneRect() const
{
    return QRectF(0, 0, width, height);
}

// ### 2 B DOCUMENTED!
// ### 2 B DONE: Maybe factor out parts that this function has in common with
//     other *::drawScenes() implementations
bool IndexPlotter::drawScenes(
    QGraphicsScene *scene_far, QGraphicsScene *scene_mid_aa, QGraphicsScene *scene_near,
    QString *error, QList<PointInfo> *pointInfos) const
{
    // --- BEGIN main graph ---

    if (timestamps.isEmpty()) {
        if (error)
            *error = "no data to plot (no values)";
        return false;
    }
    Q_ASSERT(timestamps.size() == values.size());
    Q_ASSERT(timestamps.size() == contributions.size());
    for (int i = 1; i < timestamps.size(); ++i) {
        Q_ASSERT(timestamps.at(i - 1) <= timestamps.at(i));
    }

    const qreal pad_left = 150;
    const qreal pad_right = 150;
    const qreal pad_top = 50;
    const qreal pad_bottom = 50;

    const qreal xmin = pad_left;
    const qreal xmax = width - pad_right;
    const qreal xdefault = 0.5 * (xmin + xmax);
    const qreal ymin = pad_top;
    const qreal ymax = height - pad_bottom;
    const qreal ydefault = 0.5 * (ymin + ymax);

    qreal loTimestamp = timestamps.first();
    qreal hiTimestamp = timestamps.last();

    qreal xfact = xdefault;
    if (loTimestamp < hiTimestamp) {
        const qreal tfact = 1.0 / (hiTimestamp - loTimestamp);
        xfact = tfact * (xmax - xmin);
    }

    // Shift curve vertically according to base value if present ...
    QList<qreal> dispValues(values);
    const qreal dispBaseValue = 0.0; // by definition
    qreal baseValue = 0.0;

    if (baseValuePos >= 0) {
        Q_ASSERT(baseValuePos < values.size());
        baseValue = values.at(baseValuePos);
        for (int i = 0; i < values.size(); ++i)
            dispValues[i] -= baseValue;
    }

    QList<bool> missing;
    for (int i = 0; i < contributions.size(); ++i)
        missing.append(contributions.at(i) == 0);

    int vminPos = -1;
    int vmaxPos = -1;
    const int firstNonMissing = missing.indexOf(false);
    if (firstNonMissing >= 0) {
        vminPos = vmaxPos = firstNonMissing;
        for (int i = firstNonMissing + 1; i < dispValues.size(); ++i) {
            if (!missing.at(i)) {
                if (dispValues.at(i) < dispValues.at(vminPos))
                    vminPos = i;
                if (dispValues.at(i) > dispValues.at(vmaxPos))
                    vmaxPos = i;
            }
        }
    }

    const qreal vmin = (vminPos >= 0) ? dispValues.at(vminPos) : -1;
    const qreal vmax = (vmaxPos >= 0) ? dispValues.at(vmaxPos) : -1;

    const qreal vfact = 1 / (vmax - vmin); // zero division handled elsewhere
    const qreal yfact = vfact * (ymax - ymin);

    // Compute scene coordinates of history curve ...
    QList<qreal> x;
    QList<qreal> y;
    for (int i = 0; i < timestamps.size(); ++i) {
        const qreal t = timestamps.at(i);
        const qreal v = dispValues.at(i);
        x.append(((timestamps.size() > 1) || (loTimestamp < hiTimestamp))
                 ? (xmin + (t - loTimestamp) * xfact)
                 : xdefault);
        y.append(BMMisc::v2y(v, ymax, vmin, yfact, ydefault));
    }

    // Draw background ...
    scene_far->setBackgroundBrush(QBrush(Qt::white));

    // Draw axis indicators ...
    const qreal indicatorSize = 5;
    const QColor indicatorColor(119, 119, 255, 255);
    // ... x-axis (all values) ...
    {
        QPainterPath path;
        for (int i = 0; i < x.size(); ++i) {
            path.moveTo(x.at(i), ymax);
            path.lineTo(x.at(i), ymax + indicatorSize);
        }
        scene_far->addPath(path, QPen(indicatorColor));
    }
    // ... y-axises (min and max value only) ...
    {
        QPainterPath path;
        path.moveTo(xmin, ymin);
        path.lineTo(xmin - indicatorSize, ymin);
        path.moveTo(xmin, ymax);
        path.lineTo(xmin - indicatorSize, ymax);
        path.moveTo(xmax, ymin);
        path.lineTo(xmax + indicatorSize, ymin);
        path.moveTo(xmax, ymax);
        path.lineTo(xmax + indicatorSize, ymax);
        scene_far->addPath(path, QPen(indicatorColor));
    }

    // Draw border rectangle  ...
    scene_far->addRect(xmin, ymin, xmax - xmin, ymax - ymin, QPen(QColor(220, 220, 220, 255)));

    if (baseValuePos >= 0) {
        // Draw line indicating the base value ...
        QColor color(50, 50, 255);
        const qreal y_base = BMMisc::v2y(dispBaseValue, ymax, vmin, yfact, ydefault);
        scene_far->addLine(xmin, y_base, xmax, y_base, QPen(color, 2));
    }

    if (baseTimestamp < 0)
        baseTimestamp = timestamps.last();

    if ((baseTimestamp >= timestamps.first()) && (baseTimestamp <= timestamps.last())) {
        // Draw line indicating the base timestamp ...
        QColor color(50, 50, 255);
        const qreal btx = xmin + (baseTimestamp - loTimestamp) * xfact;
        QGraphicsItem *btItem = scene_far->addLine(btx, ymin, btx, ymax, QPen(color, 2));
        btItem->setZValue(1);
    }

    const int nDispTimestamps = 8;
    const int dispTimestampStep = (hiTimestamp - loTimestamp) / (nDispTimestamps - 1);
    QList<qreal> dispTimestamps = QList<qreal>() << loTimestamp << hiTimestamp;
    for (int i = 1; i < nDispTimestamps - 1; ++i)
        dispTimestamps.insert(i, loTimestamp + i * dispTimestampStep);

    // Draw vertical lines indicating display timestamps ...
    for (int i = 0; i < dispTimestamps.size(); ++i) {
        const qreal xpos = xmin + (dispTimestamps.at(i) - loTimestamp) * xfact;
        scene_far->addLine(xpos, ymin, xpos, ymax, QPen(QColor(200, 200, 200), 1));
    }

    const qreal dpSize = 3;

    // Draw history curve between data points that are not missing ...
    if (missing.count(false) > 1)
    {
        int pos = missing.indexOf(false, 0);
        QPainterPath path(QPointF(x.at(pos), y.at(pos)));
        while (true) {
            pos = missing.indexOf(false, pos + 1);
            if (pos == -1)
                break;
            path.lineTo(x.at(pos), y.at(pos));
        }
        scene_mid_aa->addPath(path, QPen(QColor(128, 128, 128, 255)));
    }

    // Draw data points ...
    {
        QPainterPath path;
        path.setFillRule(Qt::WindingFill);
        QPainterPath missingPath;
        const qreal ymid = 0.5 * (ymin + ymax);
        const qreal dpSize_2 = 0.5 * dpSize;
        for (int i = 0; i < x.size(); ++i) {
            QRectF rect;
            if (missing.at(i)) {
                rect = QRectF(x.at(i) - dpSize_2, ymid - dpSize_2, dpSize, dpSize);
                missingPath.moveTo(rect.topLeft());
                missingPath.lineTo(rect.bottomRight());
                missingPath.moveTo(rect.bottomLeft());
                missingPath.lineTo(rect.topRight());
            } else {
                rect = QRectF(x.at(i) - dpSize_2, y.at(i) - dpSize_2, dpSize, dpSize);
                path.addRect(rect);
            }
            if (pointInfos)
                pointInfos->append(PointInfo(rect, dispValues.at(i)));
        }

        const QColor color(0, 0, 0, 255);
        scene_near->addPath(path, QPen(color), QBrush(color));
        const QColor missingColor(255, 0, 0, 255);
        scene_near->addPath(missingPath, QPen(missingColor, 2), QBrush(missingColor));
    }

    // Draw labels ...
    const qreal labelPad = 10;
    const qreal captionPad = 4;
    qreal captionHeight;
    QFont axisCaptionFont;
    axisCaptionFont.setPointSize(14);
    QFont axisNumberFont;
    axisNumberFont.setFamily("mono");

    // ... left y axis ...
    {
        QGraphicsSimpleTextItem *text = scene_near->addSimpleText("base diff", axisCaptionFont);
        const qreal x_ = xmin - text->boundingRect().height() - captionPad;
        const qreal y_ = (ymin + ymax) / 2 + text->boundingRect().width() / 2;
        text->setTransform(QTransform().translate(x_, y_).rotate(-90));
        captionHeight = text->boundingRect().height();
    }
    {
        QGraphicsSimpleTextItem *text =
            scene_near->addSimpleText(QString().setNum(vmin), axisNumberFont);
        text->setPos(
            xmin - captionPad - captionHeight - labelPad - text->boundingRect().width(),
            ymax - text->boundingRect().height() / 2);
    }
    {
        QGraphicsSimpleTextItem *text =
            scene_near->addSimpleText(QString().setNum(vmax), axisNumberFont);
        text->setPos(
            xmin - captionPad - captionHeight - labelPad - text->boundingRect().width(),
            ymin - text->boundingRect().height() / 2);
    }

    if ((baseValuePos >= 0) && (dispBaseValue >= vmin) && (dispBaseValue <= vmax)) {
        QGraphicsSimpleTextItem *text =
            scene_near->addSimpleText(QString().setNum(dispBaseValue), axisNumberFont);
        const qreal y_base = BMMisc::v2y(dispBaseValue, ymax, vmin, yfact, ydefault);
        text->setPos(
            xmin - captionPad - captionHeight - labelPad - text->boundingRect().width(),
            y_base - text->boundingRect().height() / 2);

        // Avoid overlap ...
        if (!scene_near->collidingItems(text).isEmpty()) {
            scene_near->removeItem(text);
            delete text;
        }
    }


    // ... right y axis ...
    const qreal basePercentageLo = 100 * qPow(2, vmin);
    const qreal basePercentageHi = 100 * qPow(2, vmax);
    {
        QGraphicsSimpleTextItem *text = scene_near->addSimpleText("base %", axisCaptionFont);
        const qreal x_ = xmax + captionPad;
        const qreal y_ = (ymin + ymax) / 2 + text->boundingRect().width() / 2;
        text->setTransform(QTransform().translate(x_, y_).rotate(-90));
        captionHeight = text->boundingRect().height();
    }
    {
        QGraphicsSimpleTextItem *text =
            scene_near->addSimpleText(
                QString("%1 %")
                .arg(QString().setNum(basePercentageLo, 'f', 3).rightJustified(7, ' ')),
                axisNumberFont);
        text->setPos(
            xmax + captionPad + captionHeight + labelPad,
            ymax - text->boundingRect().height() / 2);
    }
    {
        QGraphicsSimpleTextItem *text =
            scene_near->addSimpleText(
                QString("%1 %")
                .arg(QString().setNum(basePercentageHi, 'f', 3).rightJustified(7, ' ')),
                axisNumberFont);
        text->setPos(
            xmax + captionPad + captionHeight + labelPad,
            ymin - text->boundingRect().height() / 2);
    }

    if ((baseValuePos >= 0) && (basePercentageLo <= 100) && (100 <= basePercentageHi)) {
        QGraphicsSimpleTextItem *text =
            scene_near->addSimpleText(
                QString("%1 %").arg(QString().setNum(100.0, 'f', 3)), axisNumberFont);
        const qreal y_base = BMMisc::v2y(dispBaseValue, ymax, vmin, yfact, ydefault);
        text->setPos(
            xmax + captionPad + captionHeight + labelPad,
            y_base - text->boundingRect().height() / 2);

        // Avoid overlap ...
        if (!scene_near->collidingItems(text).isEmpty()) {
            scene_near->removeItem(text);
            delete text;
        }
    }


    // ... top x axis ...
    {
        QFont font;
        font.setPointSize(16);
        const QString title = QString("Qt Performance Index (<description>)");
        QGraphicsSimpleTextItem *text = scene_near->addSimpleText(title, font);
        const qreal x_ = (xmin + xmax) / 2 - text->boundingRect().width() / 2;
        const qreal y_ = ymin - labelPad - text->boundingRect().height();
        text->translate(x_, y_);
    }

    // ... bottom x axis ...

    {
        QFont font;
        font.setPointSize(14);

        for (int i = 0; i < dispTimestamps.size(); ++i) {

            const qreal timestamp = dispTimestamps.at(i);

            const QString xLabel = BMMisc::compactDateString(timestamp);
            font.setPointSize(12);

            const qreal xpos = xmin + (timestamp - loTimestamp) * xfact;

            // ... bottom x axis label ...
            {
                QGraphicsSimpleTextItem *text = scene_near->addSimpleText(xLabel, font);
                text->setPos(
                    xpos - text->boundingRect().width() / 2,
                    ymax + labelPad * 2);
            }
        }
    }

    return true;
}


HistoriesPlotter::HistoriesPlotter(
    const QList<ResultHistoryInfo *> &rhInfos, const bool showBenchmark, const int baseTimestamp,
    const QList<int> *basePos, const QList<QList<int> > *extraPos,
    const QList<int> *extraTimestamps, const QStringList *extraDescr,
    const bool commonValueRange, const bool showOutliers)
    : rhInfos(rhInfos)
    , showBenchmark(showBenchmark)
    , baseTimestamp(baseTimestamp)
    , basePos(basePos)
    , extraPos(extraPos)
    , extraTimestamps(extraTimestamps)
    , extraDescr(extraDescr)
    , commonValueRange(commonValueRange)
    , showOutliers(showOutliers)
    , width(1200)
    , rhHeight(
        qMin(showBenchmark ? 220 : 170, qMax(showBenchmark ? 170 : 120, 800 / rhInfos.size())))
    , pad_top(50)
    , pad_bottom(50)
    , height(pad_top + pad_bottom + rhInfos.size() * rhHeight)
{
}

QRectF HistoriesPlotter::sceneRect() const
{
    return QRectF(0, 0, width, height);
}

struct MetricInfo {
    qreal vmin;
    qreal vmax;
    MetricInfo(const qreal vmin, const qreal vmax)
        : vmin(vmin), vmax(vmax) {}
};

// ### 2 B DOCUMENTED!
static void appendYAxisLabel(
    const QString &label, const QColor &color, QGraphicsScene *scene, qreal *textYPos,
    qreal xmax, qreal labelPad)
{
    QFont font;
    font.setPointSize(10);
    font.setFamily("mono");

    const qreal labelPad_rh_ver = 3;

    QGraphicsSimpleTextItem *text = scene->addSimpleText(label, font);
    text->setPos(xmax + labelPad, *textYPos);
    (*textYPos) += text->boundingRect().height() + labelPad_rh_ver;

    // Adjust the scene rect horizontally to accommodate the label (assuming scene
    // top-left corner is at (0, 0)) ...
    const qreal requiredSceneWidth = text->pos().x() + text->boundingRect().width();
    if (requiredSceneWidth > scene->sceneRect().width())
        scene->setSceneRect(QRectF(0, 0, requiredSceneWidth, scene->sceneRect().height()));

    QGraphicsItem *bgRect = scene->addRect(text->boundingRect(), QPen(color), QBrush(color));
    bgRect->setPos(text->pos());
    bgRect->setZValue(-1);
}

// ### 2 B DOCUMENTED!
// ### 2 B DONE: Maybe factor out parts that this function has in common with
//     other *::drawScenes() implementations
bool HistoriesPlotter::drawScenes(
    QGraphicsScene *scene_far, QGraphicsScene *scene_mid_aa, QGraphicsScene *scene_near,
    QString *error, QList<PointInfo> *pointInfos) const
{
    Q_UNUSED(pointInfos);

    if (rhInfos.isEmpty()) {
        if (error)
            *error = "no result histories to plot";
        return false;
    }

    const qreal pad_left = 150;
    const qreal pad_right = 300;
    const qreal pad_rh = 10;
    const qreal xmin = pad_left;
    const qreal xmax = width - pad_right;
    const qreal xdefault = 0.5 * (xmin + xmax);

    // Find global timestamp range ...
    qreal loTimestamp = -1;
    qreal hiTimestamp = -1;
    for (int i = 0; i < rhInfos.size(); ++i) {
        const ResultHistoryInfo *rhInfo = rhInfos.at(i);
        if (i == 0) {
            loTimestamp = rhInfo->timestamp(0);
            hiTimestamp = rhInfo->timestamp(rhInfo->size() - 1);
        } else {
            loTimestamp = qMin(loTimestamp, rhInfo->timestamp(0));
            hiTimestamp = qMax(hiTimestamp, rhInfo->timestamp(rhInfo->size() - 1));
        }
    }

    QMap<QString, MetricInfo *> metricInfos;
    if (commonValueRange)  {
        // Find global per-metric value ranges ...
        for (int i = 0; i < rhInfos.size(); ++i) {
            const ResultHistoryInfo *rhInfo = rhInfos.at(i);
            const QString metric = rhInfo->metric();
            if (!metricInfos.contains(metric)) {
                const qreal firstValue = rhInfo->value(0);
                metricInfos.insert(metric, new MetricInfo(firstValue, firstValue));
            }
            MetricInfo *metricInfo = metricInfos.value(metric);
            for (int j = 0; j < rhInfo->size(); ++j) {
                const qreal value = rhInfo->value(j);
                metricInfo->vmin = qMin(metricInfo->vmin, value);
                metricInfo->vmax = qMax(metricInfo->vmax, value);
            }
        }
    }

    qreal xfact = xdefault;
    if (loTimestamp < hiTimestamp) {
        const qreal tfact = 1.0 / (hiTimestamp - loTimestamp);
        xfact = tfact * (xmax - xmin);
    }

    const int nDispTimestamps = 8;
    const int dispTimestampStep = (hiTimestamp - loTimestamp) / (nDispTimestamps - 1);
    QList<qreal> dispTimestamps = QList<qreal>() << loTimestamp << hiTimestamp;
    for (int i = 1; i < nDispTimestamps - 1; ++i)
        dispTimestamps.insert(i, loTimestamp + i * dispTimestampStep);

    QFont font;
    const qreal labelPad = 10;

    Q_ASSERT((!extraPos) || (extraPos->size() == rhInfos.size()));
    Q_ASSERT((!extraDescr) || (extraDescr->size() == rhInfos.size()));

    // Plot the result histories below each other ...
    for (int rh = 0; rh < rhInfos.size(); ++rh) {

        ResultHistoryInfo *rhInfo = rhInfos.at(rh);

        const qreal ymin = pad_top + rh * rhHeight + pad_rh;
        const qreal ymax = pad_top + (rh + 1) * rhHeight - pad_rh;
        const qreal ydefault = 0.5 * (ymin + ymax);
        qreal vmin;
        qreal vmax;
        if (commonValueRange)  {
            MetricInfo *metricInfo = metricInfos.value(rhInfo->metric());
            vmin = metricInfo->vmin;
            vmax = metricInfo->vmax;
        } else {
            vmin = rhInfo->minValue();
            vmax = rhInfo->maxValue();
        }

        const qreal vfact = 1 / (vmax - vmin); // zero division handled elsewhere:
        const qreal yfact = vfact * (ymax - ymin);

        // Compute scene coordinates of history curve ...
        QList<qreal> x;
        QList<qreal> y;
        for (int i = 0; i < rhInfo->size(); ++i) {
            const qreal t = rhInfo->timestamp(i);
            const qreal v = rhInfo->value(i);
            x.append(((rhInfo->size() > 1) || (loTimestamp < hiTimestamp))
                     ? (xmin + (t - loTimestamp) * xfact)
                     : xdefault);
            y.append(BMMisc::v2y(v, ymax, vmin, yfact, ydefault));
        }

        // Draw background ...
        scene_far->setBackgroundBrush(QBrush(Qt::white));

        // Draw axis indicators ...
        const qreal indicatorSize = 5;
        const QColor indicatorColor(119, 119, 255, 255);
        // ... x-axis (all values) ...
        {
            QPainterPath path;
            for (int i = 0; i < x.size(); ++i) {
                path.moveTo(x.at(i), ymax);
                path.lineTo(x.at(i), ymax + indicatorSize);
            }
            scene_far->addPath(path, QPen(indicatorColor));
        }
        // ... y-axises (min and max value only) ...
        {
            QPainterPath path;
            path.moveTo(xmin, ymin);
            path.lineTo(xmin - indicatorSize, ymin);
            path.moveTo(xmin, ymax);
            path.lineTo(xmin - indicatorSize, ymax);
            path.moveTo(xmax, ymin);
            path.lineTo(xmax + indicatorSize, ymin);
            path.moveTo(xmax, ymax);
            path.lineTo(xmax + indicatorSize, ymax);
            scene_far->addPath(path, QPen(indicatorColor));
        }

        // Draw background rectangle  ...
        {
            QGraphicsItem *bgRect = scene_far->addRect(
                xmin, ymin, xmax - xmin, ymax - ymin, QPen(Qt::NoPen),
                QBrush(QColor(245, 245, 245, 255)));
            bgRect->setZValue(-2);
        }

        // Draw border rectangle  ...
        {
            QGraphicsItem *bgRect = scene_far->addRect(
                xmin, ymin, xmax - xmin, ymax - ymin, QPen(QColor(200, 200, 200, 255)));
            bgRect->setZValue(-1);
        }

        if ((baseTimestamp >= loTimestamp) && (baseTimestamp <= hiTimestamp)) {
            // Draw line indicating the base timestamp ...
            QColor color(0, 0, 255);
            color.setAlpha(200);
            const qreal btx = xmin + (baseTimestamp - loTimestamp) * xfact;
            scene_far->addLine(btx, ymin, btx, ymax, QPen(color, 2));
        }

        if (extraTimestamps) {
            // Draw lines indicating extra timestamps ...

            for (int i = 0; i < extraTimestamps->size(); ++i) {

                const int t = extraTimestamps->at(i);

                if ((t < loTimestamp) || (t > hiTimestamp))
                    continue;

                QColor color("#803300");
                color.setAlpha(200);
                const qreal tx = xmin + (t - loTimestamp) * xfact;
                scene_far->addLine(
                    tx, ymin, tx, ymax,
                    QPen(color, 2, (i == 0) ? Qt::DashLine : Qt::SolidLine));
            }
        }

        const qreal dpSize = 4;

        // Draw history curve between data points ...
        {
            QPainterPath path(QPointF(x.first(), y.first()));
            for (int i = 0; i < x.size(); ++i)
                path.lineTo(x.at(i), y.at(i));
            scene_mid_aa->addPath(path, QPen(QColor(100, 100, 100, 255)));
        }

        // Draw data points ...
        {
            QPainterPath path;
            QPainterPath outlierPath;
            path.setFillRule(Qt::WindingFill);
            const qreal dpSize_2 = 0.5 * dpSize;
            for (int i = 0; i < x.size(); ++i) {
                const QRect rect(x.at(i) - dpSize_2, y.at(i) - dpSize_2, dpSize, dpSize);
                if (showOutliers && rhInfo->isOutlier(i))
                    outlierPath.addRect(rect);
                else
                    path.addRect(rect);
            }

            const QColor color(50, 50, 50, 255);
            scene_near->addPath(path, QPen(color), QBrush(color));
            if (!outlierPath.isEmpty()) {
                const QColor outlierColor(255, 0, 0, 255);
                QGraphicsItem *item =
                    scene_near->addPath(outlierPath, QPen(outlierColor), QBrush(outlierColor));
                item->setZValue(-1);
            }
        }

        if (basePos) {
            // Highlight base position as a blue circle (for now) ...

            const int bPos = basePos->at(rh);

            const qreal dpSize_large = dpSize * 4;
            const qreal dpSize_large_2 = dpSize_large / 2;

            QPainterPath path;
            path.addEllipse(
                x.at(bPos) - dpSize_large_2,
                y.at(bPos) - dpSize_large_2,
                dpSize_large, dpSize_large);
            QGraphicsItem *basePosItem = scene_far->addPath(
                path, QPen(QColor(0, 0, 255, 255)), QBrush(QColor(200, 200, 255, 255)));
            basePosItem->setZValue(-1);
        }

        if (extraPos) {
            // Highlight extra positions - the first one as a brown square and the others
            // as brown circles (for now) ...

            QList<int> exPos = extraPos->at(rh);

            const qreal dpSize_large = dpSize * 3;
            const qreal dpSize_large_2 = dpSize_large / 2;

            for (int i = 0; i < exPos.size(); ++i) {

                const int pos = exPos.at(i);
                if ((pos < 0) || (pos >= x.size()))
                    continue;

                QPainterPath path;

                if (i == 0) {
                    path.addRect(
                        x.at(pos) - dpSize_large_2,
                        y.at(pos) - dpSize_large_2,
                        dpSize_large, dpSize_large);
                } else {
                    path.addEllipse(
                        x.at(pos) - dpSize_large_2,
                        y.at(pos) - dpSize_large_2,
                        dpSize_large, dpSize_large);
                }

                QGraphicsItem *extraPosItem = scene_far->addPath(
                    path, QPen(QColor("#803300")), QBrush(QColor("#ff944d")));

                // Ensure the circle is above the square ...
                if (i > 1)
                    extraPosItem->setZValue(1);
            }
        }


        font.setPointSize(12);

        // Draw labels on left y axis ...
        {
            QGraphicsSimpleTextItem *text = scene_near->addSimpleText(QString().setNum(vmin), font);
            text->setPos(
                xmin - labelPad - text->boundingRect().width(),
                ymax - text->boundingRect().height() / 2);
        }
        {
            QGraphicsSimpleTextItem *text = scene_near->addSimpleText(QString().setNum(vmax), font);
            text->setPos(
                xmin - labelPad - text->boundingRect().width(),
                ymin - text->boundingRect().height() / 2);
        }

        // Draw labels on right y axis ...

        qreal textYPos = ymin;

        appendYAxisLabel(
            QString("MT: %1").arg(rhInfo->metric()), QColor(255, 255, 200, 255),
            scene_near, &textYPos, xmax, labelPad);
        appendYAxisLabel(
            QString("PF: %1").arg(rhInfo->platform()), QColor(200, 255, 255, 255),
            scene_near, &textYPos, xmax, labelPad);
        appendYAxisLabel(
            QString("HT: %1").arg(rhInfo->host()), QColor(230, 210, 255, 255),
            scene_near, &textYPos, xmax, labelPad);
        appendYAxisLabel(
            QString("BR: %1 %2").arg(rhInfo->gitRepo()).arg(rhInfo->gitBranch()),
            QColor(255, 230, 210, 255),
            scene_near, &textYPos, xmax, labelPad);
        if (showBenchmark) {
            appendYAxisLabel(
                QString("TC: %1").arg(rhInfo->testCase()), QColor(220, 220, 220, 255),
                scene_near, &textYPos, xmax, labelPad);
            appendYAxisLabel(
                QString("TF: %1").arg(rhInfo->testFunction()), QColor(220, 220, 220, 255),
                scene_near, &textYPos, xmax, labelPad);
            appendYAxisLabel(
                QString("DT: %1").arg(rhInfo->dataTag()), QColor(220, 220, 220, 255),
                scene_near, &textYPos, xmax, labelPad);
        }

        if (extraDescr) {
            appendYAxisLabel(
                QString("%1").arg(extraDescr->at(rh)), QColor(255, 255, 255, 255),
                scene_near, &textYPos, xmax, labelPad);
        }

        // Compensate for a possible expansion of the scene rect of scene_near ...
        if (scene_near->sceneRect() != scene_mid_aa->sceneRect()) {
            scene_mid_aa->setSceneRect(scene_near->sceneRect());
            scene_far->setSceneRect(scene_near->sceneRect());
        }

        // Draw vertical lines indicating display timestamps ...
        for (int i = 0; i < dispTimestamps.size(); ++i) {
            const qreal xpos = xmin + (dispTimestamps.at(i) - loTimestamp) * xfact;
            scene_far->addLine(xpos, ymin, xpos, ymax, QPen(QColor(200, 200, 200), 1));
        }
    }

    // Draw global labels ...

    // ... timestamps ...

    const qreal ymin = pad_top;
    const qreal ymax = height - pad_bottom;

    for (int i = 0; i < dispTimestamps.size(); ++i) {

        const qreal timestamp = dispTimestamps.at(i);

        const QString xLabel = BMMisc::compactDateString(timestamp);
        font.setPointSize(12);

        const qreal xpos = xmin + (timestamp - loTimestamp) * xfact;

        // ... top x axis label ...
        {
            QGraphicsSimpleTextItem *text = scene_near->addSimpleText(xLabel, font);
            text->setPos(
                xpos - text->boundingRect().width() / 2,
                ymin - (text->boundingRect().height() + labelPad));
        }

        // ... bottom x axis label ...
        {
            QGraphicsSimpleTextItem *text = scene_near->addSimpleText(xLabel, font);
            text->setPos(
                xpos - text->boundingRect().width() / 2,
                ymax + labelPad * 2);
        }
    }

    if ((baseTimestamp >= loTimestamp) && (baseTimestamp <= hiTimestamp)) {

        const QString xLabel = BMMisc::compactDateString(baseTimestamp);
        font.setPointSize(12);

        const qreal xpos = xmin + (baseTimestamp - loTimestamp) * xfact;
        const QColor color(0, 0, 255);

        // ... top x axis label ...
        {
            QGraphicsSimpleTextItem *text = scene_near->addSimpleText(xLabel, font);
            text->setPos(
                xpos - text->boundingRect().width() / 2,
                ymin - text->boundingRect().height() + labelPad / 2);
            text->setBrush(color);
        }

        // ... bottom x axis label ...
        {
            QGraphicsSimpleTextItem *text = scene_near->addSimpleText(xLabel, font);
            text->setPos(
                xpos - text->boundingRect().width() / 2,
                ymax + labelPad / 4);
            text->setBrush(color);
        }

    }

    if (extraTimestamps) {

        for (int i = 0; i < extraTimestamps->size(); ++i) {

            const int t = extraTimestamps->at(i);

            if ((t < loTimestamp) || (t > hiTimestamp))
                continue;

            const QString xLabel = BMMisc::compactDateString(t);
            font.setPointSize(12);

            const qreal xpos = xmin + (t - loTimestamp) * xfact;
            const QColor color("#803300");

            // ... top x axis label ...
            {
                QGraphicsSimpleTextItem *text = scene_near->addSimpleText(xLabel, font);
                text->setPos(
                    xpos - text->boundingRect().width() / 2,
                    ymin - text->boundingRect().height() + labelPad / 2);
                text->setBrush(color);
            }

            // ... bottom x axis label ...
            {
                QGraphicsSimpleTextItem *text = scene_near->addSimpleText(xLabel, font);
                text->setPos(
                    xpos - text->boundingRect().width() / 2,
                    ymax + labelPad / 4);
                text->setBrush(color);
            }
        }
    }

    return true;
}
