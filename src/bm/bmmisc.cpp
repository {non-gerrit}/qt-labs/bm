/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "bmmisc.h"
#include <QFile>
#include <QSqlError>
#include <QSqlDriver>
#include <QVariant>
#include <QDebug>
#include <qnumeric.h>
#include <qmath.h>
#include <cstdio>

bool BMMisc::getTimestamp(
    const QStringList &args, int pos, QString *timestamp, QString *error)
{
    Q_ASSERT((0 <= pos) && (pos < args.size()));
    *timestamp = args.at(pos);
    if ((*timestamp != "first") && (*timestamp != "last")) {
        bool ok;
        const int val = timestamp->toInt(&ok);
        if (!ok || val < 0) {
            if (error)
                *error = "failed to extract timestamp as a non-negative integer";
            return false;
        }
    }
    return true;
}

void BMMisc::printJSONOutput(const QString &s)
{
    printf("Content-type: text/json\n\n%s\n", s.toLatin1().data());
}

void BMMisc::printHTMLOutput(const QString &s)
{
    printf("Content-type: text/html\n\n%s\n", s.toLatin1().data());
}

void BMMisc::printHTMLErrorPage(const QString &s)
{
    printHTMLOutput(
        QString("<br /><br /><html><body><strong>ERROR:</strong>&nbsp;&nbsp;%1</body></html>")
        .arg(s));
}

void BMMisc::printImageOutput(
    const QImage &image, const QString &format, const QString &error, bool httpHeader)
{
    if (image.isNull()) {
        if (httpHeader)
            printf("Content-type: text/html\n\n%s\n", error.toLatin1().data());
    } else {
        if (httpHeader)
            printf("Content-type: image/png\n\n");
        QFile out;
        out.open(stdout, QIODevice::WriteOnly);
        image.save(&out, format.toLatin1().data());
        out.close();
    }
}

bool BMMisc::getLastInsertId(QSqlQuery *query, int *id)
{
    // ### hm ... maybe return false and report an error instead of just asserting here?
    // Or even better: write the backup code (involving an extra query to get the new id)!
    if (!query->driver()->hasFeature(QSqlDriver::LastInsertId))
        return false;
    QVariant lastInsertId = query->lastInsertId();
    if (!lastInsertId.isValid())
        return false; // hm ... is this really possible?
    *id = lastInsertId.toInt();
    return true;
}

QString BMMisc::lastQueryError(const QSqlQuery &query)
{
    return query.lastError().text().trimmed().replace(QChar('"'), "&quot;");
}

// Computes the normalized difference between two real values.
qreal BMMisc::normalizedDifference(qreal a, qreal b)
{
    return (a == b) ? 0 : ((a - b) / (qMax(qAbs(a), qAbs(b))));
}

// ### 2 B DOCUMENTED!
QString BMMisc::getOption(const QStringList &args, const QString &name, int offset)
{
    int pos;
    Q_ASSERT(offset >= 0);
    if (((pos = args.indexOf(name)) == -1) || (pos >= (args.size() - (1 + offset))))
        return QString();
    return args.at(pos + 1 + offset);
}

// ### 2 B DOCUMENTED!
bool BMMisc::getOption(
    const QStringList &args, const QString &option, QStringList *values, int nvalues, int nskip,
    QString *error)
{
    int pos = -1;
    for (int i = 0; i <= nskip; ++i) {
        pos = args.indexOf(option, pos + 1);
        if (pos == -1)
            return false;
    }

    if (values)
        *values = QStringList();
    const int lastValuePos = pos + nvalues;
    if (lastValuePos >= args.size()) {
        if (error)
            *error = QString("too few values for %1 option").arg(option);
        return false;
    }
    if (values) {
        for (int i = pos + 1; i <= lastValuePos; ++i)
            values->append(args.at(i).trimmed());
    }

    return true;
}

// ### 2 B DOCUMENTED!
bool BMMisc::getMultiOption(
    const QStringList &args, const QString &option, QStringList *values, QString *error,
    bool unique)
{
    for (int i = 0; ; ++i) {
        QStringList values_;
        if (getOption(args, option, &values_, 1, i, error)) {
            if (!(unique && values->contains(values_.first())))
                values->append(values_.first());
        } else {
            if (!error->isEmpty())
                return false;
            break;
        }
    }
    return true;
}

// ### 2 B DOCUMENTED!
bool BMMisc::getMultiOption2(
    const QStringList &args, const QString &option, QList<QStringList> *values, int n,
    QString *error)
{
    for (int i = 0; ; ++i) {
        QStringList values_;
        if (getOption(args, option, &values_, n, i, error)) {
            Q_ASSERT(values_.size() == n);
            values->append(values_);
        } else {
            if (!error->isEmpty())
                return false;
            break;
        }
    }
    return true;
}

bool BMMisc::hasOption(const QStringList &args, const QString &option)
{
    QStringList dummyValues;
    return getOption(args, option, &dummyValues, 0, 0);
}

// ### 2 B DOCUMENTED!
bool BMMisc::getDoubleOption(
    const QStringList &args, const QString &option, qreal *value, QString *error)
{
    QStringList values;
    if (getOption(args, option, &values, 1, 0, error)) {
        bool ok;
        *value = values.at(0).toDouble(&ok);
        if (!ok) {
            *error = QString("failed to extract %1 as a double").arg(option);
            return false;
        }
    } else {
        if (error->isEmpty())
            *error = QString("%1 option not found").arg(option);
        return false;
    }

    return true;
}

// ### 2 B DOCUMENTED!
bool BMMisc::getClampedPercentageOption(
    const QStringList &args, const QString &option, qreal *value, QString *error)
{
    if (!getDoubleOption(args, option, value, error))
        return false;
    *value = qMin(qMax(*value, 0.0), 100.0);
    return true;
}

// Computes the median value of the numbers in \a values.
qreal BMMisc::median(const QList<qreal> &values)
{
    Q_ASSERT(!values.isEmpty());
    QList<qreal> sortedValues = values;
    qSort(sortedValues);
    return sortedValues.at(sortedValues.size() / 2);
}

// Computes the (zero-based) position of the median value of the numbers in \a values.
// If the median value occurs multiple times, the position of either the last or first
// duplicate is returned depending on whether \a lastDuplicate is true or false respectively.
int BMMisc::medianPos(const QList<qreal> &values, bool lastDuplicate)
{
    Q_ASSERT(!values.isEmpty());
    QList<qreal> sortedValues = values;
    qSort(sortedValues);
    const qreal medianValue = sortedValues.at(sortedValues.size() / 2);
    return lastDuplicate ? values.lastIndexOf(medianValue) : values.indexOf(medianValue);
}

qreal BMMisc::v2y(
    const qreal v, const qreal ymax, const qreal vmin, const qreal yfact, const qreal ydefault)
{
    const qreal y = ymax - (v - vmin) * yfact;
    return qIsFinite(y) ? y : ydefault;
}

QDateTime BMMisc::createCurrDateTime(int timestamp)
{
    if (timestamp < 0)
        return QDateTime::currentDateTime();
    QDateTime currDateTime;
    currDateTime.setTime_t(timestamp);
    return currDateTime;
}

QString BMMisc::secs2daysText(const int secs, const uint precision)
{
    const int secsInDay = 86400; // 24 * 60 * 60
    return QString().setNum(secs / (double)secsInDay, 'f', precision);
}

bool BMMisc::saveImageToFile(const QImage &image, const QString &fileName, QString *error)
{
    // Open output file ...
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly)) {
        *error = QString("failed to open plot file '%1' for writing").arg(fileName);
        return false;
    }

    // Save image to output file ...
    image.save(&file, "png");
    file.close();

    return true;
}

// ### Warning: this function duplicates a JavaScript function!
QString BMMisc::ageColor(const int secs)
{
    const int minSecs = 0;
    const int maxSecs = 31536000; // secs in year
    const int minR = 255;
    const int minG = 128;
    const int minB = 0;
    const int maxR = 228;
    const int maxG = 228;
    const int maxB = 228;

    qreal frac = (qMax(qMin(secs, maxSecs), minSecs) - minSecs) / qreal(maxSecs - minSecs);

    int r = int(qRound((1 - frac) * minR + frac * maxR));
    r = qMax(qMin(r, 255), 0);
    int g = int(qRound((1 - frac) * minG + frac * maxG));
    g = qMax(qMin(g, 255), 0);
    int b = int(qRound((1 - frac) * minB + frac * maxB));
    b = qMax(qMin(b, 255), 0);

    QString color = QString("#%1%2%3")
        .arg(r, 2, 16, QChar('0'))
        .arg(g, 2, 16, QChar('0'))
        .arg(b, 2, 16, QChar('0'));
    return color;
}

// ### Warning: this function duplicates a JavaScript function!
QString BMMisc::redToGreenColor(
    qreal val, qreal minAbsVal, qreal maxAbsVal, bool higherIsBetter)
{
    if (!higherIsBetter)
        val = -val;

    int r = 255;
    int g = 255;
    int b = 255;
    const qreal frac = (qAbs(val) - minAbsVal) / (maxAbsVal - minAbsVal);
    if (val >= minAbsVal) {
        r = (val >= maxAbsVal) ? 0 : qRound((1 - frac) * 255);
        g = 255;
        b = r;
    } else if (val <= -minAbsVal) {
        r = 255;
        g = (val <= -maxAbsVal) ? 0 : qRound((1 - frac) * 255);
        b = g;
    }

    QString color = QString("#%1%2%3")
        .arg(r, 2, 16, QChar('0'))
        .arg(g, 2, 16, QChar('0'))
        .arg(b, 2, 16, QChar('0'));
    return color;
}

// ### Warning: this function duplicates a JavaScript function!
QString BMMisc::diffColor(qreal diff, bool higherIsBetter)
{
    return redToGreenColor(diff, 0.0001, 0.01, higherIsBetter);
}

bool BMMisc::lowerIsBetter(const QString &metric)
{
    if (metric == "FramesPerSecond") // ### More to be added ...
        return false;
    return true;
}

qreal BMMisc::log2val(qreal val, const QString &metric)
{
    Q_ASSERT(val > 0);
    const qreal v = qLn(val) / qLn(2);
    return lowerIsBetter(metric) ? -v : v;
}

qreal BMMisc::log2diff(qreal val1, qreal val2, const QString &metric)
{
    Q_ASSERT(val1 > 0);
    Q_ASSERT(val2 > 0);
    return (log2val(val1 / val2, metric));
}

bool BMMisc::normalize(QList<qreal> &v)
{
    qreal sum = 0.0;
    for (int i = 0; i < v.size(); ++i)
        sum += v.at(i);
    if (sum == 0.0)
        return false;
    for (int i = 0; i < v.size(); ++i) {
        v[i] /= sum;
        Q_ASSERT(qIsFinite(v.at(i)));
    }
    return true;
}

QString BMMisc::compactDateString(int timestamp)
{
    QDateTime dateTime;
    dateTime.setTime_t(timestamp);
    return dateTime.toString("dd MMM yyyy");
}
