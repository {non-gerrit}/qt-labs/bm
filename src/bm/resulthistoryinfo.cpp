/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "resulthistoryinfo.h"
#include "bmmisc.h"
#include <qnumeric.h>
#include <QDebug>

// Searches for the (zero-based) position of the last non-outlier value at or
// before \a timestamp. If found, the position is passed in \a pos and the function returns
// true. Otherwise, the function returns false.
bool ResultHistoryInfo::findSmoothPos(int timestamp, int *pos) const
{
    if (!outliersMarked)
        markOutliers();

    // Locate initial candidate position ...
    QList<int>::const_iterator begin =
        qLowerBound(timestamps_.begin(), timestamps_.end(), timestamp);
    QList<int>::const_iterator end = qUpperBound(begin, timestamps_.end(), timestamp);
    int i = (end - 1) - timestamps_.begin();

    // Skip outliers ...
    while ((i >= 0) && (outliers.testBit(i))) --i;

    if (i == -1)
        return false;

    *pos = i;
    return true;
}

// ### 2 B DOCUMENTED!
qreal ResultHistoryInfo::minValue() const
{
    if (!hasCachedMinVal) {
        cachedMinVal = values.first();
        for (int i = 1; i < timestamps_.size(); ++i)
            cachedMinVal = qMin(cachedMinVal, values.at(i));
        hasCachedMinVal = true;
    }

    return cachedMinVal;
}

// ### 2 B DOCUMENTED!
qreal ResultHistoryInfo::maxValue() const
{
    if (!hasCachedMaxVal) {
        cachedMaxVal = values.first();
        for (int i = 1; i < timestamps_.size(); ++i)
            cachedMaxVal = qMax(cachedMaxVal, values.at(i));
        hasCachedMaxVal = true;
    }

    return cachedMaxVal;
}

// ### 2 B DOCUMENTED!
bool ResultHistoryInfo::isOutlier(int i) const
{
    if ((i < 0) || (i >= values.size()))
        return false;

    if (!outliersMarked)
        markOutliers();

    return outliers.testBit(i);
}

// ### 2 B DOCUMENTED!
void ResultHistoryInfo::markOutliers() const
{
    // Initially mark all values as outliers ...
    const bool ok = outliers.fill(true, values.size());
    Q_ASSERT(ok); // hm ... can this really fail in this case?

    if (medianWinSize <= 0)
        return; // invalid window size  =>  impossible to smooth away any outlier!

    // Slide the median window across the list of values and mark
    // the median positions as non-outliers ...
    Q_ASSERT(medianWinSize > 0);
    const int lastPos = values.size() - medianWinSize;
    for (int pos = 0; pos <= lastPos; ++pos) {
        const int medianPos = pos + BMMisc::medianPos(values.mid(pos, medianWinSize));
        outliers.clearBit(medianPos);
    }

    outliersMarked = true;
}

// ### 2 B DOCUMENTED!
bool ResultHistoryInfo::equal(int i, int j, int diffTolerance) const
{
    if ((i < 0) || (j < 0) || (i >= values.size()) || (j >= values.size()))
        return false;

    if (i == j)
        return true;

    const qreal vi = values.at(i);
    const qreal vj = values.at(j);
    if (vi == vj)
        return true;

    const qreal maxRatio = qAbs(qMax(vi, vj) / qMin(vi, vj));
    if (!qIsFinite(maxRatio))
        return false;

    return (100 * (maxRatio - 1)) <= diffTolerance;
}

// ### 2 B DOCUMENTED!
void ResultHistoryInfo::startSubsequence(
    int pos, qreal diffTolerance, QMap<int, int> *uniqueLevels) const
{
    Q_ASSERT(pos >= 0);
    Q_ASSERT(pos < values.size());

    QMap<int, int>::iterator it;
    for (it = uniqueLevels->begin(); it != uniqueLevels->end(); ++it)
        if (equal(pos, it.key(), diffTolerance))
            break;

    if (it == uniqueLevels->end()) {
        // Insert new unique level ...
        uniqueLevels->insert(pos, 1);
    } else {
        // Add to existing unique level ...
        ++(it.value());
    }
}

// ### 2 B DOCUMENTED!
void ResultHistoryInfo::endSubsequence(int seqSize, int *total, int *stable, int stabTolerance)
{
    if (seqSize <= 0)
        return;
    (*total)++;
    if (seqSize >= stabTolerance)
        (*stable)++;
}

// ### 2 B DOCUMENTED!
void ResultHistoryInfo::computeMaxESSStats(
    qreal diffTolerance, int stabTolerance, int *total, int *stable) const
{
    *total = *stable = 0;

    int basePos = -1;
    int seqSize = 0;

    for (int i = 0; i < values.size(); ++i) {

        if (isOutlier(i)) {
            if (i == values.size() - 1)
                endSubsequence(seqSize, total, stable, stabTolerance);

        } else {

            ++seqSize;

            if (basePos == -1) {
                basePos = i;
            } else if (!equal(basePos, i, diffTolerance)) {
                endSubsequence(seqSize, total, stable, stabTolerance);
                seqSize = 1;
                basePos = i;
            }

            if (i == values.size() - 1)
                endSubsequence(seqSize, total, stable, stabTolerance);
        }

    }
}

// ### 2 B DOCUMENTED!
void ResultHistoryInfo::computeStabilityStats(
    qreal diffTolerance, int stabTolerance, int fromTimestamp, int toTimestamp, bool *zerosFound,
    int *total, int *stable, int *uniqueLevels, qreal *minLevel, qreal *maxLevel,
    bool *maxMinFound) const
{
    *zerosFound = false;
    *total = *stable = *uniqueLevels = 0;
    *minLevel = *maxLevel = -1.0;
    *maxMinFound = false;

    QMap<int, int> uniqueLevels_; // <pos, count>

    int basePos = -1;
    int seqSize = 0;

    bool insideRangePrev = false;
    bool firstInsideRangeSeen = false;

    int smoothFromPos = -1;
    findSmoothPos(fromTimestamp, &smoothFromPos);

    for (int i = 0; i < values.size(); ++i) {

        ++seqSize;

        const int timestamp = timestamps_.at(i);
        const bool insideRange = (i == smoothFromPos)
            || ((timestamp >= fromTimestamp) && (timestamp <= toTimestamp));
        const bool firstInsideRange = insideRange && (!insideRangePrev);
        insideRangePrev = insideRange;

        if (insideRange && (!(*zerosFound)) && (values.at(i) == 0.0))
            *zerosFound = true;

        const qreal val = values.at(i);
        if (firstInsideRange && (!firstInsideRangeSeen)) {
            *minLevel = *maxLevel = val;
            *maxMinFound = true;
            firstInsideRangeSeen = true;
        } else if (insideRange) {
            *minLevel = qMin(*minLevel, val);
            *maxLevel = qMax(*maxLevel, val);
        }

        if (basePos == -1) {
            basePos = i;
            startSubsequence(basePos, diffTolerance, &uniqueLevels_);
        } else if (!equal(basePos, i, diffTolerance)) {
            endSubsequence(seqSize, total, stable, stabTolerance);
            seqSize = 1;
            basePos = i;
            startSubsequence(basePos, diffTolerance, &uniqueLevels_);
        }

        if (i == values.size() - 1)
            endSubsequence(seqSize, total, stable, stabTolerance);
    }

    *uniqueLevels = uniqueLevels_.size();
}
