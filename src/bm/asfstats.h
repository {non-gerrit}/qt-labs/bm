/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef ASFSTATS_H
#define ASFSTATS_H

#include "resulthistoryinfo.h"

// ### 2 B DOCUMENTED!

class ASFStats {
public:
    ASFStats(
        const qreal diffTolerance, const int stabTolerance, const qreal sfTolerance,
        const qreal lfTolerance, const qreal maxLDTolerance, const int fromTimestamp,
        const int toTimestamp)
        : diffTolerance(diffTolerance)
        , stabTolerance(stabTolerance)
        , sfTolerance(sfTolerance)
        , lfTolerance(lfTolerance)
        , maxLDTolerance(maxLDTolerance)
        , fromTimestamp(fromTimestamp)
        , toTimestamp(toTimestamp)
    {}

    struct StatsInfo {
        QBitArray regressed;
        QBitArray unchanged;
        QBitArray improved;
        QBitArray unstable;
        QBitArray usZero;
        QBitArray usLowFromPos;
        QBitArray usLowSF;
        QBitArray usLowLF;
        QBitArray usHighMaxLD;
    };

    void compute(const QList<ResultHistoryInfo *> &rhInfos, StatsInfo *statsInfo);

private:
    // Tolerances:
    qreal diffTolerance; // difference
    int stabTolerance; // stability
    qreal sfTolerance; // stability fraction
    qreal lfTolerance; // level fraction
    qreal maxLDTolerance; // maximum level difference

    // Timestamps:
    int fromTimestamp;
    int toTimestamp;
};

#endif // ASFSTATS_H
