/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef DATAQUALITYSTATS_H
#define DATAQUALITYSTATS_H

#include "resulthistoryinfo.h"
#include <QList>
#include <QMap>
#include <QPair>

class DataQualityStats {
public:
    DataQualityStats(const qreal diffTolerance, const int stabTolerance)
        : diffTolerance(diffTolerance)
        , stabTolerance(stabTolerance)
    {}

    void compute(const QList<ResultHistoryInfo *> &rhInfos);
    QMap<int, int> totalMaxESSFreq() const { return totalMaxESSFreq_; }
    QMap<int, QPair<qreal, qreal> > stabFracPercentiles() const { return stabFracPercentiles_; }

private:
    qreal diffTolerance;
    int stabTolerance;
    QMap<int, int> totalMaxESSFreq_;
    QMap<int, QPair<qreal, qreal> > stabFracPercentiles_;
};

#endif // DATAQUALITYSTATS_H
