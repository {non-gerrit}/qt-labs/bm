/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef BMREQUEST_H
#define BMREQUEST_H

#include "bmmisc.h"
#include "index.h"
#include <QtXml>
#include <QImage>
#include <QColor>

class QSqlDatabase;
class QSqlQuery;

class BMRequest
{
public:
    virtual ~BMRequest() {}

    // --- BEGIN Server API --------------------
    static BMRequest * create(const QByteArray &data, QSqlDatabase *database);

    // Queries the database from the request XML data (in doc). Encodes the reply (based on
    // the query result) as XML and converts to a byte array.
    virtual QByteArray toReplyBuffer() = 0;
    // --- END Server API --------------------

    // --- BEGIN Client API --------------------
    static BMRequest * create(const QByteArray &data);

    // Encodes the request (based on subclass ctor args) as XML and converts to a byte array.
    // If an error is encountered, an empty QByteArray is returned.
    // An error may optionally be described in \a error.
    virtual QByteArray toRequestBuffer(QString *error) = 0;

    enum OutputFormat { Raw, JSON, HTML, Image };
    void handleReply(OutputFormat outputFormat, const QStringList &args) const;
    // --- END Client API --------------------

protected:
    BMRequest() : database(0) {}
    BMRequest(const QDomDocument &doc) : doc(doc), database(0) {}
    QDomDocument doc;
    QSqlDatabase *database;

    enum SpecialDBAction { None, Transaction, Rollback, Commit };
    QSqlQuery *createQuery(SpecialDBAction specialAction = None) const;
    void deleteQuery(QSqlQuery *query, SpecialDBAction specialAction = None) const;

    static QByteArray xmlConvert(
        const QString &, int *errorLine = 0, int *errorCol = 0, QDomDocument *doc_ = 0);

    bool getLastInsertId(QSqlQuery *query, int *id) const;

    bool getBenchmarkId(
        QString *reply, int *benchmarkId, const QString &testCase,
        const QString &testFunction, const QString &dataTag, bool autoInsert = false) const;
    bool getMetricId(
        QString *reply, int *metricId, const QString &metric, bool autoInsert = false) const;
    bool getPlatformId(
        QString *reply, int *platformId, const QString &platform, bool autoInsert = false) const;
    bool getHostId(
        QString *reply, int *hostId, const QString &host, bool autoInsert = false) const;
    bool getBranchId(
        QString *reply, int *branchId, const QString &gitRepo, const QString &gitBranch,
        bool autoInsert = false) const;
    bool getSnapshotId(
        QString *reply, int *snapshotId, const QString &sha1, bool autoInsert = false) const;
    bool getContextId(
        QString *reply, int *id, const QString &metric, const QString &platform,
        const QString &host, const QString &gitRepo, const QString &gitBranch) const;
    bool getOrInsertContextId(
        QString *reply, int *id, const int metricId, const int platformId,
        const int hostId, const int branchId) const;
    bool getBMContextId(
        QString *reply, int *id, const int benchmarkId, const QString &metric,
        const QString &platform, const QString &host, const QString &gitRepo,
        const QString &gitBranch) const;
    bool getBMContextId(QString *reply, int *id, const int benchmarkId, const int contextId) const;
    bool getOrInsertBMContextId(
        QString *reply, int *id, const int contextId, const int benchmarkId) const;
    bool insertOrReplaceResult(
        QString *reply, const int bmcontextId, const int snapshotId, const int timestamp,
        const qreal value) const;
    bool getBenchmark(
        QString *reply, int benchmarkId, QString *testCase, QString *testFunction,
        QString *dataTag) const;
    QString createHistoryQuery(
        int bmcontextId, const QString &timestamp1, const QString &timestamp2, int limit = -1)
        const;
    QString createBenchmarkFilterExpression(
        const QString &testCaseFilter, const QString &testFunctionFilter,
        const QString &dataTagFilter, const bool leadingAnd,
        const QString &prefix = QString()) const;
    bool appendResultHistoryToReply(
        QString *reply, int bmcontextId, const QString &suffix, const QString &timestamp1,
        const QString &timestamp2, qreal diffTolerance, int stabTolerance, int maxSize) const;

    virtual QString name() const = 0;

    // Extracts the result from the reply XML data (in doc) and dumps it to stdout in
    // different formats:
    virtual void handleReply_Raw(const QStringList &args) const = 0;
    virtual void handleReply_JSON(const QStringList &args) const = 0;
    virtual void handleReply_HTML(const QStringList &args) const
    {
        Q_UNUSED(args);
        QString reply;
        reply = QString(
            "<html><head></head><body><b>INTERNAL ERROR:</b> default implementation of "
            "handleReply_HTML() called!</body></html>");
        BMMisc::printHTMLOutput(reply);
    }
    virtual void handleReply_Image(const QStringList &args) const
    {
        Q_UNUSED(args);
        qDebug() << "INTERNAL ERROR: default implementation of handleReply_Image() called!";
    }

private:
    static BMRequest * create(const QByteArray &data, const QString &msgType);

    void setDatabase(QSqlDatabase *database)
    {
        this->database = database;
    }

    bool getId(
        QString *reply, int *id, const QString &table,
        const QStringList &colNames, const QStringList &colValues,
        const QStringList &insertColNames, const QStringList &insertColValues,
        bool autoInsert) const;
};

struct DataTag
{
    QString name;
    QString metric;
    qreal value;
    DataTag(const QString &name, const QString &metric, qreal value)
        : name(name), metric(metric), value(value) {}
};

struct TestFunction
{
    QString name;
    QList<DataTag> dataTags;
    TestFunction(const QString &name) : name(name) {}
};

class BMRequest_PutResults : public BMRequest
{
public:
    BMRequest_PutResults(
        const QString &resultsFile, const QString &platform, const QString &host
        , const QString &gitRepo, const QString &gitDir)
        : resultsFile(resultsFile), platform(platform), host(host), gitRepo(gitRepo)
        , gitDir(gitDir) {}
    BMRequest_PutResults(const QDomDocument &doc) : BMRequest(doc) {}
private:
    QString resultsFile;
    QString platform;
    QString host;
    QString gitRepo;
    QString gitDir;
    QString timestamp;
    QString sha1;
    QString gitBranch;
    QString testCase;
    QList<TestFunction> testFunctions;

    QString name() const { return "PutResults"; }
    QByteArray toRequestBuffer(QString *error);
    QByteArray toReplyBuffer();
    void handleReply_Raw(const QStringList &args) const;
    void handleReply_JSON(const QStringList &args) const { Q_UNUSED(args); /* 2 B DONE! */}
};

class BMRequest_GetContextComponents : public BMRequest
{
public:
    BMRequest_GetContextComponents(
        const QString &metric, const QString &platform, const QString &host,
        const QString &gitRepo, const QString &gitBranch)
        : metric(metric), platform(platform), host(host), gitRepo(gitRepo), gitBranch(gitBranch) {}
    BMRequest_GetContextComponents(const QDomDocument &doc) : BMRequest(doc) {}
protected:
    QString metric;
    QString platform;
    QString host;
    QString gitRepo;
    QString gitBranch;

    virtual QString table() const = 0;
    virtual QStringList colNames() const = 0;

private:
    bool addConstraint(
        const QString &constrTable, const QStringList &constrColNames,
        const QStringList &constrColValues, QString *query_s, bool *constrAdded,
        QString *reply) const;
    bool addConstraints(QString *query_s, QString *reply) const;
    QString colList() const;
    QByteArray toRequestBuffer(QString *error);
    QByteArray toReplyBuffer();
    void handleReply_Raw(const QStringList &args) const;
    void handleReply_JSON(const QStringList &args) const;
};

class BMRequest_GetMetrics : public BMRequest_GetContextComponents
{
public:
    BMRequest_GetMetrics(
        const QString &platform, const QString &host,
        const QString &gitRepo, const QString &gitBranch)
        : BMRequest_GetContextComponents(
            QString(),
            platform,
            host,
            gitRepo, gitBranch) {}
    BMRequest_GetMetrics(const QDomDocument &doc) : BMRequest_GetContextComponents(doc) {}
private:
    QString name() const { return "GetMetrics"; }
    QString table() const { return "metric"; }
    QStringList colNames() const { return QStringList() << "name"; }
};

class BMRequest_GetPlatforms : public BMRequest_GetContextComponents
{
public:
    BMRequest_GetPlatforms(
        const QString &metric, const QString &host,
        const QString &gitRepo, const QString &gitBranch)
        : BMRequest_GetContextComponents(
            metric,
            QString(),
            host,
            gitRepo, gitBranch) {}
    BMRequest_GetPlatforms(const QDomDocument &doc) : BMRequest_GetContextComponents(doc) {}
private:
    QString name() const { return "GetPlatforms"; }
    QString table() const { return "platform"; }
    QStringList colNames() const { return QStringList() << "name"; }
};

class BMRequest_GetHosts : public BMRequest_GetContextComponents
{
public:
    BMRequest_GetHosts(
        const QString &metric, const QString &platform,
        const QString &gitRepo, const QString &gitBranch)
        : BMRequest_GetContextComponents(
            metric,
            platform,
            QString(),
            gitRepo, gitBranch) {}
    BMRequest_GetHosts(const QDomDocument &doc) : BMRequest_GetContextComponents(doc) {}
private:
    QString name() const { return "GetHosts"; }
    QString table() const { return "host"; }
    QStringList colNames() const { return QStringList() << "name"; }
};

class BMRequest_GetBranches : public BMRequest_GetContextComponents
{
public:
    BMRequest_GetBranches(
        const QString &metric, const QString &platform, const QString &host)
        : BMRequest_GetContextComponents(
            metric,
            platform,
            host,
            QString(), QString()) {}
    BMRequest_GetBranches(const QDomDocument &doc) : BMRequest_GetContextComponents(doc) {}
private:
    QString name() const { return "GetBranches"; }
    QString table() const { return "branch"; }
    QStringList colNames() const { return QStringList() << "gitRepo" << "gitBranch"; }
};

class BMRequest_GetHistory : public BMRequest
{
public:
    BMRequest_GetHistory(
        const QString &testCase, const QString &testFunction, const QString &dataTag,
        const QString &metric, const QString &platform, const QString &host,
        const QString &gitRepo, const QString &gitBranch,
        const QString &timestamp1, const QString &timestamp2,
        const qreal diffTolerance, const int stabTolerance, const int maxSize,
        const QString &styleSheet, const int currTimestamp)
        : testCase(testCase), testFunction(testFunction), dataTag(dataTag), metric(metric)
        , platform(platform), host(host), gitRepo(gitRepo), gitBranch(gitBranch)
        , timestamp1(timestamp1), timestamp2(timestamp2)
        , diffTolerance(diffTolerance), stabTolerance(stabTolerance)
        , maxSize(maxSize), styleSheet(styleSheet), currTimestamp(currTimestamp)
    {}
    BMRequest_GetHistory(const QDomDocument &doc) : BMRequest(doc) {}
private:
    mutable QString testCase;
    mutable QString testFunction;
    mutable QString dataTag;
    mutable QString metric;
    mutable QString platform;
    mutable QString host;
    mutable QString gitRepo;
    mutable QString gitBranch;
    mutable QString timestamp1;
    mutable QString timestamp2;
    mutable qreal diffTolerance;
    mutable int stabTolerance;
    mutable int maxSize;
    mutable QString styleSheet;
    mutable int currTimestamp;

    QString name() const { return "GetHistory"; }
    QByteArray toRequestBuffer(QString *error);
    QByteArray toReplyBuffer();
    void handleReply_Raw(const QStringList &args) const;
    void handleReply_JSON(const QStringList &args) const;
    void handleReply_HTML(const QStringList &args) const;

    QString createHTML_detailsPage(const QString &server) const;
    QImage createHTML_historyPlot(QString *error) const;
};

class BMRequest_GetHistory2 : public BMRequest
{
public:
    BMRequest_GetHistory2(
        const QString &testCase, const QString &testFunction, const QString &dataTag,
        const QString &metric, const QString &platform, const QString &host,
        const QString &gitRepo1, const QString &gitBranch1, const QString &gitRepo2,
        const QString &gitBranch2, const QString &timestamp1, const QString &timestamp2,
        const qreal diffTolerance, const int stabTolerance, const int maxSize,
        const bool sharedTimeScale, const QString &styleSheet, const int currTimestamp)
        : testCase(testCase), testFunction(testFunction), dataTag(dataTag), metric(metric)
        , platform(platform), host(host), gitRepo1(gitRepo1), gitBranch1(gitBranch1)
        , gitRepo2(gitRepo2), gitBranch2(gitBranch2)
        , timestamp1(timestamp1), timestamp2(timestamp2)
        , diffTolerance(diffTolerance), stabTolerance(stabTolerance)
        , maxSize(maxSize), sharedTimeScale(sharedTimeScale), styleSheet(styleSheet)
        , currTimestamp(currTimestamp)
    { init(); }
    BMRequest_GetHistory2(const QDomDocument &doc) : BMRequest(doc) { init(); }
private:
    mutable QString testCase;
    mutable QString testFunction;
    mutable QString dataTag;
    mutable QString metric;
    mutable QString platform;
    mutable QString host;
    mutable QString gitRepo1;
    mutable QString gitBranch1;
    mutable QString gitRepo2;
    mutable QString gitBranch2;
    mutable QString timestamp1;
    mutable QString timestamp2;
    mutable qreal diffTolerance;
    mutable int stabTolerance;
    mutable int maxSize;
    mutable bool sharedTimeScale;
    mutable QString styleSheet;
    mutable int currTimestamp;

    QColor branch2color;

    void init() { branch2color = QColor(0, 0, 255, 255); }

    QString name() const { return "GetHistory2"; }
    QByteArray toRequestBuffer(QString *error);
    QByteArray toReplyBuffer();
    void handleReply_Raw(const QStringList &args) const { Q_UNUSED(args); /* 2 B DONE! */ };
    void handleReply_JSON(const QStringList &args) const {Q_UNUSED(args); /* 2 B DONE! */ }
    void handleReply_HTML(const QStringList &args) const;

    QString createHTML_detailsPage(const QString &server) const;
    QImage createHTML_historyPlot(QString *error) const;
};

class BMRequest_GetRankedBenchmarks : public BMRequest
{
public:
    BMRequest_GetRankedBenchmarks(
        const QString &metric, const QString &platform, const QString &host,
        const QString &gitRepo, const QString &gitBranch,
        const QString &timestamp1, const QString &timestamp2,
        const qreal diffTolerance, const int stabTolerance,
        const QString &ranking, const QString &scope, const int maxSize,
        const QString &testCaseFilter, const QString &testFunctionFilter,
        const QString &dataTagFilter)
        : metric(metric), platform(platform), host(host), gitRepo(gitRepo), gitBranch(gitBranch)
        , timestamp1(timestamp1), timestamp2(timestamp2)
        , diffTolerance(diffTolerance), stabTolerance(stabTolerance)
        , ranking(ranking), scope(scope), maxSize(maxSize), testCaseFilter(testCaseFilter)
        , testFunctionFilter(testFunctionFilter), dataTagFilter(dataTagFilter) {}
    BMRequest_GetRankedBenchmarks(const QDomDocument &doc) : BMRequest(doc) {}
private:
    QString metric;
    QString platform;
    QString host;
    QString gitRepo;
    QString gitBranch;
    QString timestamp1;
    QString timestamp2;
    qreal diffTolerance;
    int stabTolerance;
    QString ranking;
    QString scope;
    int maxSize;
    QString testCaseFilter;
    QString testFunctionFilter;
    QString dataTagFilter;

    QString name() const { return "GetRankedBenchmarks"; }
    QByteArray toRequestBuffer(QString *error);
    QByteArray toReplyBuffer();
    void handleReply_Raw(const QStringList &args) const;
    void handleReply_JSON(const QStringList &args) const;
};

class BMRequest_GetRankedBenchmarks2 : public BMRequest
{
public:
    BMRequest_GetRankedBenchmarks2(
        const QString &metric, const QString &platform, const QString &host,
        const QString &gitRepo1, const QString &gitBranch1,
        const QString &gitRepo2, const QString &gitBranch2,
        const qreal diffTolerance, const int stabTolerance,
        const QString &ranking, const QString &scope, const int maxSize,
        const QString &testCaseFilter, const QString &testFunctionFilter,
        const QString &dataTagFilter)
        : metric(metric), platform(platform), host(host)
        , gitRepo1(gitRepo1), gitBranch1(gitBranch1)
        , gitRepo2(gitRepo2), gitBranch2(gitBranch2)
        , diffTolerance(diffTolerance), stabTolerance(stabTolerance)
        , ranking(ranking), scope(scope), maxSize(maxSize), testCaseFilter(testCaseFilter)
        , testFunctionFilter(testFunctionFilter), dataTagFilter(dataTagFilter) {}
    BMRequest_GetRankedBenchmarks2(const QDomDocument &doc) : BMRequest(doc) {}
private:
    QString metric;
    QString platform;
    QString host;
    QString gitRepo1;
    QString gitBranch1;
    QString gitRepo2;
    QString gitBranch2;
    qreal diffTolerance;
    int stabTolerance;
    QString ranking;
    QString scope;
    int maxSize;
    QString testCaseFilter;
    QString testFunctionFilter;
    QString dataTagFilter;

    QString name() const { return "GetRankedBenchmarks2"; }
    QByteArray toRequestBuffer(QString *error);
    QByteArray toReplyBuffer();
    void handleReply_Raw(const QStringList &args) const;
    void handleReply_JSON(const QStringList &args) const;
};

class BMRequest_GetStats : public BMRequest
{
public:
    BMRequest_GetStats(
        const QString &metric, const QString &platform, const QString &host,
        const QString &gitRepo, const QString &gitBranch, const QString &timestamp1,
        const QString &timestamp2, const qreal diffTolerance, const int stabTolerance,
        const QString &testCaseFilter, const QString &testFunctionFilter,
        const QString &dataTagFilter)
        : metric(metric), platform(platform), host(host), gitRepo(gitRepo), gitBranch(gitBranch)
        , timestamp1(timestamp1),timestamp2(timestamp2)
        , diffTolerance(diffTolerance), stabTolerance(stabTolerance), testCaseFilter(testCaseFilter)
        , testFunctionFilter(testFunctionFilter), dataTagFilter(dataTagFilter) {}
    BMRequest_GetStats(const QDomDocument &doc) : BMRequest(doc) {}
private:
    QString metric;
    QString platform;
    QString host;
    QString gitRepo;
    QString gitBranch;
    QString timestamp1;
    QString timestamp2;
    qreal diffTolerance;
    int stabTolerance;
    QString testCaseFilter;
    QString testFunctionFilter;
    QString dataTagFilter;

    QString name() const { return "GetStats"; }
    QByteArray toRequestBuffer(QString *error);
    QByteArray toReplyBuffer();
    void handleReply_Raw(const QStringList &args) const;
    void handleReply_JSON(const QStringList &args) const;
};

class BMRequest_GetContexts : public BMRequest
{
public:
    BMRequest_GetContexts() {}
    BMRequest_GetContexts(const QDomDocument &doc) : BMRequest(doc) {}
private:
    QString name() const { return "GetContexts"; }
    QByteArray toRequestBuffer(QString *error);
    QByteArray toReplyBuffer();
    void handleReply_Raw(const QStringList &args) const;
    void handleReply_JSON(const QStringList &args) const;
};

// ### 2 B DOCUMENTED (in bmclient.html and bmproto.html)
class BMRequest_GetTCContexts : public BMRequest
{
public:
    BMRequest_GetTCContexts() {}
    BMRequest_GetTCContexts(const QDomDocument &doc) : BMRequest(doc) {}
private:
    QString name() const { return "GetTCContexts"; }
    QByteArray toRequestBuffer(QString *error);
    QByteArray toReplyBuffer();
    void handleReply_Raw(const QStringList &args) const;
    void handleReply_JSON(const QStringList &args) const;
};

// ### 2 B DOCUMENTED (in bmclient.html and bmproto.html)
class BMRequest_GetResult : public BMRequest
{
public:
    BMRequest_GetResult(const int resultId) : resultId(resultId) {}
    BMRequest_GetResult(const QDomDocument &doc) : BMRequest(doc) {}
private:
    mutable int resultId;

    QString name() const { return "GetResult"; }
    QByteArray toRequestBuffer(QString *error);
    QByteArray toReplyBuffer();
    void handleReply_Raw(const QStringList &args) const;
    void handleReply_JSON(const QStringList &args) const { Q_UNUSED(args); /* 2 B DONE! */ }
};

class ResultHistoryInfo;

// ### 2 B DOCUMENTED (in bmclient.html and bmproto.html)
class BMRequest_IndexGetValues : public BMRequest
{
public:
    BMRequest_IndexGetValues(
        const int baseTimestamp, const int medianWinSize,
        const QString &cacheKey, const QStringList &testCaseFilter,
        const QStringList &metricFilter, const QStringList &platformFilter,
        const QStringList &hostFilter, const QStringList &branchFilter,
        const bool dataQualityStats, const qreal dqStatsDiffTol, const qreal dqStatsStabTol)
        : baseTimestamp(baseTimestamp), medianWinSize(medianWinSize), cacheKey(cacheKey)
        , testCaseFilter(testCaseFilter), metricFilter(metricFilter)
        , platformFilter(platformFilter), hostFilter(hostFilter), branchFilter(branchFilter)
        , dataQualityStats(dataQualityStats), dqStatsDiffTol(dqStatsDiffTol)
        , dqStatsStabTol(dqStatsStabTol)
    {}
    BMRequest_IndexGetValues(const QDomDocument &doc) : BMRequest(doc) {}

private:
    mutable int baseTimestamp;
    mutable int medianWinSize;
    QString cacheKey;
    mutable QStringList testCaseFilter;
    mutable QStringList metricFilter;
    mutable QStringList platformFilter;
    mutable QStringList hostFilter;
    mutable QStringList branchFilter;
    mutable bool dataQualityStats;
    mutable qreal dqStatsDiffTol;
    mutable qreal dqStatsStabTol;

    QString name() const { return "IndexGetValues"; }
    QByteArray toRequestBuffer(QString *error);
    QByteArray toReplyBuffer();
    void handleReply_Raw(const QStringList &args) const { Q_UNUSED(args); /* 2 B DONE! */ };
    void handleReply_JSON(const QStringList &args) const { Q_UNUSED(args); /* 2 B DONE! */ }
    void handleReply_HTML(const QStringList &args) const;
    void handleReply_Image(const QStringList &args) const;
};

// ### 2 B DOCUMENTED (in bmclient.html and bmproto.html)
class BMRequest_IndexGetConfigs : public BMRequest
{
public:
    BMRequest_IndexGetConfigs() {}
    BMRequest_IndexGetConfigs(const QDomDocument &doc) : BMRequest(doc) {}
private:
    QString name() const { return "IndexGetConfigs"; }
    QByteArray toRequestBuffer(QString *error);
    QByteArray toReplyBuffer();
    void handleReply_Raw(const QStringList &args) const;
    void handleReply_JSON(const QStringList &args) const;
};

// ### 2 B DOCUMENTED (in bmclient.html and bmproto.html)
class BMRequest_IndexGetConfig : public BMRequest
{
public:
    BMRequest_IndexGetConfig(const QString &configName)
        : configName(configName) {}
    BMRequest_IndexGetConfig(const QDomDocument &doc) : BMRequest(doc) {}
private:
    QString configName;

    QString name() const { return "IndexGetConfig"; }
    QByteArray toRequestBuffer(QString *error);
    QByteArray toReplyBuffer();
    void handleReply_Raw(const QStringList &args) const;
    void handleReply_JSON(const QStringList &args) const;
};

// ### 2 B DOCUMENTED (in bmclient.html and bmproto.html)
class BMRequest_IndexPutConfig : public BMRequest
{
public:
    BMRequest_IndexPutConfig(
        const QString &configName, const int baseTimestamp, const int medianWinSize,
        const QStringList &testCaseFilter, const QStringList &metricFilter,
        const QStringList &platformFilter, const QStringList &hostFilter,
        const QStringList &branchFilter)
        : configName(configName), baseTimestamp(baseTimestamp), medianWinSize(medianWinSize)
        , testCaseFilter(testCaseFilter), metricFilter(metricFilter)
        , platformFilter(platformFilter), hostFilter(hostFilter), branchFilter(branchFilter) 
    {}

    BMRequest_IndexPutConfig(const QDomDocument &doc) : BMRequest(doc) {}
private:
    QString configName;
    int baseTimestamp;
    int medianWinSize;
    QStringList testCaseFilter;
    QStringList metricFilter;
    QStringList platformFilter;
    QStringList hostFilter;
    QStringList branchFilter;

    QString name() const { return "IndexPutConfig"; }
    QByteArray toRequestBuffer(QString *error);
    QByteArray toReplyBuffer();
    void handleReply_Raw(const QStringList &args) const;
    void handleReply_JSON(const QStringList &args) const;
};

// ### 2 B DOCUMENTED (in bmclient.html and bmproto.html)
class BMRequest_IndexDeleteConfig : public BMRequest
{
public:
    BMRequest_IndexDeleteConfig(const QString &configName)
        : configName(configName) {}
    BMRequest_IndexDeleteConfig(const QDomDocument &doc) : BMRequest(doc) {}
private:
    QString configName;

    QString name() const { return "IndexDeleteConfig"; }
    QByteArray toRequestBuffer(QString *error);
    QByteArray toReplyBuffer();
    void handleReply_Raw(const QStringList &args) const;
    void handleReply_JSON(const QStringList &args) const;
};

// ### 2 B DOCUMENTED (in bmclient.html and bmproto.html)
// ### NOTE: This request should be renamed to GetTCHistories ... 2 B DONE!
class BMRequest_GetHistories : public BMRequest
{
public:
    BMRequest_GetHistories(
        const QString &testCase, const QString &testFunction, const QString &dataTag,
        const QString &cacheKey)
        : testCase(testCase), testFunction(testFunction), dataTag(dataTag), cacheKey(cacheKey) {}
    BMRequest_GetHistories(const QDomDocument &doc) : BMRequest(doc) {}
private:
    QString testCase;
    QString testFunction;
    QString dataTag;
    QString cacheKey;

    QString name() const { return "GetHistories"; }
    QByteArray toRequestBuffer(QString *error);
    QByteArray toReplyBuffer();
    void handleReply_Raw(const QStringList &args) const { Q_UNUSED(args); /* 2 B DONE! */ };
    void handleReply_JSON(const QStringList &args) const { Q_UNUSED(args); /* 2 B DONE! */ }
    void handleReply_HTML(const QStringList &args) const;
    void handleReply_Image(const QStringList &args) const;
};

// ### 2 B DOCUMENTED (in bmclient.html and bmproto.html)
class BMRequest_GetBMTree : public BMRequest
{
public:
    BMRequest_GetBMTree() {}
    BMRequest_GetBMTree(const QDomDocument &doc) : BMRequest(doc) {}
private:
    QString name() const { return "GetBMTree"; }
    QByteArray toRequestBuffer(QString *error);
    QByteArray toReplyBuffer();
    void handleReply_Raw(const QStringList &args) const;
    void handleReply_JSON(const QStringList &args) const;
};

// ### 2 B DOCUMENTED (in bmclient.html and bmproto.html)
class BMRequest_GetIXHistories : public BMRequest
{
public:
    BMRequest_GetIXHistories(
        const int evalTimestamp, const int medianWinSize,
        const QList<Index::RankedInfo> &rankedInfos, const QString &cacheKey)
        : evalTimestamp(evalTimestamp), medianWinSize(medianWinSize), rankedInfos(rankedInfos)
        , cacheKey(cacheKey) {}
    BMRequest_GetIXHistories(const QDomDocument &doc) : BMRequest(doc) {}
private:
    mutable int evalTimestamp;
    mutable int medianWinSize;
    QList<Index::RankedInfo> rankedInfos;
    QString cacheKey;

    QString name() const { return "GetIXHistories"; }
    QByteArray toRequestBuffer(QString *error);
    QByteArray toReplyBuffer();
    void handleReply_Raw(const QStringList &args) const { Q_UNUSED(args); /* 2 B DONE! */ };
    void handleReply_JSON(const QStringList &args) const { Q_UNUSED(args); /* 2 B DONE! */ }
    void handleReply_HTML(const QStringList &args) const;
    void handleReply_Image(const QStringList &args) const;
};

// ### 2 B DOCUMENTED (in bmclient.html and bmproto.html)
class BMRequest_ASFStatsGetValues : public BMRequest
{
public:
    BMRequest_ASFStatsGetValues(
        const int medianWinSize, const QString &cacheKey, const QStringList &testCaseFilter,
        const QStringList &metricFilter, const QStringList &platformFilter,
        const QStringList &hostFilter, const QStringList &branchFilter,
        const int fromTimestamp, const int toTimestamp, const qreal diffTol, const int stabTol,
        const qreal sfTol, const qreal lfTol, const qreal maxLDTol)
        : medianWinSize(medianWinSize), cacheKey(cacheKey), testCaseFilter(testCaseFilter)
        , metricFilter(metricFilter), platformFilter(platformFilter), hostFilter(hostFilter)
        , branchFilter(branchFilter), fromTimestamp(fromTimestamp), toTimestamp(toTimestamp)
        , diffTol(diffTol), stabTol(stabTol), sfTol(sfTol), lfTol(lfTol), maxLDTol(maxLDTol)
    {}
    BMRequest_ASFStatsGetValues(const QDomDocument &doc) : BMRequest(doc) {}

private:
    mutable int medianWinSize;
    QString cacheKey;
    mutable QStringList testCaseFilter;
    mutable QStringList metricFilter;
    mutable QStringList platformFilter;
    mutable QStringList hostFilter;
    mutable QStringList branchFilter;
    mutable int fromTimestamp;
    mutable int toTimestamp;
    mutable qreal diffTol;
    mutable qreal stabTol;
    mutable qreal sfTol;
    mutable qreal lfTol;
    mutable qreal maxLDTol;

    QString name() const { return "ASFStatsGetValues"; }
    QByteArray toRequestBuffer(QString *error);
    QByteArray toReplyBuffer();
    void handleReply_Raw(const QStringList &args) const { Q_UNUSED(args); /* 2 B DONE! */ };
    void handleReply_JSON(const QStringList &args) const { Q_UNUSED(args); /* 2 B DONE! */ }
    void handleReply_HTML(const QStringList &args) const;
};

// ### 2 B DOCUMENTED (in bmclient.html and bmproto.html)
class BMRequest_ASFStatsGetValues2 : public BMRequest
{
public:
    BMRequest_ASFStatsGetValues2(
        const int bmcontextId, const QString &cacheKey)
        : bmcontextId(bmcontextId), cacheKey(cacheKey) {}
    BMRequest_ASFStatsGetValues2(const QDomDocument &doc) : BMRequest(doc) {}

private:
    int bmcontextId;
    QString cacheKey;

    QString name() const { return "ASFStatsGetValues2"; }
    QByteArray toRequestBuffer(QString *error);
    QByteArray toReplyBuffer();
    void handleReply_Raw(const QStringList &args) const { Q_UNUSED(args); /* 2 B DONE! */ };
    void handleReply_JSON(const QStringList &args) const { Q_UNUSED(args); /* 2 B DONE! */ }
    void handleReply_HTML(const QStringList &args) const;
    void handleReply_Image(const QStringList &args) const;
};

#endif // BMREQUEST_H
