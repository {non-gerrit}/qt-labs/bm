/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef RESULTHISTORYINFO_H
#define RESULTHISTORYINFO_H

#include <QString>
#include <QList>
#include <QMap>
#include <QBitArray>

class ResultHistoryInfo {
public:
    ResultHistoryInfo(
        const int bmcontextId, const QList<int> &timestamps_, const QList<qreal> &values,
        const int medianWinSize, const QString &metric, const QString &platform = QString(),
        const QString &host = QString(), const QString &gitRepo = QString(),
        const QString &gitBranch = QString(), const QString &testCase = QString(),
        const QString &testFunction = QString(), const QString &dataTag = QString())
        : bmcontextId_(bmcontextId), timestamps_(timestamps_), values(values)
        , medianWinSize(medianWinSize), metric_(metric), platform_(platform), host_(host)
        , gitRepo_(gitRepo), gitBranch_(gitBranch), testCase_(testCase), testFunction_(testFunction)
        , dataTag_(dataTag), hasCachedMinVal(false), hasCachedMaxVal(false), outliersMarked(false)
    {
        Q_ASSERT(!timestamps_.isEmpty());
        Q_ASSERT(timestamps_.size() == values.size());
        for (int i = 1; i < timestamps_.size(); ++i) {
            Q_ASSERT(timestamps_.at(i - 1) <= timestamps_.at(i));
        }
    }

    bool findSmoothPos(int timestamp, int *pos) const;

    QList<int> timestamps() const { return timestamps_; };
    int size() const { return timestamps_.size(); }
    qreal timestamp(int i) const { return static_cast<qreal>(timestamps_.at(i)); }
    qreal value(int i) const { return values.at(i); }
    qreal minValue() const;
    qreal maxValue() const;

    bool isOutlier(int i) const;

    int bmcontextId() const { return bmcontextId_; }
    QString metric() const { return metric_; }
    QString platform() const { return platform_; }
    QString host() const { return host_; }
    QString gitRepo() const { return gitRepo_; }
    QString gitBranch() const { return gitBranch_; }
    QString testCase() const { return testCase_; }
    QString testFunction() const { return testFunction_; }
    QString dataTag() const { return dataTag_; }

    void computeMaxESSStats(qreal diffTolerance, int stabTolerance, int *total, int *stable) const;
    void computeStabilityStats(
        qreal diffTolerance, int stabTolerance, int fromTimestamp, int toTimestamp,
        bool *zerosFound, int *total, int *stable, int *uniqueLevels, qreal *minLevel,
        qreal *maxLevel, bool *maxMinFound) const;

private:
    int bmcontextId_;
    QList<int> timestamps_;
    QList<qreal> values;
    int medianWinSize;

    QString metric_;
    QString platform_;
    QString host_;
    QString gitRepo_;
    QString gitBranch_;
    QString testCase_;
    QString testFunction_;
    QString dataTag_;

    mutable bool hasCachedMinVal;
    mutable bool hasCachedMaxVal;
    mutable qreal cachedMinVal;
    mutable qreal cachedMaxVal;

    mutable bool outliersMarked;
    mutable QBitArray outliers;
    void markOutliers() const;

    bool equal(int i, int j, int diffTolerance) const;
    void startSubsequence(int pos, qreal diffTolerance, QMap<int, int> *uniqueLevels) const;
    static void endSubsequence(int seqSize, int *total, int *stable, int stabTolerance);
};

#endif // RESULTHISTORYINFO_H
