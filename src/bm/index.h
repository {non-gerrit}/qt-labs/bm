/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef INDEX_H
#define INDEX_H

#include <QString>
#include <QList>

class ResultHistoryInfo;
class DataQualityStats;

class Index {
public:
    Index(
        const QList<ResultHistoryInfo *> &candidateRHInfos, const int baseTimestamp,
        QList<int> *evalTimestamps, int *rejectedNonPositive = 0);
    virtual ~Index();

    bool isValid() const { return valid; }
    QString invalidReason() const {return invalidReason_; }

    struct RankedInfo {
        int bmcontextId;
        int basePos;
        int diffPos1; // target pos for the eval timestamp that precedes the main eval timestamp
        int diffPos2; // target pos for the main eval timestamp
        QString descr; // free description
        RankedInfo(
            const int bmcontextId, const int basePos, const int diffPos1, const int diffPos2,
            const QString &descr)
            : bmcontextId(bmcontextId), basePos(basePos), diffPos1(diffPos1), diffPos2(diffPos2)
            , descr(descr)
        {}
    };

    // Computes the index value at each evaluation timestamp into \a values. The position
    // of the index value corresponding to the base timestamp is passed in \a baseValuePos.
    // The umber of contributing result histories for each evaluation timestamp is passed
    // in \a contrCounts.
    // If non-null, the \a topContr list will upon return contain a list of the \a topContrLimit
    // most significant contributors at each evaluation timestamp.
    // If non-null, the \a dqStats object will upon return contain data quality statistics for
    // the contributing result histories.
    virtual bool computeValues(
        QList<qreal> *values, int *baseValuePos, QList<int> *contrCounts, QString *error,
        QList<QList<RankedInfo> > *topContr = 0, int topContrLimit = -1,
        DataQualityStats *dqStats = 0) const = 0;

protected:
    QList<ResultHistoryInfo *> rhInfos;
    int baseTimestamp;
    QList<int> evalTimestamps_;

private:
    bool valid;
    QString invalidReason_;

    void init(
        const QList<ResultHistoryInfo *> &candidateRHInfos, QList<int> *evalTimestamps,
        int *rejectedNonPositive = 0);
};

// ### 2 B DOCUMENTED!
class IndexAlgorithm1 : public Index {
public:
    IndexAlgorithm1(
        const QList<ResultHistoryInfo *> &candidateRHInfos, const int baseTimestamp,
        QList<int> *evalTimestamps, int *rejectedNonPositive = 0)
        : Index(candidateRHInfos, baseTimestamp, evalTimestamps, rejectedNonPositive)
    {}

    bool computeValues(
        QList<qreal> *values, int *baseValuePos, QList<int> *contrCounts, QString *error,
        QList<QList<RankedInfo> > *topContr = 0, int topContrLimit = -1,
        DataQualityStats *dqStats = 0) const;
};

#endif // INDEX_H
