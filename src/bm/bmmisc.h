/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef BMMISC_H
#define BMMISC_H

#include <QVector>
#include <QString>
#include <QStringList>
#include <QPair>
#include <QImage>
#include <QDateTime>
#include <QSqlQuery>

class BMMisc
{
public:

    static bool getTimestamp(
        const QStringList &args, int pos, QString *timestamp, QString *error = 0);
    static void printJSONOutput(const QString &s);
    static void printHTMLOutput(const QString &s);
    static void printHTMLErrorPage(const QString &s);
    static void printImageOutput(
        const QImage &image, const QString &format, const QString &error, bool httpHeader = true);
    static bool getLastInsertId(QSqlQuery *query, int *id);
    static QString lastQueryError(const QSqlQuery &query);
    static qreal normalizedDifference(qreal a, qreal b);
    static QString getOption(const QStringList &args, const QString &name, int offset = 0);
    static bool getOption(
        const QStringList &args, const QString &option, QStringList *values, int nvalues, int nskip,
        QString *error = 0);
    static bool getMultiOption(
        const QStringList &args, const QString &option, QStringList *values, QString *error,
        bool unique = true);
    static bool getMultiOption2(
        const QStringList &args, const QString &option, QList<QStringList> *values, int n,
        QString *error);
    static bool hasOption(const QStringList &args, const QString &option);
    static bool getDoubleOption(
        const QStringList &args, const QString &option, qreal *value, QString *error);
    static bool getClampedPercentageOption(
        const QStringList &args, const QString &option, qreal *value, QString *error);
    static qreal median(const QList<qreal> &values);
    static int medianPos(const QList<qreal> &values, bool lastDuplicate = true);
    static qreal v2y(
        const qreal v, const qreal ymax, const qreal vmin, const qreal yfact, const qreal ydefault);
    static QDateTime createCurrDateTime(int timestamp);
    static QString secs2daysText(const int secs, const uint precision);
    static bool saveImageToFile(const QImage &image, const QString &fileName, QString *error);
    static QString ageColor(const int secs);
    static QString redToGreenColor(
        qreal val, qreal minAbsVal, qreal maxAbsVal, bool higherIsBetter = false);
    static QString diffColor(qreal diff, bool higherIsbetter = false);
    static bool lowerIsBetter(const QString &metric);
    static qreal log2val(qreal val, const QString &metric);
    static qreal log2diff(qreal val1, qreal val2, const QString &metric);
    static bool normalize(QList<qreal> &v);
    static QString compactDateString(int timestamp);

    // ### 2 B DOCUMENTED!
    // (Note that a function template cannot be compiled and linked independently, so its
    // definition needs to go here)
    template <typename T> static void insertRankedId(
        QList<QPair<T, qreal> > *ranked, int limit, T t, qreal value)
    {
        Q_ASSERT(limit >= 0);

        int i;
        for (i = 0; (i < limit) && (i < ranked->size()); ++i) {
            if (value > ranked->at(i).second)
                break;
        }

        if (i == limit)
            return;

        Q_ASSERT(i <= ranked->size());
        ranked->insert(i, qMakePair(t, value));

        if (ranked->size() > limit) {
            Q_ASSERT(ranked->size() == (limit + 1));
            ranked->removeLast();
            Q_ASSERT(ranked->size() == limit);
        }
    }
};

#endif // BMMISC_H
