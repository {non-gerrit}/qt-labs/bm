/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "bmrequest.h"
#include "resulthistoryinfo.h"
#include "dataqualitystats.h"
#include "asfstats.h"
#include "plotter.h"
#include "cache.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QGraphicsScene>
#include <QGraphicsSimpleTextItem>
#include <QDateTime>
#include <QPainter>

// Creates a special BMRequest to handle an incoming message.
// \a data contains the received XML structure. \a msgType should be one of 'request' or 'reply'
// depending on whether the message arrived at the server or client respectively.
// The <type> element in \a data selects the subclass to create.
BMRequest * BMRequest::create(const QByteArray &data, const QString &msgType)
{
    int line;
    int col;
    QString error;
    QDomDocument doc;
    if (doc.setContent(data, &error, &line, &col) == false) {
#ifdef BMDEBUG
        qDebug() << "BMRequest::create(type): setContent() failed"
                 << line << col << error;
#endif
        return 0;
    }

    QDomNodeList requestNodes = doc.elementsByTagName(msgType);
    if (requestNodes.size() != 1) {
#ifdef BMDEBUG
        qDebug() << "BMRequest::create(msgType): document does not contain exactly one '"
                 << msgType << "' element";
#endif
        return 0;
    }
    QDomElement elem = requestNodes.at(0).toElement();
    QString type = elem.attributeNode("type").value();

    BMRequest *request = 0;

    if (type == "PutResults")
        request = new BMRequest_PutResults(doc);
    else if (type == "GetMetrics")
        request = new BMRequest_GetMetrics(doc);
    else if (type == "GetPlatforms")
        request = new BMRequest_GetPlatforms(doc);
    else if (type == "GetHosts")
        request = new BMRequest_GetHosts(doc);
    else if (type == "GetBranches")
        request = new BMRequest_GetBranches(doc);
    else if (type == "GetHistory")
        request = new BMRequest_GetHistory(doc);
    else if (type == "GetHistory2")
        request = new BMRequest_GetHistory2(doc);
    else if (type == "GetRankedBenchmarks")
        request = new BMRequest_GetRankedBenchmarks(doc);
    else if (type == "GetRankedBenchmarks2")
        request = new BMRequest_GetRankedBenchmarks2(doc);
    else if (type == "GetStats")
        request = new BMRequest_GetStats(doc);
    else if (type == "GetContexts")
        request = new BMRequest_GetContexts(doc);
    else if (type == "GetTCContexts")
        request = new BMRequest_GetTCContexts(doc);
    else if (type == "GetResult")
        request = new BMRequest_GetResult(doc);
    else if (type == "IndexGetValues")
        request = new BMRequest_IndexGetValues(doc);
    else if (type == "IndexGetConfigs")
        request = new BMRequest_IndexGetConfigs(doc);
    else if (type == "IndexGetConfig")
        request = new BMRequest_IndexGetConfig(doc);
    else if (type == "IndexPutConfig")
        request = new BMRequest_IndexPutConfig(doc);
    else if (type == "IndexDeleteConfig")
        request = new BMRequest_IndexDeleteConfig(doc);
    else if (type == "GetHistories")
        request = new BMRequest_GetHistories(doc);
    else if (type == "GetBMTree")
        request = new BMRequest_GetBMTree(doc);
    else if (type == "GetIXHistories")
        request = new BMRequest_GetIXHistories(doc);
    else if (type == "ASFStatsGetValues")
        request = new BMRequest_ASFStatsGetValues(doc);
    else if (type == "ASFStatsGetValues2")
        request = new BMRequest_ASFStatsGetValues2(doc);
#ifdef BMDEBUG
    else
        qDebug() << "invalid request type:" << type;
#endif
    return request;
}

// Creates a special BMRequest to handle an incoming request message from the client.
// \a data contains the received XML structure.
BMRequest * BMRequest::create(const QByteArray &data, QSqlDatabase *database)
{
    BMRequest *request = create(data, "request");
    if (request)
        request->setDatabase(database);
    return request;
}

// Creates a special BMRequest to handle an incoming reply message from the server.
// \a data contains the received XML structure.
BMRequest * BMRequest::create(const QByteArray &data)
{
    return create(data, "reply");
}

// Extracts the result from the reply XML data (in doc) and dumps it to stdout.
void BMRequest::handleReply(OutputFormat outputFormat, const QStringList &args) const
{
    if (outputFormat == Raw)
        handleReply_Raw(args);
    else if (outputFormat == JSON)
        handleReply_JSON(args);
    else if (outputFormat == HTML)
        handleReply_HTML(args);
    else
        handleReply_Image(args);
}

QByteArray BMRequest::xmlConvert(
    const QString &s, int *errorLine, int *errorCol, QDomDocument *doc_)
{
    int line;
    int col;
    QString error;
    QDomDocument doc;
    if (doc.setContent(s, &error, &line, &col) == false) {
#ifdef BMDEBUG
        qDebug() << "BMRequest::xmlConvert(): setContent() failed"
                 << line << col << error;
#endif
        if (errorLine)
            *errorLine = line;
        if (errorCol)
            *errorCol = col;
        return QByteArray();
    }

    if (doc_)
        *doc_ = doc;

    return doc.toByteArray();
}

QSqlQuery *BMRequest::createQuery(BMRequest::SpecialDBAction specialAction) const
{
    Q_ASSERT(database);

    Q_ASSERT(database->isOpen());
//     if (!database->open()) {
//         qDebug() << "createQuery(): failed to open database";
//         return 0;
//     }

    QSqlQuery *query = new QSqlQuery(*database);

    if (specialAction == Transaction)
        database->transaction();

    return query;
}

void BMRequest::deleteQuery(QSqlQuery *query, BMRequest::SpecialDBAction specialAction) const
{
    Q_ASSERT(database);
    if (query->isActive())
        query->finish();

    if (specialAction == Commit) {
        database->commit();
    } else if (specialAction == Rollback) {
        database->rollback();
    }

//     database->close();
    delete query;
}

static QString errorReply(const QString &type, const QString &error)
{
    return QString("<reply type=\"%1\" error=\"%2\" />").arg(type).arg(error);
}

static QString errorReply(
    const QSqlQuery &query, const QString &type, const QString &extraInfo = QString())
{
    const QString queryError = BMMisc::lastQueryError(query);
#ifdef BMDEBUG
    if (!queryError.isEmpty())
        qDebug() << "queryError:" << queryError << queryError.size();
#endif
    return QString("<reply type=\"%1\" error=\"%2%3%4\" />")
        .arg(type)
        .arg(extraInfo)
        .arg(QString((!extraInfo.isEmpty() && !queryError.isEmpty()) ? ": " : ""))
        .arg(queryError);
}

bool BMRequest::getId(
    QString *reply, int *id, const QString &table,
    const QStringList &colNames, const QStringList &colValues,
    const QStringList &insertColNames, const QStringList &insertColValues,
    bool autoInsert) const
{
    Q_ASSERT(!colNames.isEmpty());
    Q_ASSERT(colNames.size() == colValues.size());

    QString query_s = QString("SELECT id FROM %1 WHERE").arg(table);
    QString error_s = QString("%1 not found: ").arg(table);
    for (int i = 0; i < colNames.size(); ++i) {
        if (i > 0) {
            query_s.append(" AND");
            error_s.append(", ");
        }
        query_s.append(
            QString(" %1='%2'").arg(colNames.at(i)).arg(colValues.at(i)));
        error_s.append(QString("'%1'").arg(colValues.at(i)));
    }
    query_s.append(";");

    QSqlQuery *query = createQuery();
    if (!query->exec(query_s)) {
        *reply = errorReply(*query, name(), error_s.append(" (exec() failed)"));
        deleteQuery(query);
        return false;
    }

    if (query->next()) {
        // Found ...
        *id = query->value(0).toInt();
        deleteQuery(query);
    } else if (!autoInsert) {
        // Not found and no auto-insert, so fail ...
        *reply = errorReply(*query, name(), error_s);
        deleteQuery(query);
        return false;
    } else {
        // Not found and auto-insert, so insert a new record ...

        Q_ASSERT(!insertColNames.isEmpty());
        Q_ASSERT(insertColNames.size() == insertColValues.size());

        query_s = QString("INSERT INTO %1 (").arg(table);
        for (int i = 0; i < insertColNames.size(); ++i) {
            if (i > 0)
                query_s.append(", ");
            query_s.append(QString("%1").arg(insertColNames.at(i)));
        }
        query_s.append(") VALUES (");
        for (int i = 0; i < insertColValues.size(); ++i) {
            if (i > 0)
                query_s.append(", ");
            query_s.append(QString("'%1'").arg(insertColValues.at(i)));
        }
        query_s.append(");");

        deleteQuery(query);
        query = createQuery(Transaction);
        if (!query->exec(query_s)) {
            *reply = errorReply(*query, name(), QString("failed to insert %1").arg(table));
            deleteQuery(query, Rollback);
            return false;
        }

        database->commit();

        // ... and get its id ...
        if (!BMMisc::getLastInsertId(query, id)) {
            Q_ASSERT(false); // ### Backup code (involving a separate query) to go here!
            deleteQuery(query);
            *reply = errorReply(*query, name(), "failed to get last inserted ID (in getId())");
            return false;
        }

        deleteQuery(query);
    }

    return true;
}

bool BMRequest::getBenchmarkId(
    QString *reply, int *benchmarkId, const QString &testCase, const QString &testFunction,
    const QString &dataTag, bool autoInsert) const
{
    const QStringList colNames = QStringList() << "testCase" << "testFunction" << "dataTag";
    const QStringList colValues = QStringList() << testCase << testFunction << dataTag;
    return getId(
        reply, benchmarkId, "benchmark", colNames, colValues, colNames, colValues, autoInsert);
}

bool BMRequest::getMetricId(
    QString *reply, int *metricId, const QString &metric, bool autoInsert) const
{
    const QStringList colNames = QStringList() << "name";
    const QStringList colValues = QStringList() << metric;
    return getId(
        reply, metricId, "metric", colNames, colValues, colNames, colValues, autoInsert);
}

bool BMRequest::getPlatformId(
    QString *reply, int *platformId, const QString &platform, bool autoInsert) const
{
    const QStringList colNames = QStringList() << "name";
    const QStringList colValues = QStringList() << platform;
    return getId(
        reply, platformId, "platform", colNames, colValues, colNames, colValues, autoInsert);
}

bool BMRequest::getHostId(
    QString *reply, int *hostId, const QString &host, bool autoInsert) const
{
    const QStringList colNames = QStringList() << "name";
    const QStringList colValues = QStringList() << host;
    return getId(reply, hostId, "host", colNames, colValues, colNames, colValues, autoInsert);
}

bool BMRequest::getBranchId(
    QString *reply, int *branchId, const QString &gitRepo, const QString &gitBranch,
    bool autoInsert) const
{
    const QStringList colNames = QStringList() << "gitRepo" << "gitBranch";
    const QStringList colValues = QStringList() << gitRepo << gitBranch;
    return getId(
        reply, branchId, "branch", colNames, colValues, colNames, colValues, autoInsert);
}

bool BMRequest::getSnapshotId(
    QString *reply, int *snapshotId, const QString &sha1, bool autoInsert) const
{
    const QStringList colNames = QStringList() << "sha1";
    const QStringList colValues = QStringList() << sha1;
    return getId(
        reply, snapshotId, "snapshot", colNames, colValues, colNames, colValues, autoInsert);
}

bool BMRequest::getContextId(
    QString *reply, int *id, const QString &metric, const QString &platform,
    const QString &host, const QString &gitRepo, const QString &gitBranch) const
{
    int metricId;
    int platformId;
    int hostId;
    int branchId;

    if (!getMetricId(reply, &metricId, metric))
        return false;
    if (!getPlatformId(reply, &platformId, platform))
        return false;
    if (!getHostId(reply, &hostId, host))
        return false;
    if (!getBranchId(reply, &branchId, gitRepo, gitBranch))
        return false;

    QSqlQuery *query = createQuery();
    if (!query->exec(
            QString(
                "SELECT id FROM context "
                "WHERE  metricId=%1 "
                "   AND platformId=%2 "
                "   AND hostId=%3 "
                "   AND branchId=%4;")
            .arg(metricId).arg(platformId).arg(hostId).arg(branchId))) {
        *reply = errorReply(*query, name(), "failed to get context (exec() failed)");
        deleteQuery(query);
        return false;
    }

    if (query->next()) {
        // Found ...
        *id = query->value(0).toInt();
        deleteQuery(query);
    } else {
        // Not found, so fail ...
        *reply = errorReply(*query, name(), "context not found");
        deleteQuery(query);
        return false;
    }

    return true;
}

bool BMRequest::getOrInsertContextId(
    QString *reply, int *id, const int metricId, const int platformId,
    const int hostId, const int branchId) const
{
    QSqlQuery *query = createQuery();

    // Check if the context already exists ...
    if (!query->exec(
            QString(
                "SELECT id FROM context"
                " WHERE metricId=%1"
                "   AND platformId=%2"
                "   AND hostId=%3"
                "   AND branchId=%4;")
            .arg(metricId).arg(platformId).arg(hostId).arg(branchId))) {
        *reply = errorReply(*query, name(), "failed to get context (exec() failed)");
        deleteQuery(query);
        return false;
    }

    if (query->next()) {
        // ... it does, so get its ID ...
        *id = query->value(0).toInt();
        deleteQuery(query);
    } else {
        // ... it doesn't, so insert it ...
        deleteQuery(query);
        query = createQuery(Transaction);
        if (!query->exec(
                QString(
                    "INSERT INTO context (metricId, platformId, hostId, branchId) "
                    "VALUES (%1, %2, %3, %4);")
                .arg(metricId).arg(platformId).arg(hostId).arg(branchId))) {
            *reply = errorReply(*query, name(), "failed to insert context (exec() failed)");
            deleteQuery(query, Rollback);
            return false;
        }

        database->commit();

        // ... and get its ID ...
        if (!BMMisc::getLastInsertId(query, id)) {
            Q_ASSERT(false); // ### Backup code (involving a separate query) to go here!
            deleteQuery(query);
            *reply = errorReply(*query, name(), "failed to get ID of last context inserted");
            return false;
        }

        deleteQuery(query);
    }

    return true;
}

bool BMRequest::getBMContextId(
    QString *reply, int *id, const int benchmarkId, const QString &metric, const QString &platform,
    const QString &host, const QString &gitRepo, const QString &gitBranch) const
{
    int contextId;
    if (!getContextId(reply, &contextId, metric, platform, host, gitRepo, gitBranch))
        return false;

    QSqlQuery *query = createQuery();
    if (!query->exec(
            QString(
                "SELECT id FROM bmcontext"
                " WHERE contextId=%1"
                "   AND benchmarkId=%2;")
            .arg(contextId).arg(benchmarkId))) {
        *reply = errorReply(*query, name(), "failed to get bmcontext (exec() failed)");
        deleteQuery(query);
        return false;
    }
 
    if (query->next()) {
        // Found ...
        *id = query->value(0).toInt();
        deleteQuery(query);
    } else {
        // Not found, so fail ...
        *reply = errorReply(*query, name(), "bmcontext not found");
        deleteQuery(query);
        return false;
    }

    return true;
}

bool BMRequest::getBMContextId(
    QString *reply, int *id, const int benchmarkId, const int contextId) const
{
    QSqlQuery *query = createQuery();
    if (!query->exec(
            QString(
                "SELECT id FROM bmcontext"
                " WHERE contextId=%1"
                "   AND benchmarkId=%2;")
            .arg(contextId).arg(benchmarkId))) {
        *reply = errorReply(*query, name(), "failed to get bmcontext (exec() failed)");
        deleteQuery(query);
        return false;
    }
 
    if (query->next()) {
        // Found ...
        *id = query->value(0).toInt();
        deleteQuery(query);
    } else {
        // Not found, so fail ...
        *reply = errorReply(*query, name(), "bmcontext not found");
        deleteQuery(query);
        return false;
    }

    return true;
}

bool BMRequest::getOrInsertBMContextId(
    QString *reply, int *id, const int contextId, const int benchmarkId) const
{
    QSqlQuery *query = createQuery();

    // Check if the bmcontext already exists ...
    if (!query->exec(
            QString(
                "SELECT id FROM bmcontext"
                " WHERE contextId=%1"
                "   AND benchmarkId=%2;")
            .arg(contextId).arg(benchmarkId))) {
        *reply = errorReply(*query, name(), "failed to get bmcontext (exec() failed)");
        deleteQuery(query);
        return false;
    }

    if (query->next()) {
        // ... it does, so get its ID ...
        *id = query->value(0).toInt();
        deleteQuery(query);
    } else {
        // ... it doesn't, so insert it ...
        deleteQuery(query);
        query = createQuery(Transaction);
        if (!query->exec(
                QString(
                    "INSERT INTO bmcontext (contextId, benchmarkId) "
                    "VALUES (%1, %2);")
                .arg(contextId).arg(benchmarkId))) {
            *reply = errorReply(*query, name(), "failed to insert bmcontext (exec() failed)");
            deleteQuery(query, Rollback);
            return false;
        }

        database->commit();

        // ... and get its ID ...
        if (!BMMisc::getLastInsertId(query, id)) {
            Q_ASSERT(false); // ### Backup code (involving a separate query) to go here!
            deleteQuery(query);
            *reply = errorReply(*query, name(), "failed to get ID of last bmcontext inserted");
            return false;
        }

        deleteQuery(query);
    }

    return true;
}

bool BMRequest::insertOrReplaceResult(
    QString *reply, const int bmcontextId, const int snapshotId, const int timestamp,
    const qreal value) const
{
    QSqlQuery *query = createQuery();

    // Check if the result already exists ...
    if (!query->exec(
            QString(
                "SELECT id FROM result"
                " WHERE bmcontextId=%1"
                "   AND snapshotId=%2;")
            .arg(bmcontextId).arg(snapshotId))) {
        *reply = errorReply(*query, name(), "failed to get result (exec() failed)");
        deleteQuery(query);
        return false;
    }

    if (query->next()) {
        // ... it does, so replace the value ...

        const int id = query->value(0).toInt();

        deleteQuery(query);
        query = createQuery(Transaction);

        if (!query->exec(
                QString(
                    "UPDATE result"
                    "   SET timestamp=%1, value=%2"
                    " WHERE id=%3;")
                .arg(timestamp).arg(value).arg(id))) {
            *reply = errorReply(*query, name(), "failed to update result (exec() failed)");
            deleteQuery(query, Rollback);
            return false;
        }

    } else {
        // ... it doesn't, so insert the value ...

        deleteQuery(query);
        query = createQuery(Transaction);

        if (!query->exec(
                QString(
                    "INSERT INTO result (bmcontextId, snapshotId, timestamp, value) "
                    "VALUES (%1, %2, %3, %4);")
                .arg(bmcontextId).arg(snapshotId).arg(timestamp).arg(value))) {
            *reply = errorReply(*query, name(), "failed to insert result (exec() failed)");
            deleteQuery(query, Rollback);
            return false;
        }
    }

    deleteQuery(query, Commit);

    return true;
}

// ### 2 B DOCUMENTED!
bool BMRequest::getBenchmark(
    QString *reply, int benchmarkId, QString *testCase, QString *testFunction,
    QString *dataTag) const
{
    QSqlQuery *query = createQuery();
    if (!query->exec(
            QString("SELECT testCase, testFunction, dataTag FROM benchmark WHERE id=%1;")
            .arg(benchmarkId))) {
        *reply = errorReply(*query, name(), "failed to get benchmark (exec() failed)");
        deleteQuery(query);
        return false;
    }

    if (query->next()) {
        *testCase     = query->value(0).toString();
        *testFunction = query->value(1).toString();
        *dataTag      = query->value(2).toString();
        deleteQuery(query);
    } else {
        *reply = errorReply(*query, name(), "benchmark not found");
        deleteQuery(query);
        return false;
    }

    return true;
}

// Creates a query that selects result rows of the form 'timestamp sha1 value'.
// The output rows will be in reverse chronological order (i.e. by decreasing timestamp values)
//
// The bmcontextId identifies the result history as such.
//
// The timestamp1 and timestamp2 arguments define the history range. The value
// 'first' for timestamp1 specifies the first available snapshot, while the
// value 'last' for timestamp2 specifies the last available snapshot.
// Otherwise, timestamp1 refers to the earliest snapshot that is not earlier
// than the specified value, while timestamp2 refers to the latest snapshot
// that is not later than the specified value.
//
// If 'limit' is negative (the default), no LIMIT clause is appended to the query, otherwise
// a LIMIT clause with specifying 'limit' applies (and thus ensures retrieval of no more
// than the 'limit' most recent results).
//
QString BMRequest::createHistoryQuery(
    int bmcontextId, const QString &timestamp1, const QString &timestamp2, int limit) const
{
    bool ok;
    const int tlo = timestamp1.toInt(&ok);
    Q_ASSERT(ok || (timestamp1 == "first"));
    const int thi = timestamp2.toInt(&ok);
    Q_ASSERT(ok || (timestamp2 == "last"));

    return QString(
        "SELECT DISTINCT timestamp, sha1, value, result.id FROM snapshot, result "
        "WHERE  snapshotId=snapshot.id "
        "   AND bmcontextId=%1 %2 %3 ORDER BY timestamp DESC%4;")
        .arg(bmcontextId)
        .arg((timestamp1 == "first") ? QString() : QString("AND timestamp >= %1 ").arg(tlo))
        .arg((timestamp2 == "last") ? QString() : QString("AND timestamp <= %1 ").arg(thi))
        .arg((limit < 0) ? QString() : QString(" LIMIT %1").arg(limit));
}

// ### 2 B DOCUMENTED!
QString BMRequest::createBenchmarkFilterExpression(
    const QString &testCaseFilter, const QString &testFunctionFilter,
    const QString &dataTagFilter, bool leadingAnd, const QString &prefix) const
{
    QString s = QString("%1%2%3")
        .arg((testCaseFilter.isEmpty() || (testCaseFilter.trimmed() == "*"))
             ? QString()
             : QString("AND %1testCase GLOB '%2' ").arg(prefix).arg(testCaseFilter))
        .arg((testFunctionFilter.isEmpty() || (testFunctionFilter.trimmed() == "*"))
             ? QString()
             : QString("AND %1testFunction GLOB '%2' ").arg(prefix).arg(testFunctionFilter))
        .arg((dataTagFilter.isEmpty() || (dataTagFilter.trimmed() == "*"))
             ? QString()
             : QString("AND %1dataTag GLOB '%2' ").arg(prefix).arg(dataTagFilter));
    // ### Warning: the 'GLOB' operator is non-standard SQL, but is supported by
    // at least SQLite.

    if (s.startsWith("AND ") && (!leadingAnd))
        s.remove(0, 4);

    return s;
}

struct ResultInfo {
    int timestamp;
    QString sha1;
    qreal value;
    int id;
    ResultInfo(
        const int timestamp, const QString &sha1, const qreal value, const int id = -1)
        : timestamp(timestamp), sha1(sha1), value(value), id(id) {}
};

// Computes the arithmetic mean of the n values in data.
static qreal computeArithmeticMean(const qreal *data, int n)
{
    Q_ASSERT(n > 0);
    qreal sum = 0;
    for (int i = 0; i < n; ++i)
        sum += data[i];
    return sum / n;
}

// Computes the simple linear regression of the data points in x and y.
// The function returns true and passes the two components of the line equation
// (y = a + bx) in a and b iff a regression could be computed.
//
// See http://en.wikipedia.org/wiki/Simple_linear_regression
//
static bool computeSimpleLinearRegression(
    const QVector<qreal> &x, const QVector<qreal> &y, qreal *a, qreal *b)
{
    const int n = x.size();
    if ((n < 2) || (n != y.size()))
        return false;

    const qreal *x_data = x.data();
    const qreal *y_data = y.data();

    const qreal x_mean = computeArithmeticMean(x_data, n);
    const qreal y_mean = computeArithmeticMean(y_data, n);

    qreal sum1 = 0;
    qreal sum2 = 0;

    for (int i = 0; i < n; ++i) {
        const qreal x_dev = x_data[i] - x_mean;
        sum1 += (x_dev * (y_data[i] - y_mean));
        sum2 += (x_dev * x_dev);
    }

    *b = sum1 / sum2;
    if (!qIsFinite(*b))
        return false;
    *a = y_mean - *b * x_mean;

    return true;
}

static void computeStatistics(
    const QList<ResultInfo> &results, qreal diffTolerance, int stabTolerance, int *lastDiffIndex,
    bool *lastDiffStable, bool *lastValueStable, bool *regressionValid, qreal *a, qreal *b)
{
    const int n = results.size();
    Q_ASSERT(n > 1);

    // Time series:
    QVector<qreal> t(n);
    QVector<qreal> v(n);

    // Compute max and min values, initialize t and v, and look for a last difference ...
    const qreal t_min = results.first().timestamp;
    const qreal t_max = results.last().timestamp;
    bool counting1 = false;
    bool counting2 = false;
    int s1 = -1;
    int s2 = -1;
    qreal base = -1;
    *lastDiffIndex = -1;
    for (int i = n - 1; i >= 0; --i) {
        const qreal v_ = results.at(i).value;
        Q_ASSERT(v_ >= 0);
        const qreal t_ = results.at(i).timestamp;
        if (i == (n - 1)) {
            counting1 = true;
            counting2 = false;
            s1 = 1;
            s2 = 0;
            base = v_;
        } else {
            const bool equalsBase = qAbs(BMMisc::normalizedDifference(base, v_)) <= diffTolerance;
            if (counting1) {
                if (equalsBase) {
                    s1++;
                } else {
                    counting1 = false;
                    counting2 = true;
                    s2 = 1;
                    base = v_;
                    *lastDiffIndex = i;
                }
            } else if (counting2) {
                if (equalsBase) {
                    s2++;
                } else {
                    counting1 = counting2 = false;
                }
            }
        }

        t.replace(i, t_);
        v.replace(i, v_);
    }

    *lastDiffStable = ((*lastDiffIndex > -1) && (s1 > stabTolerance) && (s2 > stabTolerance));
    *lastValueStable = (s1 > stabTolerance);
    *regressionValid = (t_min < t_max) ? computeSimpleLinearRegression(t, v, a, b) : false;
}

static void computeStatistics(
    const QList<ResultInfo> &results, qreal diffTolerance, int stabTolerance, int *lastDiffIndex,
    bool *lastDiffStable)
{
    const int n = results.size();
    Q_ASSERT(n > 1);

    bool counting1 = false;
    bool counting2 = false;
    int s1 = -1;
    int s2 = -1;
    qreal base = -1;
    *lastDiffIndex = -1;
    for (int i = n - 1; i >= 0; --i) {
        const qreal v_ = results.at(i).value;
        Q_ASSERT(v_ >= 0);
        if (i == (n - 1)) {
            counting1 = true;
            counting2 = false;
            s1 = 1;
            s2 = 0;
            base = v_;
        } else {
            const bool equalsBase = qAbs(BMMisc::normalizedDifference(base, v_)) <= diffTolerance;
            if (counting1) {
                if (equalsBase) {
                    s1++;
                } else {
                    counting1 = false;
                    counting2 = true;
                    s2 = 1;
                    base = v_;
                    *lastDiffIndex = i;
                }
            } else if (counting2) {
                if (equalsBase) {
                    s2++;
                } else {
                    counting1 = counting2 = false;
                }
            }
        }
    }

    // Determine stability of last difference (if any) ...
    *lastDiffStable = ((*lastDiffIndex > -1) && (s1 > stabTolerance) && (s2 > stabTolerance));
}

static bool lastValueIsStable(
    const QList<ResultInfo> &results, qreal diffTolerance, int stabTolerance)
{
    const int n = results.size();
    Q_ASSERT(n > 0);
    Q_ASSERT(stabTolerance >= 0);

    if (n <= stabTolerance)
        return false;

    int s = 0;
    qreal base = -1;
    for (int i = n - 1; i >= 0; --i) {
        const qreal v = results.at(i).value;
        Q_ASSERT(v >= 0);
        if (i == (n - 1)) {
            s = 1;
            base = v;
        } else {
            const bool equalsBase = qAbs(BMMisc::normalizedDifference(base, v)) <= diffTolerance;
            if (equalsBase)
                s++;
            else
                break;
        }
    }

    return (s > stabTolerance);
}

// ### 2 B DOCUMENTED!
bool BMRequest::appendResultHistoryToReply(
    QString *reply, int bmcontextId, const QString &suffix, const QString &timestamp1,
    const QString &timestamp2, qreal diffTolerance, int stabTolerance, int maxSize) const
{
    QSqlQuery *query = createQuery();
    if (!query->exec(createHistoryQuery(bmcontextId, timestamp1, timestamp2))) {
        *reply = errorReply(*query, name(), "failed to append branch history (exec() failed)");
        deleteQuery(query);
        return true;
    }

    QList<ResultInfo> results;
    while (query->next()) {
        results.prepend(
            ResultInfo(
                query->value(0).toInt(),
                query->value(1).toString(),
                query->value(2).toDouble(),
                query->value(3).toInt()));
    }

    deleteQuery(query);

    int lastDiffIndex = -1;
    bool lastDiffStable = false;
    bool lastValueStable = false;
    bool regressionValid = false;
    qreal a = 0;
    qreal b = 0;
    if (results.size() > 1) {
        computeStatistics(
            results, diffTolerance, stabTolerance, &lastDiffIndex, &lastDiffStable,
            &lastValueStable, &regressionValid, &a, &b);
    } else {
        lastValueStable = ((results.size() == 1) && (stabTolerance == 0));
    }

    if (lastDiffIndex >= 0) {
        reply->append(
            QString("<lastDiff%1 index=\"%2\" stable=\"%3\" timestamp=\"%4\" "
                    "sha1=\"%5\" value=\"%6\" />")
            .arg(suffix)
            .arg((maxSize < 0)
                 ? lastDiffIndex
                 : ((lastDiffIndex < maxSize) ? lastDiffIndex : -1))
            .arg(lastDiffStable ? "true" : "false")
            .arg(results.at(lastDiffIndex).timestamp)
            .arg(results.at(lastDiffIndex).sha1)
            .arg(results.at(lastDiffIndex).value));
    }

    if (!results.isEmpty())
        reply->append(QString("<lastValue%1 stable=\"%2\" />").arg(suffix).arg(lastValueStable));

    if (regressionValid)
        reply->append(QString("<regression%1 a=\"%2\" b=\"%3\" />").arg(suffix).arg(a).arg(b));

    const int offset = (maxSize < 0)
        ? 0
        : results.size() - qMin(maxSize, results.size());
    for (int i = offset; i < results.size(); ++i) {
        reply->append(
            QString(
                "<result%1 timestamp=\"%2\" sha1=\"%3\" value=\"%4\" id=\"%5\"/>")
            .arg(suffix)
            .arg(results.at(i).timestamp)
            .arg(results.at(i).sha1)
            .arg(results.at(i).value)
            .arg(results.at(i).id));
    }

    return true;
}

// --- PutResults ---

// Used for testing:
static bool getFakedGitValues(QString *branch, QString *sha1, QString *timestamp)
{
    QStringList sysenv = QProcess::systemEnvironment();
    QRegExp rx;
    int pos;

    rx = QRegExp("^BMFAKEBRANCH=(\\S+)$");
    if ((pos = sysenv.indexOf(rx)) != -1) {
        rx.indexIn(sysenv.at(pos));
        *branch = rx.cap(1);
    } else {
        return false;
    }

    rx = QRegExp("^BMFAKESHA1=(\\S+)$");
    if ((pos = sysenv.indexOf(rx)) != -1) {
        rx.indexIn(sysenv.at(pos));
        *sha1 = rx.cap(1);
    } else {
        return false;
    }

    rx = QRegExp("^BMFAKETIMESTAMP=(\\S+)$");
    if ((pos = sysenv.indexOf(rx)) != -1) {
        rx.indexIn(sysenv.at(pos));
        *timestamp = rx.cap(1);
    } else {
        return false;
    }

    return true;
}

static bool getGitValues(
    const QString &gitDir, QString *branch, QString *sha1, QString *timestamp,
    QString *error)
{
    if (getFakedGitValues(branch, sha1, timestamp))
        return true;

    QProcess process;

    if (!QDir().exists(gitDir)) {
        *error = QString("no such git directory: '%1'").arg(gitDir);
        return false;
    }
    process.setWorkingDirectory(gitDir);

    QRegExp rx;

    // Get current branch ...
    QStringList args = QStringList() << "branch" << "--no-color";
    process.start(QLatin1String("git"), args);
    if (!process.waitForFinished(-1)) {
        *error = QString("failed to extract current branch: waitForFinished() failed");
        return false;
    }
    rx = QRegExp("^* (\\S+)");
    if (rx.indexIn(QLatin1String(process.readAllStandardOutput())) == -1) {
        *error = QString("failed to extract current branch: indexIn() failed");
        return false;
    }
    *branch = rx.cap(1);

    // Get sha1 and timestamp of current HEAD ...
    args = QStringList() << "log" << "-n" << "1" << "HEAD" << "--pretty=format:\"%H %ct\"";
    process.setWorkingDirectory(gitDir);
    process.start(QLatin1String("git"), args);
    if (!process.waitForFinished(-1)) {
        *error = QString(
            "failed to extract sha1 and timestamp of current HEAD: waitForFinished() failed");
        return false;
    }
    rx = QRegExp("^\"(\\S+) (\\d+)\"");
    if (rx.indexIn(QLatin1String(process.readAllStandardOutput())) == -1) {
        *error =
            QString("failed to extract sha1 and timestamp of current HEAD: indexIn() failed");
        return false;
    }
    *sha1 = rx.cap(1);
    *timestamp = rx.cap(2);

    return true;
}

QByteArray BMRequest_PutResults::toRequestBuffer(QString *error)
{
    QString specificError;
    if (!getGitValues(gitDir, &gitBranch, &sha1, &timestamp, &specificError)) {
        *error = QString("failed to extract git values: %3").arg(specificError);
        return false;
    }

    // --- begin parse results file ---
    QFile file(resultsFile);
    if (!file.open(QIODevice::ReadOnly)) {
        *error = QString("failed to open results file");
        return QByteArray();
    }
    int line;
    int col;
    QDomDocument doc;
    if (doc.setContent(&file, &specificError, &line, &col) == false) {
        *error = QString("failed to parse results file at line %1, column %2: %3")
            .arg(line).arg(col).arg(specificError);
        return QByteArray();
    }

    QDomNodeList testCaseNodes = doc.elementsByTagName("TestCase");
    if (testCaseNodes.size() != 1) {
        *error = QString("results file doesn't contain exactly one <TestCase> element");
        return QByteArray();
    }

    QDomElement testCaseElem = testCaseNodes.at(0).toElement();
    testCase = testCaseElem.attributeNode("name").value();
    if (testCase.isEmpty()) {
        *error = QString("results file doesn't contain a test case name");
        return QByteArray();
    }

    QDomNodeList testFunctionNodes = testCaseElem.elementsByTagName("TestFunction");
    if (testFunctionNodes.isEmpty()) {
        *error = QString("results file doesn't contain any test functions");
        return QByteArray();
    }

    for (int i = 0; i < testFunctionNodes.size(); ++i) {

        QDomElement testFunctionElem = testFunctionNodes.at(i).toElement();
        QString testFunctionName = testFunctionElem.attributeNode("name").value();
        if (testFunctionName.isEmpty()) {
            *error = QString("results file contains an unnamed test function");
            return QByteArray();
        }

        QDomNodeList dataTagNodes = testFunctionElem.elementsByTagName("BenchmarkResult");
        if (dataTagNodes.isEmpty())
            continue; // note: benchmark results are optional for a test function

        TestFunction testFunction(testFunctionName);

        for (int j = 0; j < dataTagNodes.size(); ++j) {

            QDomElement dataTagElem = dataTagNodes.at(j).toElement();
            QString dataTagName = dataTagElem.attributeNode("tag").value();

            const QString metric = dataTagElem.attributeNode("metric").value();
            if (metric.isEmpty()) {
                *error = QString("results file contains a result with no 'metric' attribute");
                return QByteArray();
            }

            bool ok;
            qreal value = dataTagElem.attributeNode("value").value().toDouble(&ok);
            if (!ok) {
                *error = QString(
                    "results file contains a result with an invalid 'value' attribute");
                return QByteArray();
            }
            if (value < 0) {
                *error = QString("results file contains a negative 'value' attribute");
                return QByteArray();
            }
            const qint64 iterations =
                dataTagElem.attributeNode("iterations").value().toLongLong(&ok);
            if (ok && (iterations > 0))
                value /= iterations;

            testFunction.dataTags.append(DataTag(dataTagName, metric, value));
        }

        testFunctions.append(testFunction);
    }
    // --- end parse results file ---

    if (testFunctions.isEmpty()) {
        *error = QString("results file doesn't contain any results");
        return QByteArray();
    }

    QString request = QString(
        "<request type=\"%1\"><context "
        "platform=\"%2\" host=\"%3\" gitRepo=\"%4\" gitBranch=\"%5\" "
        "timestamp=\"%6\" sha1=\"%7\" testCase=\"%8\" />")
        .arg(name()).arg(platform).arg(host).arg(gitRepo).arg(gitBranch).arg(timestamp).arg(sha1)
        .arg(testCase);

    QList<TestFunction>::const_iterator tf_it;
    for (tf_it = testFunctions.constBegin(); tf_it != testFunctions.constEnd(); ++tf_it) {
        request.append(QString("<testFunction name=\"%1\">").arg((*tf_it).name));
        QList<DataTag>::const_iterator dt_it;
        for (dt_it = (*tf_it).dataTags.constBegin(); dt_it != (*tf_it).dataTags.constEnd();
             ++dt_it) {
            request.append(
                QString("<dataTag name=\"%1\" metric=\"%2\" value=\"%3\" />")
                .arg((*dt_it).name).arg((*dt_it).metric).arg((*dt_it).value));
        }
        request.append(QString("</testFunction>"));
    }

    request.append("</request>");

    return xmlConvert(request);
}

// ### This function is used to handle cases where different names are used for the same metric.
// Eventually there should be no need for this function. This would require renaming metric
// names in the database.
static QString commonMetric(const QString &metric)
{
    if ((metric == "walltime") || (metric == "WalltimeMilliseconds"))
        return "walltime";
    return metric;
}

QByteArray BMRequest_PutResults::toReplyBuffer()
{
    // ### Investigate whether all database access (queries and inserts) needed for this
    // function should go in a single transaction (for both speed and safety).
    // Note that this currently applies only to this function, since it is the only "top level"
    // function that causes new records to be inserted in the database.

    QDomElement contextElem = doc.elementsByTagName("context").at(0).toElement();
    platform = contextElem.attributeNode("platform").value();
    host = contextElem.attributeNode("host").value();
    gitRepo = contextElem.attributeNode("gitRepo").value();
    gitBranch = contextElem.attributeNode("gitBranch").value();
    bool ok;
    const int timestamp_ = contextElem.attributeNode("timestamp").value().toInt(&ok);
    Q_ASSERT(ok);
    sha1 = contextElem.attributeNode("sha1").value();
    testCase = contextElem.attributeNode("testCase").value();

    QString reply;

    int platformId;
    if (!getPlatformId(&reply, &platformId, platform, true))
        return xmlConvert(reply);

    int hostId;
    if (!getHostId(&reply, &hostId, host, true))
        return xmlConvert(reply);

    int branchId;
    if (!getBranchId(&reply, &branchId, gitRepo, gitBranch, true))
        return xmlConvert(reply);

    int snapshotId;
    if (!getSnapshotId(&reply, &snapshotId, sha1, true))
        return xmlConvert(reply);

    QMap<QString, int> contextIds; // ### replace with QHash?

    QDomNodeList testFunctionNodes = doc.elementsByTagName("testFunction");
    for (int i = 0; i < testFunctionNodes.size(); ++i) {
        QDomElement testFunctionElem = testFunctionNodes.at(i).toElement();
        const QString testFunction = testFunctionElem.attributeNode("name").value();

        QDomNodeList dataTagNodes = testFunctionElem.elementsByTagName("dataTag");
        for (int j = 0; j < dataTagNodes.size(); ++j) {
            QDomElement dataTagElem = dataTagNodes.at(j).toElement();
            const QString dataTag = dataTagElem.attributeNode("name").value();
            const QString metric = commonMetric(dataTagElem.attributeNode("metric").value());
            qreal value = dataTagElem.attributeNode("value").value().toDouble();
            bool ok;
            qint64 iterations = dataTagElem.attributeNode("iterations").value().toLongLong(&ok);
            if (ok && (iterations > 0))
                value /= iterations;

            // Get benchmark ID ...
            int benchmarkId;
            if (!getBenchmarkId(&reply, &benchmarkId, testCase, testFunction, dataTag, true))
                return xmlConvert(reply);

            // Get context ID ...
            int contextId;
            if (contextIds.contains(metric)) {
                // use cached value ...
                contextId = contextIds.value(metric);

            } else {
                // retrieve value from database ...

                int metricId;
                if (!getMetricId(&reply, &metricId, metric, true))
                    return xmlConvert(reply);

                if (!getOrInsertContextId(
                        &reply, &contextId, metricId, platformId, hostId, branchId))
                    return xmlConvert(reply);

                contextIds.insert(metric, contextId);
            }

            // Get bmcontext ID ...
            int bmcontextId;
            if (!getOrInsertBMContextId(&reply, &bmcontextId, contextId, benchmarkId))
                return xmlConvert(reply);

            // Insert timestamp and value ...
            if (!insertOrReplaceResult(&reply, bmcontextId, snapshotId, timestamp_, value))
                return xmlConvert(reply);
        }
    }

    reply = QString("<reply type=\"%1\" />").arg(name());

    return xmlConvert(reply);
}

void BMRequest_PutResults::handleReply_Raw(const QStringList &args) const
{
    Q_UNUSED(args);

    QString error =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();

    if (!error.isEmpty())
        qDebug() << "error adding results:" << error.toLatin1().data();
}

// --- GetContextComponents ---
QString BMRequest_GetContextComponents::colList() const
{
    QString s;
    const QStringList colNames_(colNames());
    for (int i = 0; i < colNames_.size(); ++i)
        s.append(QString("%1%2").arg(i > 0 ? "," : "").arg(colNames_.at(i)));
    return s;
}

QByteArray BMRequest_GetContextComponents::toRequestBuffer(QString *)
{
    const QString request = QString(
        "<request type=\"%1\"><context metric=\"%2\" platform=\"%3\" "
        "host=\"%4\" gitRepo=\"%5\" gitBranch=\"%6\" /></request>")
        .arg(name()).arg(metric).arg(platform).arg(host).arg(gitRepo).arg(gitBranch);
    return xmlConvert(request);
}

QByteArray BMRequest_GetContextComponents::toReplyBuffer()
{
    QDomElement contextElem = doc.elementsByTagName("context").at(0).toElement();
    metric = contextElem.attributeNode("metric").value();
    platform = contextElem.attributeNode("platform").value();
    host = contextElem.attributeNode("host").value();
    gitRepo = contextElem.attributeNode("gitRepo").value();
    gitBranch = contextElem.attributeNode("gitBranch").value();

    QString query_s = QString("SELECT DISTINCT %1 FROM %2").arg(colList()).arg(table());

    QString reply;

    if (!addConstraints(&query_s, &reply))
        return xmlConvert(reply);

    query_s.append(";");

    QSqlQuery *query = createQuery();
    if (!query->exec(query_s)) {
        reply = errorReply(*query, name());
        deleteQuery(query);
    } else {
        reply = QString("<reply type=\"%1\">").arg(name());
        QStringList colNames_(colNames());
        while (query->next()) {
            reply.append(QString("<%1 ").arg(table()));
            for (int i = 0; i < colNames_.size(); ++i)
                reply.append(
                    QString("%1=\"%2\" ").arg(colNames_.at(i)).arg(query->value(i).toString()));
            reply.append("/>");
        }
        reply.append("</reply>");
        deleteQuery(query);
    }

    return xmlConvert(reply);
}

void BMRequest_GetContextComponents::handleReply_Raw(const QStringList &args) const
{
    Q_UNUSED(args);

    const QString error =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();

    if (error.isEmpty()) {
        QDomNodeList nodes = doc.elementsByTagName(table());
        const QStringList colNames_(colNames());
        for (int i = 0; i < nodes.size(); ++i) {
            QDomElement elem = nodes.at(i).toElement();
            QString out;
            for (int j = 0; j < colNames_.size(); ++j)
                out.append(
                    QString("%1\n")
                    .arg(elem.attributeNode(colNames_.at(j)).value()));
            printf("%s", out.toLatin1().data());
        }
    } else {
        qDebug() << QString("error listing %1s:").arg(table()).toLatin1().data()
                 << error.toLatin1().data();
    }
}

void BMRequest_GetContextComponents::handleReply_JSON(const QStringList &args) const
{
    Q_UNUSED(args);

    const QString error =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();

    QString reply;

    if (error.isEmpty()) {
        reply = QString("{\"items\": [");
        QDomNodeList nodes = doc.elementsByTagName(table());
        const QStringList colNames_(colNames());
        for (int i = 0; i < nodes.size(); ++i) {
            QDomElement elem = nodes.at(i).toElement();
            reply.append(QString("%1[").arg((i == 0) ? "" : ", "));
            for (int j = 0; j < colNames_.size(); ++j) {
                reply.append(
                    QString("%1\"%2\"")
                    .arg((j == 0) ? "" : ", ")
                    .arg(elem.attributeNode(colNames_.at(j)).value()));
            }
            reply.append("]");
        }
        reply.append("]}");
    } else {
        reply = QString("{\"error\": \"error listing %1s: %2\"}").arg(table()).arg(error);
    }

    BMMisc::printJSONOutput(reply);
}

// Adds a constraint to \a query_s if \a constrColValues contains at least one non-empty value.
//
// Returns false if it makes no sense to execute/expand query_s after the call (either because
// an error occurred, or because no matches are found even using this constraint alone).
// Upon returning false, \a reply contains the reply message.
//
// Returns true if no error occurred and matches are possible using this constraint alone
// (in particular, this means that the function returns true if \a constrColValues contains
// only empty values).
//
bool BMRequest_GetContextComponents::addConstraint(
    const QString &constrTable, const QStringList &constrColNames,
    const QStringList &constrColValues, QString *query_s, bool *constrAdded,
    QString *reply) const
{
    Q_ASSERT(!constrColNames.isEmpty());
    Q_ASSERT(constrColNames.size() == constrColValues.size());

    bool nonEmptyExists = false;
    for (int i = 0; i < constrColValues.size(); ++i)
        if (!constrColValues.at(i).isEmpty()) {
            nonEmptyExists = true;
            break;
        }
    if (!nonEmptyExists)
        return true;

    QString subQuery_s = QString("SELECT id FROM %1 WHERE").arg(constrTable);
    for (int i = 0; i < constrColNames.size(); ++i) {
        if (i > 0)
            subQuery_s.append(" AND");
        subQuery_s.append(
            QString(" %1='%2'").arg(constrColNames.at(i)).arg(constrColValues.at(i)));
    }
    subQuery_s.append(";");

    QSqlQuery *query = createQuery();
    if (!query->exec(subQuery_s)) {
        *reply = errorReply(*query, name(), "exec() failed");
        deleteQuery(query);
        return false;
    }
    if (query->next()) {
        const int id = query->value(0).toInt();
        deleteQuery(query);
        if (!*constrAdded) {
            query_s->append(
                QString(", context WHERE context.%1Id=%2.id").arg(table()).arg(table()));
            *constrAdded = true;
        }
        query_s->append(QString(" AND context.%1Id=%2").arg(constrTable).arg(id));
    } else {
        deleteQuery(query);
        for (int i = 0; i < constrColValues.size(); ++i)
            if (!constrColValues.at(i).isEmpty()) {
                *reply = QString("<reply type=\"%1\" />").arg(name());
                return false;
            }
    }

    return true;
}

bool BMRequest_GetContextComponents::addConstraints(QString *query_s, QString *reply) const
{
    bool constraintAdded = false;

    if ((table() != "metric") &&
        (!addConstraint(
            "metric",
            QStringList() << "name", QStringList() << metric,
            query_s, &constraintAdded, reply)))
        return false;

    if ((table() != "platform") &&
        (!addConstraint(
            "platform",
            QStringList() << "name", QStringList() << platform,
            query_s, &constraintAdded, reply)))
        return false;

    if ((table() != "host") &&
        (!addConstraint(
            "host",
            QStringList() << "name", QStringList() << host,
            query_s, &constraintAdded, reply)))
        return false;

    if ((table() != "branch") &&
        (!addConstraint(
            "branch",
            QStringList() << "gitRepo" << "gitBranch", QStringList() << gitRepo << gitBranch,
            query_s, &constraintAdded, reply)))
        return false;

    return true;
}

struct BenchmarkInfo {
    QString testCase;
    QString testFunction;
    QString dataTag;
    QList<int> bmcontextIds;
    BenchmarkInfo(
        const QString &testCase, const QString &testFunction, const QString &dataTag,
        const QList<int> bmcontextIds)
        : testCase(testCase), testFunction(testFunction), dataTag(dataTag)
        , bmcontextIds(bmcontextIds)
    {
    }
};

enum Ranking { Worst, Best, Absolute };
static Ranking rankingFromString(const QString &ranking)
{
    if (ranking == "worst")
        return Worst;
    else if (ranking == "best")
        return Best;
    else
        return Absolute;
}


struct BenchmarkStats {
    QString testCase;
    QString testFunction;
    QString dataTag;

    int timestamp_lastValue;
    QString sha1_lastValue;
    qreal value_lastValue;
    bool stable_lastValue;

    int timestamp_lastDiff;
    QString sha1_lastDiff;
    qreal value_lastDiff;
    bool stable_lastDiff;

    bool regressionValid;
    qreal a;
    qreal b;

    BenchmarkStats(
        const QString &testCase, const QString &testFunction, const QString &dataTag,
        const int timestamp_lastValue, const QString sha1_lastValue, const qreal value_lastValue,
        const bool stable_lastValue, const int timestamp_lastDiff, const QString sha1_lastDiff,
        const qreal value_lastDiff, const bool stable_lastDiff, const bool regressionValid,
        const qreal a, const qreal b)
        : testCase(testCase), testFunction(testFunction), dataTag(dataTag)
        , timestamp_lastValue(timestamp_lastValue), sha1_lastValue(sha1_lastValue)
        , value_lastValue(value_lastValue), stable_lastValue(stable_lastValue)
        , timestamp_lastDiff(timestamp_lastDiff), sha1_lastDiff(sha1_lastDiff)
        , value_lastDiff(value_lastDiff), stable_lastDiff(stable_lastDiff)
        , regressionValid(regressionValid), a(a), b(b) {}

    struct LessThan {
        Ranking ranking;
        LessThan(const QString &ranking_s) : ranking(rankingFromString(ranking_s)) {}
        bool operator()(const BenchmarkStats *b1, const BenchmarkStats *b2) {

            // Ranking rule:
            // - If both benchmarks have last differences with the same stability (either stable
            //   or unstable), then rank them according to 'ranking'.
            // - Otherwise, if both benchmarks have last differences, rank stable ones over
            //   non-stable ones.
            // - Otherwise, rank an existing last difference over a non-existing one.

            if ((b1->timestamp_lastDiff > -1) && (b2->timestamp_lastDiff > -1)) {

                if ((b1->stable_lastDiff && b2->stable_lastDiff)
                    || (!b1->stable_lastDiff && !b2->stable_lastDiff)) {

                    const qreal normDiff1 =
                        BMMisc::normalizedDifference(b1->value_lastValue, b1->value_lastDiff);
                    const qreal normDiff2 =
                        BMMisc::normalizedDifference(b2->value_lastValue, b2->value_lastDiff);

                    if (ranking == Worst)
                        return normDiff1 > normDiff2;
                    else if (ranking == Best)
                        return normDiff1 < normDiff2;
                    else
                        return qAbs(normDiff1) > qAbs(normDiff2);

                } else if (b1->stable_lastDiff) {
                    return true;
                } else {
                    Q_ASSERT(b2->stable_lastDiff);
                    return false;
                }

            } else if (b1->timestamp_lastDiff > -1) {
                return true;
            } else {
                return false;
            }
        }
    };
};

struct BenchmarkStats2 {
    QString testCase;
    QString testFunction;
    QString dataTag;

    int timestamp_lastValue1;
    QString sha1_lastValue1;
    qreal value_lastValue1;
    bool stable_lastValue1;

    int timestamp_lastValue2;
    QString sha1_lastValue2;
    qreal value_lastValue2;
    bool stable_lastValue2;

    BenchmarkStats2(
        const QString &testCase, const QString &testFunction, const QString &dataTag,
        const int timestamp_lastValue1, const QString sha1_lastValue1,
        const qreal value_lastValue1, const bool stable_lastValue1,
        const int timestamp_lastValue2, const QString sha1_lastValue2,
        const qreal value_lastValue2, const bool stable_lastValue2)
        : testCase(testCase), testFunction(testFunction), dataTag(dataTag)
        , timestamp_lastValue1(timestamp_lastValue1), sha1_lastValue1(sha1_lastValue1)
        , value_lastValue1(value_lastValue1), stable_lastValue1(stable_lastValue1)
        , timestamp_lastValue2(timestamp_lastValue2), sha1_lastValue2(sha1_lastValue2)
        , value_lastValue2(value_lastValue2), stable_lastValue2(stable_lastValue2) {}

    struct LessThan {
        Ranking ranking;
        LessThan(const QString &ranking_s) : ranking(rankingFromString(ranking_s)) {}
        bool operator()(const BenchmarkStats2 *b1, const BenchmarkStats2 *b2) {

            // Rank the last value differences of the two benchmarks according to 'ranking':

            Q_ASSERT(b1->value_lastValue1 >= 0);
            Q_ASSERT(b1->value_lastValue2 >= 0);
            Q_ASSERT(b2->value_lastValue1 >= 0);
            Q_ASSERT(b2->value_lastValue2 >= 0);
            const qreal normDiff1 =
                BMMisc::normalizedDifference(b1->value_lastValue1, b1->value_lastValue2);
            const qreal normDiff2 =
                BMMisc::normalizedDifference(b2->value_lastValue1, b2->value_lastValue2);

            if (ranking == Worst)
                return normDiff1 > normDiff2;
            else if (ranking == Best)
                return normDiff1 < normDiff2;
            else
                return qAbs(normDiff1) > qAbs(normDiff2);
        }
    };
};

// ### 2 B DOCUMENTED!
static bool extractBMAndContextFromRH(
    int bmcontextId, const QString &reqName, QSqlDatabase *database,
    QString *testCase, QString *testFunction, QString *dataTag, QString *metric,
    QString *platform, QString *host, QString *gitRepo, QString *gitBranch, QString *error)
{
    QSqlQuery query(*database);

    // Get benchmark ID and context ID ...
    if (!query.exec(
            QString("SELECT benchmarkId, contextId FROM bmcontext WHERE id=%1;")
            .arg(bmcontextId))) {
        *error = errorReply(
            query, reqName, QString("failed to get IDs for benchmark and context"));
        return false;
    }
    int benchmarkId;
    int contextId;
    if (query.next()) {
        benchmarkId = query.value(0).toInt();
        contextId = query.value(1).toInt();
    } else {
        *error = errorReply(
            query, reqName, QString("no such benchmark and context (programming error?)"));
        return false;
    }

    // Get benchmark ...
    if (!query.exec(
            QString("SELECT testCase, testFunction, dataTag FROM benchmark WHERE id=%1;")
            .arg(benchmarkId))) {
        *error = errorReply(query, reqName, QString("failed to get benchmark"));
        return false;
    }
    if (query.next()) {
        *testCase = query.value(0).toString();
        *testFunction = query.value(1).toString();
        *dataTag = query.value(2).toString();
    } else {
        *error = errorReply(query, reqName, QString("no such benchmark (programming error?)"));
        return false;
    }

    // Get metric ID, platform ID, host ID, and branch ID ...
    if (!query.exec(
            QString("SELECT metricId, platformId, hostId, branchId FROM context WHERE id=%1;")
            .arg(contextId))) {
        *error = errorReply(
            query, reqName, QString("failed to get IDs for metrid, platform, host, and branch"));
        return false;
    }
    int metricId;
    int platformId;
    int hostId;
    int branchId;
    if (query.next()) {
        metricId = query.value(0).toInt();
        platformId = query.value(1).toInt();
        hostId = query.value(2).toInt();
        branchId = query.value(3).toInt();
    } else {
        *error = errorReply(query, reqName, QString("no such context (programming error?)"));
        return false;
    }

    // Get metric ...
    if (!query.exec(QString("SELECT name FROM metric WHERE id=%1;").arg(metricId))) {
        *error = errorReply(query, reqName, QString("failed to get metric"));
        return false;
    }
    if (query.next()) {
        *metric = query.value(0).toString();
    } else {
        *error = errorReply(query, reqName, QString("no such metric (programming error?)"));
        return false;
    }

    // Get platform ...
    if (!query.exec(QString("SELECT name FROM platform WHERE id=%1;").arg(platformId))) {
        *error = errorReply(query, reqName, QString("failed to get platform"));
        return false;
    }
    if (query.next()) {
        *platform = query.value(0).toString();
    } else {
        *error = errorReply(query, reqName, QString("no such platform (programming error?)"));
        return false;
    }

    // Get host ...
    if (!query.exec(QString("SELECT name FROM host WHERE id=%1;").arg(hostId))) {
        *error = errorReply(query, reqName, QString("failed to get host"));
        return false;
    }
    if (query.next()) {
        *host = query.value(0).toString();
    } else {
        *error = errorReply(query, reqName, QString("no such host (programming error?)"));
        return false;
    }

    // Get branch ...
    if (!query.exec(QString("SELECT gitRepo, gitBranch FROM branch WHERE id=%1;").arg(branchId))) {
        *error = errorReply(query, reqName, QString("failed to get branch"));
        return false;
    }
    if (query.next()) {
        *gitRepo = query.value(0).toString();
        *gitBranch = query.value(1).toString();
    } else {
        *error = errorReply(query, reqName, QString("no such branch (programming error?)"));
        return false;
    }

    return true;
}

// --- GetHistory ---
QByteArray BMRequest_GetHistory::toRequestBuffer(QString *)
{
    const QString request = QString(
        "<request type=\"%1\"><context "
        "testCase=\"%2\" testFunction=\"%3\" dataTag=\"%4\" metric=\"%5\" platform=\"%6\" "
        "host=\"%7\" gitRepo=\"%8\" gitBranch=\"%9\" timestamp1=\"%10\" "
        "timestamp2=\"%11\" diffTolerance=\"%12\" stabTolerance=\"%13\" "
        "maxSize=\"%14\" /></request>")
        .arg(name()).arg(testCase).arg(testFunction).arg(dataTag).arg(metric).arg(platform)
        .arg(host).arg(gitRepo).arg(gitBranch).arg(timestamp1).arg(timestamp2)
        .arg(diffTolerance).arg(stabTolerance).arg(maxSize);
    return xmlConvert(request);
}

QByteArray BMRequest_GetHistory::toReplyBuffer()
{
    QDomElement contextElem = doc.elementsByTagName("context").at(0).toElement();
    testCase = contextElem.attributeNode("testCase").value();
    testFunction = contextElem.attributeNode("testFunction").value();
    dataTag = contextElem.attributeNode("dataTag").value();
    metric = contextElem.attributeNode("metric").value();
    platform = contextElem.attributeNode("platform").value();
    host = contextElem.attributeNode("host").value();
    gitRepo = contextElem.attributeNode("gitRepo").value();
    gitBranch = contextElem.attributeNode("gitBranch").value();
    timestamp1 = contextElem.attributeNode("timestamp1").value();
    timestamp2 = contextElem.attributeNode("timestamp2").value();
    bool ok;
    diffTolerance = contextElem.attributeNode("diffTolerance").value().toDouble(&ok);
    Q_ASSERT(ok);
    stabTolerance = contextElem.attributeNode("stabTolerance").value().toInt(&ok);
    Q_ASSERT(ok);
    maxSize = contextElem.attributeNode("maxSize").value().toInt(&ok);
    Q_ASSERT(ok);

    QString reply = QString("<reply type=\"%1\">").arg(name());

    int benchmarkId;
    if (!getBenchmarkId(&reply, &benchmarkId, testCase, testFunction, dataTag))
        return xmlConvert(reply);
    int bmcontextId;
    if (!getBMContextId(
            &reply, &bmcontextId, benchmarkId, metric, platform, host, gitRepo, gitBranch))
        return xmlConvert(reply);

    if (!appendResultHistoryToReply(
            &reply, bmcontextId, "", timestamp1, timestamp2, diffTolerance, stabTolerance, maxSize))
        return xmlConvert(reply);

    reply.append(
        QString(
            "<bmcontext testCase=\"%1\" testFunction=\"%2\" dataTag=\"%3\" metric=\"%4\" "
            "platform=\"%5\" host=\"%6\" gitRepo=\"%7\" gitBranch=\"%8\" />")
        .arg(testCase).arg(testFunction).arg(dataTag).arg(metric).arg(platform)
        .arg(host).arg(gitRepo).arg(gitBranch));

    reply.append("</reply>");

    return xmlConvert(reply);
}

void BMRequest_GetHistory::handleReply_Raw(const QStringList &args) const
{
    Q_UNUSED(args);

    const QString error =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();

    if (error.isEmpty()) {
        // Last difference ...
        QDomNodeList lastDiffNodes = doc.elementsByTagName("lastDiff");
        if (lastDiffNodes.size() == 1) {
            QDomElement lastDiffElem = lastDiffNodes.at(0).toElement();
            printf(
                "%s\n%s\n%s\n%s\n%s\n",
                lastDiffElem.attributeNode("index").value().toLatin1().data(),
                lastDiffElem.attributeNode("stable").value().toLatin1().data(),
                lastDiffElem.attributeNode("timestamp").value().toLatin1().data(),
                lastDiffElem.attributeNode("sha1").value().toLatin1().data(),
                lastDiffElem.attributeNode("value").value().toLatin1().data());
        } else {
            printf("-1\n-1\n-1\n-1\n-1\n");
        }

        // Last value stability ...
        QDomNodeList lastValueNodes = doc.elementsByTagName("lastValue");
        if (lastValueNodes.size() == 1) {
            QDomElement lastValueElem = lastValueNodes.at(0).toElement();
            printf(
                "%s\n",
                lastValueElem.attributeNode("stable").value().toLatin1().data());
        } else {
            printf("-1\n");
        }

        // Regression ...
        QDomNodeList regressionNodes = doc.elementsByTagName("regression");
        if (regressionNodes.size() == 1) {
            QDomElement regressionElem = regressionNodes.at(0).toElement();
            printf(
                "%s\n%s\n",
                regressionElem.attributeNode("a").value().toLatin1().data(),
                regressionElem.attributeNode("b").value().toLatin1().data());
        } else {
            printf("-1\n-1\n");
        }

        // Results ...
        QDomNodeList resultNodes = doc.elementsByTagName("result");
        for (int i = 0; i < resultNodes.size(); ++i) {
            QDomElement resultElem = resultNodes.at(i).toElement();
            printf("%s\n%s\n%s\n%s\n",
                   resultElem.attributeNode("timestamp").value().toLatin1().data(),
                   resultElem.attributeNode("sha1").value().toLatin1().data(),
                   resultElem.attributeNode("value").value().toLatin1().data(),
                   resultElem.attributeNode("id").value().toLatin1().data());
        }
    } else {
        qDebug() << "error getting history:" << error.toLatin1().data();
    }
}

void BMRequest_GetHistory::handleReply_JSON(const QStringList &args) const
{
    Q_UNUSED(args);

    const QString error =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();

    QString reply;

    if (error.isEmpty()) {

        reply = QString("{");

        // Last difference ...
        QDomNodeList lastDiffNodes = doc.elementsByTagName("lastDiff");
        if (lastDiffNodes.size() == 1) {
            reply.append("\"lastDiff\": {");
            QDomElement lastDiffElem = lastDiffNodes.at(0).toElement();
            reply.append(
                QString("\"index\": %1, ").arg(lastDiffElem.attributeNode("index").value()));
            reply.append(
                QString("\"stable\": %1, ").arg(lastDiffElem.attributeNode("stable").value()));
            reply.append(
                QString("\"result\": [\"%1\", \"%2\", \"%3\"]")
                .arg(lastDiffElem.attributeNode("timestamp").value())
                .arg(lastDiffElem.attributeNode("sha1").value())
                .arg(lastDiffElem.attributeNode("value").value()));
            reply.append("}, ");
        }

        // Last value ...
        QDomNodeList lastValueNodes = doc.elementsByTagName("lastValue");
        if (lastValueNodes.size() == 1) {
            QDomElement lastValueElem = lastValueNodes.at(0).toElement();
            reply.append(
                QString("\"lastValue\": { \"stable\": %1 }, ")
                .arg(lastValueElem.attributeNode("stable").value()));
        }

        // Regression ...
        QDomNodeList regressionNodes = doc.elementsByTagName("regression");
        if (regressionNodes.size() == 1) {
            QDomElement regressionElem = regressionNodes.at(0).toElement();
            reply.append(
                QString("\"regression\": [%1, %2], ")
                .arg(regressionElem.attributeNode("a").value())
                .arg(regressionElem.attributeNode("b").value()));
        }

        // Results ...
        reply.append("\"results\": [");
        QDomNodeList resultNodes = doc.elementsByTagName("result");
        for (int i = 0; i < resultNodes.size(); ++i) {
            QDomElement resultElem = resultNodes.at(i).toElement();
            reply.append(
                QString("%1[\"%2\", \"%3\", \"%4\", \"%5\"]")
                .arg((i == 0) ? "" : ", ")
                .arg(resultElem.attributeNode("timestamp").value())
                .arg(resultElem.attributeNode("sha1").value())
                .arg(resultElem.attributeNode("value").value())
                .arg(resultElem.attributeNode("id").value()));
        }
        reply.append("]}");
    } else {
        reply = QString("{\"error\": \"error getting history: %1\"}").arg(error);
    }

    BMMisc::printJSONOutput(reply);
}

void BMRequest_GetHistory::handleReply_HTML(const QStringList &args) const
{
    int offset = args.indexOf("-server");
    Q_ASSERT(offset >= 0);
    const QString server = args.at(offset + 1);

    offset = args.indexOf("get");
    Q_ASSERT(offset >= 0);
    Q_ASSERT(args.size() >= offset + 18);
    const QString type  = args.at(offset + 1);

    QDomNodeList bmContextNodes = doc.elementsByTagName("bmcontext");
    QDomElement bmContextElem = bmContextNodes.at(0).toElement();
    testCase = bmContextElem.attributeNode("testCase").value();
    testFunction = bmContextElem.attributeNode("testFunction").value();
    dataTag = bmContextElem.attributeNode("dataTag").value();
    metric = bmContextElem.attributeNode("metric").value();
    platform = bmContextElem.attributeNode("platform").value();
    host = bmContextElem.attributeNode("host").value();
    gitRepo = bmContextElem.attributeNode("gitRepo").value();
    gitBranch = bmContextElem.attributeNode("gitBranch").value();

    bool ok;
    ok = BMMisc::getTimestamp(args, offset + 11, &timestamp1);
    Q_ASSERT(ok);
    ok = BMMisc::getTimestamp(args, offset + 12, &timestamp2);
    Q_ASSERT(ok);

    diffTolerance = args.at(offset + 13).toDouble(&ok);
    Q_ASSERT(ok);
    Q_ASSERT(diffTolerance >= 0.0);
    stabTolerance = args.at(offset + 14).toInt(&ok);;
    Q_ASSERT(ok);
    Q_ASSERT(stabTolerance >= 0);
    maxSize = args.at(offset + 15).toInt(&ok);
    Q_ASSERT(ok);

    styleSheet = args.at(offset + 16);

    currTimestamp = args.at(offset + 17).toInt(&ok);;
    Q_ASSERT(ok);

    QString reply;

    if (type == "detailspage") {
        reply = createHTML_detailsPage(server);
        BMMisc::printHTMLOutput(reply);
    } else if (type == "historyplot") {
        QString error;
        BMMisc::printImageOutput(createHTML_historyPlot(&error), "png", error);
    } else {
        reply += QString(
            "<html><head></head><body><b>INTERNAL ERROR:</b>: unknown type in "
            "BMRequest_GetHistory::handleReply_HTML(): &gt;%1&lt;</body></html>").arg(type);
        BMMisc::printHTMLOutput(reply);
    }
}

static bool extractResults(
    QDomNodeList &resultNodes, QString *error, QList<qreal> *timestamps, QList<qreal> *values,
    qreal *vmin, qreal *vmax)
{
    bool ok;

    for (int i = 0; i < resultNodes.size(); ++i) {
        const qreal value =
            resultNodes.at(i).toElement().attributeNode("value").value().toDouble(&ok);
        if (!ok) {
            *error = QString("failed to extract value for result %1:%2")
                .arg(i + 1).arg(resultNodes.size());
            return false;
        }

        const qreal timestamp =
            resultNodes.at(i).toElement().attributeNode("timestamp").value().toDouble(&ok);
        if (!ok) {
            *error = QString("failed to extract timestamp for result %1:%2")
                .arg(i + 1).arg(resultNodes.size());
            return false;
        }

        values->append(value);
        timestamps->append(timestamp);

        if (i == 0) {
            *vmin = *vmax = value;
        } else {
            *vmin = qMin(*vmin, value);
            *vmax = qMax(*vmax, value);
        }
    }

    return true;
}

QString BMRequest_GetHistory::createHTML_detailsPage(const QString &server) const
{
    // *** Header ***
    QString reply = QString("<html>\n<head>\n");
    reply += QString("<link rel=\"stylesheet\" type=\"text/css\" href=\"%1\" />\n").arg(styleSheet);
    reply += QString("</head>\n<body>\n");

    reply += "<span class=\"sectionTitle\" >Benchmark Details</span><br /><br />\n";

    // ### WARNING: The following two lines assume that help.html resides in the same
    // directory as the style sheet file:
    QString htdir = styleSheet.left(styleSheet.lastIndexOf("/") + 1);
    reply += QString("<a href=\"%1/help.html#BenchmarkDetails\">Help</a><br /><br />").arg(htdir);

    const QDateTime currDateTime = BMMisc::createCurrDateTime(currTimestamp);

    // Option 1 (table):
    reply += QString(
        "<table>\n"

        "<tr>\n"
        "<td class=\"context_table_name\" >Date:</td>"
        "<td colspan=\"7\" class=\"context_table\" >%1</td>"
        "</tr>\n"

        "<tr>\n"
        "<td class=\"context_table_name\" >Test&nbsp;case:</td>"
        "<td class=\"context_table_bmark\" >%2</td>\n"
        "<td class=\"context_table_name\" >Metric:</td>"
        "<td class=\"context_table\" >%3</td>\n"
        "<td class=\"context_table_name\" >Branch:</td>"
        "<td class=\"context_table\" >%4&nbsp;%5</td>\n"
        "<td class=\"context_table_name\" >Difference tolerance:</td>"
        "<td class=\"context_table\" >%6</td>\n"
        "</tr>\n"

        "<tr>\n"
        "<td class=\"context_table_name\" >Test&nbsp;function:</td>"
        "<td class=\"context_table_bmark\" >%7</td>\n"
        "<td class=\"context_table_name\" >Platform:</td>"
        "<td class=\"context_table\" >%8</td>\n"
        "<td></td><td></td>"
        "<td class=\"context_table_name\" >Stability tolerance:</td>"
        "<td class=\"context_table\" >%9</td>\n"
        "</tr>\n"

        "<tr>\n"
        "<td class=\"context_table_name\" >Data&nbsp;tag:</td>"
        "<td class=\"context_table_bmark\" >%10</td>\n"
        "<td class=\"context_table_name\" >Host:</td>"
        "<td class=\"context_table\" >%11</td>\n"
        "<td></td><td></td>"
        "<td class=\"context_table_name\" >Max&nbsp;history&nbsp;size:</td>"
        "<td class=\"context_table\" >%12</td>\n"
        "</tr>\n"

        "</table>\n")
        .arg(currDateTime.toString("ddd MMM dd hh:mm:ss yyyy").replace(" ", "&nbsp;"))
        .arg(testCase)
        .arg(metric)
        .arg(gitRepo)
        .arg(gitBranch)
        .arg(diffTolerance)
        .arg(testFunction)
        .arg(platform)
        .arg(stabTolerance)
        .arg(dataTag)
        .arg(host)
        .arg(maxSize)
        ;

    // Option 2 (wrapped paragraph):
/*
    reply += QString(
        "<span style=\"font-size:12px\">\n"
        "<b>Test&nbsp;case:</b>&nbsp;%1"
        "&nbsp;&nbsp; <b>Test&nbsp;function:</b>&nbsp;%2"
        "&nbsp;&nbsp; <b>Data&nbsp;tag:</b>&nbsp;%3"
        "&nbsp;&nbsp; <b>Metric:</b>&nbsp;%4"
        "&nbsp;&nbsp; <b>Platform:</b>&nbsp;%5"
        "&nbsp;&nbsp; <b>Host:</b>&nbsp;%6"
        "&nbsp;&nbsp; <b>Branch:</b>&nbsp;%7&nbsp;%8"
        "&nbsp;&nbsp; <b>Difference&nbsp;tolerance:</b>&nbsp;%9"
        "&nbsp;&nbsp; <b>Stability&nbsp;tolerance:</b>&nbsp;%10"
        "&nbsp;&nbsp; <b>Max&nbsp;history&nbsp;size:</b>&nbsp;%11"
        "&nbsp;&nbsp; <b>Date:</b>&nbsp;%12"
        "</span>\n")
        .arg(testCase)
        .arg(testFunction)
        .arg(dataTag)
        .arg(metric)
        .arg(platform)
        .arg(host)
        .arg(gitRepo)
        .arg(gitBranch)
        .arg(diffTolerance)
        .arg(stabTolerance)
        .arg(maxSize)
        .arg(currDateTime.toString("ddd MMM dd hh:mm:ss yyyy").replace(" ", "&nbsp;"));
*/

    // reply += QString("<span style=\"color:red\">ENVIRONMENT:</span>\n");
    // QStringList sysenv = QProcess::systemEnvironment();
    // for (int i = 0; i < sysenv.size(); ++i)
    //     reply += QString("<br />&gt;\"%2\"&lt;").arg(sysenv.at(i));


    // *** Plot ***
    reply += "<br />\n<img src=\"bmclientwrapper?command=";
    reply += QString("-server %1").arg(server);
    reply += " get historyplot";
    reply += QString(" %1").arg(testCase);
    reply += QString(" %1").arg(testFunction);
    reply += QString(" '%1'").arg(dataTag); // Quotes due to assumption of internal white space
    reply += QString(" %1").arg(metric);
    reply += QString(" %1").arg(platform);
    reply += QString(" %1").arg(host);
    reply += QString(" %1").arg(gitRepo);
    reply += QString(" %1").arg(gitBranch);
    reply += " -timerange first last";
    reply += QString(" %1").arg(diffTolerance);
    reply += QString(" %1").arg(stabTolerance);
    reply += QString(" %1").arg(maxSize);
    reply += QString(" %1").arg("style_sheet_n/a");
    reply += QString(" %1").arg(currTimestamp);
    reply += "\" />\n<br />\n<br />\n";


    // *** Table ***
    reply += "<br /><br />\n";
    QString error = doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();
    if (!error.isEmpty()) {
        reply += QString("failed to create table: error getting history: %1").arg(error);
        reply += "</body></html>";
        return reply;
    }

    QDomNodeList resultNodes = doc.elementsByTagName("result");

    if (resultNodes.isEmpty()) {
        reply += "empty history!";
        reply += "</body></html>";
        return reply;
    }

    QList<qreal> timestamps;
    QList<qreal> values;
    qreal vmin;
    qreal vmax;

    if (!extractResults(resultNodes, &error, &timestamps, &values, &vmin, &vmax)) {
        reply += QString("failed to create table: error extracting results: %1").arg(error);
        reply += "</body></html>";
        return reply;
    }

    reply += "<table style=\"text-align:right\">\n";
    reply += QString(
        "<tr>"
        "<th class=\"details_table_head\">n</th>"
        "<th class=\"details_table_head\">Age</th>"
        "<th class=\"details_table_head\">ND</th>" // normalized difference (significant or not)
        "<th class=\"details_table_head\">LS</th>" // highlights Last Difference and Stability
        "<th class=\"details_table_head\">Timestamp</th>"
        "<th class=\"details_table_head\">SHA-1</th>"
        "<th class=\"details_table_head\">Result ID</th>"
        "<th class=\"details_table_head\">Value</th>"
        "</tr>\n");

    QDomNodeList lastDiffNodes = doc.elementsByTagName("lastDiff");
    const int lastDiffIndex = (lastDiffNodes.size() == 1)
        ? lastDiffNodes.at(0).toElement().attributeNode("index").value().toInt()
        : -1;

    for (int i = 0; i < values.size(); ++i) {
        const int dataIndex = values.size() - 1 - i; // reverse chronological order
        const uint age = currDateTime.toTime_t() - timestamps.at(dataIndex);

        const qreal normDiff = BMMisc::normalizedDifference(values.last(), values.at(dataIndex));
        QString ldsClass;
        if (dataIndex == lastDiffIndex)
            ldsClass = "details_table_value_lastDiff";
        else if ((dataIndex >= ((resultNodes.size() - 1) - stabTolerance)
                  || ((dataIndex <= lastDiffIndex)
                      && (dataIndex >= (lastDiffIndex - stabTolerance)))))
            ldsClass = "details_table_value_stabTolerance";
        else
            ldsClass = "details_table_value";

        reply += QString(
            "<tr>"
            "<td class=\"details_table\">%1</td>"
            "<td class=\"details_table\" style=\"background-color:%2\">%3</td>"
            "<td class=\"details_table_value\" style=\"background-color:%4\">%5</td>"
            "<td class=\"%6\">%7</td>"
            "<td class=\"details_table\">%8</td>"
            "<td class=\"details_table\">%9</td>"
            "<td class=\"details_table_resultID\">%10</td>"
            "<td class=\"details_table_value\">%11</td>"
            "</tr>\n")
            .arg(dataIndex + 1)
            .arg(BMMisc::ageColor(age))
            .arg(BMMisc::secs2daysText(age, 2))
            .arg(BMMisc::diffColor(normDiff))
            .arg(QString().setNum(normDiff, 'f', 4))
            .arg(ldsClass)
            .arg((dataIndex == lastDiffIndex) ? "LD" : "")
            .arg(int(timestamps.at(dataIndex)))
            .arg(resultNodes.at(dataIndex).toElement().attributeNode("sha1").value())
            .arg(resultNodes.at(dataIndex).toElement().attributeNode("id").value())
            .arg(values.at(dataIndex));
    }

    reply += "</table>\n";

    reply += "</body></html>";
    return reply;
}


// Builds a history plot from the server reply in doc. Returns the (non-null) image upon success.
// Returns a null image (and possibly a descriptive message in \a error) upon failure.
// 
QImage BMRequest_GetHistory::createHTML_historyPlot(QString *error) const
{
    *error = QString();
    const qreal width = 1100;
    const qreal height = 450;
    QGraphicsScene scene_far(0, 0, width, height); // Background scene (rendered first)
    QGraphicsScene scene_mid_aa(0, 0, width, height); // Middle scene (antialiased)
    QGraphicsScene scene_near(0, 0, width, height); // Foreground scene (rendered last)

    const QString serverError =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();
    if (!serverError.isEmpty()) {
        *error = QString("failed to create plot: error getting history: %1").arg(serverError);
        return QImage();
    }

    QDomNodeList resultNodes = doc.elementsByTagName("result");
    if (resultNodes.size() == 0) {
        *error = QString("failed to create plot: history empty");
        return QImage();
    }

    QList<qreal> timestamps;
    QList<qreal> values;
    qreal vmin;
    qreal vmax;
    if (!extractResults(resultNodes, error, &timestamps, &values, &vmin, &vmax))
        return QImage();

    const qreal pad_left = 150;
    const qreal pad_right = 150;
    const qreal pad_top = 50;
    const qreal pad_bottom = 50;
    const qreal xmin = pad_left;
    const qreal xmax = width - pad_right;
    const qreal xdefault = 0.5 * (xmin + xmax);
    const qreal ymin = pad_top;
    const qreal ymax = height - pad_bottom;
    const qreal ydefault = 0.5 * (ymin + ymax);

    qreal xfact = 0;
    if (resultNodes.size() > 1) {
        if (timestamps.first() < timestamps.last()) {
            const qreal tfact = 1 / (timestamps.last() - timestamps.first());
            xfact = tfact * (xmax - xmin);
        } else {
            xfact = xdefault;
        }
    }

    const qreal vfact = 1 / (vmax - vmin); // zero division handled elsewhere
    const qreal yfact = vfact * (ymax - ymin);

    // Compute screen coordinates of history curve ...
    QList<qreal> x;
    QList<qreal> y;
    for (int i = 0; i < values.size(); ++i) {
        const qreal t = timestamps.at(i);
        const qreal v = values.at(i);
        x.append((resultNodes.size() > 1) ? (xmin + (t - timestamps.first()) * xfact) : xdefault);
        y.append(BMMisc::v2y(v, ymax, vmin, yfact, ydefault));
    }

    // Draw background ...
    scene_far.setBackgroundBrush(QBrush(Qt::white));

    // Draw axis indicators ...
    const qreal indicatorSize = 5;
    const QColor indicatorColor(119, 119, 255, 255);
    // ... x-axis (all values) ...
    {
        QPainterPath path;
        for (int i = 0; i < x.size(); ++i) {
            path.moveTo(x.at(i), ymax);
            path.lineTo(x.at(i), ymax + indicatorSize);
        }
        scene_far.addPath(path, QPen(indicatorColor));
    }
    // ... y-axises (min and max value only) ...
    {
        QPainterPath path;
        path.moveTo(xmin, ymin);
        path.lineTo(xmin - indicatorSize, ymin);
        path.moveTo(xmin, ymax);
        path.lineTo(xmin - indicatorSize, ymax);
        path.moveTo(xmax, ymin);
        path.lineTo(xmax + indicatorSize, ymin);
        path.moveTo(xmax, ymax);
        path.lineTo(xmax + indicatorSize, ymax);
        scene_far.addPath(path, QPen(indicatorColor));
    }

    const QColor color(0, 0, 0, 255);

    // Draw line indicating the last value ...
    {
        QColor color_ = color;
        color_.setAlpha(50);
        scene_far.addLine(xmin, y.last(), xmax, y.last(), QPen(color_, 1));
    }

    // Draw border rectangle  ...
    scene_far.addRect(xmin, ymin, xmax - xmin, ymax - ymin, QPen(QColor(220, 220, 220, 255)));

    // Draw regression line (if available) ...
    QDomNodeList regressionNodes = doc.elementsByTagName("regression");
    if (regressionNodes.size() == 1) {
        bool ok;
        const qreal a = regressionNodes.at(0).toElement().attributeNode("a").value().toDouble(&ok);
        Q_ASSERT(ok);
        const qreal b = regressionNodes.at(0).toElement().attributeNode("b").value().toDouble(&ok);
        Q_ASSERT(ok);
        const qreal v0 = a + b * timestamps.first();
        const qreal v1 = a + b * timestamps.last();
        const qreal x0 = xmin;
        const qreal x1 = xmax;
        const qreal y0 = BMMisc::v2y(v0, ymax, vmin, yfact, ydefault);
        const qreal y1 = BMMisc::v2y(v1, ymax, vmin, yfact, ydefault);
        scene_mid_aa.addLine(
            x0, y0, x1, y1,
            QPen(b < 0 ? QColor(170, 255, 170, 255) : QColor(255, 170, 170, 255)));
    }

    const qreal dpSize = 4;
    // Draw last difference highlighting ...
    QDomNodeList lastDiffNodes = doc.elementsByTagName("lastDiff");
    const int lastDiffIndex = (lastDiffNodes.size() == 1)
        ? lastDiffNodes.at(0).toElement().attributeNode("index").value().toInt()
        : -1;

    // Draw history curve ...
    if (x.size() > 1)
    {
        QPainterPath path(QPointF(x.first(), y.first()));
        for (int i = 1; i < x.size(); ++i)
            path.lineTo(x.at(i), y.at(i));
        scene_mid_aa.addPath(path, QPen(QColor(128, 128, 128, 255)));
    }

    // Draw data points (except the one representing the last difference) ...
    {
        QPainterPath path;
        for (int i = 0; i < x.size(); ++i)
            if (i != lastDiffIndex)
                path.addRect(x.at(i) - dpSize / 2, y.at(i) - dpSize / 2, dpSize, dpSize);
        scene_near.addPath(path, QPen(color), QBrush(color));
    }

    // Draw the data point representing the last difference ...
    if (lastDiffIndex != -1)
    {
        QPainterPath path;
        path.addRect(
            x.at(lastDiffIndex) - dpSize / 2, y.at(lastDiffIndex) - dpSize / 2, dpSize, dpSize);
        scene_near.addPath(path, QPen(color), QBrush(Qt::yellow)); // ### 4 NOW
    }

    // Draw labels ...
    const qreal labelPad = 10;
    const qreal captionPad = 4;
    qreal captionHeight;

    // ... left y axis ...
    {
        QFont font;
        font.setPointSize(14);
        QGraphicsSimpleTextItem *text = scene_near.addSimpleText("value", font);
        const qreal x_ = xmin - text->boundingRect().height() - captionPad;
        const qreal y_ = (ymin + ymax) / 2 + text->boundingRect().width() / 2;
        text->setTransform(QTransform().translate(x_, y_).rotate(-90));
        captionHeight = text->boundingRect().height();
    }
    {
        QGraphicsSimpleTextItem *text =
            scene_near.addSimpleText(QString().setNum(vmin));
        text->setPos(
            xmin - captionPad - captionHeight - labelPad - text->boundingRect().width(),
            ymax - text->boundingRect().height() / 2);
    }
    {
        QGraphicsSimpleTextItem *text =
            scene_near.addSimpleText(QString().setNum(vmax));
        text->setPos(
            xmin - captionPad - captionHeight - labelPad - text->boundingRect().width(),
            ymin - text->boundingRect().height() / 2);
    }
    {
        QGraphicsSimpleTextItem *text =
            scene_near.addSimpleText(QString().setNum(values.last()));
        text->setPos(
            xmin - captionPad - captionHeight - labelPad - text->boundingRect().width(),
            y.last() - text->boundingRect().height() / 2);

        // Avoid overlap
        if (!scene_near.collidingItems(text).isEmpty()) {
            scene_near.removeItem(text);
            delete text;
        }
    }

    // ... right y axis ...
    {
        QFont font;
        font.setPointSize(14);
        QGraphicsSimpleTextItem *text =
            scene_near.addSimpleText("norm. diff. between last value and value", font);
        const qreal x_ = xmax + captionPad;
        const qreal y_ = (ymin + ymax) / 2 + text->boundingRect().width() / 2;
        text->setTransform(QTransform().translate(x_, y_).rotate(-90));
        captionHeight = text->boundingRect().height();
    }
    {
        QGraphicsSimpleTextItem *text =
            scene_near.addSimpleText(
                QString().setNum(BMMisc::normalizedDifference(values.last(), vmin), 'f', 4));
        text->setPos(
            xmax + captionPad + captionHeight + labelPad,
            ymax - text->boundingRect().height() / 2);
    }
    {
        QGraphicsSimpleTextItem *text =
            scene_near.addSimpleText(
                QString().setNum(BMMisc::normalizedDifference(values.last(), vmax), 'f', 4));
        text->setPos(
            xmax + captionPad + captionHeight + labelPad,
            ymin - text->boundingRect().height() / 2);
    }
    {
        QGraphicsSimpleTextItem *text = scene_near.addSimpleText(QString().setNum(0.0, 'f', 4));
        text->setPos(
            xmax + captionPad + captionHeight + labelPad,
            y.last() - text->boundingRect().height() / 2);

        // Avoid overlap
        if (!scene_near.collidingItems(text).isEmpty()) {
            scene_near.removeItem(text);
            delete text;
        }
    }

    // ... x axis ...
    const QDateTime currDateTime = BMMisc::createCurrDateTime(currTimestamp);
    const QString xLabel_lo =
        BMMisc::secs2daysText(currDateTime.toTime_t() - timestamps.first(), 2);
    const QString xLabel_hi =
        BMMisc::secs2daysText(currDateTime.toTime_t() - timestamps.last(), 2);
    {
        QFont font;
        font.setPointSize(14);
        QGraphicsSimpleTextItem *text = scene_near.addSimpleText("days ago", font);
        const qreal x_ = (xmin + xmax) / 2 - text->boundingRect().width() / 2;
        const qreal y_ = ymax + labelPad;
        text->translate(x_, y_);
    }
    {
        QGraphicsSimpleTextItem *text = scene_near.addSimpleText(xLabel_lo);
        text->setPos(
            xmin - text->boundingRect().width() / 2,
            ymax + labelPad);
    }
    {
        QGraphicsSimpleTextItem *text = scene_near.addSimpleText(xLabel_hi);
        text->setPos(
            xmax - text->boundingRect().width() / 2,
            ymax + labelPad);
    }


    // Return the scenes rendered to an image ...
    QImage image(scene_far.width(), scene_far.height(), QImage::Format_ARGB32);
    {
        QPainter painter(&image);
        scene_far.render(&painter);
    }
    {
        QPainter painter(&image);
        painter.setRenderHint(QPainter::Antialiasing);
        scene_mid_aa.render(&painter);
    }
    {
        QPainter painter(&image);
        scene_near.render(&painter);
    }
    return image;
}

// --- GetHistory2 ---
QByteArray BMRequest_GetHistory2::toRequestBuffer(QString *)
{
    const QString request = QString(
        "<request type=\"%1\"><context "
        "testCase=\"%2\" testFunction=\"%3\" dataTag=\"%4\" metric=\"%5\" platform=\"%6\" "
        "host=\"%7\" gitRepo1=\"%8\" gitBranch1=\"%9\" gitRepo2=\"%10\" gitBranch2=\"%11\" "
        "timestamp1=\"%12\" timestamp2=\"%13\" diffTolerance=\"%14\" "
        "stabTolerance=\"%15\" maxSize=\"%16\" /></request>")
        .arg(name()).arg(testCase).arg(testFunction).arg(dataTag).arg(metric).arg(platform)
        .arg(host).arg(gitRepo1).arg(gitBranch1).arg(gitRepo2).arg(gitBranch2).arg(timestamp1)
        .arg(timestamp2).arg(diffTolerance).arg(stabTolerance).arg(maxSize);
    return xmlConvert(request);
}

QByteArray BMRequest_GetHistory2::toReplyBuffer()
{
    QDomElement contextElem = doc.elementsByTagName("context").at(0).toElement();
    testCase = contextElem.attributeNode("testCase").value();
    testFunction = contextElem.attributeNode("testFunction").value();
    dataTag = contextElem.attributeNode("dataTag").value();
    metric = contextElem.attributeNode("metric").value();
    platform = contextElem.attributeNode("platform").value();
    host = contextElem.attributeNode("host").value();
    gitRepo1 = contextElem.attributeNode("gitRepo1").value();
    gitBranch1 = contextElem.attributeNode("gitBranch1").value();
    gitRepo2 = contextElem.attributeNode("gitRepo2").value();
    gitBranch2 = contextElem.attributeNode("gitBranch2").value();
    timestamp1 = contextElem.attributeNode("timestamp1").value();
    timestamp2 = contextElem.attributeNode("timestamp2").value();
    bool ok;
    diffTolerance = contextElem.attributeNode("diffTolerance").value().toDouble(&ok);
    Q_ASSERT(ok);
    stabTolerance = contextElem.attributeNode("stabTolerance").value().toInt(&ok);
    Q_ASSERT(ok);
    maxSize = contextElem.attributeNode("maxSize").value().toInt(&ok);
    Q_ASSERT(ok);

    QString reply = QString("<reply type=\"%1\">").arg(name());

    int benchmarkId;
    if (!getBenchmarkId(&reply, &benchmarkId, testCase, testFunction, dataTag))
        return xmlConvert(reply);

    int bmcontextId1;
    if (!getBMContextId(
            &reply, &bmcontextId1, benchmarkId, metric, platform, host, gitRepo1, gitBranch1))
        return xmlConvert(reply);
    if (!appendResultHistoryToReply(
            &reply, bmcontextId1, "1", timestamp1, timestamp2, diffTolerance, stabTolerance,
            maxSize))
        return xmlConvert(reply);

    int bmcontextId2;
    if (!getBMContextId(
            &reply, &bmcontextId2, benchmarkId, metric, platform, host, gitRepo2, gitBranch2))
        return xmlConvert(reply);
    if (!appendResultHistoryToReply(
            &reply, bmcontextId2, "2", timestamp1, timestamp2, diffTolerance, stabTolerance,
            maxSize))
        return xmlConvert(reply);

    reply.append("</reply>");

    return xmlConvert(reply);
}

void BMRequest_GetHistory2::handleReply_HTML(const QStringList &args) const
{
    int offset = args.indexOf("-server");
    Q_ASSERT(offset >= 0);
    const QString server = args.at(offset + 1);

    offset = args.indexOf("get");
    Q_ASSERT(offset >= 0);
    Q_ASSERT(args.size() == offset + 21);
    const QString type  = args.at(offset + 1);
    testCase      = args.at(offset + 2);
    testFunction  = args.at(offset + 3);
    dataTag       = args.at(offset + 4);
    metric        = args.at(offset + 5);
    platform      = args.at(offset + 6);
    host          = args.at(offset + 7);
    gitRepo1      = args.at(offset + 8);
    gitBranch1    = args.at(offset + 9);
    gitRepo2      = args.at(offset + 10);
    gitBranch2    = args.at(offset + 11);

    bool ok;
    ok = BMMisc::getTimestamp(args, offset + 13, &timestamp1);
    Q_ASSERT(ok);
    ok = BMMisc::getTimestamp(args, offset + 14, &timestamp2);
    Q_ASSERT(ok);

    diffTolerance = args.at(offset + 15).toDouble(&ok);
    Q_ASSERT(ok);
    Q_ASSERT(diffTolerance >= 0.0);
    stabTolerance = args.at(offset + 16).toInt(&ok);
    Q_ASSERT(ok);
    Q_ASSERT(stabTolerance >= 0);
    maxSize = args.at(offset + 17).toInt(&ok);
    Q_ASSERT(ok);

    sharedTimeScale = bool(args.at(offset + 18).toInt(&ok));
    Q_ASSERT(ok);

    styleSheet = args.at(offset + 19);

    currTimestamp = args.at(offset + 20).toInt(&ok);;
    Q_ASSERT(ok);

    QString reply;

    if (type == "detailspage2") {
        reply = createHTML_detailsPage(server);
        BMMisc::printHTMLOutput(reply);
    } else if (type == "historyplot2") {
        QString error;
        BMMisc::printImageOutput(createHTML_historyPlot(&error), "png", error);
    } else {
        reply += QString(
            "<html><head></head><body><b>INTERNAL ERROR:</b>: unknown type in "
            "BMRequest_GetHistory2::handleReply_HTML(): &gt;%1&lt;</body></html>").arg(type);
        BMMisc::printHTMLOutput(reply);
    }
}

// ### 2 B DOCUMENTED!
static void appendBranchCellsToTable(
    QString *reply, qreal baseValue, const QDateTime &currDateTime, int i, QList<qreal> &values,
    QList<qreal> timestamps, QDomNodeList &resultNodes)
{
    const int dataIndex = values.size() - 1 - i; // reverse chronological order
    const uint age = currDateTime.toTime_t() - timestamps.at(dataIndex);
    const qreal normDiff = BMMisc::normalizedDifference(values.at(dataIndex), baseValue);
    *reply += QString(
        "<td class=\"details_table\">%1</td>"
        "<td class=\"details_table\" style=\"background-color:%2\">%3</td>"
        "<td class=\"details_table_value\" style=\"background-color:%4\">%5</td>"
        "<td class=\"details_table\">%6</td>"
        "<td class=\"details_table\">%7</td>"
        "<td class=\"details_table_value\">%8</td>")
        .arg(dataIndex + 1)
        .arg(BMMisc::ageColor(age))
        .arg(BMMisc::secs2daysText(age, 2))
        .arg(BMMisc::diffColor(normDiff))
        .arg(QString().setNum(normDiff, 'f', 4))
        .arg(int(timestamps.at(dataIndex)))
        .arg(resultNodes.at(dataIndex).toElement().attributeNode("sha1").value())
        .arg(values.at(dataIndex));
}

QString BMRequest_GetHistory2::createHTML_detailsPage(const QString &server) const
{
    // *** Header ***
    QString reply = QString("<html>\n<head>\n");
    reply += QString("<link rel=\"stylesheet\" type=\"text/css\" href=\"%1\" />\n").arg(styleSheet);
    reply += QString("</head>\n<body>\n");

    reply += "<span class=\"sectionTitle\" >Benchmark Details</span><br /><br />\n";

    // ### WARNING: The following two lines assume that help.html resides in the same
    // directory as the style sheet file:
    QString htdir = styleSheet.left(styleSheet.lastIndexOf("/") + 1);
    reply += QString("<a href=\"%1/help.html#BenchmarkDetails\">Help</a><br /><br />").arg(htdir);

    const QDateTime currDateTime = BMMisc::createCurrDateTime(currTimestamp);

    // Option 1 (table):
    reply += QString(
        "<table>\n"

        "<tr>\n"
        "<td class=\"context_table_name\" >Date:</td>"
        "<td colspan=\"7\" class=\"context_table\" >%1</td>"
        "</tr>\n"

        "<tr>\n"
        "<td class=\"context_table_name\" >Test&nbsp;case:</td>"
        "<td class=\"context_table_bmark\" >%2</td>\n"
        "<td class=\"context_table_name\" >Metric:</td>"
        "<td class=\"context_table\" >%3</td>\n"
        "<td class=\"context_table_name\" >Branch 1:</td>"
        "<td class=\"context_table\" >%4&nbsp;%5</td>\n"
        "<td class=\"context_table_name\" >Difference tolerance:</td>"
        "<td class=\"context_table\" >%6</td>\n"
        "</tr>\n"

        "<tr>\n"
        "<td class=\"context_table_name\" >Test&nbsp;function:</td>"
        "<td class=\"context_table_bmark\" >%7</td>\n"
        "<td class=\"context_table_name\" >Platform:</td>"
        "<td class=\"context_table\" >%8</td>\n"
        "<td class=\"context_table_name\" >Branch 2:</td>"
        "<td class=\"context_table\" >%9&nbsp;%10</td>\n"
        "<td class=\"context_table_name\" >Stability tolerance:</td>"
        "<td class=\"context_table\" >%11</td>\n"
        "</tr>\n"

        "<tr>\n"
        "<td class=\"context_table_name\" >Data&nbsp;tag:</td>"
        "<td class=\"context_table_bmark\" >%12</td>\n"
        "<td class=\"context_table_name\" >Host:</td>"
        "<td class=\"context_table\" >%13</td>\n"
        "<td class=\"context_table_name\" >Shared&nbsp;time&nbsp;scale:</td>"
        "<td class=\"context_table\" >%14</td>\n"
        "<td class=\"context_table_name\" >Max&nbsp;history&nbsp;size:</td>"
        "<td class=\"context_table\" >%15</td>\n"
        "</tr>\n"

        "</table>\n")
        .arg(currDateTime.toString("ddd MMM dd hh:mm:ss yyyy").replace(" ", "&nbsp;"))
        .arg(testCase)
        .arg(metric)
        .arg(gitRepo1)
        .arg(gitBranch1)
        .arg(diffTolerance)
        .arg(testFunction)
        .arg(platform)
        .arg(gitRepo2)
        .arg(gitBranch2)
        .arg(stabTolerance)
        .arg(dataTag)
        .arg(host)
        .arg(sharedTimeScale ? "yes" : "no")
        .arg(maxSize)
        ;

    // Option 2 (wrapped paragraph):
/*
    reply += QString(
        "<span style=\"font-size:12px\">\n"
        "<b>Test&nbsp;case:</b>&nbsp;%1"
        "&nbsp;&nbsp; <b>Test&nbsp;function:</b>&nbsp;%2"
        "&nbsp;&nbsp; <b>Data&nbsp;tag:</b>&nbsp;%3"
        "&nbsp;&nbsp; <b>Metric:</b>&nbsp;%4"
        "&nbsp;&nbsp; <b>Platform:</b>&nbsp;%5"
        "&nbsp;&nbsp; <b>Host:</b>&nbsp;%6"
        "&nbsp;&nbsp; <b>Branch&nbsp;1:</b>&nbsp;%7&nbsp;%8"
        "&nbsp;&nbsp; <b>Branch&nbsp;2:</b>&nbsp;%9&nbsp;%10"
        "&nbsp;&nbsp; <b>Difference&nbsp;tolerance:</b>&nbsp;%11"
        "&nbsp;&nbsp; <b>Stability&nbsp;tolerance:</b>&nbsp;%12"
        "&nbsp;&nbsp; <b>Max&nbsp;history&nbsp;size:</b>&nbsp;%13"
        "&nbsp;&nbsp; <b>Shared&nbsp;time&nbsp;scale:</b>&nbsp;%14"
        "&nbsp;&nbsp; <b>Date:</b>&nbsp;%15"
        "</span>\n")
        .arg(testCase)
        .arg(testFunction)
        .arg(dataTag)
        .arg(metric)
        .arg(platform)
        .arg(host)
        .arg(gitRepo1)
        .arg(gitBranch1)
        .arg(gitRepo2)
        .arg(gitBranch2)
        .arg(diffTolerance)
        .arg(stabTolerance)
        .arg(maxSize)
        .arg(sharedTimeScale ? "yes" : "no")
        .arg(currDateTime.toString("ddd MMM dd hh:mm:ss yyyy").replace(" ", "&nbsp;"));
*/


    // *** Plot ***
    reply += "<br />\n<img src=\"bmclientwrapper?command=";
    reply += QString("-server %1").arg(server);
    reply += " get historyplot2";
    reply += QString(" %1").arg(testCase);
    reply += QString(" %1").arg(testFunction);
    reply += QString(" '%1'").arg(dataTag); // Quotes due to assumption of internal white space
    reply += QString(" %1").arg(metric);
    reply += QString(" %1").arg(platform);
    reply += QString(" %1").arg(host);
    reply += QString(" %1").arg(gitRepo1);
    reply += QString(" %1").arg(gitBranch1);
    reply += QString(" %1").arg(gitRepo2);
    reply += QString(" %1").arg(gitBranch2);
    reply += " -timerange first last";
    reply += QString(" %1").arg(diffTolerance);
    reply += QString(" %1").arg(stabTolerance);
    reply += QString(" %1").arg(maxSize);
    reply += QString(" %1").arg(sharedTimeScale ? 1 : 0);
    reply += QString(" %1").arg("style_sheet_n/a");
    reply += QString(" %1").arg(currTimestamp);
    reply += "\" />\n<br />\n<br />\n";


    // *** Table ***
    reply += "<br /><br />\n";

    QString error = doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();
    if (!error.isEmpty()) {
        reply += QString("failed to create table: error getting histories: %1\n").arg(error);
        reply += "</body></html>";
        return reply;
    }

    QDomNodeList resultNodes1 = doc.elementsByTagName("result1");
    if (resultNodes1.isEmpty()) {
        reply += "empty history for branch 1!\n";
        reply += "</body></html>";
        return reply;
    }
    QList<qreal> timestamps1;
    QList<qreal> values1;
    qreal vmin1;
    qreal vmax1;
    if (!extractResults(resultNodes1, &error, &timestamps1, &values1, &vmin1, &vmax1)) {
        reply += QString(
            "failed to create table: error extracting results for branch 1: %1\n").arg(error);
        reply += "</body></html>";
        return reply;
    }

    QDomNodeList resultNodes2 = doc.elementsByTagName("result2");
    if (resultNodes2.isEmpty()) {
        reply += "empty history for branch 2!\n";
        reply += "</body></html>";
        return reply;
    }
    QList<qreal> timestamps2;
    QList<qreal> values2;
    qreal vmin2;
    qreal vmax2;
    if (!extractResults(resultNodes2, &error, &timestamps2, &values2, &vmin2, &vmax2)) {
        reply += QString(
            "failed to create table: error extracting results for branch 2: %1\n").arg(error);
        reply += "</body></html>";
        return reply;
    }

    // -----------------------------

    reply += "<table style=\"text-align:right\">\n";
    reply += "<tr>";
    reply += QString(
        "<th colspan=\"6\"><span style=\"font-weight:normal\">%1 %2</span></th>")
        .arg(gitRepo1).arg(gitBranch1);
    reply += QString(
        "<th colspan=\"6\" style=\"background-color:%1\">"
        "<span style=\"font-weight:normal\">%2 %3</span></th>")
        .arg(branch2color.lighter(170).name()).arg(gitRepo2).arg(gitBranch2);
    // reply += QString("<th colspan=\"6\" style=\"background-color:%1\">Branch 2</th>")
    //     .arg(branch2color.lighter(170).name());
    reply += "</tr>";

    reply += "<tr>";
    for (int i = 0; i < 2; ++i)
        reply +=
            "<th class=\"details_table_head\">n</th>"
            "<th class=\"details_table_head\">Age</th>"
            "<th class=\"details_table_head\">NDB2</th>"
            "<th class=\"details_table_head\">Timestamp</th>"
            "<th class=\"details_table_head\">SHA-1</th>"
            "<th class=\"details_table_head\">Value</th>";
    reply += "</tr>\n";

    bool done1 = false;
    bool done2 = false;

    const qreal baseValue = values2.last();

    for (int i = 0; !(done1 && done2); ++i) {
        reply += "<tr>";

        if (!done1) {
            appendBranchCellsToTable(
                &reply, baseValue, currDateTime, i, values1, timestamps1, resultNodes1);
            done1 = (i >= (values1.size() - 1));
        } else {
            reply += "<td colspan=\"6\" style=\"border:0px\" />";
        }

        if (!done2) {
            appendBranchCellsToTable(
                &reply, baseValue, currDateTime, i, values2, timestamps2, resultNodes2);
            done2 = (i >= (values2.size() - 1));
        } else {
            reply += "<td colspan=\"6\" style=\"border:0px\" />";
        }

        reply += "</tr>\n";
    }

    reply += "</table>\n";

    reply += "</body></html>";

    return reply;
}

// ### 2 B DOCUMENTED!
static void appendBranchCaption(
    QGraphicsScene *scene, int offset, const QString &gitRepo, const QString &gitBranch,
    qreal xmin, qreal ymin, qreal pad, qreal dpSize, const QColor &color, Qt::PenStyle penStyle,
    bool fill)
{
    QFont font;
    font.setPointSize(14);
    QGraphicsSimpleTextItem *text = scene->addSimpleText(
        QString("%2 %3").arg(gitRepo).arg(gitBranch), font);

    QPainterPath pointPath;
    const qreal xlo = xmin + 4 * pad;
    const qreal xhi = xmin + 4 * pad + 40;
    const qreal y =
        ymin - 2 * pad -
        offset * (pad + text->boundingRect().height()) - text->boundingRect().height() / 2;
    pointPath.addRect(xlo - dpSize / 2, y - dpSize / 2, dpSize, dpSize);
    pointPath.addRect(xhi - dpSize / 2, y - dpSize / 2, dpSize, dpSize);
    scene->addPath(pointPath, QPen(color), fill ? QBrush(color) : QBrush());

    QPainterPath linePath(QPointF(xlo, y));
    linePath.lineTo(xhi, y);
    QColor color_ = color;
    color_.setAlpha(100);
    scene->addPath(linePath, QPen(color_, 1, penStyle));

    text->setPos(xhi + 2 * pad, y - text->boundingRect().height() / 2);
}

// Builds a history plot from the server reply in doc. Returns the (non-null) image upon success.
// Returns a null image (and possibly a descriptive message in \a error) upon failure.
// 
// (### Refactor similar code in BMRequest_GetHistory?::createHTML_*() functions.)
//
QImage BMRequest_GetHistory2::createHTML_historyPlot(QString *error) const
{
    const QString serverError =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();
    if (!serverError.isEmpty()) {
        *error = QString("failed to create plot: error getting history: %1").arg(serverError);
        return QImage();
    }

    *error = QString();

    const qreal width = 1100;
    const qreal height = 500;
    const qreal pad_left = 150;
    const qreal pad_right = 150;
    const qreal pad_top = 100;
    const qreal pad_bottom = 50;
    const qreal xmin = pad_left;
    const qreal xmax = width - pad_right;
    const qreal xdefault = 0.5 * (xmin + xmax);
    const qreal ymin = pad_top;
    const qreal ymax = height - pad_bottom;
    const qreal ydefault = 0.5 * (ymin + ymax);

    QGraphicsScene scene_far(0, 0, width, height); // Background scene (rendered first)
    QGraphicsScene scene_mid_aa(0, 0, width, height); // Middle scene (antialiased)
    QGraphicsScene scene_near(0, 0, width, height); // Foreground scene (rendered last)


    // Extract timestamps and values for branch 1 ... (### refactor into a function?)
    QDomNodeList resultNodes1 = doc.elementsByTagName("result1");
    if (resultNodes1.size() == 0) {
        *error = QString("failed to create plot: history for branch 1 empty");
        return QImage();
    }
    QList<qreal> timestamps1;
    QList<qreal> values1;
    qreal vmin1;
    qreal vmax1;
    if (!extractResults(resultNodes1, error, &timestamps1, &values1, &vmin1, &vmax1))
        return QImage();

    // Extract timestamps and values for branch 2 ... (### refactor into a function?)
    QDomNodeList resultNodes2 = doc.elementsByTagName("result2");
    if (resultNodes2.size() == 0) {
        *error = QString("failed to create plot: history for branch 2 empty");
        return QImage();
    }
    QList<qreal> timestamps2;
    QList<qreal> values2;
    qreal vmin2;
    qreal vmax2;
    if (!extractResults(resultNodes2, error, &timestamps2, &values2, &vmin2, &vmax2))
        return QImage();


    const qreal tmin = qMin(timestamps1.first(), timestamps2.first());
    const qreal tmax = qMax(timestamps1.last(), timestamps2.last());
    const qreal vmin = qMin(vmin1, vmin2);
    const qreal vmax = qMax(vmax1, vmax2);

    const qreal tmin1 = sharedTimeScale ? tmin : timestamps1.first();
    const qreal tmax1 = sharedTimeScale ? tmax : timestamps1.last();
    const qreal tmin2 = sharedTimeScale ? tmin : timestamps2.first();
    const qreal tmax2 = sharedTimeScale ? tmax : timestamps2.last();
    const qreal xfact1 = (tmin1 < tmax1) ? ((xmax - xmin) / (tmax1 - tmin1)) : 0;
    const qreal xfact2 = (tmin2 < tmax2) ? ((xmax - xmin) / (tmax2 - tmin2)) : 0;
    const qreal yfact = (ymax - ymin) / (vmax - vmin); // zero division handled elsewhere

    // Compute screen coordinates of history curve for branch 1 ...
    QList<qreal> x1;
    QList<qreal> y1;
    for (int i = 0; i < values1.size(); ++i) {
        const qreal t = timestamps1.at(i);
        const qreal v = values1.at(i);
        x1.append((xfact1 > 0) ? (xmin + (t - tmin1) * xfact1) : xdefault);
        y1.append(BMMisc::v2y(v, ymax, vmin, yfact, ydefault));
    }

    // Compute screen coordinates of history curve for branch 2 ...
    QList<qreal> x2;
    QList<qreal> y2;
    for (int i = 0; i < values2.size(); ++i) {
        const qreal t = timestamps2.at(i);
        const qreal v = values2.at(i);
        x2.append((xfact2 > 0) ? (xmin + (t - tmin2) * xfact2) : xdefault);
        y2.append(BMMisc::v2y(v, ymax, vmin, yfact, ydefault));
    }


    // Draw background ...
    scene_far.setBackgroundBrush(QBrush(Qt::white));

    // Draw axis indicators ...
    const qreal indicatorSize = 5;
    const QColor indicatorColor(200, 200, 200, 255);
    // ... x-axis (branch 1; all values) ...
    {
        QPainterPath path;
        for (int i = 0; i < x1.size(); ++i) {
            path.moveTo(x1.at(i), ymax);
            path.lineTo(x1.at(i), ymax + indicatorSize);
        }
        scene_far.addPath(path, QPen(indicatorColor));
    }
    // ... x-axis (branch 2; all values) ...
    {
        QPainterPath path;
        for (int i = 0; i < x2.size(); ++i) {
            path.moveTo(x2.at(i), ymin);
            path.lineTo(x2.at(i), ymin - indicatorSize);
        }
        scene_far.addPath(path, QPen(indicatorColor));
    }
    // ... y-axises (min and max value only) ...
    {
        QPainterPath path;
        path.moveTo(xmin, ymin);
        path.lineTo(xmin - indicatorSize, ymin);
        path.moveTo(xmin, ymax);
        path.lineTo(xmin - indicatorSize, ymax);
        path.moveTo(xmax, ymin);
        path.lineTo(xmax + indicatorSize, ymin);
        path.moveTo(xmax, ymax);
        path.lineTo(xmax + indicatorSize, ymax);
        scene_far.addPath(path, QPen(indicatorColor));
    }

    const QColor branch1color(0, 0, 0, 255);
    const qreal branch1pointSize = 4;
    const qreal branch2pointSize = 6;
    const Qt::PenStyle branch1penStyle = Qt::SolidLine;
    const Qt::PenStyle branch2penStyle = Qt::DashLine;

    // Draw line indicating the last value ...
    // ... branch 1 ...
    {
        QColor color = branch1color;
        color.setAlpha(50);
        scene_far.addLine(xmin, y1.last(), xmax, y1.last(), QPen(color, 1, branch1penStyle));
    }
    // ... branch 2 ...
    {
        QColor color = branch2color;
        color.setAlpha(50);
        scene_far.addLine(xmin, y2.last(), xmax, y2.last(), QPen(color, 1, branch2penStyle));
    }

    // Draw border rectangle  ...
    scene_far.addRect(xmin, ymin, xmax - xmin, ymax - ymin, QPen(QColor(220, 220, 220, 255)));

    // Draw history curve ...
    // ... branch 1 ...
    if (x1.size() > 1)
    {
        QPainterPath path(QPointF(x1.first(), y1.first()));
        for (int i = 1; i < x1.size(); ++i)
            path.lineTo(x1.at(i), y1.at(i));
        QColor color = branch1color;
        color.setAlpha(100);
        scene_mid_aa.addPath(path, QPen(color, 1, branch1penStyle));
    }
    // ... branch 2 ...
    if (x2.size() > 1)
    {
        QPainterPath path(QPointF(x2.first(), y2.first()));
        for (int i = 1; i < x2.size(); ++i)
            path.lineTo(x2.at(i), y2.at(i));
        QColor color = branch2color;
        color.setAlpha(100);
        scene_mid_aa.addPath(path, QPen(color, 1, branch2penStyle));
    }

    // Draw data points ...
    // ... branch 1 ...
    {
        QPainterPath path;
        const qreal dpSize = branch1pointSize;
        for (int i = 0; i < x1.size(); ++i)
            path.addRect(x1.at(i) - dpSize / 2, y1.at(i) - dpSize / 2, dpSize, dpSize);
        scene_near.addPath(path, QPen(branch1color), QBrush(branch1color));
    }
    // ... branch 2 ...
    {
        QPainterPath path;
        const int dpSize = branch2pointSize;
        for (int i = 0; i < x2.size(); ++i)
            path.addRect(x2.at(i) - dpSize / 2, y2.at(i) - dpSize / 2, dpSize, dpSize);
        scene_near.addPath(path, QPen(branch2color));
    }

    // Draw labels ...
    const qreal labelPad = 10;
    const qreal captionPad = 4;
    qreal captionHeight;

    // ... left y axis ...
    {
        QFont font;
        font.setPointSize(14);
        QGraphicsSimpleTextItem *text = scene_near.addSimpleText("value", font);
        const qreal x_ = xmin - text->boundingRect().height() - captionPad;
        const qreal y_ = (ymin + ymax) / 2 + text->boundingRect().width() / 2;
        text->setTransform(QTransform().translate(x_, y_).rotate(-90));
        captionHeight = text->boundingRect().height();
    }
    {
        QGraphicsSimpleTextItem *text =
            scene_near.addSimpleText(QString().setNum(vmin));
        text->setPos(
            xmin - captionPad - captionHeight - labelPad - text->boundingRect().width(),
            ymax - text->boundingRect().height() / 2);
    }
    {
        QGraphicsSimpleTextItem *text =
            scene_near.addSimpleText(QString().setNum(vmax));
        text->setPos(
            xmin - captionPad - captionHeight - labelPad - text->boundingRect().width(),
            ymin - text->boundingRect().height() / 2);
    }
    // ### Refactor the following two blocks into a function:
    {
        QGraphicsSimpleTextItem *text =
            scene_near.addSimpleText(QString().setNum(values1.last()));
        text->setPos(
            xmin - captionPad - captionHeight - labelPad - text->boundingRect().width(),
            y1.last() - text->boundingRect().height() / 2);

        // Avoid overlap
        if (!scene_near.collidingItems(text).isEmpty()) {
            scene_near.removeItem(text);
            delete text;
        }
    }
    {
        QGraphicsSimpleTextItem *text =
            scene_near.addSimpleText(QString().setNum(values2.last()));
        text->setBrush(branch2color);
        text->setPos(
            xmin - captionPad - captionHeight - labelPad - text->boundingRect().width(),
            y2.last() - text->boundingRect().height() / 2);

        // Avoid overlap
        if (!scene_near.collidingItems(text).isEmpty()) {
            scene_near.removeItem(text);
            delete text;
        }
    }

    // ... right y axis ...
    {
        QFont font;
        font.setPointSize(14);
        QGraphicsSimpleTextItem *text =
            scene_near.addSimpleText("norm. diff. between value and last value in Branch 2", font);
        const qreal x_ = xmax + captionPad;
        const qreal y_ = (ymin + ymax) / 2 + text->boundingRect().width() / 2;
        text->setTransform(QTransform().translate(x_, y_).rotate(-90));
        captionHeight = text->boundingRect().height();
    }
    {
        QGraphicsSimpleTextItem *text =
            scene_near.addSimpleText(
                QString().setNum(BMMisc::normalizedDifference(vmin, values2.last()), 'f', 4));
        text->setPos(
            xmax + captionPad + captionHeight + labelPad,
            ymax - text->boundingRect().height() / 2);
    }
    {
        QGraphicsSimpleTextItem *text =
            scene_near.addSimpleText(
                QString().setNum(BMMisc::normalizedDifference(vmax, values2.last()), 'f', 4));
        text->setPos(
            xmax + captionPad + captionHeight + labelPad,
            ymin - text->boundingRect().height() / 2);
    }
    // ### Refactor the following two blocks into a function:
    {
        QGraphicsSimpleTextItem *text =
            scene_near.addSimpleText(
                QString().setNum(
                    BMMisc::normalizedDifference(values1.last(), values2.last()), 'f', 4));
        text->setPos(
            xmax + captionPad + captionHeight + labelPad,
            y1.last() - text->boundingRect().height() / 2);

        // Avoid overlap
        if (!scene_near.collidingItems(text).isEmpty()) {
            scene_near.removeItem(text);
            delete text;
        }
    }
    {
        QGraphicsSimpleTextItem *text = scene_near.addSimpleText(QString().setNum(0.0, 'f', 4));
        text->setBrush(branch2color);
        text->setPos(
            xmax + captionPad + captionHeight + labelPad,
            y2.last() - text->boundingRect().height() / 2);

        // Avoid overlap
        if (!scene_near.collidingItems(text).isEmpty()) {
            scene_near.removeItem(text);
            delete text;
        }
    }

    // ... x axis ...
    const QDateTime currDateTime = BMMisc::createCurrDateTime(currTimestamp);
    // ... branch 1 ...
    {
        QFont font;
        font.setPointSize(14);
        QGraphicsSimpleTextItem *text =
            scene_near.addSimpleText(
                QString("days ago%1").arg(sharedTimeScale ? "" : " (Branch 1)"), font);
        text->setBrush(branch1color);
        const qreal x_ = (xmin + xmax) / 2 - text->boundingRect().width() / 2;
        const qreal y_ = ymax + labelPad;
        text->translate(x_, y_);
    }
    {
        const QString xLabel_lo = BMMisc::secs2daysText(currDateTime.toTime_t() - tmin1, 2);
        QGraphicsSimpleTextItem *text = scene_near.addSimpleText(xLabel_lo);
        text->setPos(xmin - text->boundingRect().width() / 2, ymax + labelPad);
    }
    {
        const QString xLabel_hi = BMMisc::secs2daysText(currDateTime.toTime_t() - tmax1, 2);
        QGraphicsSimpleTextItem *text = scene_near.addSimpleText(xLabel_hi);
        text->setPos(xmax - text->boundingRect().width() / 2, ymax + labelPad);
    }
    // ... branch 2 ...
    if (!sharedTimeScale) {
        {
            QFont font;
            font.setPointSize(14);
            QGraphicsSimpleTextItem *text = scene_near.addSimpleText("days ago (Branch 2)", font);
            text->setBrush(branch2color);
            const qreal x_ = (xmin + xmax) / 2 - text->boundingRect().width() / 2;
            const qreal y_ = ymin - labelPad - text->boundingRect().height();
            text->translate(x_, y_);
        }
        {
            const QString xLabel_lo = BMMisc::secs2daysText(currDateTime.toTime_t() - tmin2, 2);
            QGraphicsSimpleTextItem *text = scene_near.addSimpleText(xLabel_lo);
            text->setBrush(branch2color);
            text->setPos(
                xmin - text->boundingRect().width() / 2,
                ymin - labelPad - text->boundingRect().height());
        }
        {
            const QString xLabel_hi = BMMisc::secs2daysText(currDateTime.toTime_t() - tmax2, 2);
            QGraphicsSimpleTextItem *text = scene_near.addSimpleText(xLabel_hi);
            text->setBrush(branch2color);
            text->setPos(
                xmax - text->boundingRect().width() / 2,
                ymin - labelPad - text->boundingRect().height());
        }
    }

    // Draw captions ...
    appendBranchCaption(
        &scene_near, 1, gitRepo1, gitBranch1, xmin, ymin, labelPad, branch1pointSize,
        branch1color, branch1penStyle, true);
    appendBranchCaption(
        &scene_near, 0, gitRepo2, gitBranch2, xmin, ymin, labelPad, branch2pointSize,
        branch2color, branch2penStyle, false);

    // Return the scenes rendered to an image ...
    QImage image(scene_far.width(), scene_far.height(), QImage::Format_ARGB32);
    {
        QPainter painter(&image);
        scene_far.render(&painter);
    }
    {
        QPainter painter(&image);
        painter.setRenderHint(QPainter::Antialiasing);
        scene_mid_aa.render(&painter);
    }
    {
        QPainter painter(&image);
        scene_near.render(&painter);
    }
    return image;
}

// --- GetRankedBenchmarks ---
QByteArray BMRequest_GetRankedBenchmarks::toRequestBuffer(QString *)
{
    const QString request = QString(
        "<request type=\"%1\"><context "
        "metric=\"%2\" platform=\"%3\" host=\"%4\" gitRepo=\"%5\" gitBranch=\"%6\" "
        "timestamp1=\"%7\" timestamp2=\"%8\" "
        "diffTolerance=\"%9\" stabTolerance=\"%10\" "
        "ranking=\"%11\" scope=\"%12\" maxSize=\"%13\" testCaseFilter=\"%14\" "
        "testFunctionFilter=\"%15\" dataTagFilter=\"%16\" /></request>")
        .arg(name()).arg(metric).arg(platform).arg(host).arg(gitRepo).arg(gitBranch)
        .arg(timestamp1).arg(timestamp2)
        .arg(diffTolerance).arg(stabTolerance).arg(ranking).arg(scope).arg(maxSize)
        .arg(testCaseFilter).arg(testFunctionFilter).arg(dataTagFilter);
    return xmlConvert(request);
}

QByteArray BMRequest_GetRankedBenchmarks::toReplyBuffer()
{
    QDomElement contextElem = doc.elementsByTagName("context").at(0).toElement();
    metric = contextElem.attributeNode("metric").value();
    platform = contextElem.attributeNode("platform").value();
    host = contextElem.attributeNode("host").value();
    gitRepo = contextElem.attributeNode("gitRepo").value();
    gitBranch = contextElem.attributeNode("gitBranch").value();
    timestamp1 = contextElem.attributeNode("timestamp1").value();
    timestamp2 = contextElem.attributeNode("timestamp2").value();
    bool ok;
    diffTolerance = contextElem.attributeNode("diffTolerance").value().toDouble(&ok);
    Q_ASSERT(ok);
    stabTolerance = contextElem.attributeNode("stabTolerance").value().toInt(&ok);
    Q_ASSERT(ok);
    ranking = contextElem.attributeNode("ranking").value();
    scope = contextElem.attributeNode("scope").value();
    maxSize = contextElem.attributeNode("maxSize").value().toInt(&ok);
    Q_ASSERT(ok);
    testCaseFilter = contextElem.attributeNode("testCaseFilter").value();
    testFunctionFilter = contextElem.attributeNode("testFunctionFilter").value();
    dataTagFilter = contextElem.attributeNode("dataTagFilter").value();

    QString reply;

    // Step 1: Get all benchmark contexts matching the context and the benchmark filters ...
    int contextId;
    if (!getContextId(&reply, &contextId, metric, platform, host, gitRepo, gitBranch))
        return xmlConvert(reply);

    QSqlQuery *query = createQuery();
    if (!query->exec(
            QString(
                "SELECT DISTINCT bmcontext.id, benchmark.id"
                "  FROM bmcontext, benchmark"
                " WHERE contextId=%1"
                "   AND benchmarkId=benchmark.id %2;")
            .arg(contextId)
            .arg(createBenchmarkFilterExpression(
                     testCaseFilter, testFunctionFilter, dataTagFilter, true, "benchmark.")))) {
        reply = errorReply(*query, name(), "failed to get filtered benchmarks (exec() failed)");
        deleteQuery(query);
        return xmlConvert(reply);
    }

    QList<int> bmcontextIds;
    QList<int> benchmarkIds;
    while (query->next()) {
        bmcontextIds.append(query->value(0).toInt());
        benchmarkIds.append(query->value(1).toInt());
    }
    deleteQuery(query);

    QMap<QString, QList<BenchmarkStats *> *> bmStatsMap;

    // Step 2: For each benchmark context ...
    for (int i = 0; i < bmcontextIds.size(); ++i) {

        // Get the results in the specified snapshot range.
        query = createQuery();
        if (!query->exec(createHistoryQuery(bmcontextIds.at(i), timestamp1, timestamp2))) {
            reply = errorReply(*query, name(), "failed to get history (exec() failed)");
            deleteQuery(query);
            return xmlConvert(reply);
        }

        QList<ResultInfo> results;
        while (query->next()) {
            results.prepend(
                ResultInfo(
                    query->value(0).toInt(),
                    query->value(1).toString(),
                    query->value(2).toDouble()));
        }
        deleteQuery(query);

        // Compute statistics for the results.
        int lastDiff = -1;
        bool lastDiffStable = false;
        bool lastValueStable = false;
        bool regressionValid = false;
        qreal a = 0;
        qreal b = 0;
        if (results.size() > 1) {
            computeStatistics(
                results, diffTolerance, stabTolerance, &lastDiff, &lastDiffStable,
                &lastValueStable, &regressionValid, &a, &b);
        } else {
            lastValueStable = ((results.size() == 1) && (stabTolerance == 0));
        }

        QString testCase;
        QString testFunction;
        QString dataTag;
        if (!getBenchmark(&reply, benchmarkIds.at(i), &testCase, &testFunction, &dataTag))
            return xmlConvert(reply);

        // Register the benchmark with its statistics in either a global list or in
        // a list for this combination of testCase/testFunction (depending on the scope).
        const QString key =
            ((scope == "global")
             ? QString()
             : QString("%1-%2").arg(testCase).arg(testFunction));
        QList<BenchmarkStats *> * bmStatsList = bmStatsMap.value(key);
        if (!bmStatsList) {
            bmStatsList = new QList<BenchmarkStats *>;
            bmStatsMap.insert(key, bmStatsList);
        }
        bmStatsList->append(
            new BenchmarkStats(
                testCase, testFunction, dataTag,
                (!results.isEmpty()) ? results.last().timestamp : -1,
                (!results.isEmpty()) ? results.last().sha1 : QString(),
                (!results.isEmpty()) ? results.last().value : -1,
                lastValueStable,
                (lastDiff > -1) ? results.at(lastDiff).timestamp : -1,
                (lastDiff > -1) ? results.at(lastDiff).sha1 : QString(),
                (lastDiff > -1) ? results.at(lastDiff).value : -1,
                lastDiffStable,
                regressionValid, a, b));
    }

    reply = QString("<reply type=\"%1\">").arg(name());

    // Step 3: For each list ...
    QList<QList<BenchmarkStats  *> *> bmStatsLists = bmStatsMap.values();
    for (int i = 0; i < bmStatsLists.size(); ++i) {
        QList<BenchmarkStats *> * bmStatsList = bmStatsLists.at(i);

        // Sort according to the ranking.
        qSort(bmStatsList->begin(), bmStatsList->end(), BenchmarkStats::LessThan(ranking));

        // Output at most the first maxSize items ...
        const int maxSize_ = (maxSize < 0) ? bmStatsList->size() : maxSize;
        for (int j = 0; j < qMin(maxSize_, bmStatsList->size()); ++j) {
            reply.append(
                QString("<benchmark testCase=\"%1\" testFunction=\"%2\" dataTag=\"%3\">")
                .arg(bmStatsList->at(j)->testCase)
                .arg(bmStatsList->at(j)->testFunction)
                .arg(bmStatsList->at(j)->dataTag));

            if (bmStatsList->at(j)->value_lastValue > -1) {
                reply.append(
                    QString("<lastValue timestamp=\"%1\" sha1=\"%2\" value=\"%3\" "
                            " stable=\"%4\" />")
                    .arg(bmStatsList->at(j)->timestamp_lastValue)
                    .arg(bmStatsList->at(j)->sha1_lastValue)
                    .arg(bmStatsList->at(j)->value_lastValue)
                    .arg(bmStatsList->at(j)->stable_lastValue ? "true" : "false"));
            }

            if (bmStatsList->at(j)->value_lastDiff > -1) {
                reply.append(
                    QString("<lastDiff timestamp=\"%1\" sha1=\"%2\" value=\"%3\" "
                            " stable=\"%4\" />")
                    .arg(bmStatsList->at(j)->timestamp_lastDiff)
                    .arg(bmStatsList->at(j)->sha1_lastDiff)
                    .arg(bmStatsList->at(j)->value_lastDiff)
                    .arg(bmStatsList->at(j)->stable_lastDiff ? "true" : "false"));
            }

            if (bmStatsList->at(j)->regressionValid) {
                reply.append(
                    QString("<regression a=\"%1\" b=\"%2\" />")
                    .arg(bmStatsList->at(j)->a)
                    .arg(bmStatsList->at(j)->b));
            }

            reply.append("</benchmark>");
        }

        // Clean up dynamic memory ...
        for (int j = 0; j < bmStatsList->size(); ++j)
            delete bmStatsList->at(j);
        delete bmStatsList;
    }

    reply.append("</reply>");

    return xmlConvert(reply);
}

void BMRequest_GetRankedBenchmarks::handleReply_Raw(const QStringList &args) const
{
    printf("NOTE: raw output not supported yet; printing JSON output for now:\n\n");
    handleReply_JSON(args);
}

void BMRequest_GetRankedBenchmarks::handleReply_JSON(const QStringList &args) const
{
    Q_UNUSED(args);

    const QString error =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();

    QString reply;

    if (error.isEmpty()) {

        reply = QString("{ \"benchmarks\": [");

        QDomNodeList benchmarkNodes = doc.elementsByTagName("benchmark");
        for (int i = 0; i < benchmarkNodes.size(); ++i) {
            reply.append(QString("%1{").arg((i == 0) ? "" : ", "));

            QDomElement benchmarkElem = benchmarkNodes.at(i).toElement();

            // Name ...
            reply.append(
                QString("\"name\": [\"%1\", \"%2\", \"%3\"]")
                .arg(benchmarkElem.attributeNode("testCase").value())
                .arg(benchmarkElem.attributeNode("testFunction").value())
                .arg(benchmarkElem.attributeNode("dataTag").value()));

            // Last value ...
            QDomNodeList lastValueNodes = benchmarkElem.elementsByTagName("lastValue");
            if (lastValueNodes.size() == 1) {
                QDomElement lastValueElem = lastValueNodes.at(0).toElement();
                reply.append(
                    QString(", \"lastValue\": [%1, \"%2\", %3, %4]")
                    .arg(lastValueElem.attributeNode("timestamp").value())
                    .arg(lastValueElem.attributeNode("sha1").value())
                    .arg(lastValueElem.attributeNode("value").value())
                    .arg(lastValueElem.attributeNode("stable").value()));
            }

            // Last difference ...
            QDomNodeList lastDiffNodes = benchmarkElem.elementsByTagName("lastDiff");
            if (lastDiffNodes.size() == 1) {
                QDomElement lastDiffElem = lastDiffNodes.at(0).toElement();
                reply.append(
                    QString(", \"lastDiff\": [%1, \"%2\", %3, %4]")
                    .arg(lastDiffElem.attributeNode("timestamp").value())
                    .arg(lastDiffElem.attributeNode("sha1").value())
                    .arg(lastDiffElem.attributeNode("value").value())
                    .arg(lastDiffElem.attributeNode("stable").value()));
            }

            // Regression ...
            QDomNodeList regressionNodes = benchmarkElem.elementsByTagName("regression");
            if (regressionNodes.size() == 1) {
                QDomElement regressionElem = regressionNodes.at(0).toElement();
                reply.append(
                    QString(", \"regression\": [%1, %2]")
                    .arg(regressionElem.attributeNode("a").value())
                    .arg(regressionElem.attributeNode("b").value()));
            }

            reply.append("}");
        }

        reply.append("]}");

    } else {
        reply = QString("{\"error\": \"error getting ranked benchmarks: %1\"}").arg(error);
    }

    BMMisc::printJSONOutput(reply);
}

// --- GetRankedBenchmarks2 ---
QByteArray BMRequest_GetRankedBenchmarks2::toRequestBuffer(QString *)
{
    const QString request = QString(
        "<request type=\"%1\"><context "
        "metric=\"%2\" platform=\"%3\" host=\"%4\" gitRepo1=\"%5\" gitBranch1=\"%6\" "
        "gitRepo2=\"%7\" gitBranch2=\"%8\" diffTolerance=\"%9\" stabTolerance=\"%10\" "
        "ranking=\"%11\" scope=\"%12\" maxSize=\"%13\" testCaseFilter=\"%14\" "
        "testFunctionFilter=\"%15\" dataTagFilter=\"%16\" /></request>")
        .arg(name()).arg(metric).arg(platform).arg(host).arg(gitRepo1).arg(gitBranch1)
        .arg(gitRepo2).arg(gitBranch2).arg(diffTolerance).arg(stabTolerance).arg(ranking)
        .arg(scope).arg(maxSize).arg(testCaseFilter).arg(testFunctionFilter).arg(dataTagFilter);
    return xmlConvert(request);
}

QByteArray BMRequest_GetRankedBenchmarks2::toReplyBuffer()
{
    QDomElement contextElem = doc.elementsByTagName("context").at(0).toElement();
    metric = contextElem.attributeNode("metric").value();
    platform = contextElem.attributeNode("platform").value();
    host = contextElem.attributeNode("host").value();
    gitRepo1 = contextElem.attributeNode("gitRepo1").value();
    gitBranch1 = contextElem.attributeNode("gitBranch1").value();
    gitRepo2 = contextElem.attributeNode("gitRepo2").value();
    gitBranch2 = contextElem.attributeNode("gitBranch2").value();
    bool ok;
    diffTolerance = contextElem.attributeNode("diffTolerance").value().toDouble(&ok);
    Q_ASSERT(ok);
    stabTolerance = contextElem.attributeNode("stabTolerance").value().toInt(&ok);
    Q_ASSERT(ok);
    ranking = contextElem.attributeNode("ranking").value();
    scope = contextElem.attributeNode("scope").value();
    maxSize = contextElem.attributeNode("maxSize").value().toInt(&ok);
    Q_ASSERT(ok);
    testCaseFilter = contextElem.attributeNode("testCaseFilter").value();
    testFunctionFilter = contextElem.attributeNode("testFunctionFilter").value();
    dataTagFilter = contextElem.attributeNode("dataTagFilter").value();

    QString reply;

    // Step 1: Get all benchmarks that match both the branch 1 and branch 2 context as well as
    // the benchmark filters ...

    // Get the two context IDs ...
    int contextId1;
    if (!getContextId(&reply, &contextId1, metric, platform, host, gitRepo1, gitBranch1))
        return xmlConvert(reply);
    int contextId2;
    if (!getContextId(&reply, &contextId2, metric, platform, host, gitRepo2, gitBranch2))
        return xmlConvert(reply);

    // Get the benchmarks IDs that match both context IDs ...
    QString query_s = QString(
        "SELECT DISTINCT benchmark.id"
        " FROM  bmcontext, benchmark"
        " WHERE contextId=%1"
        "   AND benchmarkId=benchmark.id %2"
        "INTERSECT "
        "SELECT DISTINCT benchmark.id"
        " FROM  bmcontext, benchmark"
        " WHERE contextId=%3"
        "   AND benchmarkId=benchmark.id;")
        .arg(contextId1)
        .arg(createBenchmarkFilterExpression(
                 testCaseFilter, testFunctionFilter, dataTagFilter, true, "benchmark."))
        .arg(contextId2);
    QSqlQuery *query = createQuery();
    if (!query->exec(query_s)) {
        reply = errorReply(
            *query, name(), "failed to get intersection of benchmark IDs (exec() failed)");
        deleteQuery(query);
        return xmlConvert(reply);
    }

    QList<int> benchmarkIds;
    while (query->next())
        benchmarkIds.append(query->value(0).toInt());
    deleteQuery(query);

    // Retrieve bmcontext IDs and benchmark names for each benchmark ...

    QList<BenchmarkInfo *> bmInfos;

    for (int i = 0; i < benchmarkIds.size(); ++i) {
        int bmcontextId1;
        if (!getBMContextId(&reply, &bmcontextId1, benchmarkIds.at(i), contextId1))
            return xmlConvert(reply);

        int bmcontextId2;
        if (!getBMContextId(&reply, &bmcontextId2, benchmarkIds.at(i), contextId2))
            return xmlConvert(reply);

        QString testCase;
        QString testFunction;
        QString dataTag;
        if (!getBenchmark(&reply, benchmarkIds.at(i), &testCase, &testFunction, &dataTag))
            return xmlConvert(reply);
        bmInfos.append(
            new BenchmarkInfo(
                testCase, testFunction, dataTag, QList<int>() << bmcontextId1 << bmcontextId2));
    }

    QMap<QString, QList<BenchmarkStats2 *> *> bmStatsMap;

    const int limit = 1 + stabTolerance;

    // Step 2: Compute branch 1 vs branch 2 differences for each benchmark ...
    for (int i = 0; i < bmInfos.size(); ++i) {

        // Get the last few results for branch 1 and determine stability ...
        query = createQuery();
        if (!query->exec(
                createHistoryQuery(bmInfos.at(i)->bmcontextIds.at(0), "first", "last", limit))) {
            reply =
                errorReply(*query, name(), "failed to get history for branch 1 (exec() failed)");
            deleteQuery(query);
            return xmlConvert(reply);
        }
        QList<ResultInfo> results1;
        while (query->next()) {
            results1.prepend(
                ResultInfo(
                    query->value(0).toInt(),
                    query->value(1).toString(),
                    query->value(2).toDouble()));
        }
        deleteQuery(query);
        const bool stable_lastValue1 = lastValueIsStable(results1, diffTolerance, stabTolerance);

        // Get the last few results for branch 2 and determine stability ...
        query = createQuery();
        if (!query->exec(
                createHistoryQuery(bmInfos.at(i)->bmcontextIds.at(1), "first", "last", limit))) {
            reply =
                errorReply(*query, name(), "failed to get history for branch 2 (exec() failed)");
            deleteQuery(query);
            return xmlConvert(reply);
        }
        QList<ResultInfo> results2;
        while (query->next()) {
            results2.prepend(
                ResultInfo(
                    query->value(0).toInt(),
                    query->value(1).toString(),
                    query->value(2).toDouble()));
        }
        deleteQuery(query);
        const bool stable_lastValue2 = lastValueIsStable(results2, diffTolerance, stabTolerance);

        // Register the benchmark with its statistics in either a global list or in
        // a list for this combination of testCase/testFunction (depending on the scope).
        const QString key =
            ((scope == "global")
             ? QString()
             : QString("%1-%2").arg(bmInfos.at(i)->testCase).arg(bmInfos.at(i)->testFunction));
        QList<BenchmarkStats2 *> * bmStatsList = bmStatsMap.value(key);
        if (!bmStatsList) {
            bmStatsList = new QList<BenchmarkStats2 *>;
            bmStatsMap.insert(key, bmStatsList);
        }
        Q_ASSERT(!results1.isEmpty());
        Q_ASSERT(!results2.isEmpty());
        bmStatsList->append(
            new BenchmarkStats2(
                bmInfos.at(i)->testCase,
                bmInfos.at(i)->testFunction,
                bmInfos.at(i)->dataTag,
                results1.last().timestamp,
                results1.last().sha1,
                results1.last().value,
                stable_lastValue1,
                results2.last().timestamp,
                results2.last().sha1,
                results2.last().value,
                stable_lastValue2));
    }

    reply = QString("<reply type=\"%1\">").arg(name());

    // Step 3: For each list ...
    QList<QList<BenchmarkStats2  *> *> bmStatsLists = bmStatsMap.values();
    for (int i = 0; i < bmStatsLists.size(); ++i) {
        QList<BenchmarkStats2 *> * bmStatsList = bmStatsLists.at(i);

        // Sort according to the ranking.
        qSort(bmStatsList->begin(), bmStatsList->end(), BenchmarkStats2::LessThan(ranking));

        // Output at most the first maxSize items ...
        const int maxSize_ = (maxSize < 0) ? bmStatsList->size() : maxSize;
        for (int j = 0; j < qMin(maxSize_, bmStatsList->size()); ++j) {
            reply.append(
                QString("<benchmark testCase=\"%1\" testFunction=\"%2\" dataTag=\"%3\">")
                .arg(bmStatsList->at(j)->testCase)
                .arg(bmStatsList->at(j)->testFunction)
                .arg(bmStatsList->at(j)->dataTag));

            reply.append(
                QString("<lastValue1 timestamp=\"%1\" sha1=\"%2\" value=\"%3\" "
                        " stable=\"%4\" />")
                .arg(bmStatsList->at(j)->timestamp_lastValue1)
                .arg(bmStatsList->at(j)->sha1_lastValue1)
                .arg(bmStatsList->at(j)->value_lastValue1)
                .arg(bmStatsList->at(j)->stable_lastValue1 ? "true" : "false"));

            reply.append(
                QString("<lastValue2 timestamp=\"%1\" sha1=\"%2\" value=\"%3\" "
                        " stable=\"%4\" />")
                .arg(bmStatsList->at(j)->timestamp_lastValue2)
                .arg(bmStatsList->at(j)->sha1_lastValue2)
                .arg(bmStatsList->at(j)->value_lastValue2)
                .arg(bmStatsList->at(j)->stable_lastValue2 ? "true" : "false"));

            reply.append("</benchmark>");
        }

        // Clean up dynamic memory ...
        for (int j = 0; j < bmStatsList->size(); ++j)
            delete bmStatsList->at(j);
        delete bmStatsList;
    }

    reply.append("</reply>");

    return xmlConvert(reply);
}

void BMRequest_GetRankedBenchmarks2::handleReply_Raw(const QStringList &args) const
{
    printf("NOTE: raw output not supported yet; printing JSON output for now:\n\n");
    handleReply_JSON(args);
}

void BMRequest_GetRankedBenchmarks2::handleReply_JSON(const QStringList &args) const
{
    Q_UNUSED(args);

    const QString error =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();

    QString reply;

    if (error.isEmpty()) {

        reply = QString("{ \"benchmarks\": [");

        QDomNodeList benchmarkNodes = doc.elementsByTagName("benchmark");
        for (int i = 0; i < benchmarkNodes.size(); ++i) {
            reply.append(QString("%1{").arg((i == 0) ? "" : ", "));

            QDomElement benchmarkElem = benchmarkNodes.at(i).toElement();

            // Name ...
            reply.append(
                QString("\"name\": [\"%1\", \"%2\", \"%3\"]")
                .arg(benchmarkElem.attributeNode("testCase").value())
                .arg(benchmarkElem.attributeNode("testFunction").value())
                .arg(benchmarkElem.attributeNode("dataTag").value()));

            // Last value 1 ...
            QDomNodeList lastValue1Nodes = benchmarkElem.elementsByTagName("lastValue1");
            if (lastValue1Nodes.size() == 1) {
                QDomElement lastValue1Elem = lastValue1Nodes.at(0).toElement();
                reply.append(
                    QString(", \"lastValue1\": [%1, \"%2\", %3, %4]")
                    .arg(lastValue1Elem.attributeNode("timestamp").value())
                    .arg(lastValue1Elem.attributeNode("sha1").value())
                    .arg(lastValue1Elem.attributeNode("value").value())
                    .arg(lastValue1Elem.attributeNode("stable").value()));
            }

            // Last value 2 ...
            QDomNodeList lastValue2Nodes = benchmarkElem.elementsByTagName("lastValue2");
            if (lastValue2Nodes.size() == 1) {
                QDomElement lastValue2Elem = lastValue2Nodes.at(0).toElement();
                reply.append(
                    QString(", \"lastValue2\": [%1, \"%2\", %3, %4]")
                    .arg(lastValue2Elem.attributeNode("timestamp").value())
                    .arg(lastValue2Elem.attributeNode("sha1").value())
                    .arg(lastValue2Elem.attributeNode("value").value())
                    .arg(lastValue2Elem.attributeNode("stable").value()));
            }

            reply.append("}");
        }

        reply.append("]}");

    } else {
        reply = QString("{\"error\": \"error getting ranked benchmarks: %1\"}").arg(error);
    }

    BMMisc::printJSONOutput(reply);
}

// --- GetStats ---
QByteArray BMRequest_GetStats::toRequestBuffer(QString *)
{
    const QString request = QString(
        "<request type=\"%1\"><context "
        "metric=\"%2\" platform=\"%3\" host=\"%4\" gitRepo=\"%5\" gitBranch=\"%6\" "
        "timestamp1=\"%7\" timestamp2=\"%8\" "
        "diffTolerance=\"%9\" stabTolerance=\"%10\" testCaseFilter=\"%11\" "
        "testFunctionFilter=\"%12\" dataTagFilter=\"%13\" /></request>")
        .arg(name()).arg(metric).arg(platform).arg(host).arg(gitRepo).arg(gitBranch)
        .arg(timestamp1).arg(timestamp2).arg(diffTolerance).arg(stabTolerance)
        .arg(testCaseFilter).arg(testFunctionFilter).arg(dataTagFilter);
    return xmlConvert(request);
}

QByteArray BMRequest_GetStats::toReplyBuffer()
{
    QDomElement contextElem = doc.elementsByTagName("context").at(0).toElement();
    metric = contextElem.attributeNode("metric").value();
    platform = contextElem.attributeNode("platform").value();
    host = contextElem.attributeNode("host").value();
    gitRepo = contextElem.attributeNode("gitRepo").value();
    gitBranch = contextElem.attributeNode("gitBranch").value();
    timestamp1 = contextElem.attributeNode("timestamp1").value();
    timestamp2 = contextElem.attributeNode("timestamp2").value();
    bool ok;
    diffTolerance = contextElem.attributeNode("diffTolerance").value().toDouble(&ok);
    Q_ASSERT(ok);
    stabTolerance = contextElem.attributeNode("stabTolerance").value().toInt(&ok);
    Q_ASSERT(ok);
    testCaseFilter = contextElem.attributeNode("testCaseFilter").value();
    testFunctionFilter = contextElem.attributeNode("testFunctionFilter").value();
    dataTagFilter = contextElem.attributeNode("dataTagFilter").value();

    QString reply;

    // --- Step 1: Get all benchmark contexts matching the context and the benchmark filters ---
    int contextId;
    if (!getContextId(&reply, &contextId, metric, platform, host, gitRepo, gitBranch))
        return xmlConvert(reply);

    QList<int> bmcontextIds;

    const QString bmarkFilterExpr =
        createBenchmarkFilterExpression(testCaseFilter, testFunctionFilter, dataTagFilter, false);

    QSqlQuery *query = createQuery();

    if (bmarkFilterExpr.isEmpty()) {
        // Filter disabled, so get all benchmarks matching context ...

        if (!query->exec(
                QString(
                    "SELECT id"
                    "  FROM bmcontext"
                    " WHERE contextId=%1;")
                .arg(contextId))) {
            reply =
                errorReply(*query, name(), "failed to get unfiltered benchmarks (exec() failed)");
            deleteQuery(query);
            return xmlConvert(reply);
        }

        while (query->next())
            bmcontextIds.append(query->value(0).toInt());

    } else {

        // Get filtered benchmarks matching context ...

        // ... first get all benchmarks matching the filter ...
        if (!query->exec(
                QString(
                    "SELECT id"
                    "  FROM benchmark"
                    " WHERE %2;")
                    .arg(bmarkFilterExpr))) {
            reply = errorReply(*query, name(), "failed to filter benchmarks (exec() failed)");
            deleteQuery(query);
            return xmlConvert(reply);
        }

        QList<int> benchmarkIds;
        while (query->next())
            benchmarkIds.append(query->value(0).toInt());


        // ... then get all bmcontexts matching a filtered benchmark and the context ...
        for (int i = 0; i < benchmarkIds.size(); ++i) {
            if (!query->exec(
                    QString(
                        "SELECT id"
                        "  FROM bmcontext"
                        " WHERE contextId=%1"
                        "   AND benchmarkId=%2;")
                    .arg(contextId)
                    .arg(benchmarkIds.at(i)))) {
                reply =
                    errorReply(
                        *query, name(),
                        "failed to get filtered benchmark matching context (exec() failed)");
                deleteQuery(query);
                return xmlConvert(reply);
            }

            while (query->next())
                bmcontextIds.append(query->value(0).toInt());
        }
    }

    deleteQuery(query);


    // --- Step 2: Compute statistics ---

    QMap<QString, QList<BenchmarkStats *> *> bmStatsMap;

    int nLastDiffs = 0;
    int nStableLastDiffs = 0;
    qreal minStableLastDiff = 0;
    qreal sumStableLastDiff = 0;
    qreal maxStableLastDiff = 0;

    for (int i = 0; i < bmcontextIds.size(); ++i) {

        // Get the results in the specified snapshot range.
        query = createQuery();
        if (!query->exec(createHistoryQuery(bmcontextIds.at(i), timestamp1, timestamp2))) {
            reply = errorReply(*query, name(), "failed to get history (exec() failed)");
            deleteQuery(query);
            return xmlConvert(reply);
        }
        QList<ResultInfo> results;
        while (query->next()) {
            results.prepend(
                ResultInfo(
                    query->value(0).toInt(),
                    query->value(1).toString(),
                    query->value(2).toDouble()));
        }
        deleteQuery(query);

        // Compute statistics for the results.
        int lastDiffIndex = -1;
        bool lastDiffStable = false;
        if (results.size() > 1) {
            computeStatistics(
                results, diffTolerance, stabTolerance, &lastDiffIndex, &lastDiffStable);
        }

        // Accumulate overall statistics.
        if (lastDiffIndex > -1) {
            nLastDiffs++;

            if (lastDiffStable) {
                nStableLastDiffs++;

                const qreal lastDiff =
                    BMMisc::normalizedDifference(
                        results.last().value, results.at(lastDiffIndex).value);
                if (nStableLastDiffs == 1) {
                    minStableLastDiff = maxStableLastDiff = lastDiff;
                } else {
                    minStableLastDiff = qMin(minStableLastDiff, lastDiff);
                    maxStableLastDiff = qMax(maxStableLastDiff, lastDiff);
                }
                sumStableLastDiff += lastDiff;
            }
        }
    }

    reply = QString(
        "<reply type=\"%1\">"
        "<stats "
        "nBenchmarks=\"%2\" "
        "nLastDiffs=\"%3\" "
        "nStableLastDiffs=\"%4\" "
        "minStableLastDiff=\"%5\" "
        "avgStableLastDiff=\"%6\" "
        "maxStableLastDiff=\"%7\" "
        "/></reply>"
        )
        .arg(name())
        .arg(bmcontextIds.size())
        .arg(nLastDiffs)
        .arg(nStableLastDiffs)
        .arg((nStableLastDiffs > 0) ? minStableLastDiff : 0)
        .arg((nStableLastDiffs > 0) ? (sumStableLastDiff / nStableLastDiffs) : 0)
        .arg((nStableLastDiffs > 0) ? maxStableLastDiff : 0);

    return xmlConvert(reply);
}

void BMRequest_GetStats::handleReply_Raw(const QStringList &args) const
{
    printf("NOTE: raw output not supported yet; printing JSON output for now:\n\n");
    handleReply_JSON(args);
}

void BMRequest_GetStats::handleReply_JSON(const QStringList &args) const
{
    Q_UNUSED(args);

    const QString error =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();

    QString reply;

    if (error.isEmpty()) {

        QDomNodeList statsNodes = doc.elementsByTagName("stats");
        Q_ASSERT(statsNodes.size() == 1);
        QDomElement statsElem = statsNodes.at(0).toElement();

        reply = QString(
            "{ \"nBenchmarks\": %1, "
            "\"nLastDiffs\": %2, "
            "\"nStableLastDiffs\": %3, "
            "\"minStableLastDiff\": %4, "
            "\"avgStableLastDiff\": %5, "
            "\"maxStableLastDiff\": %6 }")
            .arg(statsElem.attributeNode("nBenchmarks").value())
            .arg(statsElem.attributeNode("nLastDiffs").value())
            .arg(statsElem.attributeNode("nStableLastDiffs").value())
            .arg(statsElem.attributeNode("minStableLastDiff").value())
            .arg(statsElem.attributeNode("avgStableLastDiff").value())
            .arg(statsElem.attributeNode("maxStableLastDiff").value());

    } else {
        reply = QString("{\"error\": \"error getting statistics for benchmarks: %1\"}").arg(error);
    }

    BMMisc::printJSONOutput(reply);
}

// --- GetContexts ---
QByteArray BMRequest_GetContexts::toRequestBuffer(QString *)
{
    const QString request = QString("<request type=\"%1\" />").arg(name());
    return xmlConvert(request);
}

QByteArray BMRequest_GetContexts::toReplyBuffer()
{
    QString reply = QString("<reply type=\"%1\">").arg(name());
    QSqlQuery *query;
    int index;

    // Get all metrics ...
    query = createQuery();
    if (!query->exec("SELECT id, name FROM metric;")) {
        reply = errorReply(*query, name(), QString("(exec() failed)"));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    QMap<int, int> metricId2Index;
    index = 0;
    while (query->next()) {
        reply += QString("<metric name=\"%1\" />").arg(query->value(1).toString());
        metricId2Index.insert(query->value(0).toInt(), index++);
    }
    deleteQuery(query);

    // Get all platforms ...
    query = createQuery();
    if (!query->exec("SELECT id, name FROM platform;")) {
        reply = errorReply(*query, name(), QString("(exec() failed)"));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    QMap<int, int> platformId2Index;
    index = 0;
    while (query->next()) {
        reply += QString("<platform name=\"%1\" />").arg(query->value(1).toString());
        platformId2Index.insert(query->value(0).toInt(), index++);
    }
    deleteQuery(query);

    // Get all hosts ...
    query = createQuery();
    if (!query->exec("SELECT id, name FROM host;")) {
        reply = errorReply(*query, name(), QString("(exec() failed)"));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    QMap<int, int> hostId2Index;
    index = 0;
    while (query->next()) {
        reply += QString("<host name=\"%1\" />").arg(query->value(1).toString());
        hostId2Index.insert(query->value(0).toInt(), index++);
    }
    deleteQuery(query);

    // Get all branches ...
    query = createQuery();
    if (!query->exec("SELECT id, gitRepo, gitBranch FROM branch;")) {
        reply = errorReply(*query, name(), QString("(exec() failed)"));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    QMap<int, int> branchId2Index;
    index = 0;
    while (query->next()) {
        reply += QString("<branch gitRepo=\"%1\" gitBranch=\"%2\" />")
            .arg(query->value(1).toString())
            .arg(query->value(2).toString());
        branchId2Index.insert(query->value(0).toInt(), index++);
    }
    deleteQuery(query);

    // Get all contexts ...
    query = createQuery();
    if (!query->exec("SELECT metricId, platformId, hostId, branchId FROM context;")) {
        reply = errorReply(*query, name(), QString("(exec() failed)"));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    while (query->next())
        reply += QString(
            "<context metric=\"%1\" platform=\"%2\" host=\"%3\" branch=\"%4\" />")
            .arg(  metricId2Index.value(query->value(0).toInt()))
            .arg(platformId2Index.value(query->value(1).toInt()))
            .arg(    hostId2Index.value(query->value(2).toInt()))
            .arg(  branchId2Index.value(query->value(3).toInt()));
    deleteQuery(query);

    reply += "</reply>";

    return xmlConvert(reply);
}

void BMRequest_GetContexts::handleReply_Raw(const QStringList &args) const
{
    printf("NOTE: raw output not supported yet; printing JSON output for now:\n\n");
    handleReply_JSON(args);
}

void BMRequest_GetContexts::handleReply_JSON(const QStringList &args) const
{
    Q_UNUSED(args);

    const QString error =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();

    QString reply = QString("{");

    if (error.isEmpty()) {

        reply += "\n\"values\": [";

        // Metrics
        QDomNodeList metricNodes = doc.elementsByTagName("metric");
        reply += "\n[";
        for (int i = 0; i < metricNodes.size(); ++i) {
            QDomElement metricElem = metricNodes.at(i).toElement();
            reply += QString("%1[\"%2\"]")
                .arg((i == 0) ? "" : ", ")
                .arg(metricElem.attributeNode("name").value());
        }
        reply += "],";

        // Platforms
        QDomNodeList platformNodes = doc.elementsByTagName("platform");
        reply += "\n[";
        for (int i = 0; i < platformNodes.size(); ++i) {
            QDomElement platformElem = platformNodes.at(i).toElement();
            reply += QString("%1[\"%2\"]")
                .arg((i == 0) ? "" : ", ")
                .arg(platformElem.attributeNode("name").value());
        }
        reply += "],";

        // Hosts
        QDomNodeList hostNodes = doc.elementsByTagName("host");
        reply += "\n[";
        for (int i = 0; i < hostNodes.size(); ++i) {
            QDomElement hostElem = hostNodes.at(i).toElement();
            reply += QString("%1[\"%2\"]")
                .arg((i == 0) ? "" : ", ")
                .arg(hostElem.attributeNode("name").value());
        }
        reply += "],";

        // Branches
        QDomNodeList branchNodes = doc.elementsByTagName("branch");
        reply += "\n[";
        for (int i = 0; i < branchNodes.size(); ++i) {
            QDomElement branchElem = branchNodes.at(i).toElement();
            reply += QString("%1[\"%2\", \"%3\"]")
                .arg((i == 0) ? "" : ", ")
                .arg(branchElem.attributeNode("gitRepo").value())
                .arg(branchElem.attributeNode("gitBranch").value());
        }
        reply += "]\n],";

        // Contexts
        QDomNodeList contextNodes = doc.elementsByTagName("context");
        reply += "\n\"contexts\": [";
        for (int i = 0; i < contextNodes.size(); ++i) {
            QDomElement contextElem = contextNodes.at(i).toElement();
            reply += QString("%1[%2, %3, %4, %5]")
                .arg((i == 0) ? "" : ", ")
                .arg(contextElem.attributeNode("metric").value())
                .arg(contextElem.attributeNode("platform").value())
                .arg(contextElem.attributeNode("host").value())
                .arg(contextElem.attributeNode("branch").value());
        }
        reply += "]";

        reply += "\n}";

    } else {
        reply = QString("{\"error\": \"error getting contexts: %1\"}").arg(error);
    }

    BMMisc::printJSONOutput(reply);
}


// --- GetTCContexts ---
QByteArray BMRequest_GetTCContexts::toRequestBuffer(QString *)
{
    const QString request = QString("<request type=\"%1\" />").arg(name());
    return xmlConvert(request);
}

QByteArray BMRequest_GetTCContexts::toReplyBuffer()
{
    QString reply = QString("<reply type=\"%1\">").arg(name());
    QSqlQuery *query;
    int index;

    // Get all test cases ...
    query = createQuery();
    if (!query->exec("SELECT DISTINCT testCase FROM benchmark ORDER BY testCase ASC;")) {
        reply = errorReply(*query, name(), QString("(exec() failed)"));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    QMap<QString, int> testCase2Index;
    index = 0;
    while (query->next()) {
        reply += QString("<testCase name=\"%1\" />").arg(query->value(0).toString());
        testCase2Index.insert(query->value(0).toString(), index++);
    }
    deleteQuery(query);

    // Get all metrics ...
    // (NOTE: Results measuring the "events" metric are skipped for now since
    // they seem to be of little value when computing the performance index (they are
    // typically integers close to (and often at) zero))
    query = createQuery();
    if (!query->exec("SELECT id, name FROM metric WHERE name != 'events' ORDER BY name ASC;")) {
        reply = errorReply(*query, name(), QString("(exec() failed)"));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    QMap<int, int> metricId2Index;
    index = 0;
    while (query->next()) {
        reply += QString("<metric name=\"%1\" />").arg(query->value(1).toString());
        metricId2Index.insert(query->value(0).toInt(), index++);
    }
    deleteQuery(query);

    // Get all platforms ...
    query = createQuery();
    if (!query->exec("SELECT id, name FROM platform ORDER BY name ASC;")) {
        reply = errorReply(*query, name(), QString("(exec() failed)"));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    QMap<int, int> platformId2Index;
    index = 0;
    while (query->next()) {
        reply += QString("<platform name=\"%1\" />").arg(query->value(1).toString());
        platformId2Index.insert(query->value(0).toInt(), index++);
    }
    deleteQuery(query);

    // Get all hosts ...
    query = createQuery();
    if (!query->exec("SELECT id, name FROM host ORDER BY name ASC;")) {
        reply = errorReply(*query, name(), QString("(exec() failed)"));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    QMap<int, int> hostId2Index;
    index = 0;
    while (query->next()) {
        reply += QString("<host name=\"%1\" />").arg(query->value(1).toString());
        hostId2Index.insert(query->value(0).toInt(), index++);
    }
    deleteQuery(query);

    // Get all branches ...
    query = createQuery();
    if (!query->exec(
            "SELECT id, gitRepo, gitBranch FROM branch ORDER BY gitRepo, gitBranch ASC;")) {
        reply = errorReply(*query, name(), QString("(exec() failed)"));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    QMap<int, int> branchId2Index;
    index = 0;
    while (query->next()) {
        reply += QString("<branch gitRepo=\"%1\" gitBranch=\"%2\" />")
            .arg(query->value(1).toString())
            .arg(query->value(2).toString());
        branchId2Index.insert(query->value(0).toInt(), index++);
    }
    deleteQuery(query);

    // Get all test case / context combinations ...
    query = createQuery();
    if (!query->exec(
            "SELECT DISTINCT testCase, metricId, platformId, hostId, branchId "
            "  FROM bmcontext, benchmark, context "
            " WHERE benchmark.id = benchmarkid "
            "   AND context.id = contextid;")) {
        reply = errorReply(*query, name(), QString("(exec() failed)"));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    while (query->next())
        reply += QString(
            "<tccontext testCase=\"%1\" metric=\"%2\" platform=\"%3\" host=\"%4\" branch=\"%5\" />")
            .arg(  testCase2Index.value(query->value(0).toString()))
            .arg(  metricId2Index.value(query->value(1).toInt()))
            .arg(platformId2Index.value(query->value(2).toInt()))
            .arg(    hostId2Index.value(query->value(3).toInt()))
            .arg(  branchId2Index.value(query->value(4).toInt()));
    deleteQuery(query);

    reply += "</reply>";

    return xmlConvert(reply);
}

void BMRequest_GetTCContexts::handleReply_Raw(const QStringList &args) const
{
    printf("NOTE: raw output not supported yet; printing JSON output for now:\n\n");
    handleReply_JSON(args);
}

void BMRequest_GetTCContexts::handleReply_JSON(const QStringList &args) const
{
    Q_UNUSED(args);

    const QString error =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();

    QString reply = QString("{");

    if (error.isEmpty()) {

        reply += "\n\"values\": [";

        // Test cases
        QDomNodeList testCaseNodes = doc.elementsByTagName("testCase");
        reply += "\n[";
        for (int i = 0; i < testCaseNodes.size(); ++i) {
            QDomElement testCaseElem = testCaseNodes.at(i).toElement();
            reply += QString("%1[\"%2\"]")
                .arg((i == 0) ? "" : ", ")
                .arg(testCaseElem.attributeNode("name").value());
        }
        reply += "],";

        // Metrics
        QDomNodeList metricNodes = doc.elementsByTagName("metric");
        reply += "\n[";
        for (int i = 0; i < metricNodes.size(); ++i) {
            QDomElement metricElem = metricNodes.at(i).toElement();
            reply += QString("%1[\"%2\"]")
                .arg((i == 0) ? "" : ", ")
                .arg(metricElem.attributeNode("name").value());
        }
        reply += "],";

        // Platforms
        QDomNodeList platformNodes = doc.elementsByTagName("platform");
        reply += "\n[";
        for (int i = 0; i < platformNodes.size(); ++i) {
            QDomElement platformElem = platformNodes.at(i).toElement();
            reply += QString("%1[\"%2\"]")
                .arg((i == 0) ? "" : ", ")
                .arg(platformElem.attributeNode("name").value());
        }
        reply += "],";

        // Hosts
        QDomNodeList hostNodes = doc.elementsByTagName("host");
        reply += "\n[";
        for (int i = 0; i < hostNodes.size(); ++i) {
            QDomElement hostElem = hostNodes.at(i).toElement();
            reply += QString("%1[\"%2\"]")
                .arg((i == 0) ? "" : ", ")
                .arg(hostElem.attributeNode("name").value());
        }
        reply += "],";

        // Branches
        QDomNodeList branchNodes = doc.elementsByTagName("branch");
        reply += "\n[";
        for (int i = 0; i < branchNodes.size(); ++i) {
            QDomElement branchElem = branchNodes.at(i).toElement();
            reply += QString("%1[\"%2\", \"%3\"]")
                .arg((i == 0) ? "" : ", ")
                .arg(branchElem.attributeNode("gitRepo").value())
                .arg(branchElem.attributeNode("gitBranch").value());
        }
        reply += "]\n],";

        // Test case / context combinations ...
        QDomNodeList tccontextNodes = doc.elementsByTagName("tccontext");
        reply += "\n\"tccontexts\": [";
        for (int i = 0; i < tccontextNodes.size(); ++i) {
            QDomElement tccontextElem = tccontextNodes.at(i).toElement();
            reply += QString("%1[%2, %3, %4, %5, %6]")
                .arg((i == 0) ? "" : ", ")
                .arg(tccontextElem.attributeNode("testCase").value())
                .arg(tccontextElem.attributeNode("metric").value())
                .arg(tccontextElem.attributeNode("platform").value())
                .arg(tccontextElem.attributeNode("host").value())
                .arg(tccontextElem.attributeNode("branch").value());
        }
        reply += "]";

        reply += "\n}";

    } else {
        reply = QString("{\"error\": \"error getting test cases + contexts: %1\"}").arg(error);
    }

    BMMisc::printJSONOutput(reply);
}


// --- GetResult ---
QByteArray BMRequest_GetResult::toRequestBuffer(QString *)
{
    const QString request =
        QString("<request type=\"%1\"><args resultId=\"%2\" /></request>")
        .arg(name()).arg(resultId);
    return xmlConvert(request);
}

QByteArray BMRequest_GetResult::toReplyBuffer()
{
    // Get result ID ...
    QDomElement argsElem = doc.elementsByTagName("args").at(0).toElement();
    bool ok;
    resultId = argsElem.attributeNode("resultId").value().toInt(&ok);
    Q_ASSERT(ok);

    QString reply;
    QSqlQuery *query;

    // Get BM context ID ...
    query = createQuery();
    if (!query->exec(
            QString("SELECT bmcontextId FROM result WHERE id=%1;").arg(resultId))) {
        reply = errorReply(*query, name(), QString("failed to get result (exec() failed (1))"));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    int bmcontextId;
    if (query->next()) {
        bmcontextId = query->value(0).toInt();
    } else {
        reply = errorReply(*query, name(), QString("no such result: %1").arg(resultId));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    deleteQuery(query);


    QString error;

    QString testCase;
    QString testFunction;
    QString dataTag;
    QString metric;
    QString platform;
    QString host;
    QString gitRepo;
    QString gitBranch;
    if (!extractBMAndContextFromRH(
            bmcontextId, name(), database, &testCase, &testFunction, &dataTag, &metric, &platform,
            &host, &gitRepo, &gitBranch, &error)) {
        return xmlConvert(error);
    }


    // Get values ...
    query = createQuery();
    if (!query->exec(
            QString(
                "SELECT DISTINCT result.id, value, timestamp, sha1 FROM result, snapshot "
                "WHERE  snapshotId=snapshot.id "
                "   AND bmcontextId=%1 ORDER BY timestamp ASC;")
            .arg(bmcontextId))) {
        reply = errorReply(*query, name(), QString("failed to get result (exec() failed (10))"));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    QList<int> resultIds;
    QList<qreal> values;
    QList<int> timestamps;
    QStringList sha1s;
    while (query->next()) {
        resultIds.append(query->value(0).toInt());
        values.append(query->value(1).toDouble());
        timestamps.append(query->value(2).toInt());
        sha1s.append(query->value(3).toString());
    }

    const int basePos = resultIds.indexOf(resultId);
    Q_ASSERT(basePos >= 0);

    // Add to reply ...
    reply = QString(
        "<reply type=\"%1\"><descr testCase=\"%2\" testFunction=\"%3\" dataTag=\"%4\" "
        "metric=\"%5\" platform=\"%6\" host=\"%7\" gitRepo=\"%8\" gitBranch=\"%9\" "
        "basePos=\"%10\" />")
        .arg(name())
        .arg(testCase)
        .arg(testFunction)
        .arg(dataTag)
        .arg(metric)
        .arg(platform)
        .arg(host)
        .arg(gitRepo)
        .arg(gitBranch)
        .arg(basePos);

    for (int i = 0; i < timestamps.size(); ++i)
        reply += QString("<value timestamp=\"%1\" sha1=\"%2\" value=\"%3\" />")
            .arg(timestamps.at(i)).arg(sha1s.at(i)).arg(values.at(i));

    reply += "</reply>";

    return xmlConvert(reply);
}

void BMRequest_GetResult::handleReply_Raw(const QStringList &args) const
{
    const QString error =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();

    if (error.isEmpty()) {

        QList<int> timestamps;
        QStringList sha1s;
        QList<qreal> values;
        qreal maxNeighborRatio = -1;
        int maxNeighborRatioPos = -1;

        QDomNodeList valueNodes = doc.elementsByTagName("value");
        for (int i = 0; i < valueNodes.size(); ++i) {
            QDomElement valueElem = valueNodes.at(i).toElement();
            timestamps.append(valueElem.attributeNode("timestamp").value().toInt());
            sha1s.append(valueElem.attributeNode("sha1").value());
            const qreal value = valueElem.attributeNode("value").value().toDouble();
            values.append(value);

            const qreal prevValue = (i == 0) ? -1 : values.at(i - 1);
            const qreal neighbourDiff = qMax(value, prevValue) / qMin(value, prevValue);
            if ((i == 1) || ((i > 1) && (neighbourDiff > maxNeighborRatio))) {
                maxNeighborRatio = neighbourDiff;
                maxNeighborRatioPos = i;
            }
        }

        QDomNodeList descrNodes = doc.elementsByTagName("descr");
        Q_ASSERT(descrNodes.size() == 1);
        QDomElement descrElem = descrNodes.at(0).toElement();
        const int basePos = descrElem.attributeNode("basePos").value().toInt();
        Q_ASSERT(basePos >= 0);
        Q_ASSERT(basePos < timestamps.size());
        printf(
            "    Test Case: %s\n"
            "Test Function: %s\n"
            "     Data Tag: %s\n"
            "       Metric: %s\n"
            "     Platform: %s\n"
            "         Host: %s\n"
            "     Git Repo: %s\n"
            "   Git Branch: %s\n"
            "    Timestamp: %d\n"
            "        SHA-1: %s\n"
            "        Value: %g\n",
            descrElem.attributeNode("testCase")    .value().toLatin1().data(),
            descrElem.attributeNode("testFunction").value().toLatin1().data(),
            descrElem.attributeNode("dataTag")     .value().toLatin1().data(),
            descrElem.attributeNode("metric")      .value().toLatin1().data(),
            descrElem.attributeNode("platform")    .value().toLatin1().data(),
            descrElem.attributeNode("host")        .value().toLatin1().data(),
            descrElem.attributeNode("gitRepo")     .value().toLatin1().data(),
            descrElem.attributeNode("gitBranch")   .value().toLatin1().data(),
            timestamps.at(basePos),
            sha1s.at(basePos).toLatin1().data(),
            values.at(basePos));

        QString error_;
        QStringList optValues;

        // Dump values if requested ...
        if (BMMisc::getOption(args, "-verbose", &optValues, 0, 0, &error_)) {

            printf(
                "\n%-10s %-13s %s\n", "Timestamp", "Value", "(MNR indicates max neighbor ratio)");
            for (int i = 0; i < timestamps.size(); ++i) {
                printf(
                    "%10d %13g%s%s%s\n", timestamps.at(i), values.at(i),
                    (i == basePos) ? " base" : "",
                    (i == (maxNeighborRatioPos - 1)) ? " MNR" : "",
                    (i == maxNeighborRatioPos)
                    ? QString(" MNR: %1").arg(maxNeighborRatio).toLatin1().data()
                    : "");
            }

        } else if (!error_.isEmpty()) {
            qDebug() << "error getting -verbose option:" << error_ << "(programming error?)";
            return;
        }


        // Create plot if requested ...
        if (BMMisc::getOption(args, "-plot", &optValues, 1, 0, &error_)) {
            const QString plotFile = optValues.first().trimmed();
            if (!plotFile.isEmpty()) {

                if (BMMisc::getOption(args, "-id", &optValues, 1, 0, &error_)) {
                    bool ok;
                    resultId = optValues.first().toInt(&ok);
                    if (!ok) {
                        qDebug() << "result ID not an integer:" << optValues.first() <<
                            "(programming error?)";
                        return;
                    }
                } else if (!error_.isEmpty()) {
                    qDebug() << "error getting result ID:" << error_;
                    return;
                }

                Plotter *plotter = new ResultHistoryPlotter(timestamps, values, resultId, basePos);
                const QImage image = plotter->createImage(&error_);
                if (!image.isNull()) {
                    if (!BMMisc::saveImageToFile(image, plotFile, &error_))
                        qDebug() << "failed to save image to file:" << error_;
                } else {
                    qDebug() << "failed to create result history plot:" << error_;
                }
            }
        } else if (!error_.isEmpty()) {
            qDebug() << "error getting plot file:" << error_ << "(programming error?)";
            return;
        }

    } else {
        qDebug() << "error getting result:" << error.toLatin1().data();
    }
}

// ### 2 B DOCUMENTED!
static bool createFilteredRHInfos(
    QList<ResultHistoryInfo *> *rhInfos, int *totCandidates, const QString &reqName,
    QSqlDatabase *database, const QStringList &testCaseFilter, const QStringList &metricFilter,
    const QStringList &platformFilter, const QStringList &hostFilter,
    const QStringList &branchFilter, const int medianWinSize, QString *error,
    const bool debugPrint = false)
{
    QSqlQuery query(*database);

    // Get the initial list of candidate result histories by getting the BM context ID and metric
    // for all result histories that match the logical OR-values.
    // NOTE: Results measuring the "events" metric are skipped for now since
    // they seem to be of little value when computing the performance index (they are
    // typically integers close to (and often at) zero) ...

    if (debugPrint)
        fprintf(stderr, "fetching list of relevant result histories ... ");

    QString query_s =
        "SELECT bmcontext.id, metric.name"
        "  FROM bmcontext, benchmark, context, metric, platform, host, branch"
        " WHERE benchmarkId = benchmark.id"
        "   AND contextId = context.id"
        "   AND metricId = metric.id"
        "   AND platformId = platform.id"
        "   AND hostId = host.id"
        "   AND branchId = branch.id"
        "   AND metric.name != 'events'";

    if (!testCaseFilter.isEmpty()) {
        query_s += " AND (";
        for (int i = 0; i < testCaseFilter.size(); ++i)
            query_s += QString("%1benchmark.testCase = '%2'")
                .arg((i > 0) ? " OR " : "")
                .arg(testCaseFilter.at(i));
        query_s += ")";
    }

    if (!metricFilter.isEmpty()) {
        query_s += " AND (";
        for (int i = 0; i < metricFilter.size(); ++i)
            query_s +=
                QString("%1metric.name = '%2'").arg((i > 0) ? " OR " : "").arg(metricFilter.at(i));
        query_s += ")";
    }

    if (!platformFilter.isEmpty()) {
        query_s += " AND (";
        for (int i = 0; i < platformFilter.size(); ++i)
            query_s += QString("%1platform.name = '%2'")
                .arg((i > 0) ? " OR " : "")
                .arg(platformFilter.at(i));
        query_s += ")";
    }

    if (!hostFilter.isEmpty()) {
        query_s += " AND (";
        for (int i = 0; i < hostFilter.size(); ++i)
            query_s += QString("%1host.name = '%2'")
                .arg((i > 0) ? " OR " : "")
                .arg(hostFilter.at(i));
        query_s += ")";
    }

    if (!branchFilter.isEmpty()) {
        query_s += " AND (";
        for (int i = 0; i < branchFilter.size(); ++i)
            query_s += QString("%1(branch.gitRepo || branch.gitBranch) = '%2'")
                .arg((i > 0) ? " OR " : "")
                .arg(branchFilter.at(i));
        query_s += ")";
    }

    query_s += ";";

    if (!query.exec(query_s)) {
        *error = errorReply(query, reqName, "failed to get bmcontext IDs (exec() failed)");
        return false;
    }
    QList<int> bmcontextIds;
    QStringList metrics;
    while (query.next()) {
        bmcontextIds += query.value(0).toInt();
        metrics += query.value(1).toString();
    }
    if (debugPrint)
        fprintf(stderr, "done\n");

    if (bmcontextIds.isEmpty()) {
        *error = errorReply(reqName, "no matching result histories found");
        return false;
    }


    // Loop over initial result history candidates ...
    for (int i = 0; i < bmcontextIds.size(); ++i) {

        if (!(i % qMax(1, (bmcontextIds.size() / 20)))) {
            if (debugPrint)
                fprintf(
                    stderr, "fetching result histories (%0.0f%% done)\r",
                    100 * (qreal(i) / bmcontextIds.size()));
        }

        // Get the time series ...

        if (!query.exec(
                QString(
                    "SELECT timestamp, value FROM result "
                    "WHERE bmcontextId=%1 "
                    "ORDER BY timestamp ASC;")
                .arg(bmcontextIds.at(i)))) {
            *error = errorReply(query, reqName, "failed to get time series (exec() failed)");
            return false;
        }
        QList<int> timestamps;
        QList<qreal> values;
        while (query.next()) {
            timestamps += query.value(0).toInt();
            values += query.value(1).toDouble();
        }

        ResultHistoryInfo *rhInfo =
            new ResultHistoryInfo(
                bmcontextIds.at(i), timestamps, values, medianWinSize, metrics.at(i));
        rhInfos->append(rhInfo);
    }

    *totCandidates = bmcontextIds.size();

    if (debugPrint)
        fprintf(stderr, "fetching result histories (100%% done)\n");

    return true;
}


// --- IndexGetValues ---
QByteArray BMRequest_IndexGetValues::toRequestBuffer(QString *)
{
    QString request =
        QString(
            "<request type=\"%1\"><args baseTimestamp=\"%2\" medianWinSize=\"%3\" "
            "cacheKey=\"%4\" dataQualityStats=\"%5\" dqStatsDiffTol=\"%6\" "
            "dqStatsStabTol=\"%7\" />")
        .arg(name())
        .arg(baseTimestamp)
        .arg(medianWinSize)
        .arg(cacheKey)
        .arg(dataQualityStats ? 1 : 0)
        .arg(dqStatsDiffTol)
        .arg(dqStatsStabTol);

    for (int i = 0; i < testCaseFilter.size(); ++i)
        request += QString("<testCase name=\"%1\" />").arg(testCaseFilter.at(i));

    for (int i = 0; i < metricFilter.size(); ++i)
        request += QString("<metric name=\"%1\" />").arg(metricFilter.at(i));

    for (int i = 0; i < platformFilter.size(); ++i)
        request += QString("<platform name=\"%1\" />").arg(platformFilter.at(i));

    for (int i = 0; i < hostFilter.size(); ++i)
        request += QString("<host name=\"%1\" />").arg(hostFilter.at(i));

    for (int i = 0; i < branchFilter.size(); ++i)
        request += QString("<branch name=\"%1\" />").arg(branchFilter.at(i));

    request += "</request>";

    return xmlConvert(request);
}

QByteArray BMRequest_IndexGetValues::toReplyBuffer()
{
    QDomElement argsElem = doc.elementsByTagName("args").at(0).toElement();

    QString error;
    QString reply;
    bool ok;

    const bool debugPrint = false;

    // Check if a cached reply can be returned ...
    int cacheKey_ = argsElem.attributeNode("cacheKey").value().toInt(&ok);
    if (debugPrint) {
        static int calls_ = 0;
        fprintf(
            stderr,
            "BMRequest_IndexGetValues::toReplyBuffer() called (%d), cacheKey: >%s<\n",
            calls_++, argsElem.attributeNode("cacheKey").value().toLatin1().data());
    }
    if (ok) {
        reply = Cache::instance()->get(cacheKey_);
        if (!reply.isEmpty()) {
            if (debugPrint)
                fprintf(stderr, "returning cached reply (key = %d)\n", cacheKey_);
            return xmlConvert(reply);
        }
    }

    // Compute from scratch ...
    if (debugPrint)
        fprintf(stderr, "computing from scratch ...\n");

    // Get base timestamp ...
    baseTimestamp = argsElem.attributeNode("baseTimestamp").value().toInt(&ok);
    Q_ASSERT(ok);

    // Get median window size ...
    medianWinSize = argsElem.attributeNode("medianWinSize").value().toInt(&ok);
    Q_ASSERT(ok);

    // Get data quality stats params ...
    dataQualityStats = argsElem.attributeNode("dataQualityStats").value().toInt(&ok);
    Q_ASSERT(ok);
    dqStatsDiffTol = argsElem.attributeNode("dqStatsDiffTol").value().toDouble(&ok);
    Q_ASSERT(ok);
    dqStatsStabTol = argsElem.attributeNode("dqStatsStabTol").value().toInt(&ok);
    Q_ASSERT(ok);

    // Get filters ...
    QDomNodeList testCaseNodes = doc.elementsByTagName("testCase");
    for (int i = 0; i < testCaseNodes.size(); ++i)
        testCaseFilter.append(testCaseNodes.at(i).toElement().attributeNode("name").value());

    QDomNodeList metricNodes = doc.elementsByTagName("metric");
    for (int i = 0; i < metricNodes.size(); ++i)
        metricFilter.append(metricNodes.at(i).toElement().attributeNode("name").value());

    QDomNodeList platformNodes = doc.elementsByTagName("platform");
    for (int i = 0; i < platformNodes.size(); ++i)
        platformFilter.append(platformNodes.at(i).toElement().attributeNode("name").value());

    QDomNodeList hostNodes = doc.elementsByTagName("host");
    for (int i = 0; i < hostNodes.size(); ++i)
        hostFilter.append(hostNodes.at(i).toElement().attributeNode("name").value());

    QDomNodeList branchNodes = doc.elementsByTagName("branch");
    for (int i = 0; i < branchNodes.size(); ++i)
        branchFilter.append(branchNodes.at(i).toElement().attributeNode("name").value());


    QList<ResultHistoryInfo *> rhInfos;
    int totRHCandidates;
    if (!createFilteredRHInfos(
            &rhInfos, &totRHCandidates, name(), database, testCaseFilter, metricFilter,
            platformFilter, hostFilter, branchFilter, medianWinSize, &error, debugPrint)) {

        // Free dynamic memory ...
        for (int i = 0; i < rhInfos.size(); ++i)
            delete rhInfos.at(i);

        return xmlConvert(error);
    }


    reply = QString("<reply type=\"%1\" >").arg(name());

    int nonPositiveRH = 0;
    int baseValuePos;

    if (!rhInfos.isEmpty()) {

        // Candidate result histories exist, so compute index values ...

        QList<int> evalTimestamps;
        IndexAlgorithm1 index(rhInfos, baseTimestamp, &evalTimestamps, &nonPositiveRH);
        if (!index.isValid())
            return xmlConvert(
                errorReply(
                    name(), QString("failed to compute index (1): %1").arg(index.invalidReason())));

        QList<qreal> indexValues;
        QList<int> contrCounts;
        QString error_;
        QList<QList<Index::RankedInfo> > topContr; // Top contributors for each index value
        const int topContrLimit = 10; // ### hard-coded for now!
        DataQualityStats dqStats(dqStatsDiffTol, dqStatsStabTol); // Stats for all contributors

        if (!index.computeValues(
                &indexValues, &baseValuePos, &contrCounts, &error_, &topContr, topContrLimit,
                dataQualityStats ? &dqStats : static_cast<DataQualityStats *>(0)))
            return xmlConvert(
                errorReply(name(), QString("failed to compute index (2): %1").arg(error_)));

        // Add index values to reply ...
        Q_ASSERT(indexValues.size() == evalTimestamps.size());
        Q_ASSERT(indexValues.size() == contrCounts.size());
        for (int i = 0; i < indexValues.size(); ++i) {
            reply +=
                QString("<value timestamp=\"%1\" indexValue=\"%2\" contributions=\"%3\" >")
                .arg(evalTimestamps.at(i))
                .arg(indexValues.at(i))
                .arg(contrCounts.at(i));

            for (int j = 0; j < topContr.at(i).size(); ++j) {
                reply +=
                    QString(
                        "<rankedInfo id=\"%1\" basePos=\"%2\" diffPos1=\"%3\" "
                        "diffPos2=\"%4\" descr=\"%5\" />")
                    .arg(topContr.at(i).at(j).bmcontextId)
                    .arg(topContr.at(i).at(j).basePos)
                    .arg(topContr.at(i).at(j).diffPos1)
                    .arg(topContr.at(i).at(j).diffPos2)
                    .arg(topContr.at(i).at(j).descr);
            }

            reply += "</value>";
        }

        if (dataQualityStats) {
            // Add data quality stats to reply ...

            reply += QString("<dataQualityStats diffTol=\"%1\" stabTol=\"%2\">")
                .arg(dqStatsDiffTol).arg(dqStatsStabTol);

            const QMap<int, int> totalMaxESSFreq_ = dqStats.totalMaxESSFreq();
            {
                QMap<int, int>::const_iterator it;
                for (it = totalMaxESSFreq_.constBegin(); it != totalMaxESSFreq_.constEnd(); ++it)
                    reply += QString("<totMaxESSCountFreq count=\"%1\" freq=\"%2\" />")
                        .arg(it.key()).arg(it.value());
            }

            const QMap<int, QPair<qreal, qreal> > stabFracPercentiles_ =
                dqStats.stabFracPercentiles();
            {
                QMap<int, QPair<qreal, qreal> >::const_iterator it;
                for (it = stabFracPercentiles_.constBegin();
                     it != stabFracPercentiles_.constEnd(); ++it)
                    reply += QString("<stabFracPercentile p=\"%1\" val=\"%2\" val2=\"%3\" />")
                        .arg(it.key())
                        .arg(it.value().first)
                        .arg(it.value().second);
            }

            reply += "</dataQualityStats>";
        }
    }


    //----------------------------------------------------------------------------------------

    for (int i = 0; i < testCaseFilter.size(); ++i)
        reply += QString("<testCase name=\"%1\" />").arg(testCaseFilter.at(i));

    for (int i = 0; i < metricFilter.size(); ++i)
        reply += QString("<metric name=\"%1\" />").arg(metricFilter.at(i));

    for (int i = 0; i < platformFilter.size(); ++i)
        reply += QString("<platform name=\"%1\" />").arg(platformFilter.at(i));

    for (int i = 0; i < hostFilter.size(); ++i)
        reply += QString("<host name=\"%1\" />").arg(hostFilter.at(i));

    for (int i = 0; i < branchFilter.size(); ++i)
        reply += QString("<branch name=\"%1\" />").arg(branchFilter.at(i));

    // Finalize the reply and cache it ...
    cacheKey_ = Cache::instance()->nextId();
    reply += QString(
        "<args baseTimestamp=\"%1\" medianWinSize=\"%2\" totalRH=\"%3\" nonPositiveRH=\"%4\" "
        "baseValuePos=\"%5\" cacheKey=\"%6\" /></reply>")
        .arg(baseTimestamp).arg(medianWinSize).arg(totRHCandidates).arg(nonPositiveRH)
        .arg(baseValuePos).arg(cacheKey_);
    Cache::instance()->put(cacheKey_, reply);

    return xmlConvert(reply);
}

// ### 2 B DOCUMENTED!
static void appendToFilterTable(
    const QStringList &filter, const QString &filterName, QString *reply)
{
    reply->append(
        QString(
            "<tr><td style=\"text-align:right\">%1:</td><td>"
            "<table style=\"border:0px; padding:0px\">\n")
        .arg(filterName));
    if (filter.isEmpty()) {
        reply->append("<tr><td style=\"border:0px; padding:0px; color:green\">any</td></tr>\n");
    } else {
        for (int i = 0; i < filter.size(); ++i)
            reply->append(
                QString("<tr><td style=\"border:0px; padding:0px\">%1</td></tr>\n")
                .arg(filter.at(i)));
    }
    reply->append("</table></td></tr>\n");
}

void BMRequest_IndexGetValues::handleReply_HTML(const QStringList &args) const
{
    QString error;

    error = doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();
    if (!error.isEmpty()) {
        BMMisc::printHTMLErrorPage(
            QString("failed to create details page: error evaluating index: %1").arg(error));
        return;
    }

    QStringList optValues;

    // Get server ...
    QString server;
    if (BMMisc::getOption(args, "-server", &optValues, 1, 0, &error)) {
        server = optValues.first().trimmed();
    } else {
        BMMisc::printHTMLErrorPage("-server option not found");
        return;
    }

    // Get style sheet ...
    QString styleSheet;
    if (BMMisc::getOption(args, "-stylesheet", &optValues, 1, 0, &error)) {
        styleSheet = optValues.first().trimmed();
    } else {
        BMMisc::printHTMLErrorPage("-stylesheet option not found");
        return;
    }

    // *** Header ***
    QString reply = QString("<html>\n<head>\n");
    reply += QString("<link rel=\"stylesheet\" type=\"text/css\" href=\"%1\" />\n").arg(styleSheet);
    reply += QString("</head>\n<body>\n");

    reply += "<span style=\"font-size:18\">Qt Performance Index</span>";

    reply += "<br /><br /><table><tr><td style=\"background-color:#eeeeee\">";

    reply += "This page shows the performance index for certain benchmarks in certain contexts ";
    reply += "(where the context represents platform etc.).";

    reply += "<br /><br />";

    reply += "The index is evaluated at each of the timestamps for which a result exist for ";
    reply += "at least one of the contributing result histories (it is only at these timestamps ";
    reply += "the index value may potentially change).";

    reply += "<br /><br />";

    reply += "The <u>change</u> in index value between any two points in time indicates the ";
    reply += "average performance increase during this time period in terms of a ";
    reply += "<u>log2</u>-based difference. ";
    reply += "An index value change of 1, 0, and -1 thus indicates a doubled, unchanged, ";
    reply += "and halved performance respectively. ";

    reply += "<br /><br />";

    reply += "<b>Note:</b> Individual index values bear no useful meaning when regarded in ";
    reply += "isolation. The graph is however shifted vertically so that each individual index ";
    reply += "value indicates the change from the index value at the given base time ";
    reply += "(the latter being indicated by blue lines). ";

    reply += "<br /><br />";

    reply += "The <u>left-hand vertical axis</u> indicates how much the index value differs ";
    reply += "from the index value at the base time. Note that the scale as such is linear ";
    reply += "even though the values represent logarithmic differences.";

    reply += "<br /><br />";

    reply += "The <u>right-hand vertical axis</u> indicates the performance as a percentage of ";
    reply += "the performance at the base time. Note that the scale as such is logarithmic. ";
    reply += "For example, the left-hand values -2, -1, 0, 1, and 2 map to the right-hand values ";
    reply += "25%, 50%, 100%, 200%, and 400% respectively.";

    reply += "</td></tr></table>\n";

    QDomElement argsElem = doc.elementsByTagName("args").at(0).toElement();

    bool ok;

    // Get median window size ...
    medianWinSize = argsElem.attributeNode("medianWinSize").value().toInt(&ok);
    Q_ASSERT(ok);


    // *** Plot ***

    // reply += "<br />args:<br />";
    // for (int i = 0; i < args.size(); ++i)
    //     reply += QString("arg %1: &gt;%2&lt;<br />").arg(i).arg(args.at(i));
    // reply += "<br />";

    // Image ...
    reply +=
        "<br />\n<img usemap=\"#plotmap\" style=\"border-style:none\" "
        "src=\"bmclientwrapper?command=";

    for (int i = args.indexOf("-server"); i < args.size(); ++i)
        reply += QString("%1 ")
            .arg((args.at(i) == "detailspage") ? QLatin1String("plot") : args.at(i));
    reply += QString("-cachekey %1 ").arg(argsElem.attributeNode("cacheKey").value());
    reply += "-httpheader ";
    reply += "\" />\n<br />\n<br />\n";

    // Image map ...
    reply += "<br />\n<map name=\"plotmap\">\n";

    // ... get timestamps, index values, and contributions ...
    QList<int> timestamps;
    QList<qreal> indexValues;
    QList<int> contributions;
    QDomNodeList valueNodes = doc.elementsByTagName("value");
    for (int i = 0; i < valueNodes.size(); ++i) {
        QDomElement valueElem = valueNodes.at(i).toElement();

        timestamps += valueElem.attributeNode("timestamp").value().toInt(&ok);
        Q_ASSERT(ok);

        const qreal indexValue = valueElem.attributeNode("indexValue").value().toDouble(&ok);
        Q_ASSERT(ok);
        indexValues += indexValue;

        contributions += valueElem.attributeNode("contributions").value().toInt(&ok);
        Q_ASSERT(ok);
    }

    const int baseValuePos = argsElem.attributeNode("baseValuePos").value().toInt(&ok);
    Q_ASSERT(ok);
    const qreal baseValue = indexValues.at(baseValuePos);

    // ... get point infos ...
    Plotter *plotter = new IndexPlotter(timestamps, indexValues, contributions);
    QList<Plotter::PointInfo> pointInfos;
    if (plotter->createImage(&error, &pointInfos).isNull())  {
        BMMisc::printHTMLErrorPage(
            QString("failed to extract point infos: %1").arg(error));
         return;
    }
    Q_ASSERT(pointInfos.size() == timestamps.size());

    // ... extract the web server from the style sheet ...
    QRegExp rx("^(\\S+:\\d+)\\D+$");
    if (rx.indexIn(styleSheet) == -1) {
        BMMisc::printHTMLErrorPage(QString("failed to extract web server from style sheet"));
        return;
    }
    const QString webServer = rx.cap(1);


    // ... add <area> tags for valid points ...
    for (int i = 0; i < pointInfos.size(); ++i) {

        if (contributions.at(i) == 0)
            continue; // skip invalid point

        QDomElement valueElem = valueNodes.at(i).toElement();
        QDomNodeList rankedInfoNodes = valueElem.elementsByTagName("rankedInfo");
        QString url;

        if (rankedInfoNodes.size() > 0) {

            url = QString("%1/cgi-bin/bmclientwrapper").arg(webServer);
            url += QString("?command=-server %1").arg(server);
            url += QString(" get ixhistories detailspage -stylesheet %1").arg(styleSheet);
            url += QString(" -evaltimestamp %1 -medianwinsize %2")
                .arg(timestamps.at(i)).arg(medianWinSize);

            for (int j = 0; j < rankedInfoNodes.size(); ++j) {
                QDomElement rankedInfoElem = rankedInfoNodes.at(j).toElement();

                const int bmcontextId = rankedInfoElem.attributeNode("id").value().toInt(&ok);
                Q_ASSERT(ok);
                const int basePos = rankedInfoElem.attributeNode("basePos").value().toInt(&ok);
                Q_ASSERT(ok);
                const int diffPos1 = rankedInfoElem.attributeNode("diffPos1").value().toInt(&ok);
                Q_ASSERT(ok);
                const int diffPos2 = rankedInfoElem.attributeNode("diffPos2").value().toInt(&ok);
                Q_ASSERT(ok);
                const QString descr = rankedInfoElem.attributeNode("descr").value();
            
                url += QString(" -rankedinfo %1 %2 %3 %4 '%5'")
                    .arg(bmcontextId).arg(basePos).arg(diffPos1).arg(diffPos2).arg(descr);
            }
        }

        QRect rect = pointInfos.at(i).rect.toAlignedRect();
        const qreal baseDiff = indexValues.at(i) - baseValue;
        const qreal basePercentage = 100 * qPow(2, baseDiff);

        reply += QString(
            "<area shape=\"rect\" coords=\"%1,%2,%3,%4\" title=\"%5 %\"%6 />\n")
            .arg(rect.topLeft().x())
            .arg(rect.topLeft().y())
            .arg(rect.bottomRight().x())
            .arg(rect.bottomRight().y())
            .arg(QString().setNum(basePercentage, 'f', 3))
            .arg(url.isEmpty() ? QString() : QString(" href=\"%1\"").arg(url));
    }

    reply += "</map>\n";

    QDateTime dateTime;


    // *** Meta information (characterizing the results) ***

    reply += "\n<br /><br /><table style=\"border:0px\">\n";
    reply += "<tr>\n";

    // Left main table ...

    reply += "<td style=\"border:0px\">\n";

    reply += "<table>\n";
    int baseTimestamp = argsElem.attributeNode("baseTimestamp").value().toInt(&ok);
    Q_ASSERT(ok);
    if (baseTimestamp < 0)
        baseTimestamp = timestamps.last();
    dateTime.setTime_t(baseTimestamp);
    reply += QString("<tr><td>Base time:</td><td>%1 (%2)</td></tr>\n")
        .arg(baseTimestamp)
        .arg(dateTime.toString());
    reply += QString("<tr><td>Median window size:</td><td>%1</td></tr>\n").arg(medianWinSize);
    reply += "</table>\n";

    // Get filters ...
    QDomNodeList testCaseNodes = doc.elementsByTagName("testCase");
    for (int i = 0; i < testCaseNodes.size(); ++i)
        testCaseFilter.append(testCaseNodes.at(i).toElement().attributeNode("name").value());

    QDomNodeList metricNodes = doc.elementsByTagName("metric");
    for (int i = 0; i < metricNodes.size(); ++i)
        metricFilter.append(metricNodes.at(i).toElement().attributeNode("name").value());

    QDomNodeList platformNodes = doc.elementsByTagName("platform");
    for (int i = 0; i < platformNodes.size(); ++i)
        platformFilter.append(platformNodes.at(i).toElement().attributeNode("name").value());

    QDomNodeList hostNodes = doc.elementsByTagName("host");
    for (int i = 0; i < hostNodes.size(); ++i)
        hostFilter.append(hostNodes.at(i).toElement().attributeNode("name").value());

    QDomNodeList branchNodes = doc.elementsByTagName("branch");
    for (int i = 0; i < branchNodes.size(); ++i)
        branchFilter.append(branchNodes.at(i).toElement().attributeNode("name").value());


    // 'Filters' table ...
    reply += "\n<br /><br /><b>Filters:</b><table>\n";
    appendToFilterTable(testCaseFilter, "Test case", &reply);
    appendToFilterTable(metricFilter, "Metric", &reply);
    appendToFilterTable(platformFilter, "Platform", &reply);
    appendToFilterTable(hostFilter, "Host", &reply);
    appendToFilterTable(branchFilter, "Branch", &reply);
    reply += "</table>";

    // 'Number of contributors' table ...
    reply += "\n<br /><br /><b>Number of contributors:</b><table style=\"text-align:right\">\n";
    const int totalRH = argsElem.attributeNode("totalRH").value().toInt(&ok);
    Q_ASSERT(ok);
    reply += QString(
        "<tr><td>Initial candidates (note: 'events' metric is excluded):"
        "</td><td>%1</td></tr>\n")
        .arg(totalRH);
    const int nonPositiveRH = argsElem.attributeNode("nonPositiveRH").value().toInt(&ok);
    Q_ASSERT(ok);
    reply +=
        QString("<tr><td>Rejected due to existence of non-positive values:</td><td>%1</td></tr>\n")
        .arg(nonPositiveRH);
    const int finalTotalRH = totalRH - nonPositiveRH;
    Q_ASSERT(ok);
    reply += QString("<tr><td>Final set:</td><td>%1</td></tr>\n").arg(finalTotalRH);
    reply += "</table>\n";

    // 'Timestamp neighbor distances' table ...
    qreal distSecsSum = 0.0;
    qreal minDistSecs = -1;
    qreal maxDistSecs = -1;
    for (int i = 1; i < timestamps.size(); ++i) {
        const qreal distSecs = timestamps.at(i) - timestamps.at(i - 1);
        distSecsSum += distSecs;
        if (i == 1) {
            minDistSecs = maxDistSecs = distSecs;
        } else {
            minDistSecs = qMin(minDistSecs, distSecs);
            maxDistSecs = qMax(maxDistSecs, distSecs);
        }
    }
    const qreal secsInDay = 86400.0;
    reply +=
        "\n<br /><br /><b>Timestamp neighbor distances:</b><table style=\"text-align:right\">\n";
    reply += QString("<tr><td>Min days:</td><td>%1</td></tr>\n")
        .arg((timestamps.size() < 2) ? "n/a" : QString().setNum(minDistSecs / secsInDay, 'f', 2));
    reply += QString("<tr><td>Avg days:</td><td>%1</td></tr>\n")
        .arg((timestamps.size() < 2)
             ? "n/a" : QString().setNum((distSecsSum / timestamps.size()) / secsInDay, 'f', 2));
    reply += QString("<tr><td>Max days:</td><td>%1</td></tr>\n")
        .arg((timestamps.size() < 2) ? "n/a" : QString().setNum(maxDistSecs / secsInDay, 'f', 2));
    reply += "</table>\n";

    reply += "</td>\n";


    // Right main table ...

    reply += "<td style=\"border:0px\">\n";

    QDomNodeList dqStatsNodes = doc.elementsByTagName("dataQualityStats");
    if (dqStatsNodes.size() > 0) {

        QDomElement dqStatsElem = dqStatsNodes.at(0).toElement();

        // Get tolerance values ...
        dqStatsDiffTol = dqStatsElem.attributeNode("diffTol").value().toDouble(&ok);
        Q_ASSERT(ok);
        dqStatsStabTol = dqStatsElem.attributeNode("stabTol").value().toInt(&ok);
        Q_ASSERT(ok);

        reply += "<fieldset><legend style=\"font-size:12\">Data Quality Statistics</legend>";

        // --- BEGIN Legend table ---
        reply += "<table style=\"border:0px\">"
            "<tr><td style=\"border:0px; background-color:#eeeeee\">"
            "<table style=\"border:0px\">";
        reply += QString(
            "<tr><td style=\"padding:1px; border:0px; text-align:right\">"
            "Subsequence:&nbsp;</td><td style=\"padding:1px; border:0px; text-align:left\">"
            "In the result history of any given contributor, a maximum contiguous sequence "
            "in which all values differ by at most <u>%1 %</u>.</td></tr>")
            .arg(dqStatsDiffTol);
        reply += "<tr><td style=\"padding:1px; border:0px; text-align:right; font-weight:bold\">"
            "SSC:&nbsp;</td><td style=\"padding:1px; border:0px; text-align:left\">"
            "Subsequence count in a single contributor.</td></tr>";
        reply += "<tr><td style=\"padding:1px; border:0px; text-align:right; font-weight:bold\">"
            "Freq:&nbsp;</td><td style=\"padding:1px; border:0px; text-align:left\">"
            "Frequency (number of occurrences).</td></tr>";
        reply += QString(
            "<tr><td style=\"padding:1px; border:0px; text-align:right\">"
            "Stability&nbsp;fraction:&nbsp;</td><td style=\"padding:1px; border:0px; "
            "text-align:left\">"
            "In any given contributor, the percentage of subsequences that have a length "
            "of at least <u>%1</u>.</td></tr>")
            .arg(dqStatsStabTol);
        reply += "<tr><td style=\"padding:1px; border:0px; text-align:right; font-weight:bold\">"
            "SF:&nbsp;</td><td style=\"padding:1px; border:0px; text-align:left\">"
            "Stability fraction in a single contributor (regardless of its subsequence count)."
            "</td></tr>";
        reply += "<tr><td style=\"padding:1px; border:0px; text-align:right; font-weight:bold\">"
            "SF2:&nbsp;</td><td style=\"padding:1px; border:0px; text-align:left\">"
            "Stability fraction in a single contributor having subsequence count >= 2."
            "</td></tr>";
        reply += "<tr><td style=\"padding:1px; border:0px; text-align:right; font-weight:bold\">"
            "P<sub><i>k</i></sub>:&nbsp;</td><td style=\"padding:1px; border:0px; "
            "text-align:left\">"
            "The <i>k</i>th percentile of the stability fractions (i.e. the <i>maximum</i> "
            "stability fraction of the <i>worst</i> <i>k</i>&nbsp;% of the contributors)."
            "</td></tr>";
        reply += "</table></td></tr></table>";
        // --- END Legend table ---

        reply += "<table style=\"border:0px\">";

        // --- BEGIN Stats tables ---
        reply += "<tr>";

        // .. subsequence count ...
        reply += "<td style=\"border:0px\"><table><tr><th>SSC</th><th>Freq</th><th></th></tr>\n";
        QDomNodeList ssFreqNodes = dqStatsElem.elementsByTagName("totMaxESSCountFreq");
        qreal ssFreqSum = 0.0;
        for (int i = 0; i < ssFreqNodes.size(); ++i)
            ssFreqSum += ssFreqNodes.at(i).toElement().attributeNode("freq").value().toInt();
        // Q_ASSERT(ssFreqSum > 0);
        for (int i = 0; i < ssFreqNodes.size(); ++i) {
            QDomElement ssFreqElem = ssFreqNodes.at(i).toElement();
            const int ssFreq = ssFreqElem.attributeNode("freq").value().toInt();
            reply += QString(
                "<tr style=\"text-align:right\"><td>%1</td><td>%2</td><td>%3%</td></tr>\n")
                .arg(ssFreqElem.attributeNode("count").value())
                .arg(ssFreq)
                .arg(QString().setNum(100 * (ssFreq / ssFreqSum), 'f', 1));
        }
        reply += "</table></td>\n";

        QDomNodeList sfPercNodes = dqStatsElem.elementsByTagName("stabFracPercentile");

        // .. stability fraction percentiles (all subsequence counts) ...
        reply += "<td style=\"border:0px\"><table>"
            "<tr><th></th><th>SF</th></tr>\n";
        for (int i = sfPercNodes.size() - 1; i >= 0; --i) {
            QDomElement sfPercElem = sfPercNodes.at(i).toElement();
            const qreal val = sfPercElem.attributeNode("val").value().toDouble(&ok);
            reply += QString(
                "<tr><td><b>P<sub>%1</sub></b></td><td style=\"text-align:right%2\">%3</td></tr>\n")
                .arg(sfPercElem.attributeNode("p").value())
                .arg((val >= 0) ? QString() : QString("; color:red"))
                .arg((val >= 0) ? QString("%1%").arg(QString().setNum(val, 'f', 1))
                     : QString("n/a"));
            Q_ASSERT(ok);
        }
        reply += "</table></td>\n";

        // .. stability fraction percentiles (subsequence counts >= 2) ...
        reply += "<td style=\"border:0px\"><table>"
            "<tr><th></th><th>SF2</th></tr>\n";
        for (int i = sfPercNodes.size() - 1; i >= 0; --i) {
            QDomElement sfPercElem = sfPercNodes.at(i).toElement();
            const qreal val = sfPercElem.attributeNode("val2").value().toDouble(&ok);
            reply += QString(
                "<tr><td><b>P<sub>%1</sub></b></td><td style=\"text-align:right%2\">%3</td></tr>\n")
                .arg(sfPercElem.attributeNode("p").value())
                .arg((val >= 0) ? QString() : QString("; color:red"))
                .arg((val >= 0) ? QString("%1%").arg(QString().setNum(val, 'f', 1))
                     : QString("n/a"));
            Q_ASSERT(ok);
        }
        reply += "</table></td>\n";

        reply += "</tr>\n";
        // --- END Stats tables ---

        reply += "</table>\n";

        reply += "</fieldset>";
    }

    reply += "</td>\n";


    reply += "</tr>\n";
    reply += "<table>\n";


    // *** Values ***
    reply += "\n<br /><br /><table style=\"border: 0px\">\n";
    reply += "<tr style=\"border: 0px\">\n";

    // Table 1 ...
    reply += "<td style=\"border: 0px\"><table>\n";
    reply += "<tr><th></th><th>Date</th><th>Contributions</th></tr>\n";

    for (int i = 0; i < valueNodes.size(); ++i) {
        const int i_ = (valueNodes.size() - 1) - i;
        const int timestamp = timestamps.at(i_);
        const int contributions_ = contributions.at(i_);

        const QString style = QString("%1")
            .arg((timestamp >= baseTimestamp)
                 ? "index_table_after_base"
                 : "index_table");

        reply += "<tr>";
        reply += QString("<td class=\"%1\">%2</td>").arg(style).arg(i_ + 1);
        dateTime.setTime_t(timestamp);
        reply += QString("<td class=\"%1\">%2</td>").arg(style).arg(dateTime.toString());
        reply += QString("<td class=\"%1\">%2%3</td>")
            .arg(style)
            .arg(contributions_)
            .arg((finalTotalRH == 0)
                 ? QString()
                 : QString(" (%1%)").arg(100 * (qreal(contributions_) / finalTotalRH), 0, 'f', 2));
        reply += "</tr>\n";
    }
    reply += "</td></table>\n";

    // Table 2 ...
    reply += "<td style=\"border: 0px\"><table>\n";
    reply += "<tr><th>Timestamp</th></tr>\n";

    for (int i = 0; i < valueNodes.size(); ++i) {
        const int i_ = (valueNodes.size() - 1) - i;
        const int timestamp = timestamps.at(i_);

        const QString style = QString("%1")
            .arg((timestamp >= baseTimestamp)
                 ? "index_table_after_base"
                 : "index_table");

        reply += "<tr>";
        reply += QString("<td class=\"%1\">%2</td>").arg(style).arg(timestamp);
        reply += "</tr>\n";
    }
    reply += "</td></table>\n";

    // Absolute log2 base diffs that are at or below minColorAbsVal will be highlighted by the
    // "weakest" (i.e. least saturated) color (red or green depending on the sign), while those
    // that are at or above maxColorAbsVal will be highlighted by the "strongest" (i.e. most
    // saturated) color:
    const qreal minColorAbsVal = 0.0;
    const qreal maxColorAbsVal = 0.5;

    // Table 3 (log2 base diff factor) ...
    reply += "<td style=\"border: 0px\"><table>\n";
    reply += "<tr><th>Base diff</th></tr>\n";

    for (int i = 0; i < valueNodes.size(); ++i) {
        const int i_ = (valueNodes.size() - 1) - i;
        const qreal baseDiff = indexValues.at(i_) - baseValue;
        const int contributions_ = contributions.at(i_);

        const QString style = "index_table";

        reply += "<tr>";
        if (contributions_ > 0) {
            reply += QString(
                "<td class=\"%1\" style=\"background-color:%2; text-align:right\">%3</td>")
                .arg(style)
                .arg(BMMisc::redToGreenColor(baseDiff, minColorAbsVal, maxColorAbsVal, true))
                .arg(QString().setNum(baseDiff, 'f', 4));
        } else {
            reply += QString(
                "<td class=\"%1\" style=\"color:red; background-color:white\">n/a</td>").arg(style);
        }
        reply += "</tr>\n";
    }
    reply += "</td></table>\n";

    // Table 4 (base %) ...
    reply += "<td style=\"border: 0px\"><table>\n";
    reply += "<tr><th>Base %</th></tr>\n";

    for (int i = 0; i < valueNodes.size(); ++i) {
        const int i_ = (valueNodes.size() - 1) - i;
        const qreal baseDiff = indexValues.at(i_) - baseValue;
        const qreal basePercentage = 100 * qPow(2, baseDiff);
        const int contributions_ = contributions.at(i_);

        const QString style = "index_table";

        reply += "<tr>";
        if (contributions_ > 0) {
            reply += QString(
                "<td class=\"%1\" style=\"background-color:%2; text-align:right\">%3</td>")
                .arg(style)
                .arg(BMMisc::redToGreenColor(baseDiff, minColorAbsVal, maxColorAbsVal, true))
                .arg(QString().setNum(basePercentage, 'f', 3));
        } else {
            reply += QString(
                "<td class=\"%1\" style=\"color:red; background-color:white\">n/a</td>").arg(style);
        }
        reply += "</tr>\n";
    }
    reply += "</td></table>\n";

    reply += "</table>\n";

    reply += "</body></html>";

    BMMisc::printHTMLOutput(reply);
}

void BMRequest_IndexGetValues::handleReply_Image(const QStringList &args) const
{
    const QString error =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();

    if (error.isEmpty()) {
        // Create the image ...

        bool ok;

        QDomElement argsElem = doc.elementsByTagName("args").at(0).toElement();

        const int baseTimestamp = argsElem.attributeNode("baseTimestamp").value().toInt(&ok);
        Q_ASSERT(ok);
        const int baseValuePos = argsElem.attributeNode("baseValuePos").value().toInt(&ok);
        Q_ASSERT(ok);

        // Get timestamps, index values, and contributions ...
        QList<int> timestamps;
        QList<qreal> indexValues;
        QList<int> contributions;
        QDomNodeList valueNodes = doc.elementsByTagName("value");
        for (int i = 0; i < valueNodes.size(); ++i) {
            QDomElement valueElem = valueNodes.at(i).toElement();

            timestamps += valueElem.attributeNode("timestamp").value().toInt(&ok);
            Q_ASSERT(ok);

            const qreal indexValue = valueElem.attributeNode("indexValue").value().toDouble(&ok);
            Q_ASSERT(ok);
            indexValues += indexValue;

            contributions += valueElem.attributeNode("contributions").value().toInt(&ok);
            Q_ASSERT(ok);
        }

        // Create the plot ...
        QString error_;
        Plotter *plotter =
            new IndexPlotter(timestamps, indexValues, contributions, baseTimestamp, baseValuePos);
        const QImage image = plotter->createImage(&error_);
        if (!image.isNull()) {
            BMMisc::printImageOutput(
                image, "png", QString(), BMMisc::hasOption(args, "-httpheader"));
        } else {
            BMMisc::printHTMLErrorPage(QString("failed to create index plot: %1").arg(error_));
        }

    } else {
        BMMisc::printHTMLErrorPage(QString("failed to compute index values: %1").arg(error));
    }
}


// --- IndexGetConfigs ---
QByteArray BMRequest_IndexGetConfigs::toRequestBuffer(QString *)
{
    const QString request = QString("<request type=\"%1\" />").arg(name());
    return xmlConvert(request);
}

QByteArray BMRequest_IndexGetConfigs::toReplyBuffer()
{
    QString reply = QString("<reply type=\"%1\">").arg(name());
    QSqlQuery *query;

    query = createQuery();
    if (!query->exec("SELECT name FROM indexConfig ORDER BY name ASC;")) {
        reply = errorReply(*query, name(), QString("(exec() failed)"));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    while (query->next())
        reply += QString("<indexConfig name=\"%1\" />").arg(query->value(0).toString());
    deleteQuery(query);

    reply += "</reply>";

    return xmlConvert(reply);
}

void BMRequest_IndexGetConfigs::handleReply_Raw(const QStringList &args) const
{
    printf("NOTE: raw output not supported yet; printing JSON output for now:\n\n");
    handleReply_JSON(args);
}

void BMRequest_IndexGetConfigs::handleReply_JSON(const QStringList &args) const
{
    Q_UNUSED(args);

    const QString error =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();

    QString reply = QString("{");

    if (error.isEmpty()) {

        reply += "\n\"names\": [";

        QDomNodeList indexConfigNodes = doc.elementsByTagName("indexConfig");
        for (int i = 0; i < indexConfigNodes.size(); ++i) {
            QDomElement indexConfigElem = indexConfigNodes.at(i).toElement();
            reply += QString("%1\n\"%2\"")
                .arg((i == 0) ? "" : ", ")
                .arg(indexConfigElem.attributeNode("name").value());
        }

        reply += "\n]\n}";

    } else {
        reply = QString("{\"error\": \"error getting index configurations: %1\"}").arg(error);
    }

    BMMisc::printJSONOutput(reply);
}


// --- IndexGetConfig ---
QByteArray BMRequest_IndexGetConfig::toRequestBuffer(QString *)
{
    const QString request =
        QString("<request type=\"%1\"><args configName=\"%2\" /></request>")
        .arg(name()).arg(configName);
    return xmlConvert(request);
}

QByteArray BMRequest_IndexGetConfig::toReplyBuffer()
{
    // Get config name ...
    QDomElement argsElem = doc.elementsByTagName("args").at(0).toElement();
    configName = argsElem.attributeNode("configName").value();

    QString reply = QString("<reply type=\"%1\">").arg(name());
    QSqlQuery *query;

    // Get config ID and non-filter attributes ...
    query = createQuery();
    if (!query->exec(
            QString("SELECT id, baseTimestamp, medianWinSize FROM indexConfig WHERE name='%1';")
            .arg(configName))) {
        reply = errorReply(*query, name(), QString("failed to get config (exec() failed (1))"));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    int configId;
    int baseTimestamp;
    int medianWinSize;
    if (query->next()) {
        configId = query->value(0).toInt();
        baseTimestamp = query->value(1).toInt();
        medianWinSize = query->value(2).toInt();
    } else {
        reply = errorReply(*query, name(), QString("index config '%1' not found").arg(configName));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    deleteQuery(query);


    // Get filter attributes (### note the refactoring potential here) ...

    // ... test case ...
    query = createQuery();
    if (!query->exec(
            QString("SELECT name FROM icTestCase WHERE indexConfigId=%1 ORDER BY name ASC;")
            .arg(configId))) {
        reply = errorReply(*query, name(), QString("failed to get config (exec() failed (2))"));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    QStringList testCaseFilter;
    while (query->next())
        testCaseFilter.append(query->value(0).toString());
    deleteQuery(query);

    // ... metric ...
    query = createQuery();
    if (!query->exec(
            QString("SELECT name FROM icMetric WHERE indexConfigId=%1 ORDER BY name ASC;")
            .arg(configId))) {
        reply = errorReply(*query, name(), QString("failed to get config (exec() failed (2))"));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    QStringList metricFilter;
    while (query->next())
        metricFilter.append(query->value(0).toString());
    deleteQuery(query);

    // ... platform ...
    query = createQuery();
    if (!query->exec(
            QString("SELECT name FROM icPlatform WHERE indexConfigId=%1 ORDER BY name ASC;")
            .arg(configId))) {
        reply = errorReply(*query, name(), QString("failed to get config (exec() failed (2))"));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    QStringList platformFilter;
    while (query->next())
        platformFilter.append(query->value(0).toString());
    deleteQuery(query);

    // ... host ...
    query = createQuery();
    if (!query->exec(
            QString("SELECT name FROM icHost WHERE indexConfigId=%1 ORDER BY name ASC;")
            .arg(configId))) {
        reply = errorReply(*query, name(), QString("failed to get config (exec() failed (2))"));
        deleteQuery(query);
        return xmlConvert(reply);
    }
    QStringList hostFilter;
    while (query->next())
        hostFilter.append(query->value(0).toString());
    deleteQuery(query);

    // ... branch ...
    query = createQuery();
    if (!query->exec(
            QString("SELECT name FROM icBranch WHERE indexConfigId=%1 ORDER BY name ASC;")
            .arg(configId))) {
        reply = errorReply(*query, name(), "failed to get config (exec() failed (2))");
        deleteQuery(query);
        return xmlConvert(reply);
    }
    QStringList branchFilter;
    while (query->next())
        branchFilter.append(query->value(0).toString());
    deleteQuery(query);


    //--------------------------------------

    reply += QString("<args baseTimestamp=\"%1\" medianWinSize=\"%2\" />")
        .arg(baseTimestamp)
        .arg(medianWinSize);

    for (int i = 0; i < testCaseFilter.size(); ++i)
        reply += QString("<testCase name=\"%1\" />").arg(testCaseFilter.at(i));
    for (int i = 0; i < metricFilter.size(); ++i)
        reply += QString("<metric name=\"%1\" />").arg(metricFilter.at(i));
    for (int i = 0; i < platformFilter.size(); ++i)
        reply += QString("<platform name=\"%1\" />").arg(platformFilter.at(i));
    for (int i = 0; i < hostFilter.size(); ++i)
        reply += QString("<host name=\"%1\" />").arg(hostFilter.at(i));
    for (int i = 0; i < branchFilter.size(); ++i)
        reply += QString("<branch name=\"%1\" />").arg(branchFilter.at(i));

    reply += "</reply>";

    return xmlConvert(reply);
}

void BMRequest_IndexGetConfig::handleReply_Raw(const QStringList &args) const
{
    printf("NOTE: raw output not supported yet; printing JSON output for now:\n\n");
    handleReply_JSON(args);
}

void BMRequest_IndexGetConfig::handleReply_JSON(const QStringList &args) const
{
    Q_UNUSED(args);

    const QString error =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();

    QString reply = QString("{");

    if (error.isEmpty()) {

        QDomElement argsElem = doc.elementsByTagName("args").at(0).toElement();
        bool ok;

        // Non-filter values ...

        reply += QString("\n\"baseTimestamp\": %1")
            .arg(argsElem.attributeNode("baseTimestamp").value().toInt(&ok));
        Q_ASSERT(ok);

        reply += QString(",\n\"medianWinSize\": %1")
            .arg(argsElem.attributeNode("medianWinSize").value().toInt(&ok));
        Q_ASSERT(ok);

        // Filter values ...

        const QStringList filterNames =
            QStringList() << "testCase" << "metric" << "platform" << "host" << "branch";
        for (int i = 0; i < filterNames.size(); ++i) {
            reply += QString(",\n\"%1Filter\": [").arg(filterNames.at(i));
            QDomNodeList filterNodes = doc.elementsByTagName(filterNames.at(i));
            for (int j = 0; j < filterNodes.size(); ++j) {
                QDomElement filterElem = filterNodes.at(j).toElement();
                reply += QString("%1\"%2\"")
                    .arg((j == 0) ? "" : ", ")
                    .arg(filterElem.attributeNode("name").value());
            }
            reply += "]";
        }

        reply += "\n}";

    } else {
        reply = QString("{\"error\": \"error getting index configuration: %1\"}").arg(error);
    }

    BMMisc::printJSONOutput(reply);
}


// --- IndexPutConfig ---
QByteArray BMRequest_IndexPutConfig::toRequestBuffer(QString *)
{
    QString request = QString("<request type=\"%1\">").arg(name());

    // Non-filter values ...
    request += QString(
        "<args configName=\"%1\" baseTimestamp=\"%2\" medianWinSize=\"%3\" />")
        .arg(configName)
        .arg(baseTimestamp)
        .arg(medianWinSize);

    // Filter values ...
    for (int i = 0; i < testCaseFilter.size(); ++i)
        request += QString("<testCase name=\"%1\" />").arg(testCaseFilter.at(i));
    for (int i = 0; i < metricFilter.size(); ++i)
        request += QString("<metric name=\"%1\" />").arg(metricFilter.at(i));
    for (int i = 0; i < platformFilter.size(); ++i)
        request += QString("<platform name=\"%1\" />").arg(platformFilter.at(i));
    for (int i = 0; i < hostFilter.size(); ++i)
        request += QString("<host name=\"%1\" />").arg(hostFilter.at(i));
    for (int i = 0; i < branchFilter.size(); ++i)
        request += QString("<branch name=\"%1\" />").arg(branchFilter.at(i));

    request += "</request>";

    return xmlConvert(request);
}

// ### 2 B DOCUMENTED!
static bool deleteIndexConfig(
    QSqlDatabase *database, const QString &type, int configId, const QStringList &filterTables,
    QString *reply)
{
    database->transaction();

    QSqlQuery query(*database);

    // Delete from filter tables ...
    for (int i = 0; i < filterTables.size(); ++i) {
        if (!query.exec(
                QString("DELETE FROM %1 WHERE indexConfigId=%2;")
                .arg(filterTables.at(i)).arg(configId))) {
            *reply = errorReply(
                query, type, QString("failed to delete from %1").arg(filterTables.at(i)));
            database->rollback();
            return false;
        }
    }

    // Delete from main table ...
    if (!query.exec(
            QString("DELETE FROM indexConfig WHERE id=%1;").arg(configId))) {
        *reply = errorReply(query, type, "failed to delete from indexConfig");
        database->rollback();
        return false;
    }

    database->commit();
    return true;
}

QByteArray BMRequest_IndexPutConfig::toReplyBuffer()
{
    QDomElement argsElem = doc.elementsByTagName("args").at(0).toElement();

    // Get config name ...
    configName = argsElem.attributeNode("configName").value();

    // Get non-filter values ...
    bool ok;

    baseTimestamp = argsElem.attributeNode("baseTimestamp").value().toInt(&ok);
    Q_ASSERT(ok);

    medianWinSize = argsElem.attributeNode("medianWinSize").value().toInt(&ok);
    Q_ASSERT(ok);

    // Get filter values ...

    QDomNodeList testCaseNodes = doc.elementsByTagName("testCase");
    for (int i = 0; i < testCaseNodes.size(); ++i)
        testCaseFilter.append(testCaseNodes.at(i).toElement().attributeNode("name").value());

    QDomNodeList metricNodes = doc.elementsByTagName("metric");
    for (int i = 0; i < metricNodes.size(); ++i)
        metricFilter.append(metricNodes.at(i).toElement().attributeNode("name").value());

    QDomNodeList platformNodes = doc.elementsByTagName("platform");
    for (int i = 0; i < platformNodes.size(); ++i)
        platformFilter.append(platformNodes.at(i).toElement().attributeNode("name").value());

    QDomNodeList hostNodes = doc.elementsByTagName("host");
    for (int i = 0; i < hostNodes.size(); ++i)
        hostFilter.append(hostNodes.at(i).toElement().attributeNode("name").value());

    QDomNodeList branchNodes = doc.elementsByTagName("branch");
    for (int i = 0; i < branchNodes.size(); ++i)
        branchFilter.append(branchNodes.at(i).toElement().attributeNode("name").value());


    //------------------------------------

    // Insert config (overwriting any existing one) ...

    QString reply;
    QSqlQuery *query = createQuery();
    const QStringList filterTables =
        QStringList() << "icTestCase" << "icMetric" << "icPlatform" << "icHost" << "icBranch";
    QMap<QString, QStringList> filters;
    filters.insert("icTestCase", testCaseFilter);
    filters.insert("icMetric", metricFilter);
    filters.insert("icPlatform", platformFilter);
    filters.insert("icHost", hostFilter);
    filters.insert("icBranch", branchFilter);
    int configId;

    // Check if the config already exists ...
    if (!query->exec(QString("SELECT id FROM indexConfig WHERE name='%1';").arg(configName))) {
        reply = errorReply(*query, name(), "failed to save index config (exec() failed (1))");
        deleteQuery(query);
        return xmlConvert(reply);
    }

    if (query->next()) {
        // ... it does, so delete it ...

        configId = query->value(0).toInt();
        deleteQuery(query);
        if (!deleteIndexConfig(database, name(), configId, filterTables, &reply))
            return xmlConvert(reply);
    } else {
        deleteQuery(query);
    }


    // Insert the new config ...

    query = createQuery(Transaction);

    // Insert into main table ...
    if (!query->exec(
            QString(
                "INSERT INTO indexConfig (name, baseTimestamp, medianWinSize) "
                "VALUES ('%1', %2, %3);")
            .arg(configName)
            .arg(baseTimestamp)
            .arg(medianWinSize))) {
        reply = errorReply(*query, name(), "failed to insert into indexConfig");
        deleteQuery(query, Rollback);
        return xmlConvert(reply);
    }

    // Get last insert ID ...
    if (!BMMisc::getLastInsertId(query, &configId)) {
        Q_ASSERT(false); // ### Backup code (involving a separate query) to go here!
        reply = errorReply(*query, name(), "failed to get last ID inserted in indexConfig");
        deleteQuery(query, Rollback);
        return xmlConvert(reply);
    }

    // Insert into filter tables ...
    for (int i = 0; i < filterTables.size(); ++i) {
        QStringList filter = filters.value(filterTables.at(i));
        for (int j = 0; j < filter.size(); ++j) {
            if (!query->exec(
                    QString("INSERT INTO %1 (indexConfigId, name) VALUES (%2, '%3');")
                    .arg(filterTables.at(i)).arg(configId).arg(filter.at(j)))) {
                reply = errorReply(
                    *query, name(),
                    QString("failed to insert value '%1' into filter table %1")
                    .arg(filter.at(j)).arg(filterTables.at(i)));
                deleteQuery(query, Rollback);
                return xmlConvert(reply);
            }
        }
    }

    deleteQuery(query, Commit);


    reply = QString("<reply type=\"%1\" />").arg(name());
    return xmlConvert(reply);
}

void BMRequest_IndexPutConfig::handleReply_Raw(const QStringList &args) const
{
    printf("NOTE: raw output not supported yet; printing JSON output for now:\n\n");
    handleReply_JSON(args);
}

void BMRequest_IndexPutConfig::handleReply_JSON(const QStringList &args) const
{
    Q_UNUSED(args);

    QString reply;

    const QString error =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();

    if (error.isEmpty())
        reply = "{}";
    else
        reply = QString("{\"error\": \"error saving index configuration: %1\"}").arg(error);

    BMMisc::printJSONOutput(reply);
}


// --- IndexDeleteConfig ---
QByteArray BMRequest_IndexDeleteConfig::toRequestBuffer(QString *)
{
    const QString request =
        QString("<request type=\"%1\"><args configName=\"%2\" /></request>")
        .arg(name()).arg(configName);
    return xmlConvert(request);
}

QByteArray BMRequest_IndexDeleteConfig::toReplyBuffer()
{
    QDomElement argsElem = doc.elementsByTagName("args").at(0).toElement();

    // Get config name ...
    configName = argsElem.attributeNode("configName").value();

    QString reply;
    QSqlQuery *query = createQuery();
    const QStringList filterTables =
        QStringList() << "icTestCase" << "icMetric" << "icPlatform" << "icHost" << "icBranch";
    int configId;

    // Check if the config already exists ...
    if (!query->exec(QString("SELECT id FROM indexConfig WHERE name='%1';").arg(configName))) {
        reply = errorReply(*query, name(), "failed to save index config (exec() failed (1))");
        deleteQuery(query);
        return xmlConvert(reply);
    }

    if (query->next()) {
        // ... it does, so delete it ...
        configId = query->value(0).toInt();
        deleteQuery(query);
        if (!deleteIndexConfig(database, name(), configId, filterTables, &reply))
            return xmlConvert(reply);
    } else {
        deleteQuery(query);
    }

    reply = QString("<reply type=\"%1\" />").arg(name());
    return xmlConvert(reply);
}

void BMRequest_IndexDeleteConfig::handleReply_Raw(const QStringList &args) const
{
    printf("NOTE: raw output not supported yet; printing JSON output for now:\n\n");
    handleReply_JSON(args);
}

void BMRequest_IndexDeleteConfig::handleReply_JSON(const QStringList &args) const
{
    Q_UNUSED(args);

    QString reply;

    const QString error =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();

    if (error.isEmpty())
        reply = "{}";
    else
        reply = QString("{\"error\": \"error deleting index configuration: %1\"}").arg(error);

    BMMisc::printJSONOutput(reply);
}


// --- GetHistories ---
QByteArray BMRequest_GetHistories::toRequestBuffer(QString *)
{
    const QString request =
        QString(
            "<request type=\"%1\"><args testCase=\"%2\" testFunction=\"%3\" dataTag=\"%4\" "
            "cacheKey=\"%5\" /></request>")
        .arg(name()).arg(testCase).arg(testFunction).arg(dataTag).arg(cacheKey);
    return xmlConvert(request);
}

QByteArray BMRequest_GetHistories::toReplyBuffer()
{
    QDomElement argsElem = doc.elementsByTagName("args").at(0).toElement();

    QString error;
    QString reply;
    bool ok;

    // Check if a cached reply can be returned ...
    int cacheKey_ = argsElem.attributeNode("cacheKey").value().toInt(&ok);
    if (ok) {
        reply = Cache::instance()->get(cacheKey_);
        if (!reply.isEmpty())
            return xmlConvert(reply);
    }

    // Compute from scratch ...

    // Get benchmark ...
    testCase = argsElem.attributeNode("testCase").value();
    testFunction = argsElem.attributeNode("testFunction").value();
    dataTag = argsElem.attributeNode("dataTag").value();

    QSqlQuery *query;

    // Get the BM context IDs and contexts for this benchmark ...
    query = createQuery();
    if (!query->exec(
            QString(
                "SELECT bmcontext.id, metric.name, platform.name, host.name, branch.gitRepo, "
                "       branch.gitBranch"
                "  FROM bmcontext, benchmark, context, metric, platform, host, branch"
                " WHERE benchmarkid = benchmark.id"
                "   AND contextId = context.id"
                "   AND metricId = metric.id"
                "   AND platformId = platform.id"
                "   AND hostId = host.id"
                "   AND branchId = branch.id"
                "   AND testCase = '%1' "
                "   AND testFunction = '%2' "
                "   AND dataTag = '%3';")
            .arg(testCase)
            .arg(testFunction)
            .arg(dataTag))) {
        deleteQuery(query);
        return xmlConvert(
            errorReply(*query, name(), "failed to get bmcontext IDs (exec() failed)"));
    }
    QList<int> bmcontextIds;
    QStringList metrics;
    QStringList platforms;
    QStringList hosts;
    QStringList gitRepos;
    QStringList gitBranches;
    while (query->next()) {
        bmcontextIds += query->value(0).toInt();
        metrics      += query->value(1).toString();
        platforms    += query->value(2).toString();
        hosts        += query->value(3).toString();
        gitRepos     += query->value(4).toString();
        gitBranches  += query->value(5).toString();
    }
    deleteQuery(query);

    if (bmcontextIds.isEmpty())
        return xmlConvert(errorReply(name(), "no result histories found"));


    //------------------------------------------------------------------------------

    reply = QString("<reply type=\"%1\" >").arg(name());

    // Add result histories to reply ...
    for (int i = 0; i < bmcontextIds.size(); ++i) {

        reply += QString(
            "<resultHistory metric=\"%1\" platform=\"%2\" host=\"%3\" gitRepo=\"%4\" "
            "gitBranch=\"%5\" >")
            .arg(metrics.at(i))
            .arg(platforms.at(i))
            .arg(hosts.at(i))
            .arg(gitRepos.at(i))
            .arg(gitBranches.at(i));

        query = createQuery();
        if (!query->exec(
                QString(
                    "SELECT timestamp, value FROM result "
                    "WHERE bmcontextId=%1 "
                    "ORDER BY timestamp ASC;")
                .arg(bmcontextIds.at(i)))) {
            deleteQuery(query);
            return xmlConvert(
                errorReply(*query, name(), "failed to get time series (exec() failed)"));
        }
        QList<int> timestamps;
        QList<qreal> values;
        while (query->next()) {
            timestamps += query->value(0).toInt();
            values += query->value(1).toDouble();
        }
        deleteQuery(query);

        for (int j = 0; j < timestamps.size(); ++j)
            reply += QString("<val t=\"%1\" v=\"%2\" />").arg(timestamps.at(j)).arg(values.at(j));

        reply += "</resultHistory>";
    }

    // Finalize the reply and cache it ...
    cacheKey_ = Cache::instance()->nextId();
    reply += QString(
        "<args testCase=\"%1\" testFunction=\"%2\" dataTag=\"%3\" cacheKey=\"%4\" /></reply>")
        .arg(testCase).arg(testFunction).arg(dataTag).arg(cacheKey_);
    Cache::instance()->put(cacheKey_, reply);

    return xmlConvert(reply);
}

void BMRequest_GetHistories::handleReply_HTML(const QStringList &args) const
{
    QString error;

    error = doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();
    if (!error.isEmpty()) {
        BMMisc::printHTMLErrorPage(
            QString("failed to create details page: error getting histories: %1").arg(error));
        return;
    }

    QStringList optValues;

    // Get server ...
    QString server;
    if (BMMisc::getOption(args, "-server", &optValues, 1, 0, &error)) {
        server = optValues.first().trimmed();
    } else {
        BMMisc::printHTMLErrorPage("-server option not found");
        return;
    }

    // Get style sheet ...
    QString styleSheet;
    if (BMMisc::getOption(args, "-stylesheet", &optValues, 1, 0, &error)) {
        styleSheet = optValues.first().trimmed();
    } else {
        BMMisc::printHTMLErrorPage("-stylesheet option not found");
        return;
    }

    // *** Header ***
    QString reply = QString("<html>\n<head>\n");
    reply += QString("<link rel=\"stylesheet\" type=\"text/css\" href=\"%1\" />\n").arg(styleSheet);
    reply += QString("</head>\n<body>\n");

    reply += "<span style=\"font-size:18\">Benchmark Cross-Context Comparison</span>";

    reply += "<br /><br /><table><tr><td style=\"background-color:#eeeeee\">";
    reply += "This page compares the result histories of a particular benchmark in ";
    reply += "all available contexts.";
    reply += "</td></tr></table>\n";

    QDomElement argsElem = doc.elementsByTagName("args").at(0).toElement();

    reply += "<br />";
    reply += QString(
        "<table>\n"

        "<tr>\n"
        "<td class=\"context_table_name\" >Test Case:</td>"
        "<td class=\"context_table_bmark\" >%1</td>"
        "</tr>\n"

        "<tr>\n"
        "<td class=\"context_table_name\" >Test Function:</td>"
        "<td class=\"context_table_bmark\" >%2</td>"
        "</tr>\n"

        "<tr>\n"
        "<td class=\"context_table_name\" >Data Tag:</td>"
        "<td class=\"context_table_bmark\" >%3</td>"
        "</tr>\n"

        "</table>\n")
        .arg(argsElem.attributeNode("testCase").value())
        .arg(argsElem.attributeNode("testFunction").value())
        .arg(argsElem.attributeNode("dataTag").value())
        ;

    // *** Plot ***

    // reply += "<br />args:<br />";
    // for (int i = 0; i < args.size(); ++i)
    //     reply += QString("arg %1: &gt;%2&lt;<br />").arg(i).arg(args.at(i));
    // reply += "<br />";

    reply += "<br />\n<img src=\"bmclientwrapper?command=";
    for (int i = args.indexOf("-server"); i < args.size(); ++i)
        reply += QString("%1 ")
            .arg((args.at(i) == "detailspage") ? QLatin1String("plot") : args.at(i));
    reply += QString("-cachekey %1 ").arg(argsElem.attributeNode("cacheKey").value());
    reply += "-httpheader ";
    reply += "\" />\n<br />\n<br />\n";

    reply += "</body></html>";

    BMMisc::printHTMLOutput(reply);
}

void BMRequest_GetHistories::handleReply_Image(const QStringList &args) const
{
    const QString error =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();

    if (error.isEmpty()) {

        QList<ResultHistoryInfo *> rhInfos;

        // Get the time series and context for each result history ...

        // Loop over result histories ...
        QDomNodeList rhNodes = doc.elementsByTagName("resultHistory");
        for (int i = 0; i < rhNodes.size(); ++i) {
            QDomElement rhElem = rhNodes.at(i).toElement();
       
            // Get the context ...
            const QString metric = rhElem.attributeNode("metric").value();
            const QString platform = rhElem.attributeNode("platform").value();
            const QString host = rhElem.attributeNode("host").value();
            const QString gitRepo = rhElem.attributeNode("gitRepo").value();
            const QString gitBranch = rhElem.attributeNode("gitBranch").value();

            // Get the time series ...
            QList<int> timestamps;
            QList<qreal> values;
            QDomNodeList valueNodes = rhElem.elementsByTagName("val");
            for (int j = 0; j < valueNodes.size(); ++j) {
                bool ok;
                QDomElement valueElem = valueNodes.at(j).toElement();
                timestamps += valueElem.attributeNode("t").value().toInt(&ok);
                Q_ASSERT(ok);
                values += valueElem.attributeNode("v").value().toDouble(&ok);
                Q_ASSERT(ok);
            }

            rhInfos.append(
                new ResultHistoryInfo(
                    -1, timestamps, values, -1, metric, platform, host, gitRepo,
                    gitBranch));
        }

        // Create image ...
        QString error_;
        Plotter *plotter = new HistoriesPlotter(rhInfos);
        const QImage image = plotter->createImage(&error_);

        // Free dynamic memory ...
        for (int i = 0; i < rhInfos.size(); ++i)
            delete rhInfos.at(i);

        // Output image ...
        if (!image.isNull()) {
            BMMisc::printImageOutput(
                image, "png", QString(), BMMisc::hasOption(args, "-httpheader"));
        } else {
            BMMisc::printHTMLErrorPage(QString("failed to create histories plot: %1").arg(error_));
        }

    } else {
        BMMisc::printHTMLErrorPage(QString("failed to get histories: %1").arg(error));
    }
}


// --- GetBMTree ---
QByteArray BMRequest_GetBMTree::toRequestBuffer(QString *)
{
    const QString request = QString("<request type=\"%1\" />").arg(name());
    return xmlConvert(request);
}

struct TFInfo
{
    QString name;
    TFInfo(const QString &name) : name(name) {}
};

struct TCInfo
{
    QList<TFInfo *> tfInfos;
    QString name;
    TCInfo(const QString &name) : name(name) {}
    ~TCInfo() {
        for (int i = 0; i < tfInfos.size(); ++i)
            delete tfInfos.at(i);
    }
};

struct TCInfos
{
    QList<TCInfo *> tcInfos;
    ~TCInfos() {
        for (int i = 0; i < tcInfos.size(); ++i)
            delete tcInfos.at(i);
    }
};

QByteArray BMRequest_GetBMTree::toReplyBuffer()
{
    QString reply = QString("<reply type=\"%1\">").arg(name());
    QSqlQuery *query;

    // Get all test cases ...
    query = createQuery();
    if (!query->exec("SELECT DISTINCT testCase FROM benchmark ORDER BY testCase ASC;")) {
        reply = errorReply(*query, name(), QString("failed to get test cases (exec() failed)"));
        deleteQuery(query);
        return xmlConvert(reply);
    }

    TCInfos tcInfos_;
    while (query->next()) {
        TCInfo *tcInfo = new TCInfo(query->value(0).toString());
        tcInfos_.tcInfos.append(tcInfo);
    }
    deleteQuery(query);

    // Get all test functions for each test case ...
    for (int i = 0; i < tcInfos_.tcInfos.size(); ++i) {
        query = createQuery();
        if (!query->exec(
                QString(
                    "SELECT DISTINCT testFunction FROM benchmark"
                    " WHERE testCase = '%1'"
                    " ORDER BY testFunction ASC;")
                .arg(tcInfos_.tcInfos.at(i)->name))) {
            reply = errorReply(
                *query, name(), QString("failed to get test functions (exec() failed)"));
            deleteQuery(query);
            return xmlConvert(reply);
        }
        while (query->next()) {
            TFInfo *tfInfo = new TFInfo(query->value(0).toString());
            tcInfos_.tcInfos.at(i)->tfInfos.append(tfInfo);
        }
        deleteQuery(query);
    }

    // Get all data tags for each test function in each test case and output
    // hierarchical structure to reply ...
    for (int i = 0; i < tcInfos_.tcInfos.size(); ++i) {

        reply += QString("<tc n=\"%1\">").arg(tcInfos_.tcInfos.at(i)->name);

        for (int j = 0; j < tcInfos_.tcInfos.at(i)->tfInfos.size(); ++j) {

            reply += QString("<tf n=\"%1\">").arg(tcInfos_.tcInfos.at(i)->tfInfos.at(j)->name);

            query = createQuery();
            if (!query->exec(
                    QString(
                        "SELECT DISTINCT dataTag FROM benchmark"
                        " WHERE testCase = '%1'"
                        "   AND testFunction = '%2'"
                        " ORDER BY dataTag ASC;")
                    .arg(tcInfos_.tcInfos.at(i)->name)
                    .arg(tcInfos_.tcInfos.at(i)->tfInfos.at(j)->name))) {
                reply = errorReply(
                    *query, name(), QString("failed to get data tags (exec() failed)"));
                deleteQuery(query);
                return xmlConvert(reply);
            }
            while (query->next()) {
                reply += QString("<dt n=\"%1\" />").arg(query->value(0).toString());
            }
            deleteQuery(query);

            reply += "</tf>";
        }

        reply += "</tc>";
    }

    reply += "</reply>";

    return xmlConvert(reply);
}

void BMRequest_GetBMTree::handleReply_Raw(const QStringList &args) const
{
    printf("NOTE: raw output not supported yet; printing JSON output for now:\n\n");
    handleReply_JSON(args);
}

void BMRequest_GetBMTree::handleReply_JSON(const QStringList &args) const
{
    Q_UNUSED(args);

    const QString error =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();

    QString reply;

    if (error.isEmpty()) {

        reply = QString("{\n\"testCases\":\n[\n");

        QDomNodeList testCaseNodes = doc.elementsByTagName("tc");
        for (int i = 0; i < testCaseNodes.size(); ++i) {
            QDomElement testCaseElem = testCaseNodes.at(i).toElement();
            reply += QString("%1{\"name\": \"%2\",\n\"testFunctions\":\n[\n")
                .arg((i == 0) ? "" : ",\n")
                .arg(testCaseElem.attributeNode("n").value());

            QDomNodeList testFunctionNodes = testCaseElem.elementsByTagName("tf");
            for (int j = 0; j < testFunctionNodes.size(); ++j) {
                QDomElement testFunctionElem = testFunctionNodes.at(j).toElement();
                reply += QString("%1{\"name\": \"%2\",\n\"dataTags\":\n[\n")
                    .arg((j == 0) ? "" : ",\n")
                    .arg(testFunctionElem.attributeNode("n").value());

                QDomNodeList dataTagNodes = testFunctionElem.elementsByTagName("dt");
                for (int k = 0; k < dataTagNodes.size(); ++k) {
                    QDomElement dataTagElem = dataTagNodes.at(k).toElement();
                    reply += QString("%1\"%2\"")
                        .arg((k == 0) ? "" : ",\n")
                        .arg(dataTagElem.attributeNode("n").value());
                }

                reply += "\n]\n}";
            }

            reply += "\n]\n}";
        }

        reply += "\n]\n}";

    } else {
        reply = QString("{\"error\": \"error getting benchmark tree: %1\"\n}").arg(error);
    }

    BMMisc::printJSONOutput(reply);
}


// --- GetIXHistories ---
QByteArray BMRequest_GetIXHistories::toRequestBuffer(QString *)
{
    QString request =
        QString(
            "<request type=\"%1\"><args evalTimestamp=\"%2\" cacheKey=\"%3\" />")
        .arg(name()).arg(evalTimestamp).arg(cacheKey);

    for (int i = 0; i < rankedInfos.size(); ++i)
        request += QString(
            "<rankedInfo id=\"%1\" basePos=\"%2\" diffPos1=\"%3\" diffPos2=\"%4\" descr=\"%5\" />")
            .arg(rankedInfos.at(i).bmcontextId)
            .arg(rankedInfos.at(i).basePos)
            .arg(rankedInfos.at(i).diffPos1)
            .arg(rankedInfos.at(i).diffPos2)
            .arg(rankedInfos.at(i).descr);

    request += "</request>";

    return xmlConvert(request);
}

QByteArray BMRequest_GetIXHistories::toReplyBuffer()
{
    QDomElement argsElem = doc.elementsByTagName("args").at(0).toElement();

    QString error;
    QString reply;
    bool ok;

    // Check if a cached reply can be returned ...
    int cacheKey_ = argsElem.attributeNode("cacheKey").value().toInt(&ok);
    if (ok) {
        reply = Cache::instance()->get(cacheKey_);
        if (!reply.isEmpty())
            return xmlConvert(reply);
    }

    // Compute from scratch ...

    // Get eval timestamp ...
    evalTimestamp = argsElem.attributeNode("evalTimestamp").value().toInt(&ok);
    Q_ASSERT(ok);

    // Get ranked infos ...
    QDomNodeList rankedInfoNodes = doc.elementsByTagName("rankedInfo");
    for (int i = 0; i < rankedInfoNodes.size(); ++i) {
        QDomElement rankedInfoElem = rankedInfoNodes.at(i).toElement();
        const int bmcontextId = rankedInfoElem.attributeNode("id").value().toInt(&ok);
        Q_ASSERT(ok);
        const int basePos = rankedInfoElem.attributeNode("basePos").value().toInt(&ok);
        Q_ASSERT(ok);
        const int diffPos1 = rankedInfoElem.attributeNode("diffPos1").value().toInt(&ok);
        Q_ASSERT(ok);
        const int diffPos2 = rankedInfoElem.attributeNode("diffPos2").value().toInt(&ok);
        Q_ASSERT(ok);
        const QString descr = rankedInfoElem.attributeNode("descr").value();

        rankedInfos.append(Index::RankedInfo(bmcontextId, basePos, diffPos1, diffPos2, descr));
    }

    QSqlQuery *query;

    reply = QString("<reply type=\"%1\" >").arg(name());

    // Add result histories to reply ...
    for (int i = 0; i < rankedInfos.size(); ++i) {

        // Get meta info ...
        query = createQuery();
        if (!query->exec(
                QString(
                    "SELECT testCase, testFunction, dataTag, metric.name, platform.name, "
                    "       host.name, branch.gitRepo, branch.gitBranch "
                    "  FROM bmcontext, benchmark, context, metric, platform, host, branch"
                    " WHERE benchmarkid = benchmark.id"
                    "   AND contextId = context.id"
                    "   AND metricId = metric.id"
                    "   AND platformId = platform.id"
                    "   AND hostId = host.id"
                    "   AND branchId = branch.id"
                    "   AND bmcontext.id = %1;")
                .arg(rankedInfos.at(i).bmcontextId))) {
            deleteQuery(query);
            return xmlConvert(
                errorReply(
                    *query, name(), "failed to get result history meta info (exec() failed)"));
        }
        QString testCase;
        QString testFunction;
        QString dataTag;
        QString metric;
        QString platform;
        QString host;
        QString gitRepo;
        QString gitBranch;
        while (query->next()) {
            testCase     = query->value(0).toString();
            testFunction = query->value(1).toString();
            dataTag      = query->value(2).toString();
            metric       = query->value(3).toString();
            platform     = query->value(4).toString();
            host         = query->value(5).toString();
            gitRepo      = query->value(6).toString();
            gitBranch    = query->value(7).toString();
        }
        deleteQuery(query);

        reply += QString(
            "<resultHistory testCase=\"%1\" testFunction=\"%2\" dataTag=\"%3\" metric=\"%4\" "
            "platform=\"%5\" host=\"%6\" gitRepo=\"%7\" gitBranch=\"%8\" basePos=\"%9\" "
            "diffPos1=\"%10\" diffPos2=\"%11\" descr=\"%12\" >")
            .arg(testCase)
            .arg(testFunction)
            .arg(dataTag)
            .arg(metric)
            .arg(platform)
            .arg(host)
            .arg(gitRepo)
            .arg(gitBranch)
            .arg(rankedInfos.at(i).basePos)
            .arg(rankedInfos.at(i).diffPos1)
            .arg(rankedInfos.at(i).diffPos2)
            .arg(rankedInfos.at(i).descr);

        // Get time series ...
        query = createQuery();
        if (!query->exec(
                QString(
                    "SELECT timestamp, value FROM result "
                    "WHERE bmcontextId=%1 "
                    "ORDER BY timestamp ASC;")
                .arg(rankedInfos.at(i).bmcontextId))) {
            deleteQuery(query);
            return xmlConvert(
                errorReply(*query, name(), "failed to get time series (exec() failed)"));
        }
        QList<int> timestamps;
        QList<qreal> values;
        while (query->next()) {
            timestamps += query->value(0).toInt();
            values += query->value(1).toDouble();
        }
        deleteQuery(query);

        for (int j = 0; j < timestamps.size(); ++j)
            reply += QString("<val t=\"%1\" v=\"%2\" />").arg(timestamps.at(j)).arg(values.at(j));

        reply += "</resultHistory>";
    }

    // Finalize the reply and cache it ...
    cacheKey_ = Cache::instance()->nextId();
    reply += QString("<args evalTimestamp=\"%1\" cacheKey=\"%2\" /></reply>")
        .arg(evalTimestamp)
        .arg(cacheKey_);
    Cache::instance()->put(cacheKey_, reply);

    return xmlConvert(reply);
}

void BMRequest_GetIXHistories::handleReply_HTML(const QStringList &args) const
{
    QString error;

    error = doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();
    if (!error.isEmpty()) {
        BMMisc::printHTMLErrorPage(
            QString(
                "failed to create details page for main index contributors: %1").arg(error));
        return;
    }

    QStringList optValues;
    bool ok;

    // Get style sheet ...
    QString styleSheet;
    if (BMMisc::getOption(args, "-stylesheet", &optValues, 1, 0)) {
        styleSheet = optValues.first().trimmed();
    } else {
        BMMisc::printHTMLErrorPage("-stylesheet option not found");
        return;
    }

    // Get median window size ...
    if (BMMisc::getOption(args, "-medianwinsize", &optValues, 1, 0)) {
        medianWinSize = optValues.first().toInt(&ok);
        Q_ASSERT(ok);
    } else {
        BMMisc::printHTMLErrorPage("-medianwinsize option not found");
        return;
    }

    QDomElement argsElem = doc.elementsByTagName("args").at(0).toElement();

    // Get eval timestamp ...
    evalTimestamp = argsElem.attributeNode("evalTimestamp").value().toInt(&ok);
    Q_ASSERT(ok);
    QDateTime evalDateTime;
    evalDateTime.setTime_t(evalTimestamp);

    // *** Header ***
    QString reply = QString("<html>\n<head>\n");
    reply += QString("<link rel=\"stylesheet\" type=\"text/css\" href=\"%1\" />\n").arg(styleSheet);
    reply += QString("</head>\n<body>\n");

    reply += "<span style=\"font-size:18\">Main Index Value Contributors</span>";

    reply += "<br /><br /><table><tr><td style=\"background-color:#eeeeee\">";

    reply += QString(
        "This page shows the %1 result histories that contributed most significantly ")
        .arg(doc.elementsByTagName("resultHistory").size());
    reply += QString("to the index value at timestamp %1 (%2)")
        .arg(evalTimestamp).arg(evalDateTime.toString());

    reply += "<br /><br />";

    reply += QString(
        "Normal data points are indicated in black, whereas potential outliers "
        "(filtered out by median of %1 smoothing) are indicated in red.")
        .arg(medianWinSize);

    reply += "<br /><br />";

    reply += "The result histories are ranked according to the following steps:<br /><ol>";

    reply += "<li>Let <i>baseVal</i> be the first median smoothed value found.</li>";

    reply += "<li>Let <i>val</i><sub>1</sub> and <i>val</i><sub>2</sub> be the the ";
    reply += "median smoothed values at the ";
    reply += "previous and selected evaluation time respectively.</li>";

    reply += "<li>Let <i>diffChange</i> be the change in metric-adjusted log2 difference ";
    reply += "from the previous to the selected evaluation time: ";
    reply += "ma_log<sub>2</sub>(<i>val</i><sub>2</sub>&nbsp;/&nbsp;<i>baseVal</i>)";
    reply += "&nbsp;-&nbsp;";
    reply += "ma_log<sub>2</sub>(<i>val</i><sub>1</sub>&nbsp;/&nbsp;<i>baseVal</i>) ";
    reply += "<br />(<b>Note:</b> a metric-adjusted log2 value eliminates the distinction ";
    reply += "between \"lower is better\" and \"higher is better\"by ensuring that e.g. ";
    reply += "-1, 0, 1, and 2 always mean halved, unchanged, doubled, and quadrupled performance ";
    reply += "respectively).";

    reply += "<li>Rank in descending order on either <i>diffChange</i> or -<i>diffChange</i> ";
    reply += "depending on whether the index value went up or down from the previous to the ";
    reply += "selected evaluation time respectively.</li>";

    reply += "</ol>";

    reply += "The data points used for <i>baseVal</i>, <i>val</i><sub>1</sub>, and ";
    reply += "<i>val</i><sub>2</sub> are indicated in the graph as a blue circle, an orange ";
    reply += "square, and an orange circle respectively.";

    reply += "<br /><br />";
    reply += "The selected evaluation time is indicated by a blue vertical line ";
    reply += "(unless it falls outside the global time range of the displayed result histories).";

    reply += "</td></tr></table>\n";


    // *** Plot ***

    // reply += "<br />args:<br />";
    // for (int i = 0; i < args.size(); ++i)
    //     reply += QString("arg %1: &gt;%2&lt;<br />").arg(i).arg(args.at(i));
    // reply += "<br />";

    reply += "<br />\n<img src=\"bmclientwrapper?command=";
    for (int i = args.indexOf("-server"); i < args.size(); ++i)
        reply += QString("%1 ")
            .arg((args.at(i) == "detailspage") ? QLatin1String("plot") : args.at(i));
    reply += QString("-cachekey %1 ").arg(argsElem.attributeNode("cacheKey").value());
    reply += "-httpheader ";
    reply += "\" />\n<br />\n<br />\n";


    reply += "</body></html>";

    BMMisc::printHTMLOutput(reply);
}

void BMRequest_GetIXHistories::handleReply_Image(const QStringList &args) const
{
    const QString error =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();

    if (!error.isEmpty()) {
        BMMisc::printHTMLErrorPage(
            QString(
                "failed to create plot for main index contributors: %1").arg(error));
        return;
    }

    bool ok;
    QStringList optValues;

    // Get median window size ...
    if (BMMisc::getOption(args, "-medianwinsize", &optValues, 1, 0)) {
        medianWinSize = optValues.first().toInt(&ok);
        Q_ASSERT(ok);
    } else {
        BMMisc::printHTMLErrorPage("-medianwinsize option not found");
        return;
    }

    QDomElement argsElem = doc.elementsByTagName("args").at(0).toElement();

    // Get eval timestamp ...
    evalTimestamp = argsElem.attributeNode("evalTimestamp").value().toInt(&ok);
    Q_ASSERT(ok);

    QList<ResultHistoryInfo *> rhInfos;
    QList<int> basePos;
    QList<QList<int> > extraPos;
    QStringList descr;

    // Loop over result histories ...
    QDomNodeList rhNodes = doc.elementsByTagName("resultHistory");
    for (int i = 0; i < rhNodes.size(); ++i) {
        QDomElement rhElem = rhNodes.at(i).toElement();

        // Get the context ...
        const QString metric = rhElem.attributeNode("metric").value();
        const QString platform = rhElem.attributeNode("platform").value();
        const QString host = rhElem.attributeNode("host").value();
        const QString gitRepo = rhElem.attributeNode("gitRepo").value();
        const QString gitBranch = rhElem.attributeNode("gitBranch").value();

        // Get the benchmark ...
        const QString testCase = rhElem.attributeNode("testCase").value();
        const QString testFunction = rhElem.attributeNode("testFunction").value();
        const QString dataTag = rhElem.attributeNode("dataTag").value();

        // Get the time series ...
        QList<int> timestamps;
        QList<qreal> values;
        QDomNodeList valueNodes = rhElem.elementsByTagName("val");
        for (int j = 0; j < valueNodes.size(); ++j) {
            bool ok;
            QDomElement valueElem = valueNodes.at(j).toElement();
            timestamps += valueElem.attributeNode("t").value().toInt(&ok);
            Q_ASSERT(ok);
            values += valueElem.attributeNode("v").value().toDouble(&ok);
            Q_ASSERT(ok);
        }

        rhInfos.append(
            new ResultHistoryInfo(
                -1, timestamps, values, medianWinSize, metric, platform, host, gitRepo, gitBranch,
                testCase, testFunction, dataTag));

        // Get the base- and diff positions and description ...
        const int basePos_ = rhElem.attributeNode("basePos").value().toInt(&ok);
        Q_ASSERT(ok);
        const int diffPos1 = rhElem.attributeNode("diffPos1").value().toInt(&ok);
        Q_ASSERT(ok);
        const int diffPos2 = rhElem.attributeNode("diffPos2").value().toInt(&ok);
        Q_ASSERT(ok);

        basePos.append(basePos_);
        extraPos.append(QList<int>() << diffPos1 << diffPos2);

        descr.append(rhElem.attributeNode("descr").value());
    }

    // Create image ...
    QString error_;
    Plotter *plotter =
        new HistoriesPlotter(
            rhInfos, true, evalTimestamp, &basePos, &extraPos, 0, &descr, false, true);
    const QImage image = plotter->createImage(&error_);

    // Free dynamic memory ...
    for (int i = 0; i < rhInfos.size(); ++i)
        delete rhInfos.at(i);

    // Output image ...
    if (!image.isNull()) {
        BMMisc::printImageOutput(
            image, "png", QString(), BMMisc::hasOption(args, "-httpheader"));
    } else {
        BMMisc::printHTMLErrorPage(
            QString(
                "failed to create plot for main index contributors: %1").arg(error_));
    }
}


// --- ASFStatsGetValues ---
QByteArray BMRequest_ASFStatsGetValues::toRequestBuffer(QString *)
{
    QString request =
        QString(
            "<request type=\"%1\"><args medianWinSize=\"%2\" cacheKey=\"%3\" "
            "fromTimestamp=\"%4\" toTimestamp=\"%5\" diffTol=\"%6\" stabTol=\"%7\" "
            "sfTol=\"%8\" lfTol=\"%9\" maxLDTol=\"%10\" />")
        .arg(name())
        .arg(medianWinSize)
        .arg(cacheKey)
        .arg(fromTimestamp)
        .arg(toTimestamp)
        .arg(diffTol)
        .arg(stabTol)
        .arg(sfTol)
        .arg(lfTol)
        .arg(maxLDTol);

    for (int i = 0; i < testCaseFilter.size(); ++i)
        request += QString("<testCase name=\"%1\" />").arg(testCaseFilter.at(i));

    for (int i = 0; i < metricFilter.size(); ++i)
        request += QString("<metric name=\"%1\" />").arg(metricFilter.at(i));

    for (int i = 0; i < platformFilter.size(); ++i)
        request += QString("<platform name=\"%1\" />").arg(platformFilter.at(i));

    for (int i = 0; i < hostFilter.size(); ++i)
        request += QString("<host name=\"%1\" />").arg(hostFilter.at(i));

    for (int i = 0; i < branchFilter.size(); ++i)
        request += QString("<branch name=\"%1\" />").arg(branchFilter.at(i));

    request += "</request>";

    return xmlConvert(request);
}

QByteArray BMRequest_ASFStatsGetValues::toReplyBuffer()
{
    QDomElement argsElem = doc.elementsByTagName("args").at(0).toElement();

    QString error;
    QString reply;
    bool ok;

    const bool debugPrint = false;

    // Check if a cached reply can be returned ...
    int cacheKey_ = argsElem.attributeNode("cacheKey").value().toInt(&ok);
    if (debugPrint) {
        static int calls_ = 0;
        fprintf(
            stderr,
            "BMRequest_ASFStatsGetValues::toReplyBuffer() called (%d), cacheKey: >%s<\n",
            calls_++, argsElem.attributeNode("cacheKey").value().toLatin1().data());
    }
    if (ok) {
        reply = Cache::instance()->get(cacheKey_);
        if (!reply.isEmpty()) {
            if (debugPrint)
                fprintf(stderr, "returning cached reply (key = %d)\n", cacheKey_);
            return xmlConvert(reply);
        }
    }

    // Compute from scratch ...
    if (debugPrint)
        fprintf(stderr, "computing from scratch ...\n");

    // Get from timestamp ...
    fromTimestamp = argsElem.attributeNode("fromTimestamp").value().toInt(&ok);
    Q_ASSERT(ok);

    // Get to timestamp ...
    toTimestamp = argsElem.attributeNode("toTimestamp").value().toInt(&ok);
    Q_ASSERT(ok);

    // Get median window size ...
    medianWinSize = argsElem.attributeNode("medianWinSize").value().toInt(&ok);
    Q_ASSERT(ok);

    // Get tolerance values ...
    diffTol = argsElem.attributeNode("diffTol").value().toDouble(&ok);
    Q_ASSERT(ok);
    stabTol = argsElem.attributeNode("stabTol").value().toInt(&ok);
    Q_ASSERT(ok);
    sfTol = argsElem.attributeNode("sfTol").value().toDouble(&ok);
    Q_ASSERT(ok);
    lfTol = argsElem.attributeNode("lfTol").value().toDouble(&ok);
    Q_ASSERT(ok);
    maxLDTol = argsElem.attributeNode("maxLDTol").value().toDouble(&ok);
    Q_ASSERT(ok);

    // Get filters ...
    QDomNodeList testCaseNodes = doc.elementsByTagName("testCase");
    for (int i = 0; i < testCaseNodes.size(); ++i)
        testCaseFilter.append(testCaseNodes.at(i).toElement().attributeNode("name").value());

    QDomNodeList metricNodes = doc.elementsByTagName("metric");
    for (int i = 0; i < metricNodes.size(); ++i)
        metricFilter.append(metricNodes.at(i).toElement().attributeNode("name").value());

    QDomNodeList platformNodes = doc.elementsByTagName("platform");
    for (int i = 0; i < platformNodes.size(); ++i)
        platformFilter.append(platformNodes.at(i).toElement().attributeNode("name").value());

    QDomNodeList hostNodes = doc.elementsByTagName("host");
    for (int i = 0; i < hostNodes.size(); ++i)
        hostFilter.append(hostNodes.at(i).toElement().attributeNode("name").value());

    QDomNodeList branchNodes = doc.elementsByTagName("branch");
    for (int i = 0; i < branchNodes.size(); ++i)
        branchFilter.append(branchNodes.at(i).toElement().attributeNode("name").value());


    QList<ResultHistoryInfo *> rhInfos;
    int totRHCandidates;
    if (!createFilteredRHInfos(
            &rhInfos, &totRHCandidates, name(), database, testCaseFilter, metricFilter,
            platformFilter, hostFilter, branchFilter, medianWinSize, &error, debugPrint)) {

        // Free dynamic memory ...
        for (int i = 0; i < rhInfos.size(); ++i)
            delete rhInfos.at(i);

        return xmlConvert(error);
    }


    reply = QString("<reply type=\"%1\" >").arg(name());

    ASFStats asfStats(diffTol, stabTol, sfTol, lfTol, maxLDTol, fromTimestamp, toTimestamp);
    ASFStats::StatsInfo statsInfo;
    asfStats.compute(rhInfos, &statsInfo);

    // Add stats to reply ...
    reply += QString(
        "<stats regressed=\"%1\" unchanged=\"%2\" improved=\"%3\" unstable=\"%4\" "
        "usZeroCount=\"%5\" usLowFromPosCount=\"%6\" usLowSFCount=\"%7\" usLowLFCount=\"%8\" "
        "usHighMaxLDCount=\"%9\" />")
        .arg(statsInfo.regressed.count(true))
        .arg(statsInfo.unchanged.count(true))
        .arg(statsInfo.improved.count(true))
        .arg(statsInfo.unstable.count(true))
        .arg(statsInfo.usZero.count(true))
        .arg(statsInfo.usLowFromPos.count(true))
        .arg(statsInfo.usLowSF.count(true))
        .arg(statsInfo.usLowLF.count(true))
        .arg(statsInfo.usHighMaxLD.count(true));

    // Add result history stats to reply ...
    for (int i = 0; i < rhInfos.size(); ++i) {
        reply += QString(
            "<rh id=\"%1\" rg=\"%2\" uc=\"%3\" im=\"%4\" zr=\"%5\" fp=\"%6\" "
            "sf=\"%7\" lf=\"%8\" ld=\"%9\" />")
            .arg(rhInfos.at(i)->bmcontextId())
            .arg(statsInfo.regressed.testBit(i))
            .arg(statsInfo.unchanged.testBit(i))
            .arg(statsInfo.improved.testBit(i))
            .arg(statsInfo.usZero.testBit(i))
            .arg(statsInfo.usLowFromPos.testBit(i))
            .arg(statsInfo.usLowSF.testBit(i))
            .arg(statsInfo.usLowLF.testBit(i))
            .arg(statsInfo.usHighMaxLD.testBit(i));
    }

    //----------------------------------------------------------------------------------------

    for (int i = 0; i < testCaseFilter.size(); ++i)
        reply += QString("<testCase name=\"%1\" />").arg(testCaseFilter.at(i));

    for (int i = 0; i < metricFilter.size(); ++i)
        reply += QString("<metric name=\"%1\" />").arg(metricFilter.at(i));

    for (int i = 0; i < platformFilter.size(); ++i)
        reply += QString("<platform name=\"%1\" />").arg(platformFilter.at(i));

    for (int i = 0; i < hostFilter.size(); ++i)
        reply += QString("<host name=\"%1\" />").arg(hostFilter.at(i));

    for (int i = 0; i < branchFilter.size(); ++i)
        reply += QString("<branch name=\"%1\" />").arg(branchFilter.at(i));

    // Finalize the reply and cache it ...
    cacheKey_ = Cache::instance()->nextId();
    reply += QString(
        "<args medianWinSize=\"%1\" cacheKey=\"%2\" "
            "fromTimestamp=\"%3\" toTimestamp=\"%4\" diffTol=\"%5\" stabTol=\"%6\" "
            "sfTol=\"%7\" lfTol=\"%8\" maxLDTol=\"%9\" /></reply>")
        .arg(medianWinSize).arg(cacheKey_).arg(fromTimestamp).arg(toTimestamp).arg(diffTol)
        .arg(stabTol).arg(sfTol).arg(lfTol).arg(maxLDTol);
    Cache::instance()->put(cacheKey_, reply);

    // Free dynamic memory ...
    for (int i = 0; i < rhInfos.size(); ++i)
        delete rhInfos.at(i);

    return xmlConvert(reply);
}

// ### 2 B DOCUMENTED!
struct RHStats {
    int id; // BM context ID
    bool rg; // regressed
    bool uc; // unchanged
    bool im; // improved

    // Conditions for being unstable:
    bool zr; // existence of zeros
    bool fp; // 'from' position to low (i.e. no data before the 'from' time)
    bool sf; // stability fraction too low
    bool lf; // unique level fraction too low
    bool ld; // max level difference to high

    RHStats(
        const int id, const bool rg, const bool uc, const bool im, const bool zr, const bool fp,
        const bool sf, const bool lf, const bool ld)
        : id(id), rg(rg), uc(uc), im(im), zr(zr), fp(fp), sf(sf), lf(lf), ld(ld) {}

    bool unstable() const { return (!rg) && (!uc) && (!im); }

    struct LessThan {
        LessThan() {}
        bool operator()(const RHStats *s1, const RHStats *s2) {

            // Ranking priority:
            // 1: Regressed (ranked first)
            // 2: Unchanged
            // 3: Improved
            // 4: All others (ranked last)

            if (s1->rg && !s2->rg)
                return true;
            if (!s1->rg && s2->rg)
                return false;

            if (s1->uc && !s2->uc)
                return true;
            if (!s1->uc && s2->uc)
                return false;

            if (s1->im && !s2->im)
                return true;
            if (!s1->im && s2->im)
                return false;

            return false;
        }
    };
};

// ### 2 B DOCUMENTED!
static QString createRHDetailsLink(
    const QString &webServer, const QString &server, const QString &styleSheet, int bmcontextId,
    int medianWinSize, int fromTime, int toTime)
{
    QString url;
    url = QString("%1/cgi-bin/bmclientwrapper").arg(webServer);
    url += QString("?command=-server %1").arg(server);
    url += QString(
        " asfstats get detailspage2 -bmcontextid %1 -medianwinsize %2 -fromtime %3 -totime %4"
        " -stylesheet %5")
        .arg(bmcontextId)
        .arg(medianWinSize)
        .arg(fromTime)
        .arg(toTime)
        .arg(styleSheet);

    return QString("<a href=\"%1\">details</a>").arg(url);
}

void BMRequest_ASFStatsGetValues::handleReply_HTML(const QStringList &args) const
{
    QString error;

    error = doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();
    if (!error.isEmpty()) {
        BMMisc::printHTMLErrorPage(
            QString("failed to create details page: error evaluating ASF stats: %1").arg(error));
        return;
    }

    QStringList optValues;

    // Get server ...
    QString server;
    if (BMMisc::getOption(args, "-server", &optValues, 1, 0, &error)) {
        server = optValues.first().trimmed();
    } else {
        BMMisc::printHTMLErrorPage("-server option not found");
        return;
    }

    // Get style sheet ...
    QString styleSheet;
    if (BMMisc::getOption(args, "-stylesheet", &optValues, 1, 0, &error)) {
        styleSheet = optValues.first().trimmed();
    } else {
        BMMisc::printHTMLErrorPage("-stylesheet option not found");
        return;
    }

    // Extract the web server from the style sheet ...
    QRegExp rx("^(\\S+:\\d+)\\D+$");
    if (rx.indexIn(styleSheet) == -1) {
        BMMisc::printHTMLErrorPage(QString("failed to extract web server from style sheet"));
        return;
    }
    const QString webServer = rx.cap(1);

    // *** Header ***
    QString reply = QString("<html>\n<head>\n");
    reply += QString("<link rel=\"stylesheet\" type=\"text/css\" href=\"%1\" />\n").arg(styleSheet);
    reply += QString("</head>\n<body>\n");

    reply += "<span style=\"font-size:18\">ASF Stats (for scorecard)</span>";

    reply += "<br /><br /><table><tr><td style=\"background-color:#eeeeee\">";

    reply += "This page shows statistics suitable for presentation on the ASF Scorecard.";

    reply += "</td></tr></table>\n";

    QDomElement argsElem = doc.elementsByTagName("args").at(0).toElement();

    bool ok;


    reply += "\n<br /><br /><table style=\"border:0px\">\n";
    reply += "<tr>\n";

    // Left main table ...

    reply += "<td style=\"border:0px\">\n";

    reply += "<fieldset><legend style=\"font-size:12\">Parameters</legend>";

    // 'Misc' table ...
    fromTimestamp = argsElem.attributeNode("fromTimestamp").value().toInt(&ok);
    Q_ASSERT(ok);
    toTimestamp = argsElem.attributeNode("toTimestamp").value().toInt(&ok);
    Q_ASSERT(ok);
    medianWinSize = argsElem.attributeNode("medianWinSize").value().toInt(&ok);
    Q_ASSERT(ok);
    diffTol = argsElem.attributeNode("diffTol").value().toDouble(&ok);
    Q_ASSERT(ok);
    stabTol = argsElem.attributeNode("stabTol").value().toInt(&ok);
    Q_ASSERT(ok);
    sfTol = argsElem.attributeNode("sfTol").value().toInt(&ok);
    Q_ASSERT(ok);
    lfTol = argsElem.attributeNode("lfTol").value().toInt(&ok);
    Q_ASSERT(ok);
    maxLDTol = argsElem.attributeNode("maxLDTol").value().toDouble(&ok);
    Q_ASSERT(ok);

    QDateTime dateTime;
    reply += "\n<br /><table style=\"text-align:right\">\n";
    dateTime.setTime_t(fromTimestamp);
    reply += QString("<tr><td>From time:</td><td>%1</td></tr>\n").arg(dateTime.toString());
    dateTime.setTime_t(toTimestamp);
    reply += QString("<tr><td>To time:</td><td>%1</td></tr>\n").arg(dateTime.toString());
    reply += QString("<tr><td>Median window size:</td><td>%1</td></tr>\n").arg(medianWinSize);
    reply += QString("<tr><td>Difference tolerance (%):</td><td>%1</td></tr>\n").arg(diffTol);
    reply += QString("<tr><td>Stability tolerance:</td><td>%1</td></tr>\n").arg(stabTol);
    reply += QString("<tr><td>Stability fraction tolerance (%):</td><td>%1</td></tr>\n").arg(sfTol);
    reply += QString("<tr><td>Unique level fraction tolerance (%):</td><td>%1</td></tr>\n")
        .arg(lfTol);
    reply += QString("<tr><td>Maximim level distance tolerance (%):</td><td>%1</td></tr>\n")
        .arg(maxLDTol);
    reply += "</table>\n";


    // 'Filters' table ...
    QDomNodeList testCaseNodes = doc.elementsByTagName("testCase");
    for (int i = 0; i < testCaseNodes.size(); ++i)
        testCaseFilter.append(testCaseNodes.at(i).toElement().attributeNode("name").value());

    QDomNodeList metricNodes = doc.elementsByTagName("metric");
    for (int i = 0; i < metricNodes.size(); ++i)
        metricFilter.append(metricNodes.at(i).toElement().attributeNode("name").value());

    QDomNodeList platformNodes = doc.elementsByTagName("platform");
    for (int i = 0; i < platformNodes.size(); ++i)
        platformFilter.append(platformNodes.at(i).toElement().attributeNode("name").value());

    QDomNodeList hostNodes = doc.elementsByTagName("host");
    for (int i = 0; i < hostNodes.size(); ++i)
        hostFilter.append(hostNodes.at(i).toElement().attributeNode("name").value());

    QDomNodeList branchNodes = doc.elementsByTagName("branch");
    for (int i = 0; i < branchNodes.size(); ++i)
        branchFilter.append(branchNodes.at(i).toElement().attributeNode("name").value());

    reply += "\n<br /><b>Filters:</b><table>\n";
    appendToFilterTable(testCaseFilter, "Test case", &reply);
    appendToFilterTable(metricFilter, "Metric", &reply);
    appendToFilterTable(platformFilter, "Platform", &reply);
    appendToFilterTable(hostFilter, "Host", &reply);
    appendToFilterTable(branchFilter, "Branch", &reply);
    reply += "</table>";

    reply += "</fieldset>";

    reply += "</td>\n";

    // Right main table ...

    reply += "<td style=\"border:0px\">\n";

    // 'Stats' table ...

    QDomNodeList statsNodes = doc.elementsByTagName("stats");
    QDomElement statsElem = statsNodes.at(0).toElement();

    reply += "<fieldset><legend style=\"font-size:12\">Statistics</legend>";

    const int regressed = statsElem.attributeNode("regressed").value().toInt(&ok);
    Q_ASSERT(ok);
    const int unchanged = statsElem.attributeNode("unchanged").value().toInt(&ok);
    Q_ASSERT(ok);
    const int improved = statsElem.attributeNode("improved").value().toInt(&ok);
    Q_ASSERT(ok);
    const int unstable = statsElem.attributeNode("unstable").value().toInt(&ok);
    Q_ASSERT(ok);
    const int total = regressed + unchanged + improved + unstable;
    const int usZeroCount = statsElem.attributeNode("usZeroCount").value().toInt(&ok);
    Q_ASSERT(ok);
    const int usLowFromPosCount =
        statsElem.attributeNode("usLowFromPosCount").value().toInt(&ok);
    Q_ASSERT(ok);
    const int usLowSFCount = statsElem.attributeNode("usLowSFCount").value().toInt(&ok);
    Q_ASSERT(ok);
    const int usLowLFCount = statsElem.attributeNode("usLowLFCount").value().toInt(&ok);
    Q_ASSERT(ok);
    const int usHighMaxLDCount = statsElem.attributeNode("usHighMaxLDCount").value().toInt(&ok);
    Q_ASSERT(ok);

    const QString usColor("#ffffdd"); // unstable
    const QString rgColor("#ffdddd"); // regressed
    const QString ucColor("#ffffff"); // unchanged
    const QString imColor("#ddffdd"); // improved

    reply += "<br /><table style=\"text-align:right\">\n";

    reply += QString("<tr><td><b>Total:</b></td><td><b>%1</b></td><td><b>100.00 %</b></td></tr>\n")
        .arg(total);
    if (total > 0) {
        reply += QString(
            "<tr style=\"background-color:%1\"><td>Regressed:</td><td>%2</td><td>%3 %</td></tr>\n")
            .arg(rgColor).arg(regressed)
            .arg(QString().setNum(100 * (regressed / qreal(total)), 'f', 2));
        reply += QString(
            "<tr style=\"background-color:%1\"><td>Unchanged:</td><td>%2</td><td>%3 %</td></tr>\n")
            .arg(ucColor).arg(unchanged)
            .arg(QString().setNum(100 * (unchanged / qreal(total)), 'f', 2));
        reply += QString(
            "<tr style=\"background-color:%1\"><td>Improved:</td><td>%2</td><td>%3 %</td></tr>\n")
            .arg(imColor).arg(improved)
            .arg(QString().setNum(100 * (improved / qreal(total)), 'f', 2));
        reply += QString(
            "<tr style=\"background-color:%1\"><td>Unstable:</td><td>%2</td><td>%3 %</td></tr>\n")
            .arg(usColor).arg(unstable)
            .arg(QString().setNum(100 * (unstable / qreal(total)), 'f', 2));
    }

    reply += "</table>\n";

    if (unstable > 0) {
        reply += "<br /><b>Categories of instabilities:</b>";
        reply += QString("<table style=\"text-align:right; background-color:%1\">\n").arg(usColor);
        reply += QString(
            "<tr><td>Existence of zeros within the [from,&nbsp;to] time range:</td>"
            "<td>%1</td><td>%2 %</td></tr>\n")
            .arg(usZeroCount).arg(QString().setNum(100 * (usZeroCount / qreal(unstable)), 'f', 2));
        reply += QString("<tr><td>No data before 'from' time:</td><td>%1</td><td>%2 %</td></tr>\n")
            .arg(usLowFromPosCount)
            .arg(QString().setNum(100 * (usLowFromPosCount / qreal(unstable)), 'f', 2));
        reply += QString(
            "<tr><td>Stability fraction too low:</td><td>%1</td><td>%2 %</td></tr>\n")
            .arg(usLowSFCount)
            .arg(QString().setNum(100 * (usLowSFCount / qreal(unstable)), 'f', 2));
        reply += QString(
            "<tr><td>Unique level fraction too low:</td><td>%1</td><td>%2 %</td></tr>\n")
            .arg(usLowLFCount)
            .arg(QString().setNum(100 * (usLowLFCount / qreal(unstable)), 'f', 2));
        reply += QString(
            "<tr><td>Max level difference too high within the [from,&nbsp;to] time range:</td>"
            "<td>%1</td><td>%2 %</td></tr>\n")
            .arg(usHighMaxLDCount)
            .arg(QString().setNum(100 * (usHighMaxLDCount / qreal(unstable)), 'f', 2));
        reply += "</table>\n";
    }

    reply += "</fieldset>";

    reply += "</td>\n";

    reply += "</tr>\n";
    reply += "<table>\n";


    // Result history details table ...

    reply += "<br /><br />";

    reply += "<table style=\"background-color:#eeeeee\">\n";
    reply += "<tr><td style=\"text-align:right\">RG:</td><td>Regressed</td></tr>";
    reply += "<tr><td style=\"text-align:right\">UC:</td><td>Unchanged</td></tr>";
    reply += "<tr><td style=\"text-align:right\">IM:</td><td>Improved</td></tr>";
    reply += "<tr><td style=\"text-align:right\">ZR:</td><td>Existence of zeros within the "
        "[from,&nbsp;to] time range</td></tr>";
    reply += "<tr><td style=\"text-align:right\">FP:</td>"
        "<td>'From' position to low (i.e. no data before the 'from' time)</td></tr>";
    reply += "<tr><td style=\"text-align:right\">SF:</td><td>Stability fraction too low</td></tr>";
    reply += "<tr><td style=\"text-align:right\">LF:</td>"
        "<td>Unique level fraction too low</td></tr>";
    reply += "<tr><td style=\"text-align:right\">LD:</td>"
        "<td>Max level difference to high within the [from,&nbsp;to] time range </td></tr>";
    reply += "</table>\n";

    reply += "<br /><table style=\"text-align:right\">\n";

    QList<RHStats *> rhStats;
    QDomNodeList rhNodes = doc.elementsByTagName("rh");
    for (int i = 0; i < rhNodes.size(); ++i) {
        QDomElement rhElem = rhNodes.at(i).toElement();
        rhStats.append(
            new RHStats(
                rhElem.attributeNode("id").value().toInt(),
                rhElem.attributeNode("rg").value().toInt(),
                rhElem.attributeNode("uc").value().toInt(),
                rhElem.attributeNode("im").value().toInt(),
                rhElem.attributeNode("zr").value().toInt(),
                rhElem.attributeNode("fp").value().toInt(),
                rhElem.attributeNode("sf").value().toInt(),
                rhElem.attributeNode("lf").value().toInt(),
                rhElem.attributeNode("ld").value().toInt()
                )
            );
    }

    qSort(rhStats.begin(), rhStats.end(), RHStats::LessThan());

    for (int i = 0; i < rhStats.size(); ++i) {

        const QString bgColor =
            (rhStats.at(i)->unstable())
            ? usColor
            : (rhStats.at(i)->rg
               ? rgColor
               : (rhStats.at(i)->uc
                  ? ucColor
                  : imColor));
        reply += QString(
            "<tr style=\"background-color:%1\">"
            "<td>%2</td>"
            "<td>%3</td>"
            "<td>%4</td>"
            "<td>%5</td>"
            "<td>%6</td>"
            "<td>%7</td>"
            "<td>%8</td>"
            "<td>%9</td>"
            "<td>%10</td>"
            "<td>%11</td>"
            "</tr>\n")
            .arg(bgColor)
            .arg(i)
            .arg(createRHDetailsLink(
                     webServer, server, styleSheet, rhStats.at(i)->id, medianWinSize,
                     fromTimestamp, toTimestamp))
            .arg(rhStats.at(i)->rg ? "RG" : "")
            .arg(rhStats.at(i)->uc ? "UC" : "")
            .arg(rhStats.at(i)->im ? "IM" : "")
            .arg(rhStats.at(i)->zr ? "ZR" : "")
            .arg(rhStats.at(i)->fp ? "FP" : "")
            .arg(rhStats.at(i)->sf ? "SF" : "")
            .arg(rhStats.at(i)->lf ? "LF" : "")
            .arg(rhStats.at(i)->ld ? "LD" : "");
    }

    // Free dynamic memory ...
    for (int i = 0; i < rhStats.size(); ++i)
        delete rhStats.at(i);

    reply += "</table>";


    reply += "</body></html>";

    BMMisc::printHTMLOutput(reply);
}


// --- ASFStatsGetValues2 ---
QByteArray BMRequest_ASFStatsGetValues2::toRequestBuffer(QString *)
{
    const QString request =
        QString("<request type=\"%1\"><args bmcontextId=\"%2\" cacheKey=\"%3\" /></request>")
        .arg(name())
        .arg(bmcontextId)
        .arg(cacheKey);

    return xmlConvert(request);
}

QByteArray BMRequest_ASFStatsGetValues2::toReplyBuffer()
{
    QString error;
    QString reply;
    bool ok;

    QDomElement argsElem = doc.elementsByTagName("args").at(0).toElement();

    // Check if a cached reply can be returned ...
    int cacheKey_ = argsElem.attributeNode("cacheKey").value().toInt(&ok);
    if (ok) {
        reply = Cache::instance()->get(cacheKey_);
        if (!reply.isEmpty())
            return xmlConvert(reply);
    }

    // Compute from scratch ...

    bmcontextId = argsElem.attributeNode("bmcontextId").value().toInt(&ok);
    Q_ASSERT(ok);

    QString testCase;
    QString testFunction;
    QString dataTag;
    QString metric;
    QString platform;
    QString host;
    QString gitRepo;
    QString gitBranch;

    if (!extractBMAndContextFromRH(
            bmcontextId, name(), database, &testCase, &testFunction, &dataTag,
            &metric, &platform, &host, &gitRepo, &gitBranch, &error)) {
        return xmlConvert(error);
    }

    reply = QString("<reply type=\"%1\">").arg(name());

    const qreal diffTolerance = 0.0; // 4 NOW
    const qreal stabTolerance = 0; // 4 NOW
    if (!appendResultHistoryToReply(
            &reply, bmcontextId, "", "first", "last", diffTolerance, stabTolerance, -1))
        return xmlConvert(reply);

    reply += QString(
        "<bmcontext testCase=\"%1\" testFunction=\"%2\" dataTag=\"%3\" metric=\"%4\" "
        "platform=\"%5\" host=\"%6\" gitRepo=\"%7\" gitBranch=\"%8\" />")
        .arg(testCase).arg(testFunction).arg(dataTag).arg(metric).arg(platform)
        .arg(host).arg(gitRepo).arg(gitBranch);

    // Finalize the reply and cache it ...
    cacheKey_ = Cache::instance()->nextId();
    reply += QString("<args cacheKey=\"%1\" /></reply>").arg(cacheKey_);
    Cache::instance()->put(cacheKey_, reply);

    return xmlConvert(reply);
}

void BMRequest_ASFStatsGetValues2::handleReply_HTML(const QStringList &args) const
{
    QString error;

    error = doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();
    if (!error.isEmpty()) {
        BMMisc::printHTMLErrorPage(
            QString(
                "failed to create details page for RH contributing to ASF stats: %1").arg(error));
        return;
    }

    QStringList optValues;

    // Get style sheet ...
    QString styleSheet;
    if (BMMisc::getOption(args, "-stylesheet", &optValues, 1, 0)) {
        styleSheet = optValues.first().trimmed();
    } else {
        BMMisc::printHTMLErrorPage("-stylesheet option not found");
        return;
    }

    // Get median window size ...
    int medianWinSize = -1;
    if (BMMisc::getOption(args, "-medianwinsize", &optValues, 1, 0)) {
        bool ok;
        medianWinSize = optValues.first().toInt(&ok);
        if ((!ok) || (medianWinSize < 1)) {
            BMMisc::printHTMLErrorPage("-medianwinsize not a positive integer");
            return;
        }
    } else {
        BMMisc::printHTMLErrorPage("-medianwinsize option not found");
        return;
    }

    // Get 'from' timestamp ...
    int fromTimestamp = -1;
    if (BMMisc::getOption(args, "-fromtime", &optValues, 1, 0)) {
        bool ok;
        fromTimestamp = optValues.first().toInt(&ok);
        if ((!ok) || (fromTimestamp < 0)) {
            BMMisc::printHTMLErrorPage("-fromtime not a non-negative integer");
            return;
        }
    } else {
        BMMisc::printHTMLErrorPage("-fromtime option not found");
        return;
    }

    // Get 'to' timestamp ...
    int toTimestamp = -1;
    if (BMMisc::getOption(args, "-totime", &optValues, 1, 0)) {
        bool ok;
        toTimestamp = optValues.first().toInt(&ok);
        if ((!ok) || (toTimestamp < fromTimestamp)) {
            BMMisc::printHTMLErrorPage(QString("-totime not >= %1").arg(fromTimestamp));
            return;
        }
    } else {
        BMMisc::printHTMLErrorPage("-totime option not found");
        return;
    }

    // *** Header ***
    QString reply = QString("<html>\n<head>\n");
    reply += QString("<link rel=\"stylesheet\" type=\"text/css\" href=\"%1\" />\n").arg(styleSheet);
    reply += QString("</head>\n<body>\n");

    reply += "<span style=\"font-size:18\">ASF stats contributor</span>";

    reply += "<br /><br /><table><tr><td style=\"background-color:#eeeeee\">";

    reply += "This page shows a result history that contributed to the ASF stats.";

    reply += "<br /><br />";

    reply += QString(
        "Normal data points are indicated in black, whereas potential outliers "
        "(filtered out by median of %1 smoothing) are indicated in red.")
        .arg(medianWinSize);

    reply += "<br /><br />";

    reply +=
        "The 'from' and 'to' times are indicated by dashed and solid brown vertical lines "
        "respectively. The data points to be used for sampling the result history at "
        "these times are indicated in the graph as an orange square and an orange circle "
        "respectively. If the result history is classified as <i>stable</i>, the two samples "
        "serve to further classify it as <i>regressed</i>, <i>unchanged</i>, or <i>improved</i>. "
        "Otherwise the two samples are just ignored.";

    reply += "</td></tr></table>\n";


    // *** Plot ***

    // reply += "<br />args:<br />";
    // for (int i = 0; i < args.size(); ++i)
    //     reply += QString("arg %1: &gt;%2&lt;<br />").arg(i).arg(args.at(i));
    // reply += "<br />";

    QDomElement argsElem = doc.elementsByTagName("args").at(0).toElement();

    reply += "<br />\n<img src=\"bmclientwrapper?command=";
    for (int i = args.indexOf("-server"); i < args.size(); ++i)
        reply += QString("%1 ")
            .arg((args.at(i) == "detailspage2") ? QLatin1String("plot2") : args.at(i));
    reply += QString("-cachekey %1 ").arg(argsElem.attributeNode("cacheKey").value());
    reply += "-httpheader ";
    reply += "\" />\n";


    // *** Table of values ***

    reply += "<br />\n<br />\n";

    reply += "<table style=\"text-align:right\">";

    reply += QString(
        "<tr>"
        "<th class=\"details_table_head\">n</th>"
        "<th class=\"details_table_head\">Timestamp</th>"
        "<th class=\"details_table_head\">SHA-1</th>"
        "<th class=\"details_table_head\">Value</th>"
        "</tr>\n");

    // Get the time series ...
    QDomNodeList resultNodes = doc.elementsByTagName("result");
    QList<int> timestamps;
    QList<qreal> values;
    QStringList sha1s;
    for (int i = 0; i < resultNodes.size(); ++i) {
        bool ok;
        QDomElement resultElem = resultNodes.at(i).toElement();
        timestamps += resultElem.attributeNode("timestamp").value().toInt(&ok);
        Q_ASSERT(ok);
        values += resultElem.attributeNode("value").value().toDouble(&ok);
        Q_ASSERT(ok);
        sha1s += resultElem.attributeNode("sha1").value();
    }

    ResultHistoryInfo rhInfo(-1, timestamps, values, medianWinSize, "");

    // Get the actual data positions used for sampling at 'from' and 'to' timestamps ...
    int fromPos = -1;
    rhInfo.findSmoothPos(fromTimestamp, &fromPos);
    int toPos = -1;
    rhInfo.findSmoothPos(toTimestamp, &toPos);

    for (int i = 0; i < timestamps.size(); ++i) {

        const int i_ = resultNodes.size() - 1 - i; // reverse chronological order
        reply += QString(
            "<tr>"
            "<td>%1</td>"
            "<td>%2</td>"
            "<td>%3</td>"
            "<td%4>%5</td>"
            "</tr>\n"
            )
            .arg(i_ + 1)
            .arg(timestamps.at(i_))
            .arg(sha1s.at(i_))
            .arg(rhInfo.isOutlier(i_)
                 ? " style=\"color:red\""
                 : (((i_ == fromPos) || (i_ == toPos))
                    ? " style=\"background-color:#ff944d\""
                    : ""
                     ))
            .arg(values.at(i_));
    }

    reply += "</table>\n";

    reply += "</body></html>";

    BMMisc::printHTMLOutput(reply);
}

void BMRequest_ASFStatsGetValues2::handleReply_Image(const QStringList &args) const
{
    const QString error =
        doc.elementsByTagName("reply").at(0).toElement().attributeNode("error").value();

    if (!error.isEmpty()) {
        BMMisc::printHTMLErrorPage(
            QString(
                "failed to create plot for ASF stats contributor: %1").arg(error));
        return;
    }

    bool ok;
    QStringList optValues;


    // Get median window size ...
    int medianWinSize = -1;
    if (BMMisc::getOption(args, "-medianwinsize", &optValues, 1, 0)) {
        medianWinSize = optValues.first().toInt(&ok);
        if ((!ok) || (medianWinSize < 1)) {
            BMMisc::printHTMLErrorPage("-medianwinsize not a positive integer");
            return;
        }
    } else {
        BMMisc::printHTMLErrorPage("-medianwinsize option not found");
        return;
    }

    // Get 'from' timestamp ...
    int fromTimestamp = -1;
    if (BMMisc::getOption(args, "-fromtime", &optValues, 1, 0)) {
        fromTimestamp = optValues.first().toInt(&ok);
        if ((!ok) || (fromTimestamp < 0)) {
            BMMisc::printHTMLErrorPage("-fromtime not a non-negative integer");
            return;
        }
    } else {
        BMMisc::printHTMLErrorPage("-fromtime option not found");
        return;
    }

    // Get 'to' timestamp ...
    int toTimestamp = -1;
    if (BMMisc::getOption(args, "-totime", &optValues, 1, 0)) {
        toTimestamp = optValues.first().toInt(&ok);
        if ((!ok) || (toTimestamp < fromTimestamp)) {
            BMMisc::printHTMLErrorPage(QString("-totime not >= %1").arg(fromTimestamp));
            return;
        }
    } else {
        BMMisc::printHTMLErrorPage("-totime option not found");
        return;
    }


    QDomElement argsElem = doc.elementsByTagName("args").at(0).toElement();


    // Get the BM context ...
    QDomNodeList bmcontextNodes = doc.elementsByTagName("bmcontext");
    QDomElement bmcontextElem = bmcontextNodes.at(0).toElement();
    const QString testCase = bmcontextElem.attributeNode("testCase").value();
    const QString testFunction = bmcontextElem.attributeNode("testFunction").value();
    const QString dataTag = bmcontextElem.attributeNode("dataTag").value();
    const QString metric = bmcontextElem.attributeNode("metric").value();
    const QString platform = bmcontextElem.attributeNode("platform").value();
    const QString host = bmcontextElem.attributeNode("host").value();
    const QString gitRepo = bmcontextElem.attributeNode("gitRepo").value();
    const QString gitBranch = bmcontextElem.attributeNode("gitBranch").value();

    // Get the time series ...
    QDomNodeList resultNodes = doc.elementsByTagName("result");
    QList<int> timestamps;
    QList<qreal> values;
    for (int i = 0; i < resultNodes.size(); ++i) {
        bool ok;
        QDomElement resultElem = resultNodes.at(i).toElement();
        timestamps += resultElem.attributeNode("timestamp").value().toInt(&ok);
        Q_ASSERT(ok);
        values += resultElem.attributeNode("value").value().toDouble(&ok);
        Q_ASSERT(ok);
    }

    QList<ResultHistoryInfo *> rhInfos;
    ResultHistoryInfo *rhInfo = new ResultHistoryInfo(
        -1, timestamps, values, medianWinSize, metric, platform, host, gitRepo, gitBranch,
        testCase, testFunction, dataTag);
    rhInfos.append(rhInfo);

    // Get the actual data positions used for sampling at 'from' and 'to' timestamps ...
    int fromPos = -1;
    rhInfo->findSmoothPos(fromTimestamp, &fromPos);
    int toPos = -1;
    rhInfo->findSmoothPos(toTimestamp, &toPos);
    QList<QList<int> > extraPos;
    extraPos.append(QList<int>() << fromPos << toPos);
    const QList<int> extraTimestamps = QList<int>() << fromTimestamp << toTimestamp;

    const QStringList descr = QStringList() << "<no description>";


    // Create image ...
    QString error_;
    Plotter *plotter =
        new HistoriesPlotter(
            rhInfos, true, -1, 0, &extraPos, &extraTimestamps, &descr, false, true);
    const QImage image = plotter->createImage(&error_);

    // Free dynamic memory ...
    for (int i = 0; i < rhInfos.size(); ++i)
        delete rhInfos.at(i);

    // Output image ...
    if (!image.isNull()) {
        BMMisc::printImageOutput(
            image, "png", QString(), BMMisc::hasOption(args, "-httpheader"));
    } else {
        BMMisc::printHTMLErrorPage(
            QString(
                "failed to create plot for main index contributors: %1").arg(error_));
    }
}
