TEMPLATE = lib
CONFIG += shared
TARGET = bm
SOURCES += bm.cpp bmrequest.cpp bmmisc.cpp plotter.cpp resulthistoryinfo.cpp cache.cpp index.cpp \
           dataqualitystats.cpp asfstats.cpp
HEADERS += bm.h bmrequest.h bmmisc.h plotter.h resulthistoryinfo.h cache.h index.h \
           dataqualitystats.h asfstats.h
QT += network
QT += xml
QT += sql
#DEFINES += BMDEBUG
