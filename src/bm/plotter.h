/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef PLOTTER_H
#define PLOTTER_H

#include "resulthistoryinfo.h"
#include <QList>
#include <QString>
#include <QImage>

class QGraphicsScene;

class Plotter
{
public:
    virtual ~Plotter() {}

    struct PointInfo
    {
        QRectF rect;
        qreal value;
        PointInfo(const QRectF &rect, const qreal value) : rect(rect), value(value) {}
    };

    QImage createImage(QString *error = 0, QList<PointInfo> *pointInfos = 0) const;

private:
    virtual QRectF sceneRect() const = 0;
    virtual bool drawScenes(
        QGraphicsScene *scene_far, QGraphicsScene *scene_mid_aa, QGraphicsScene *scene_near,
        QString *error = 0, QList<PointInfo> *pointInfos = 0) const = 0;
};

class ResultHistoryPlotter : public Plotter
{
public:
    ResultHistoryPlotter(
        const QList<int> &timestamps, const QList<qreal> &values, int resultId, int basePos = -1);

private:
    QList<int> timestamps;
    QList<qreal> values;
    int resultId;
    int basePos;

    qreal width;
    qreal height;

    QRectF sceneRect() const;
    bool drawScenes(
        QGraphicsScene *scene_far, QGraphicsScene *scene_mid_aa, QGraphicsScene *scene_near,
        QString *error, QList<PointInfo> *pointInfos) const;
};

class IndexPlotter : public Plotter
{
public:
    IndexPlotter(
        const QList<int> &timestamps, const QList<qreal> &values, const QList<int> &contributions,
        const int baseTimestamp = -1, const int baseValuePos = -1);

private:
    QList<int> timestamps;
    QList<qreal> values;
    QList<int> contributions;
    mutable int baseTimestamp;
    int baseValuePos;

    qreal width;
    qreal height;

    QRectF sceneRect() const;
    bool drawScenes(
        QGraphicsScene *scene_far, QGraphicsScene *scene_mid_aa, QGraphicsScene *scene_near,
        QString *error, QList<PointInfo> *pointInfos) const;
};

class HistoriesPlotter : public Plotter
{
public:
    HistoriesPlotter(
        const QList<ResultHistoryInfo *> &rhInfos, const bool showBenchmark = false,
        const int baseTimestamp = -1, const QList<int> *basePos = 0,
        const QList<QList<int> > *extraPos = 0, const QList<int> *extraTimestamps = 0,
        const QStringList *extraDescr = 0, const bool commonValueRange = true,
        const bool showOutliers = false);

private:
    QList<ResultHistoryInfo *> rhInfos;
    bool showBenchmark;
    int baseTimestamp;
    const QList<int> *basePos;
    const QList<QList<int> > *extraPos;
    const QList<int> *extraTimestamps;
    const QStringList *extraDescr;
    bool commonValueRange;
    bool showOutliers;
    qreal width;
    qreal rhHeight;
    qreal pad_top;
    qreal pad_bottom;
    qreal height;

    QRectF sceneRect() const;
    bool drawScenes(
        QGraphicsScene *scene_far, QGraphicsScene *scene_mid_aa, QGraphicsScene *scene_near,
        QString *error, QList<PointInfo> *pointInfos) const;
};

#endif // PLOTTER_H
