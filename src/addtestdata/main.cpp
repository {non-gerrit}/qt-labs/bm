/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include <QtCore>

// Extracts up to \a n words from the next line in \a file and returns them as a string list.
// If n is negative, a string list containing the entire line as its only element is returned.
// If an error occurred or the line was empty, an empty string list is returned.
static QStringList getWordsInNextLine(QFile &file, int n)
{
    QStringList sl;
    char data[1024];
    if (file.readLine(data, 1024) != -1) {
        QString line(data);
        line.truncate(line.size() - 1);
        line = line.trimmed();
        sl = (n < 0) ? (QStringList() << line) : line.split(" ", QString::SkipEmptyParts);
    }
    return sl;
}

static QString randomSHA1()
{
    QString sha1;
    for (int i = 0; i < 20; ++i)
        sha1.append(QByteArray(1, qrand() % 256).toHex());
    return sha1;
}

int main(int argc, char *argv[])
{
    // WARNING: Running this program twice within the same second results in
    // identical SHA-1 values:
    qsrand(QDateTime::currentDateTime().toTime_t());

    QCoreApplication app(argc, argv); // needed?


    // --- READ INPUT  ---------------------------------------------------------------------

    QFile input;
    if (!input.open(stdin, QIODevice::ReadOnly)) {
        qDebug() << "failed to open stdin";
        return 1;
    }

    const QString host = getWordsInNextLine(input, 1).first();
    const QString platform = getWordsInNextLine(input, 1).first();
    const QString gitRepo = getWordsInNextLine(input, 1).first();
    const QString gitBranch = getWordsInNextLine(input, 1).first();
    const QString testCase = getWordsInNextLine(input, 1).first();
    const QString testFunction = getWordsInNextLine(input, 1).first();
    const QString dataTag = getWordsInNextLine(input, -1).first();
    const QString metric = getWordsInNextLine(input, 1).first();

//     qDebug() << "host:" << host;
//     qDebug() << "platform:" << platform;
//     qDebug() << "gitRepo:" << gitRepo;
//     qDebug() << "gitBranch:" << gitBranch;
//     qDebug() << "testCase:" << testCase;
//     qDebug() << "testFunction:" << testFunction;
//     qDebug() << "dataTag:" << dataTag;
//     qDebug() << "metric:" << metric;

    QStringList timestamps;
    QStringList values;
    QStringList iterations;

    if (argc == 1) {
        // Read results from standard input ...
        while (true) {
            const QStringList result = getWordsInNextLine(input, 3);
            if (result.isEmpty())
                break;
            timestamps.append(result.at(0));
            values.append(result.at(1));
            iterations.append(result.at(2));
        }

    } else {
        // Generate results ...

        const int n = 200;
        const int t0 = 1220078160;
        const int t1 = 1254206160;
        const int min0 = 100;
        const int max0 = 110;
        const int min1 = 108;
        const int max1 = 111;

        for (int i = 0; i < n; ++i) {
            const qreal tfrac = i / qreal(n - 1);

            const int t = t0 + int(tfrac * (t1 - t0));
            timestamps.append(QString("%1").arg(t));

            const int min = min0 + int(tfrac * (min1 - min0));
            const int max = max0 + int(tfrac * (max1 - max0));
            const int v = min + qrand() % ((max - min) + 1);
            values.append(QString("%1").arg(v));

            iterations.append("1");
        }
    }

//     qDebug() << "timestamps:" << timestamps;
//     qDebug() << "values:" << values;
//     qDebug() << "iterations:" << iterations;

    input.close();


    // --- WRITE OUTPUT ---------------------------------------------------------------------

    fprintf(stderr, "sending %d datasets to the server ... ", timestamps.size());
    for (int i = 0; i < timestamps.size(); ++i) {

        fprintf(stderr, "%d ", i);

        // Create file ...
        QFile output("tmp.xml");
        if (!output.open(QIODevice::WriteOnly | QIODevice::Text)) {
            qDebug() << "failed to open" << output.fileName() << "for writing";
            return 1;
        }

        // Write file ...
        QByteArray ba;
        ba.append(
            QString(
                "<TestCase name=\"%1\">\n<TestFunction name=\"%2\">\n"
                "<BenchmarkResult metric=\"%3\" tag=\"%4\" value=\"%5\" "
                "iterations=\"%6\" />\n</TestFunction>\n</TestCase>\n")
            .arg(testCase).arg(testFunction).arg(metric).arg(dataTag)
            .arg(values.at(i)).arg(iterations.at(i)));
        if (output.write(ba) == -1) {
            qDebug() << "failed to write byte array to file";
            return 1;
        }
        output.close();

        // Run bmclient ...
        QProcess process;
        process.setEnvironment(
            QStringList()
            << QString("BMSERVER=localhost:1107")
            << QString("BMFAKEBRANCH=%1").arg(gitBranch)
            << QString("BMFAKESHA1=%1").arg(randomSHA1())
            << QString("BMFAKETIMESTAMP=%1").arg(timestamps.at(i))
            );
        process.start(
            "bmclient",
            QStringList()
            << "put" << "results" << output.fileName() << platform << host << gitRepo);
        if (!process.waitForFinished(-1)) {
            qDebug() << "failed to start bmclient process, error type:" << process.error();
            return 1;
        }
        if (process.exitCode() != 0) {
            qDebug() << "bmclient failed:" << process.readAllStandardError().data();
            return 1;
        }

        // Remove file ...
        if (!QFile::remove(output.fileName())) {
            qDebug() << "failed to remove output.fileName()";
            return 1;
        }
    }

    fprintf(stderr, "done\n");

    return 0;
}
