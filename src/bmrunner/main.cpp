/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include <QtCore>

static void printUsage(const QString &execName)
{
    qDebug().nospace()
        << "usage: " << execName.toLatin1().data() << " \\\n"
        << "    -metric { walltime | tickcounter | callgrind | eventcounter } \\\n"
        << "    [-median <median>] \\\n"
        << "    -platform <platform> \\\n"
        << "    -host <host> \\\n"
        << "    -gitdir <gitdir> \\\n"
        << "    -gitrepo <gitrepo> \\\n"
        << "    -bmserver <host>:<port> \\\n"
        << "    -bmdir <bmdir> \\\n"
        << "    [-display <display>] \\\n"
        << "    [-timeout <secs>]";
}

static bool findOption(const QStringList &args, const QString &option, QString *value)
{
    const int i = args.indexOf(option);
    if (i == -1)
        return false;
    if (i == (args.size() - 1))
        *value = QString();
    else
        *value = args.at(i + 1).trimmed();
    return true;
}

static bool parseArgs(
    const QStringList &args, QString *metric, QString *median, QString *platform, QString *host,
    QString *gitdir, QString *gitrepo, QString *bmserver, QString *bmdir, QString *display,
    int *timeout, QString *error)
{
    // Metric
    if (!findOption(args, "-metric", metric) || metric->isEmpty()) {
        *error = "metric not specified";
        return false;
    }
    if ((*metric != "walltime") && (*metric != "tickcounter") && (*metric != "callgrind")
        && (*metric != "eventcounter")) {
        *error = QString("unsupported metric: %1").arg(*metric);
        return false;
    }

    // Median
    if (findOption(args, "-median", median)) {
        bool ok;
        median->toInt(&ok);
        if (!ok) {
            *error = QString("median not an integer: %1").arg(*median);
            return false;
        }
    } else {
        *median = "";
    }

    // Platform
    if (!findOption(args, "-platform", platform) || platform->isEmpty()) {
        *error = "platform not specified";
        return false;
    }

    // Host
    if (!findOption(args, "-host", host) || host->isEmpty()) {
        *error = "host not specified";
        return false;
    }

    // Git directory
    if (!findOption(args, "-gitdir", gitdir) || gitdir->isEmpty()) {
        *error = "git directory not specified";
        return false;
    }

    // Git repository
    if (!findOption(args, "-gitrepo", gitrepo) || gitrepo->isEmpty()) {
        *error = "git repository not specified";
        return false;
    }

    // Benchmark server
    if (!findOption(args, "-bmserver", bmserver) || bmserver->isEmpty()) {
        *error = "benchmark server not specified";
        return false;
    }

    // Benchmark directory
    if (!findOption(args, "-bmdir", bmdir) || bmdir->isEmpty()) {
        *error = "benchmark directory not specified";
        return false;
    }

    // Display
    if (findOption(args, "-display", display)) {
        if (display->isEmpty()) {
            *error = "empty display";
            return false;
        }
    } else {
        *display = "";
    }

    // Timeout
    QString timeout_s;
    if (findOption(args, "-timeout", &timeout_s)) {
        bool ok;
        *timeout = timeout_s.toInt(&ok);
        if (!ok) {
            *error = "timeout not an integer";
            return false;
        }
        *timeout = qMax(*timeout * 1000, -1);
    } else {
        *timeout = 300 * 1000; // default
    }

    return true;
}

static QString metricOption(const QString &metric)
{
    if (metric == "walltime")
        return QString();
    return QString("-%1").arg(metric);
}

// Looks in directory \a dir for potential benchmark programs (i.e. files of the
// form 'tst_*' that doesn't contain the substring '.debug') and appends their file infos
// to \a bmprogs.
// Applies the search recursively for all subdirectories.
static void findBenchmarkProgs(const QDir &dir, QFileInfoList *bmprogs)
{
    // Append potential benchmark programs in this directory ...
    QFileInfoList progCandidates =
        dir.entryInfoList(
            QStringList() << "tst_*", QDir::Files | QDir::Readable | QDir::Executable);
    for (int i = 0; i < progCandidates.size(); ++i)
        if (!progCandidates.at(i).fileName().contains(".debug"))
            bmprogs->append(progCandidates.at(i));

    // Repeat search in subdirectories ...
    QFileInfoList subdirs =
        dir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot | QDir::Readable | QDir::Executable);
    for (int i = 0; i < subdirs.size(); ++i)
        findBenchmarkProgs(QDir(subdirs.at(i).absoluteFilePath()), bmprogs);
}

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv); // needed?

    const QStringList args = qApp->arguments();

    if ((args.size() == 1) || (args.at(1).endsWith("help"))) {
        printUsage(args.at(0));
        return 0;
    }

    QString error;


    // Parse command-line arguments ...
    QString metric;
    QString median;
    QString platform;
    QString host;
    QString gitdir;
    QString gitrepo;
    QString bmserver;
    QString bmdir;
    QString display;
    int timeout;
    if (!parseArgs(
            args, &metric, &median, &platform, &host, &gitdir, &gitrepo, &bmserver, &bmdir,
            &display, &timeout, &error)) {
        qDebug() << "failed to parse command-line arguments:" << error.toLatin1().data();
        return 1;
    }


    // Construct the list of benchmark programs ...
    QDir dir(bmdir);
    if (!dir.exists()) {
        qDebug() << "benchmark directory doesn't exist:" << bmdir;
        return 1;
    }
    if (!dir.isReadable()) {
        qDebug() << "benchmark directory is unreadable:" << bmdir;
        return 1;
    }
    QFileInfoList bmprogs;
    findBenchmarkProgs(dir, &bmprogs);
    if (bmprogs.isEmpty()) {
        qDebug() << "benchmark directory contains no benchmark programs:" << bmdir;
        return 1;
    }


    // Set up command-line argument templates for benchmark programs and bmclient ...
    QStringList bmprogBaseArgs;
    const QString metricOpt = metricOption(metric);
    if (!metricOpt.isEmpty())
        bmprogBaseArgs << metricOpt;
    if (!median.isEmpty())
        bmprogBaseArgs << "-median" << median;
    bmprogBaseArgs << "-xml" << "-o";
    //
    QStringList bmclientBaseArgs =
        QStringList() << "-server" << bmserver << "put" << "results" << "<file name>" <<
        platform << host << gitrepo << gitdir;


    // Loop over benchmark programs ...
    int successCount = 0;
    for (int i = 0; i < bmprogs.size(); ++i) {

        // Open a temporary file for storing results ...
        QTemporaryFile resultsFile;
        if (!resultsFile.open()) {
            qDebug() << "failed to open temporary results file";
            return 1;
        }

        // Execute benchmark program ...
        {
            QStringList args = bmprogBaseArgs + QStringList(resultsFile.fileName());
            QProcess process;
            if (!display.isEmpty())
                process.setEnvironment(QStringList() << QString("DISPLAY=%1").arg(display));
            qDebug() << "running" << bmprogs.at(i).absoluteFilePath() << "with args" <<
                args << "...";
            process.start(bmprogs.at(i).absoluteFilePath(), args);
            if (!process.waitForFinished(timeout)) {

                if (process.error() == QProcess::Timedout) {
                    qDebug() << bmprogs.at(i).absoluteFilePath() <<
                        "timed out after" << (timeout / 1000) << "secs (continuing)";
                    continue;
                } else {
                    qDebug() << bmprogs.at(i).absoluteFilePath() <<
                        "did not finish correctly; error type:" << process.error() <<
                        "(continuing)";
                    continue;
                }

            }
            if (process.exitCode() != 0) {
                qDebug() << bmprogs.at(i).absoluteFilePath() << "failed:" <<
                    process.readAllStandardError().data() << "(aborting)";
                return 1;
            }
        }

        // Pass results to bmserver via bmclient ...
        {
            bmclientBaseArgs.replace(4, resultsFile.fileName());
            QProcess process;
            qDebug() << "executing bmclient with args" << bmclientBaseArgs << "...";
            process.start("bmclient", bmclientBaseArgs);
            if (!process.waitForFinished(-1)) {
                qDebug() << "bmclient did not finish correctly; error type:" << process.error() <<
                    "(continuing)";
                continue;
            }
            if (process.exitCode() != 0) {
                qDebug() << "bmclient failed:" << process.readAllStandardError().data() <<
                    "(continuing)";
                continue;
            }
        }

        successCount++;
    }


    return (successCount > 0) ? 0 : 1;
}
