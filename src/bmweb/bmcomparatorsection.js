/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

function updateShowLink()
{
    // Initialize URL ...
    url = "http://" + location.host + "/cgi-bin/bmclientwrapper";
    url += "?command=";
    url += "-server " + server();
    url += " get histories detailspage";
    url += " -stylesheet " + document.styleSheets[0].href;

    // Add benchmark ...
    url += " -testcase " + selectedOptions("bcTestCases")[0];
    url += " -testfunction " + selectedOptions("bcTestFunctions")[0];
    // The data tag may consist of "anything", so we need to quote it
    // and escape problematic characters:
    url += " -datatag " + "'" + encodeURIComponent(selectedOptions("bcDataTags")[0]) + "'";

    // Update link ...
    showLink = document.getElementById("bcShowLink");
    showLink.setAttribute("href", url);

    showLink.setAttribute("target", "_blank"); // prevent page from opening in same window
}

function BMComparatorSection()
{
    var bmtree;

    this.name = function() { return "bcSection"; }

    this.reset = function(handleDone)
    {
        this.finalize_reset = function()
        {
            setMessage("");
            document.getElementById("bcReset").disabled = false;
            if (handleDone)
                handleDone();
        }

        this.handleReply_reset = function(reply)
        {
            if (reply.error) {
                alert("reset() failed when fetching benchmark tree (2): " + reply.error);

                this.finalize_reset();

            } else {

                bmtree = reply;

                // Load the test cases ...
                testCases = bmtree.testCases;
                tcSelect = document.getElementById("bcTestCases");
                tcSelect.options.length = 0;
                for (i = 0; i < testCases.length; ++i)
                    tcSelect.options[tcSelect.options.length] = new Option(testCases[i].name);
                // ... and select the first one ...
                tcSelect.options[0].selected = true;
                this.selectTestCase(0);
            }

            this.finalize_reset();
        }

        this.handleError_reset = function(error)
        {
            alert("reset() failed when fetching benchmark tree (1): " + error);
            this.finalize_reset();
        }

        var thisSection = this; // ### hack?

        document.getElementById("bcReset").disabled = true;

        // Fetch benchmark tree ...
        setMessage("fetching benchmark tree ...");
        var args = ["get", "bmtree"];
        sendRequest(
            args,
            function(reply) { thisSection.handleReply_reset(reply); },
            function(error) { thisSection.handleError_reset(error); });
    }

    this.selectTestFunction = function(tcIndex, tfIndex) {
        // Load the data tags for this test case / test function ...
        dataTags = bmtree.testCases[tcIndex].testFunctions[tfIndex].dataTags;
        dtSelect = document.getElementById("bcDataTags");
        dtSelect.options.length = 0;
        for (i = 0; i < dataTags.length; ++i) {
            dtSelect.options[dtSelect.options.length] = new Option(dataTags[i]);
        }
        // ... and select the first one ...
        dtSelect.options[0].selected = true;
    }

    this.selectTestCase = function(tcIndex) {
        // Load the test functions for this test case ...
        testFunctions = bmtree.testCases[tcIndex].testFunctions;
        tfSelect = document.getElementById("bcTestFunctions");
        tfSelect.options.length = 0;
        for (i = 0; i < testFunctions.length; ++i)
            tfSelect.options[tfSelect.options.length] = new Option(testFunctions[i].name);
        // ... and select the first one ...
        tfSelect.options[0].selected = true;
        this.selectTestFunction(tcIndex, 0);
    }

    this.appendAll = function(section)
    {
        var thisSection = this; // ### hack?

        section.appendChild(document.createElement("br"));
        section.appendChild(document.createElement("br"));

        // Create a link that opens a page that shows the comparison ...
        link = section.appendChild(document.createElement("a"));
        link.id = "bcShowLink";
        link.appendChild(document.createTextNode("Show"));
        link.setAttribute("class", "bcLink");
        link.setAttribute("href", "dummy"); // for correct initial rendering
        link.setAttribute("onmouseover", "updateShowLink()");

        section.appendChild(document.createElement("br"));
        section.appendChild(document.createElement("br"));

        // Create 'Reset' button ...
        input = section.appendChild(document.createElement("input"));
        input.setAttribute("type", "button");
        input.setAttribute("id", "bcReset");
        input.setAttribute("value", "Reset");
        input.onclick = function() {
            try { thisSection.reset(); }
            catch (ex) { alert("thisSection.reset() failed: " + ex); }
        }

        section.appendChild(document.createElement("br"));
        section.appendChild(document.createElement("br"));

        // Create benchmark selectors ...
        table = section.appendChild(document.createElement("table"));
        table.setAttribute("style", "border:0px; padding:0px");
        // ... test case ...
        tr = table.insertRow(0);
        td = tr.insertCell(0);
        td.innerHTML = "Test Case:";
        td = tr.insertCell(1);
        select = td.appendChild(document.createElement("select"));
        select.setAttribute("id", "bcTestCases");
        select.onchange = function() {
            tcSelect = document.getElementById("bcTestCases");
            thisSection.selectTestCase(tcSelect.selectedIndex);
        }
        // ... test function ...
        tr = table.insertRow(1);
        td = tr.insertCell(0);
        td.innerHTML = "Test Function:";
        td = tr.insertCell(1);
        select = td.appendChild(document.createElement("select"));
        select.setAttribute("id", "bcTestFunctions");
        select.onchange = function() {
            tcSelect = document.getElementById("bcTestCases");
            tfSelect = document.getElementById("bcTestFunctions");
            thisSection.selectTestFunction(tcSelect.selectedIndex, tfSelect.selectedIndex);
        }
        // ... data tag ...
        tr = table.insertRow(2);
        td = tr.insertCell(0);
        td.innerHTML = "Data Tag:";
        td = tr.insertCell(1);
        select = td.appendChild(document.createElement("select"));
        select.setAttribute("id", "bcDataTags");
    }

    this.create = function()
    {
        var sectionElem = document.createElement("div");
        insertTitle(sectionElem, "Benchmark Comparison");
        sectionElem.appendChild(document.createElement("br"));
        appendHelpLink(sectionElem, "help.html#BMComparison");

        this.appendAll(sectionElem);

        return sectionElem;
    }

    this.state = function() {
        var state_ = [];

        // 2 B DONE!

//        state_["bcTestState"] = document.getElementById("bcTestState").value;

        return state_;
    }

    this.setState = function(state_, handleDone) {

        // 2 B DONE!

        // name = "bcTestState";
        // value = parseInt(state_[name]);
        // if (!isNaN(value))
        //     document.getElementById(name).value = value;

        handleDone();
    }

    SectionBase.call(this, "bmComparatorButton");
    this.sectionElem = this.create();
}

BMComparatorSection.prototype = new SectionBase();
BMComparatorSection.prototype.constructor = BMComparatorSection;
BMComparatorSection.instance = function()
{
    if (!BMComparatorSection.instance_)
        BMComparatorSection.instance_ = new BMComparatorSection();
    return BMComparatorSection.instance_;
}
function bcSection() { return BMComparatorSection.instance(); }
