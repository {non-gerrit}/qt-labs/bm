<html>

<head>

<style type="text/css">
table { border-collapse:collapse; }
table, th, td { border: 1px solid #aaaaaa; }
th {vertical-align:top; padding:10px}
td {vertical-align:top; padding:10px}
li {padding:5px}
</style>

</head>

<body>

<span style="text-align:center">
<h1>Benchmarks Results Viewer Manual</h1>
</span>

<!-- //////////////////////////////////////////////////////////////////////////////////// -->
<h2>Introduction</h2>

The Benchmark Results Viewer is the web interface to
<a href="http://labs.trolltech.com/blogs/2009/11/05/analyzing-benchmark-results-with-bm/">BM</a>.
It presents benchmark results recorded in the same output file format as the one
produced by QTestLib. The use cases are:

<ul>
<li>
View the overall status for all available platforms within a user-defined
context.
</li>
<li>
View the overall status for each individual benchmark within a user-defined
context for a single branch.
</li>
<li>
View the complete history of a given benchmark for a single branch (a graph
and a table).
</li>
<li>
Compare the latest results of two branches.
</li>
<li>
View the complete history of a given benchmark for two branches (two graphs in
the same plot and two tables side by side).
</li>
<li>
Compare the results in a local file (in QTestLib XML format) against the latest
results of a given branch.
</li>
</ul>


<!-- //////////////////////////////////////////////////////////////////////////////////// -->
<h2>Basic Concepts</h2>

<p>
The application is based on the following concepts:
</p>

<table border=1>
<tr>
    <td><b>Benchmark</b></td>
    <td>
    The combination of a <i>test case</i>, <i>test function</i>, and
    (optional) <i>data tag</i> for test functions that contain the QBENCHMARK
    macro.
    </td>
</tr>
<tr>
    <td><b>Metric</b></td>
    <td>
    The quantity or "thing" that is measured during a particular execution of
    a benchmark. Examples include <i>walltime</i> (actual wall clock time)
    and <i>callgrind</i> (instruction load count).
    <br /><br />

    <b>Note</b>: A metric can be classified into "lower is better"
    (like <i>walltime</i> or <i>seconds per frame</i>), or "higher is better"
    (like <i>bit rate</i> or <i>frames per second</i>).
    </td>
</tr>
<tr>
    <td><b>Platform</b></td>
    <td>
    The <i>general</i> environment used for building and executing the product being
    measured (typically an OS/compiler combination like <i>linux-g++</i>,
    <i>solaris-cc-64</i> etc.).
    </td>
</tr>
<tr>
    <td><b>Host</b></td>
    <td>The physical computer on which a benchmark was executed. This concept
    currently also includes the entire execution environment (HW and
    SW). Two benchmark results R1 and R2 that are produced under different execution
    environments should be be associated with different hosts (because R1 and
    R2 may in general have been affected by different external effects).</td>
</tr>
<tr>
    <td><b>Branch</b></td>
    <td>
    Essentially the version of the product being measured. The branch is
    normally made up of two components: The <i>git repository</i> and the
    <i>git branch</i>.
    </td>
</tr>
<tr>
    <td><b>Benchmark Result</b></td>
    <td>
    The measured value along with its association to a certain context
    (platform etc.) and to a certain snapshot in the revision history of the
    product.

    <br /><br />
    <b>Note</b>: The measured value can be
    any <a href="http://en.wikipedia.org/wiki/Real_number">real number</a>. A
    typical use case for negative values is a benchmark that measures the
    difference between two different implementations of the same problem/task.
    </td>
</tr>
<tr>
    <td><b>Time Series</b></td>
    <td>
    The time series of benchmark results for a certain benchmark within a
    certain context:
    <br /><br />
    &nbsp;&nbsp;&nbsp;&nbsp;
    (t<sub>1</sub>,&nbsp;v<sub>1</sub>),&nbsp;(t<sub>2</sub>,&nbsp;v<sub>2</sub>),&nbsp;
    &hellip;,&nbsp;(t<sub><i>n</i></sub>,&nbsp;v<sub><i>n</i></sub>),
    <br /><br />
    where t<sub><i>i</i></sub> is the timestamp component of snapshot <i>i</i>
    and v<sub><i>i</i></sub> is the value (a non-negative real number).
    <br /><br />
    <a name="Timestamp" />
    <b>Note:</b> The timestamp is the <a
    href="http://en.wikipedia.org/wiki/Unix_timestamp">UNIX timestamp</a> of
    the <i>committer date</i> of the head commit from which the measured product
    was built.
    </td>
</tr>
<tr>
    <td><a name="NormalizedDifference" /><b>Normalized Difference</b></td>
    <td>

    The <i>normalized difference</i> between two real numbers a and b,
    nd(a,&nbsp;b), is defined as
    <br /><br />
    &nbsp;&nbsp;&nbsp;&nbsp;
    (a - b) / max(|a|, |b|)
    <br /><br />
    if a &ne; b, and
    <br /><br />
    &nbsp;&nbsp;&nbsp;&nbsp;
    0
    <br /><br />
    otherwise.

    <br /><br />
    Note the following properties of the normalized difference:

    <ul>
    <li>0 &le; |nd(a, b)| &le; 2</li>
    <li>nd(a, b) &gt; 0 &rArr; a > b</li>
    <li>nd(a, b) &lt; 0 &rArr; a < b</li>
    <li>nd(a, b) = 0 &rArr; a = b</li>
    <li>0 &lt; |nd(a, b)| &lt; 1 &rArr; a and b are either both positive or both negative</li>
    <li>|nd(a, b)| = 1 &rArr; exactly one of a and b is zero</li>
    <li>1 &lt; |nd(a, b)| &lt; 2 &rArr; a and b have opposite signs (and neither is zero)</li>
    <li>|nd(a, b)| = 2 &rArr; a = -b</li>
    </ul>

    The <i>difference tolerance</i> is the maximum normalized
    difference that is accepted between two values for them to be considered
    equal. Values that are equal/different by this definition are said to
    be <i>significantly</i> equal/different.

    </td>
</tr>
<tr>
    <td><a name="Stability" /><b>Stability</b></td>
    <td>
    A value v<sub><i>i</i></sub> in a time series is said to be <i>stable</i>
    if it is contiguously preceded by a minimum number of values that are
    significantly equal to itself. This number is the <i>stability
    tolerance</i>.
    </td>
</tr>
<tr>
    <td><a name="LastDifference" /><b>Last Difference</b></td>
    <td>
    The last value v<sub><i>d</i></sub> (if any) before v<sub><i>n</i></sub>
    that differs significantly from v<sub><i>n</i></sub>.
    </td>
</tr>
<tr>
    <td><a name="StrongStabilityOfLastDifference" />
      <b>Strong Stability of the Last Difference</b></td>
    <td>
    The last difference v<sub><i>d</i></sub> (if any) is said to be <i>stable
    in the strong sense</i> if and only if v<sub><i>d</i></sub> and
    v<sub><i>n</i></sub> are both stable (in the normal sense).
    </td>
</tr>
<tr>
    <td><a name="Trend" /><b>Trend</b></td>
    <td>
    The <i>&beta;</i> value of the <a
    href="http://en.wikipedia.org/wiki/Simple_linear_regression">simple linear
    regression</a> of the time series: <br /><br />
    &nbsp;&nbsp;&nbsp;&nbsp;
    <i>y</i>&nbsp;=&nbsp;<i>&alpha;</i>&nbsp;+&nbsp;<i>&beta;x</i>

    <br /><br />

    If the value at time <i>t</i> is <i>v</i>(<i>t</i>), and the trend
    continues, the value at time <i>t</i>&nbsp;+&nbsp;<i>dt</i> is expected to
    be <i>v</i>(<i>t</i>)&nbsp;+&nbsp;<i>&beta;</i>*<i>dt</i>.

    <br /><br />

    (Observe that positive and negative <i>&beta;</i> values indicate
    decreased and increased performance respectively.)

    </td>
</tr>
</table>


<!-- //////////////////////////////////////////////////////////////////////////////////// -->
<hr />
<h2>Top Section</h2>
<span style="color:red">2 B DONE</span>


<!-- //////////////////////////////////////////////////////////////////////////////////// -->
<hr />
<a name="PlatformSummary" />
<h2>Platform Summary</h2>
<span style="color:red">2 B DONE</span>

<!-- //////////////////////////////////////////////////////////////////////////////////// -->
<hr />
<a name="BenchmarkSummary" />
<h2>Benchmark Summary</h2>

<h3><i>Configuration</i></h3>
<span style="color:red">2 B DONE</span>

<h3><i>Summary</i></h3>

Table columns:
<table border=1>
<tr>
    <td><b>Age</b></td>
    <td>
        The number of days since the revision associated with the last result
        in Branch 1.
    </td>
</tr>
<tr>
    <td><b>LD</b></td>
    <td>
        The <a href="#LastDifference">last difference</a> in Branch 1.
    </td>
</tr>
<tr>
    <td><b>S</b></td>
    <td>
        Stability:
	<ul>
	    <li>
	      In <i>primary branch</i> mode:
	      The <a href="#StrongStabilityOfLastDifference">strong stability
	      of the last difference</a>.
            </li>
	    <li>
	      In <i>primary vs. secondary branch</i> mode: Whether the last
	      result is <a href="#Stability">stable</a> in both branches.
            </li>
	    <li>
	      In <i>file vs. primary branch</i> mode: Whether the last
	      result is <a href="#Stability">stable</a> in the primary branch.
            </li>
	</ul>
    </td>
</tr>
<tr>
    <td><b>LD&nbsp;Age</b></td>
    <td>
        The number of days between the revisions associated with the result
        representing the <a href="#LastDifference">last difference</a> and the
        last result (both in Branch 1).
    </td>
</tr>
<tr>
    <td><b>Trend</b></td>
    <td>
        The <a href="#Trend">trend</a> of the entire result history in Branch 1.
    </td>
</tr>
<tr>
    <td><b>B2&nbsp;Diff</b></td>
    <td>
        The <a href="#NormalizedDifference">normalized difference</a> between the
        last values of Branch 1 (v1) and Branch 2 (v2) : nd(v1,&nbsp;v2)
    </td>
</tr>
<tr>
    <td><b>B2&nbsp;Age</b></td>
    <td>
        The number of days since the revision associated with the last result
        in Branch 2.
    </td>
</tr>
<tr>
    <td><b>File&nbsp;Diff</b></td>
    <td>
        The <a href="#NormalizedDifference">normalized difference</a> between the
        value in the file (vf) and the last value in Branch 1 (v1): nd(vf,&nbsp;v1)
    </td>
</tr>
</table>


<!-- //////////////////////////////////////////////////////////////////////////////////// -->
<hr />
<a name="Settings" />
<h2>Settings</h2>
<span style="color:red">2 B DONE</span>

<!-- //////////////////////////////////////////////////////////////////////////////////// -->
<hr />
<a name="Index" />
<h2>Performance Index</h2>
<span style="color:red">2 B DONE</span>

<!-- //////////////////////////////////////////////////////////////////////////////////// -->
<hr />
<a name="BMComparison" />
<h2>Benchmark Comparison</h2>
<span style="color:red">2 B DONE</span>

<!-- //////////////////////////////////////////////////////////////////////////////////// -->
<hr />
<a name="BenchmarkDetails" />
<h2>Benchmark Details</h2>

Table columns:
<table border=1>
<tr>
    <td><b>n</b></td>
    <td>
        Result index (the higher the number, the more recent the result).
    </td>
</tr>
<tr>
    <td><b>Age</b></td>
    <td>
        The number of days since the revision associated with the result.
    </td>
</tr>
<tr>
    <td><b>ND</b></td>
    <td>
        The <a href="#NormalizedDifference">normalized difference</a> between
        the last value and the value: nd(&lt;last&nbsp;value&gt;,&nbsp;v)
    </td>
</tr>
<tr>
    <td><b>NDB2</b></td>
    <td>
        The <a href="#NormalizedDifference">normalized difference</a> between the
        value and the last value in Branch 2:
        nd(v, &lt;last&nbsp;value&nbsp;in&nbsp;Branch&nbsp;2&gt;)
    </td>
</tr>
<tr>
    <td><b>LS</b></td>
    <td>
        Highlighting of <a href="#LastDifference">last difference</a> and
        <a href="#Stability">stability</a>.
    </td>
</tr>
<tr>
    <td><b>Timestamp</b></td>
    <td>
        <a href="#Timestamp">Timestamp</a> of the revision associated with the
        result.
    </td>
</tr>
<tr>
    <td><b>SHA-1</b></td>
    <td>
        SHA-1 of the revision associated with the result.
    </td>
</tr>
<tr>
    <td><b>Value</b></td>
    <td>
        The result value.
    </td>
</tr>
</table>

</body>

</html>
