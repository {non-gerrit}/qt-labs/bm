/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

function SectionBase(buttonId)
{
    this.buttonId = buttonId;
}

SectionBase.prototype.install = function()
{
    sectionParent = document.getElementById("section");

    var sectionElems = sectionParent.getElementsByTagName("div");
    for (var i = 0; i < sectionElems.length; ++i) {
        if (sectionElems[i] == this.sectionElem)
            showElement(sectionElems[i]);
        else
            hideElement(sectionElems[i]);
    }

    if (this.handleShow)
        this.handleShow();
    this.updateTopSection();
}

SectionBase.prototype.updateTopSection = function()
{
    var topSection = document.getElementById("topSection");
    var buttons = topSection.getElementsByTagName("input");
//    alert("updateTopSection; this.buttonId: " + this.buttonId);
    for (var i = 0; i < buttons.length; ++i) {
        var button = buttons[i];
        button.disabled = (button.id == this.buttonId);
    }
}
