/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

function SettingsSection()
{ 
    this.name = function() { return "settingsSection"; }

    function create()
    {
        var sectionElem = document.createElement("div");
        insertTitle(sectionElem, "Settings");

        sectionElem.appendChild(document.createElement("br"));
        appendHelpLink(sectionElem, "help.html#Settings");

        sectionElem.appendChild(document.createElement("br"));
        sectionElem.appendChild(document.createElement("br"));

        table = sectionElem.appendChild(document.createElement("table"));
        table.setAttribute("style", "text-align:right");

        currRow = 0;

        // Default server
        tr = table.insertRow(currRow++);
        td = tr.insertCell(0);
        td.innerHTML = "Default server: ";
        td = tr.insertCell(1);
        td.setAttribute("id", "defaultServer");
        td.setAttribute("style", "text-align:left");

        // Custom server
        tr = table.insertRow(currRow++);
        td = tr.insertCell(0);
        td.innerHTML = "Custom server: ";
        td = tr.insertCell(1);
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "text");
        input.setAttribute("id", "customServer");
        input.setAttribute("value", "");

        // Max number of items in a 'details' view (table or plot).
        tr = table.insertRow(currRow++);
        td = tr.insertCell(0);
        td.innerHTML = "Max details history: ";
        td = tr.insertCell(1);
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "text");
        input.setAttribute("id", "maxDetailsHistory");
        input.setAttribute("value", "100");

        // Ranking
        tr = table.insertRow(currRow++);
        td = tr.insertCell(0);
        td.innerHTML = "Ranking: ";
        td = tr.insertCell(1);
        td.setAttribute("style", "text-align:left");
        select = td.appendChild(document.createElement("select"));
        select.setAttribute("id", "ranking");
        select.options[0] = new Option("worst", "worst"); 
        select.options[1] = new Option("best", "best");
        select.options[2] = new Option("absolute", "absolute");
        select.options[0].selected = true;

        // Scope
        tr = table.insertRow(currRow++);
        td = tr.insertCell(0);
        td.innerHTML = "Scope: ";
        td = tr.insertCell(1);
        td.setAttribute("style", "text-align:left");
        select = td.appendChild(document.createElement("select"));
        select.setAttribute("id", "scope");
        select.options[0] = new Option("global", "global");
        select.options[1] = new Option("test function", "testFunction");
        select.options[0].selected = true;

        // Max number of benchmarks per scope
        tr = table.insertRow(currRow++);
        td = tr.insertCell(0);
        td.innerHTML = "Max benchmarks per scope: ";
        td = tr.insertCell(1);
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "text");
        input.setAttribute("id", "maxBenchmarksPerScope");
        input.setAttribute("value", "10");

        // Benchmark filters
        tr = table.insertRow(currRow++);
        td = tr.insertCell(0);
        td.innerHTML = "Test case filter: ";
        td = tr.insertCell(1);
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "text");
        input.setAttribute("id", "testCaseFilter");
        input.setAttribute("value", "*");
        //
        tr = table.insertRow(currRow++);
        td = tr.insertCell(0);
        td.innerHTML = "Test function filter: ";
        td = tr.insertCell(1);
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "text");
        input.setAttribute("id", "testFunctionFilter");
        input.setAttribute("value", "*");
        //
        tr = table.insertRow(currRow++);
        td = tr.insertCell(0);
        td.innerHTML = "Data tag filter: ";
        td = tr.insertCell(1);
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "text");
        input.setAttribute("id", "dataTagFilter");
        input.setAttribute("value", "*");

        // Shared time scale
        tr = table.insertRow(currRow++);
        td = tr.insertCell(0);
        td.innerHTML = "Shared time scale for branch 2 comparison";
        td = tr.insertCell(1);
        td.setAttribute("style", "text-align:left");
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "checkbox");
        input.setAttribute("id", "sharedTimeScale");
        input.checked = true;

        return sectionElem;
    }

    this.state = function() {
        var state_ = [];

        // Custom server
        state_["customServer"] = document.getElementById("customServer").value;

        // Max details history
        state_["maxDetailsHistory"] = document.getElementById("maxDetailsHistory").value;

        // Ranking
        state_["ranking"] = selectedOptions("ranking")[0];

        // Scope
        state_["scope"] = selectedOptions("scope")[0];

        // Max benchmarks per scope
        state_["maxBenchmarksPerScope"] = document.getElementById("maxBenchmarksPerScope").value;

        // Benchmark filters
        state_["testCaseFilter"] = document.getElementById("testCaseFilter").value;
        state_["testFunctionFilter"] = document.getElementById("testFunctionFilter").value;
        state_["dataTagFilter"] = document.getElementById("dataTagFilter").value;

        // Shared time scale
        state_["sharedTimeScale"] =
            document.getElementById("sharedTimeScale").checked ? "true" : "false";

        return state_;
    }

    this.setState = function(state_) {

        var name;
        var value;

        // Custom server
        name = "customServer";
        value = state_[name];
        if (value != null)
            document.getElementById(name).value = value;

        // Max details history
        name = "maxDetailsHistory";
        value = parseInt(state_[name]);
        if (!isNaN(value))
            document.getElementById(name).value = value;

        // Ranking
        name = "ranking";
        value = state_[name];
        select = document.getElementById("ranking");
        options = select.options;
        for (i = 0; i < options.length; ++i)
            options[i].selected = (options[i].value == value);

        // Scope
        name = "scope";
        value = state_[name];
        select = document.getElementById("scope");
        options = select.options;
        for (i = 0; i < options.length; ++i)
            options[i].selected = (options[i].value == value);

        // Max benchmarks per scope
        name = "maxBenchmarksPerScope";
        value = parseInt(state_[name]);
        if (!isNaN(value))
            document.getElementById(name).value = value;

        // Benchmark filters
        var names = ["testCaseFilter", "testFunctionFilter", "dataTagFilter"];
        for (i in names) {
            name = names[i];
            value = state_[name];
            if (value != null)
                document.getElementById(name).value = value;
        }

        // Shared time scale
        name = "sharedTimeScale";
        document.getElementById(name).checked = (state_[name] == "true");
    }

    SectionBase.call(this, "settingsButton");
    this.sectionElem = create();
}

SettingsSection.prototype = new SectionBase();
SettingsSection.prototype.constructor = SettingsSection;
SettingsSection.instance = function()
{
    if (!SettingsSection.instance_)
        SettingsSection.instance_ = new SettingsSection();
    return SettingsSection.instance_;
}
function settingsSection() { return SettingsSection.instance(); }
