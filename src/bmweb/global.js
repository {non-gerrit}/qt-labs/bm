/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

function trim(s)
{
    var i;
    var j;
    for (i = 0; i < s.length && s[i] == ' '; ++i) ;
    for (j = s.length - 1; j >= i && s[j] == ' '; --j) ;
    return (i <= j) ? s.substr(i, (j - i) + 1) : "";
}

function server()
{
    val = trim(document.getElementById("customServer").value);
    if (val != "")
        return val;

    val = trim(document.getElementById("defaultServer").innerHTML);
    if (val != "")
        return val;

    return "";
}

function sendRequest(args, handleReply, handleError, omitServer)
{
    var request = new XMLHttpRequest(); // ### may not work in all browsers!

    // Register notification handler ...
    request.onreadystatechange = function() {
        if (request.readyState == 4) {
            if (request.status == 200) {
//                alert("request.responseText: " + request.responseText);
                handleReply(eval('(' + request.responseText + ')'));
            } else {
//                alert("Error" + request.status + ": " + request.statusText);
                handleError("Error" + request.status + ": " + request.statusText);
            }
        }
    }

    // Set up URL ....
//    var command = "\"";
    var command = "";

    if (!omitServer) {
        var s = server();
        if (s != "") {
            command += "-server";
            command += " " + s;
        }
    }

    for (var i = 0; i < args.length; i++)
        command += (" " + args[i]);
//    command += "\"";
    command += "";
    var url = "http://" + location.host + "/cgi-bin/bmclientwrapper?command=" + command;


    // Send request ...
    // alert("sending request; URL: " + url);
    request.open("get", url);
    request.send(null);
}

function sendFileRequest(file, handleReply, handleError)
{
    var request = new XMLHttpRequest(); // ### may not work in all browsers!

    // Register notification handler ...
    request.onreadystatechange = function() {
        if (request.readyState == 4) {
            if (request.status == 200) {
//                alert("request.responseText: " + request.responseText);
                handleReply(eval('(' + request.responseText + ')'));
            } else {
//                alert("Error" + request.status + ": " + request.statusText);
                handleError("Error" + request.status + ": " + request.statusText);
            }
        }
    }

    // Set up URL ....
    var url = "http://" + location.host + "/cgi-bin/bmclientwrapper";

    // Send request ...
    request.open("post", url);
    request.sendAsBinary(file.getAsBinary());
}

function setMessage(msg)
{
    document.getElementById("message").innerHTML = msg;
}

var startTime;

function startTiming()
{
    startTime = new Date();
}

function stopTiming(operation)
{
    stopTime = new Date();
    var elapsedSecs = (stopTime.getTime() - startTime.getTime()) / 1000;
    document.getElementById("timing").innerHTML =
        "elapsed secs (" + operation + "): " + elapsedSecs;
}

// Replaces word i in s with r.
// A word is assumed to be a consecutive sequence of non-whitespace characters.
// Words are indexed from 0 to n - 1 inclusive where n is the number of words in s.
// If i is negative, the word to be replaced is n + i.
function replaceWord(s, i, r)
{
    var ts = trim(s);
    var pos; // Index 0: Start position of ith word. Index 1: Next position after ith word,
             // or -1 if i is the last word.

    function wordPos(s, i)
    {
        var pos1;
        var pos2;
        for (j = 0, start = 0; j <= i; ++j, start = pos2) {
            var p1 = s.substring(start).search(/\S+/); // assert(pos1 >= 0);
            pos1 = start + p1;
            start = pos1;
            var p2 = s.substring(start).search(/\s+/);
            pos2 = start + p2;
        }
        return [pos1, pos2];
    }

    if (i >= 0) {
        pos = wordPos(ts, i);
    } else {
        pos = wordPos(ts.split('').reverse().join(''), -i - 1);
        var pos0 = pos[0];
        var pos1 = pos[1];
        pos[0] = ts.length - pos1;
        pos[1] = ts.length - pos0;
    }

    // Replace word ...
    var rs = ts.substring(0, pos[0]) + r;
    if (pos[1] != -1)
        rs += ts.substring(pos[1])

    return rs;
}

// Updates the href of the details link use case 1 (primary branch analysis).
function updateDetailsLink(idSuffix)
{
    detailsLink = document.getElementById("detailsLink" + idSuffix);
    href = detailsLink.getAttribute("href");
    href = replaceWord(href, 1, server());
    href = replaceWord(href, -3, maxDetailsHistory());
    href = replaceWord(href, -1, currTimestamp());
    detailsLink.setAttribute("href", href);
}

// Updates the href of the details link for use case 2 (primary/secondary branch comparison).
function updateDetailsLink2(idSuffix)
{
    detailsLink = document.getElementById("detailsLink" + idSuffix);
    href = detailsLink.getAttribute("href");
    href = replaceWord(href, 1, server());
    href = replaceWord(href, -4, maxDetailsHistory());
    href = replaceWord(href, -3, sharedTimeScale());
    href = replaceWord(href, -1, currTimestamp());
    detailsLink.setAttribute("href", href);
}

function initSections()
{
    function finalizeInit()
    {
        setMessage("");

        // Show initial section ...
        state_ = cookie.getDynamicProperties();
        if (state_["currentSection"] == ixSection().name()) {
            ixSection().install();
        } else if (state_["currentSection"] == bcSection().name()) {
            bcSection().install();
        } else if (state_["currentSection"] == psSection().name()) {
            psSection().install();
        } else if (state_["currentSection"] == bsSection().name()) {
            bsSection().install();
        } else if (state_["currentSection"] == settingsSection().name()) {
            settingsSection().install();
        } else {
            psSection().install(); // default
        }
    }

    function loadIXStateFromCookie()
    {
        loadStateFromCookie(ixSection(), finalizeInit);
    }

    function loadBCStateFromCookie()
    {
        loadStateFromCookie(bcSection(), loadIXStateFromCookie);
    }

    function loadBSStateFromCookie()
    {
        loadStateFromCookie(bsSection(), loadBCStateFromCookie);
    }

    function loadPSStateFromCookie()
    {
        loadStateFromCookie(psSection(), loadBSStateFromCookie);
    }

    function resetIXSection()
    {
        ixSection().reset(loadPSStateFromCookie);
    }

    function resetBCSection()
    {
        bcSection().reset(resetIXSection);
    }

    function resetBSSection()
    {
        if (Cookie.enabled()) {
            bsSection().reset(resetBCSection);
        } else {
            finalizeInit();
        }
    }

    function resetPSSection()
    {
        psSection().reset(resetBSSection);
    }

    function handleReply(reply)
    {
        if (reply.error) {
            setMessage("initialization failed: " + reply.error);
        } else {
            resetPSSection();
        }
    }

    function handleError(error)
    {
        setMessage("initialization failed: " + error);
    }

    setMessage("initializing ...");

    // Send a dummy request to the server just to check if it is alive and responding ...
    sendRequest(["get", "metrics"], handleReply, handleError);
}

function getDefaultServer()
{
    function handleReply(reply)
    {
        var name;
        var color;
        if (reply.error) {
//            alert("getDefaultServer() got error reply: " + reply.error);
            name = "none";
            color = "red";
        } else {
            name = reply.server;
            color = "green";
        }
        var defaultServer = document.getElementById("defaultServer");
        defaultServer.innerHTML = name;
        defaultServer.style.color = color;
    }

    function handleError(error)
    {
        alert("getDefaultServer() failed: " + error);
    }

    sendRequest(["get", "server"], handleReply, handleError, true);
}

function sharedTimeScale()
{
    return document.getElementById("sharedTimeScale").checked ? 1 : 0;
}

function getIntFromText(text, defaultValue)
{
    var value = parseInt(text);
    if (isNaN(value))
        value = defaultValue;
    return value;
}

function getIntFromTextInput(id, defaultValue)
{
    return getIntFromText(document.getElementById(id).value, defaultValue);
}

function getMinIntFromText(text, minValue)
{
    var value = parseInt(text);
    if (isNaN(value) || (value < minValue))
        value = minValue;
    return value;
}

function getMinIntFromTextInput(id, minValue)
{
    return getMinIntFromText(document.getElementById(id).value, minValue);
}

function getMinMaxIntFromText(text, minValue, maxValue)
{
    var value = parseInt(text);
    if (isNaN(value) || (value < minValue))
        value = minValue;
    else if (value > maxValue)
        value = maxValue;
    return value;
}

function getMinMaxIntFromTextInput(id, minValue, maxValue)
{
    return getMinMaxIntFromText(document.getElementById(id).value, minValue, maxValue);
}

function getMinMaxRealFromText(text, minValue, maxValue)
{
    var value = parseFloat(text);
    if (isNaN(value) || (value < minValue))
        value = minValue;
    else if (value > maxValue)
        value = maxValue;
    return value;
}

function getMinRealFromText(text, minValue)
{
    var value = parseFloat(text);
    if (isNaN(value) || (value < minValue))
        value = minValue;
    return value;
}

function maxDetailsHistory()
{
    var mdh = parseInt(document.getElementById("maxDetailsHistory").value);
    if (isNaN(mdh))
        mdh = -1; // ### use e.g. 100 as default instead, in case the database is large?
    return mdh;
}

function maxBenchmarksPerScope()
{
    var mbps = parseInt(document.getElementById("maxBenchmarksPerScope").value);
    if (isNaN(mbps))
        mbps = -1; // ### use e.g. 100 as default instead, in case the database is large?
    return mbps;
}

function benchmarkFilter(id)
{
    var f = trim(document.getElementById(id).value);
    if (f == "")
        f = "*";
    return f;
}

function currTimestamp()
{
    var currDate = new Date();
    return Math.round(currDate.getTime() / 1000);
}

function disableTopSectionButtons(exceptId)
{
    var topSection = document.getElementById("topSection");
    var buttons = topSection.getElementsByTagName("input");
    var exceptButton = (exceptId != null) ? document.getElementById(exceptId) : null;
    for (var i = 0; i < buttons.length; ++i) {
        buttons[i].disabled = (buttons[i] != exceptButton);
    }
}

function hideAllSections()
{
    sectionParent = document.getElementById("section");
    var sections = sectionParent.getElementsByTagName("div");
    for (var i = 0; i < sections.length; ++i)
        hideElement(sections[i]);
}

function showElement(element)
{
    element.setAttribute("style", "display:block");
}

function hideElement(element)
{
    element.setAttribute("style", "display:none");
}

function removeElementChildren(element)
{
    while (element.childNodes.length >= 1)
        element.removeChild(element.lastChild);
}

function insertTitle(section, title)
{
    section.appendChild(document.createElement("br"));
    var span = section.appendChild(document.createElement("span"));
    span.setAttribute("class", "sectionTitle");
    span.appendChild(document.createTextNode(title));
}

function selectedOptions(id)
{
    var select = document.getElementById(id);
    if (!select)
        return [];
    var result = [];
    var options = select.options;
    for (i = 0; i < options.length; ++i) {
        if (options[i].selected)
            result = result.concat(options[i].value);
    }
    return result;
}

function sortOptions(id)
{
    var select = document.getElementById(id);
    if (!select)
        return;
    var optionValues = [];
    for (i = 0; i < select.options.length; ++i)
        optionValues[optionValues.length] = select.options[i].value;
    optionValues.sort(function(val1, val2) {
        return val1 > val2;
    });
    select.options.length = 0;
    for (i = 0; i < optionValues.length; ++i)
        select.options[select.options.length] = new Option(optionValues[i]);
}

function setDiffToleranceOptions(select)
{
    select.options[0] = new Option("0.00", "0.00");
    select.options[1] = new Option("0.01", "0.01");
    select.options[2] = new Option("0.02", "0.02");
    select.options[3] = new Option("0.05", "0.05");
    select.options[4] = new Option("0.10", "0.10");
    select.options[5] = new Option("0.20", "0.20");
    select.options[6] = new Option("0.50", "0.50");
    select.options[7] = new Option("1.00", "1.00");
}

function setStabToleranceOptions(select)
{
    select.options[0] = new Option("0", "0");
    select.options[1] = new Option("1", "1");
    select.options[2] = new Option("2", "2");
    select.options[3] = new Option("3", "3");
    select.options[4] = new Option("4", "4");
    select.options[5] = new Option("5", "5");
}

function matchesContext(contexts, candidate)
{
    for (var i = 0; i < contexts.length; ++i) {
        var context = contexts[i];
        var j;
        for (j = 0; j < candidate.length; ++j) {
            if ((candidate[j] != -1) && (candidate[j] != context[j]))
                break; // mismatch
        }
        if (j == candidate.length)
            return true; // no mismatch found for this context
    }
    return false; // mismatch found for all contexts
}

function ageText(msecs)
{
    var totSecs = Math.round(msecs / 1000);
    var secsInDay = 86400; // 24 * 60 * 60
    if (false) {
        // *** Option 1 ***
        var secs = totSecs % secsInDay;
        // Subtraction required to get integer division:
        var days = (totSecs - secs) / secsInDay;
        return days + " day" + ((days == 1) ? "" : "s") + " and " + secs + " secs ago";
    } else {
        // *** Option 2 ***
        //            return (totSecs / secsInDay).toFixed(2) + " days ago";
        return (totSecs / secsInDay).toFixed(2);
    }
}

function zeroPad2(s)
{
    return (s.length == 2) ? s : ("0" + s);
}

// ### refactor against other *Color functions?
function ageColor(msecs)
{
    var secs = Math.round(msecs / 1000);
    var secsInDay = 86400; // 24 * 60 * 60

    var minSecs = 0;
    var maxSecs = 31536000; // 365 * secsInDay

    var minR = 255;
    var minG = 128;
    var minB = 0;

    var maxR = 228;
    var maxG = 228;
    var maxB = 228;

    var frac = (secs - minSecs) / (maxSecs - minSecs);
    frac = Math.max(Math.min(frac, 1), 0);

    var r = Math.round((1 - frac) * minR + frac * maxR);
    var g = Math.round((1 - frac) * minG + frac * maxG);
    var b = Math.round((1 - frac) * minB + frac * maxB);
    var color =
        "#" + zeroPad2(r.toString(16)) + zeroPad2(g.toString(16))
        + zeroPad2(b.toString(16));
    return color;
}

function redToGreenColor(val, minAbsVal, maxAbsVal)
{
    if (Math.abs(val) < minAbsVal)
        return "#ffffff";

    var lo = -maxAbsVal;
    var hi = maxAbsVal;
    var frac = (val - lo) / (hi - lo);
    frac = Math.max(Math.min(frac, 1), 0);

    var val1 = Math.round(frac * 255 * 1.5);
    var val2 = Math.round((1 - frac) * 255 * 1.5);
    var r = (frac > 0.5) ? 255 : val1;
    var g = (frac < 0.5) ? 255 : val2;
    var b = (frac < 0.5) ? val1 : val2;
    var color =
        "#" + zeroPad2(r.toString(16)) + zeroPad2(g.toString(16))
        + zeroPad2(b.toString(16));
    return color;
}

function diffColor(diff)
{
    return redToGreenColor(diff, 0.0001, 0.01); // ### does this scaling interval make sense?
}

function trendColor(trend)
{
    // ### Does this scaling interval make sense? Since the trend line is computed from
    // the raw (i.e. unnormalized) time series, maybe only use three fixed colors
    // (for positive, zero, and negative trend)?
    return redToGreenColor(trend, 0.0001, 1);
}

function normalizedDifference(a, b)
{
    return (a == b) ? 0 : ((a - b) / (Math.max(Math.abs(a), Math.abs(b))));
}

function platformImage(platform)
{
    imageDir = "images/";
    map = [
        ["linux", "linux.png"],
        ["solaris", "solaris.png"],
        ["mac", "mac.png"],
        ["wince", "wince.png"],
        ["win", "windows.png"],
        ["s60", "s60.png"],
        ["symbian", "generic.png"]
    ];
    for (i = 0; i < map.length; ++i)
        if (platform.indexOf(map[i][0]) != -1)
            return imageDir + map[i][1];
    return imageDir + "defaultplatform.png";
}

function metricImage(metric)
{
    imageDir = "images/";
    map = [
        ["walltime", "walltime.png"],
        ["callgrind", "callgrind.png"]
    ];
    for (i = 0; i < map.length; ++i)
        if (map[i][0] == metric)
            return imageDir + map[i][1];
    return imageDir + "defaultmetric.png";
}

function arrayContains(a, v)
{
    for (i = 0; i < a.length; ++i)
        if (a[i] == v)
            return true;
    return false;
}

function appendSpans(elem)
{
    for (i = 1; i < arguments.length; ++i) {
        span = elem.appendChild(document.createElement("span"));
        span.title = arguments[i];
    }
}

function initialized()
{
    // ### the following trick may not work in all browsers (in Opera for instance?)
    if (!navigator.init) {
        navigator.init = true;
        return false;
    }
    return true;
}

function appendHelpLink(sectionElem, href)
{
    sectionElem.appendChild(document.createElement("br"));
    helpLink = sectionElem.appendChild(document.createElement("a"));
    helpLink.setAttribute("href", href);
    helpLink.appendChild(document.createTextNode("Help"));
}

function sectionElem2Name(sectionElem)
{
    if (sectionElem == ixSection().sectionElem)
        return ixSection().name();
    if (sectionElem == bcSection().sectionElem)
        return bcSection().name();
    if (sectionElem == psSection().sectionElem)
        return psSection().name();
    if (sectionElem == bsSection().sectionElem)
        return bsSection().name();
    if (sectionElem == settingsSection().sectionElem)
        return settingsSection().name();
    return null;
}

function currentSection()
{
    var sectionElems = document.getElementById("section").getElementsByTagName("div");
    for (var i = 0; i < sectionElems.length; ++i) {
        if (sectionElems[i].getAttribute("style").indexOf("block") != -1) {
            return sectionElem2Name(sectionElems[i]);
        }
    }
    return null;
}

var cookie; // For keeping state across sessions

function saveStateToCookie()
{
    function appendCookieState(state) {
        for (var name in state) {
            cookie.setDynamicProperty(name, state[name]);
        }
    }

    appendCookieState(ixSection().state());
    appendCookieState(bcSection().state());
    appendCookieState(psSection().state());
    appendCookieState(bsSection().state());
    appendCookieState(settingsSection().state());

    cookie.setDynamicProperty("currentSection", currentSection());

    cookie.store();
}

function loadStateFromCookie(section, handleDone)
{
    section.setState(cookie.getDynamicProperties(), handleDone);
}

function initialize()
{
    function initialize_()
    {
//         if (initialized()) {
//             alert("already initialized!");
//             return;
//         }

        getDefaultServer();

        // Install event handlers ...
        document.getElementById("indexButton").setAttribute(
            "onclick", "try { ixSection().install(); } catch (ex) "
            + "{ alert('ixSection().install() failed: ' + ex); }");
        document.getElementById("bmComparatorButton").setAttribute(
            "onclick", "try { bcSection().install(); } catch (ex) "
            + "{ alert('bcSection().install() failed: ' + ex); }");
        document.getElementById("platformSummaryButton").setAttribute(
            "onclick", "try { psSection().install(); } catch (ex) "
            + "{ alert('psSection().install() failed: ' + ex); }");
        document.getElementById("benchmarkSummaryButton").setAttribute(
            "onclick", "try { bsSection().install(); } catch (ex) "
            + "{ alert('bsSection().install() failed: ' + ex); }");
        document.getElementById("settingsButton").setAttribute(
            "onclick", "try { settingsSection().install(); } catch (ex) "
                + "{ alert('settingsSection().install() failed: ' + ex); }");

        // Create sections ...
        var sectionParent = document.getElementById("section");
        sectionParent.appendChild(ixSection().sectionElem);
        sectionParent.appendChild(bcSection().sectionElem);
        sectionParent.appendChild(psSection().sectionElem);
        sectionParent.appendChild(bsSection().sectionElem);
        sectionParent.appendChild(settingsSection().sectionElem);

        // If possible, load some state from a cookie ...
        if (Cookie.enabled()) {
            cookie = new Cookie("bmstate");
            window.onbeforeunload = function() { saveStateToCookie(); }
            loadStateFromCookie(settingsSection());
        }

        hideAllSections();
        disableTopSectionButtons("settingsButton");

        // Initialize sections (loading more state from the cookie etc.) ...
        initSections();
    }

    try { initialize_() } catch (ex) { alert("initialize_() failed: " + ex); }
}
