/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

function getCheckedFilterValues(tableId) {
    table = document.getElementById(tableId);
    values = [];
    rows = table.rows;
    for (i = 0; i < rows.length; ++i) {
        input = document.getElementById(tableId + ":" + i);
        if (input.checked)
            values[values.length] = rows[i].cells[1].innerHTML;
    }
    return values;
}

function createFilterOptionsString(option, values) {
    s = "";
    for (i = 0; i < values.length; ++i)
        s += " " + option + " " + values[i];
    return s;
}

function confirmComputeLink() {
    var testCaseFilter = getCheckedFilterValues("ixTestCaseFilter");
    var metricFilter = getCheckedFilterValues("ixMetricFilter");
    var platformFilter = getCheckedFilterValues("ixPlatformFilter");
    var hostFilter = getCheckedFilterValues("ixHostFilter");
    var branchFilter = getCheckedFilterValues("ixBranchFilter");

    var filterValuesSet =
        (testCaseFilter.length > 0)
        || (metricFilter.length > 0)
        || (platformFilter.length > 0)
        || (hostFilter.length > 0)
        || (branchFilter.length > 0);

    return filterValuesSet
        ? true
        : confirm("No filter values have been explicitly set! Compute " + linkDescr + " anyway?");
}

function updateComputeIndexLink(maxTimestamp, defaultMedianWinSize)
{
    // Initialize URL ...
    url = "http://" + location.host + "/cgi-bin/bmclientwrapper";
    url += "?command=";
    url += "-server " + server();
    url += " index get detailspage";
    url += " -stylesheet " + document.styleSheets[0].href;


    // Add base timestamp ...
    baseTime = getMinMaxIntFromTextInput("ixBaseTime", -1, maxTimestamp);
    document.getElementById("ixBaseTime").value = baseTime;

    url += " -basetimestamp " + baseTime;


    // Add median window size ...
    medianWinSize = getMinIntFromTextInput("ixMedianWinSize", 1);
    document.getElementById("ixMedianWinSize").value = medianWinSize;

    url += " -medianwinsize " + medianWinSize;


    // Add filters ...
    url += createFilterOptionsString("-testcase", getCheckedFilterValues("ixTestCaseFilter"));
    url += createFilterOptionsString("-metric",   getCheckedFilterValues("ixMetricFilter"));
    url += createFilterOptionsString("-platform", getCheckedFilterValues("ixPlatformFilter"));
    url += createFilterOptionsString("-host",     getCheckedFilterValues("ixHostFilter"));
    url += createFilterOptionsString("-branch",   getCheckedFilterValues("ixBranchFilter"));


    if (document.getElementById("ixDQStatsEnabled").checked) {
        // Add parameters for data quality statistics ...

        diffTol = getMinRealFromText(document.getElementById("ixDQStatsDiffTol").value, 0);
        document.getElementById("ixDQStatsDiffTol").value = diffTol;

        stabTol = getMinIntFromText(document.getElementById("ixDQStatsStabTol").value, 2);
        document.getElementById("ixDQStatsStabTol").value = stabTol;

        url += " -dataqualitystats -dqstatsdifftol " + diffTol + " -dqstatsstabtol " + stabTol;
    }


    // Update link ...
    link = document.getElementById("ixComputeIndexLink");
    link.setAttribute("href", url)

    link.setAttribute("target", "_blank"); // prevent page from opening in same window

    linkDescr = "index";
}

function updateComputeASFStatsLink(maxTimestamp, defaultMedianWinSize)
{
    // Initialize URL ...
    url = "http://" + location.host + "/cgi-bin/bmclientwrapper";
    url += "?command=";
    url += "-server " + server();
    url += " asfstats get detailspage";
    url += " -stylesheet " + document.styleSheets[0].href;

    // Add median window size ...
    medianWinSize = getIntFromTextInput("ixMedianWinSize", defaultMedianWinSize);
    document.getElementById("ixMedianWinSize").value = medianWinSize;
    url += " -medianwinsize " + medianWinSize;

    // Add filters ...
    url += createFilterOptionsString("-testcase", getCheckedFilterValues("ixTestCaseFilter"));
    url += createFilterOptionsString("-metric",   getCheckedFilterValues("ixMetricFilter"));
    url += createFilterOptionsString("-platform", getCheckedFilterValues("ixPlatformFilter"));
    url += createFilterOptionsString("-host",     getCheckedFilterValues("ixHostFilter"));
    url += createFilterOptionsString("-branch",   getCheckedFilterValues("ixBranchFilter"));

    // Add ASF-specific parameters ...
    fromTime = getMinMaxIntFromText(
        document.getElementById("ixASFStatsFromTime").value, 0, maxTimestamp);
    document.getElementById("ixASFStatsFromTime").value = fromTime;

    toTime = getMinMaxIntFromText(
        document.getElementById("ixASFStatsToTime").value, fromTime, maxTimestamp);
    document.getElementById("ixASFStatsToTime").value = toTime;

    diffTol = getMinRealFromText(document.getElementById("ixASFStatsDiffTol").value, 0);
    document.getElementById("ixASFStatsDiffTol").value = diffTol;

    stabTol = getMinIntFromText(document.getElementById("ixASFStatsStabTol").value, 1);
    document.getElementById("ixASFStatsStabTol").value = stabTol;

    sfTol = getMinMaxRealFromText(document.getElementById("ixASFStatsSFTol").value, 0, 100);
    document.getElementById("ixASFStatsSFTol").value = sfTol;

    lfTol = getMinMaxRealFromText(document.getElementById("ixASFStatsLFTol").value, 0, 100);
    document.getElementById("ixASFStatsLFTol").value = lfTol;

    maxLDTol = getMinRealFromText(document.getElementById("ixASFStatsMaxLDTol").value, 0);
    document.getElementById("ixASFStatsMaxLDTol").value = maxLDTol;

    url += " -fromtime " + fromTime
        + " -totime " + toTime
        + " -difftol " + diffTol
        + " -stabtol " + stabTol
        + " -sftol " + sfTol
        + " -lftol " + lfTol
        + " -maxldtol " + maxLDTol;

    // Update link ...
    link = document.getElementById("ixComputeASFStatsLink");
    link.setAttribute("href", url);

    link.setAttribute("target", "_blank"); // prevent page from opening in same window

    linkDescr = "ASF stats";
}

function IndexSection()
{
    var maxTimestamp         = 2147483647;
    var defaultBaseTime      = -1; // current time
    var defaultMedianWinSize = 8;

    // Variables to support highlighting of values that contribute to combination matches:
    var cmb; // (= combinations): a 2D index array (i.e. cmb[i][j] is the index of the
             // jth component of the ith combination)
    var sel; // (= selections): a 2D boolean array (i.e. sel[i][j] is true iff the
             // jth value of the ith component is selected)
    var mts; // (= matches): a 2D array that counts the number of combinations a given
             // value matches (i.e. mts[i][j] is the match count of the jth
             // value of the ith component). In order for a value to match a
             // given combination, the value itself and all the other values of
             // the combination need to be selected.

    var linkDescr = "<no descr>";

    this.name = function() { return "ixSection"; }

    this.reset = function(handleDone)
    {
        this.finalize_reset = function()
        {
            setMessage("");
            document.getElementById("ixReset").disabled = false;
            if (handleDone)
                handleDone();
        }

        this.handleReply_resetConfigs = function(reply)
        {
            if (reply.error) {
                alert("reset() failed when fetching configs (2): " + reply.error);
            } else {
                // Populate <select> tag ...
                select = document.getElementById("ixConfigs");
                select.options.length = 0;
                for (i = 0; i < reply.names.length; ++i)
                    select.options[select.options.length] = new Option(reply.names[i]);
            }

            this.finalize_reset();
        }

        this.handleError_resetConfigs = function(error)
        {
            alert("reset() failed when fetching configs (1): " + error);
            this.finalize_resetConfigs();
        }

        this.populateFilterTable = function(tableId, values, skipValue)
        {
            // alert("populating " + tableId + " filter table; values (" + values.length
            //       + "): " + values);
            var table = document.getElementById(tableId);

            var rowPos = 0;

            for (i = 0; i < values.length; ++i)  {

                if (values[i] == skipValue)
                    continue;

                tr = table.insertRow(rowPos);

                td = tr.insertCell(0);
                td.setAttribute("style", "border:0px; padding:0px");
                input = td.appendChild(document.createElement("input"));
                input.setAttribute("id", tableId + ":" + rowPos);
                input.setAttribute("type", "checkbox");
                input.onchange = function() {
                    thisSection.updateMatches();
                }

                td = tr.insertCell(1);
                td.setAttribute("style", "border:0px; padding:0px");

                // ### Just display the concatenation of multi-component values for now:
                value = "";
                for (j = 0; j < values[i].length; ++j)
                    value += values[i][j];
                td.innerHTML = value;

                rowPos++;
            }
        }

        this.handleReply_resetFilters = function(reply)
        {
            if (reply.error) {
                alert("reset() failed when fetching test cases and contexts (2): " + reply.error);

                this.finalize_reset();

            } else {

                this.populateFilterTable("ixTestCaseFilter", reply.values[0]);
                // Populate context component filter tables ...
                // (NOTE: Results measuring the "events" metric are skipped for now since
                // they seem to be of little value when computing the performance index (they are
                // typically integers close to (and often at) zero))
                this.populateFilterTable("ixMetricFilter", reply.values[1], "events");
                this.populateFilterTable("ixPlatformFilter", reply.values[2]);
                this.populateFilterTable("ixHostFilter", reply.values[3]);
                this.populateFilterTable("ixBranchFilter", reply.values[4]);

                // Initialize match-highlighting ... 

                // ... cmb array ...
                cmb = reply.tccontexts;

                // ... sel and mts arrays ...
                mainTable = document.getElementById("ixFilters");
                filterTablesRow = mainTable.rows[2];
                nFilters = filterTablesRow.cells.length; 
                sel = new Array(nFilters);
                mts = new Array(nFilters);
                for (i = 0; i < nFilters; ++i) {
                    filterTable = filterTablesRow.cells[i].childNodes[0];
                    nValues = filterTable.rows.length;
                    sel[i] = new Array(nValues);
                    mts[i] = new Array(nValues);
                    for (j = 0; j < nValues; ++j) {
                        sel[i][j] = true;
                        mts[i][j] = 0;
                    }
                }


                this.updateMatches();
            }

            // Fetch index configurations ...
            setMessage("fetching configurations ...");
            var args = ["index", "get", "configs"];
            sendRequest(
                args,
                function(reply) { thisSection.handleReply_resetConfigs(reply); },
                function(error) { thisSection.handleError_resetConfigs(error); });
        }

        this.handleError_resetFilters = function(error)
        {
            alert("reset() failed when fetching test cases and contexts (1): " + error);
            this.finalize_reset();
        }


        var thisSection = this; // ### hack?

        document.getElementById("ixReset").disabled = true;

        // Clear tables ...
        removeElementChildren(document.getElementById("ixTestCaseFilter"));
        removeElementChildren(document.getElementById("ixMetricFilter"));
        removeElementChildren(document.getElementById("ixPlatformFilter"));
        removeElementChildren(document.getElementById("ixHostFilter"));
        removeElementChildren(document.getElementById("ixBranchFilter"));

        // Fetch test cases + contexts ...
        setMessage("fetching test cases and contexts ...");
        var args = ["get", "tccontexts"];
        sendRequest(
            args,
            function(reply) { thisSection.handleReply_resetFilters(reply); },
            function(error) { thisSection.handleError_resetFilters(error); });
    }

    this.clearFilter = function(tableId, skipUpdate) {
        table = document.getElementById(tableId);
        for (i = 0; i < table.rows.length; ++i) {
            input = document.getElementById(tableId + ":" + i);
            input.checked = false;
        }

        if (!skipUpdate)
            this.updateMatches();
    }

    this.updateMatches = function() {

        nFilters = filterTablesRow.cells.length; 

        // Initialize ...
        for (i = 0; i < nFilters; ++i) {
            filterTable = filterTablesRow.cells[i].childNodes[0];
            nValues = filterTable.rows.length;

            // Zero the match count and count the number of checked values ...
            nChecked = 0;
            for (j = 0; j < nValues; ++j) {
                mts[i][j] = 0;
                // ### Note that we could optimize a bit by replacing the below line
                // with code to increment or decrement a counter!
                nChecked += (filterTable.rows[j].cells[0].childNodes[0].checked ? 1 : 0);
            }

            if (nChecked == 0) {
                // Regard all values as selected (since this filter is effectively disabled) ...
                for (j = 0; j < nValues; ++j)
                    sel[i][j] = true;
            } else {
                // Regard only the explicitly checked ones as selected ...
                for (j = 0; j < nValues; ++j)
                    sel[i][j] = (filterTable.rows[j].cells[0].childNodes[0].checked);
            }
        }

        // Count how many times each value contributes to a combination match ...
        for (i = 0; i < cmb.length; ++i) {
            // Check if all values of this combination are selected, and if so,
	    // increment the match count for each value ...
            match = true;
            for (j = 0; j < cmb[i].length; ++j) {
                k = cmb[i][j];
                if (!sel[j][k]) {
                    match = false;
                    break;
                }
            }
            if (match) {
                for (j = 0; j < cmb[i].length; ++j) {
                    k = cmb[i][j];
                    ++mts[j][k];
                }
            }
        }

        // Update background colors to highlight cells that contribute to a combination match ...
        color = ["#ffffff", "#aaffaa"];
        for (i = 0; i < nFilters; ++i) {
            filterTable = filterTablesRow.cells[i].childNodes[0];
            nValues = filterTable.rows.length;
            for (j = 0; j < nValues; ++j) {
                filterTable.rows[j].setAttribute(
                    "style", "background-color:" + color[(mts[i][j] > 0) ? 1 : 0]);
            }
        }
    }

    this.loadConfig = function()
    {
        this.finalize_loadConfig = function()
        {
            setMessage("");
        }

        this.handleReply_loadConfig = function(reply)
        {
            if (reply.error) {
                alert("loadConfig() failed (2): " + reply.error);

            } else {

                // Load non-filter values ...
                document.getElementById("ixBaseTime").value = reply.baseTimestamp;
                document.getElementById("ixMedianWinSize").value = reply.medianWinSize;

                // Load filter values ...
                function loadFilterValues(tableId, filter) {
                    thisSection.clearFilter(tableId, true);
                    table = document.getElementById(tableId);
                    for (i = 0; i < table.rows.length; ++i) {
                        name = table.rows[i].cells[1].innerHTML;
                        if (filter.indexOf(name) != -1)
                            table.rows[i].cells[0].childNodes[0].checked = true;
                    }
                }
                loadFilterValues("ixTestCaseFilter", reply.testCaseFilter);
                loadFilterValues("ixMetricFilter", reply.metricFilter);
                loadFilterValues("ixPlatformFilter", reply.platformFilter);
                loadFilterValues("ixHostFilter", reply.hostFilter);
                loadFilterValues("ixBranchFilter", reply.branchFilter);

                this.updateMatches();
            }


            this.finalize_loadConfig();
        }

        this.handleError_loadConfig = function(error)
        {
            alert("loadConfig() failed (1): " + error);
            this.finalize_loadConfig();
        }


        var thisSection = this; // ### hack?

        // Get current config name ...
        configName = selectedOptions("ixConfigs")[0]

        // Fetch config values ...
        setMessage("fetching config values ...");
        var args = ["index", "get", "config", "-name", "'" + configName + "'"];
        sendRequest(
            args,
            function(reply) { thisSection.handleReply_loadConfig(reply); },
            function(error) { thisSection.handleError_loadConfig(error); });
    }

    this.deleteConfig = function()
    {
        this.finalize_deleteConfig = function()
        {
            setMessage("");
        }

        this.handleReply_deleteConfig = function(reply)
        {
            if (reply.error) {
                alert("deleteConfig() failed (2): " + reply.error);
            } else {
                select = document.getElementById("ixConfigs");
                pos = -1;
                for (i = 0; i < select.options.length; ++i)
                    if (select.options[i].value == configName)  {
                        pos = i;
                        break;
                    }
                // assert(pos >= 0);
                select.remove(pos);

//                alert("configuration successfully deleted");
            }

            this.finalize_deleteConfig();
        }

        this.handleError_deleteConfig = function(error)
        {
            alert("deleteConfig() failed (1): " + error);
            this.finalize_deleteConfig();
        }

        var thisSection = this; // ### hack?

        // Get current config name ...
        configName = selectedOptions("ixConfigs")[0]

        if (!confirm("Really delete configuration '" + configName + "' ?"))
            return;

        setMessage("deleting config values ...");
        var args = ["index", "delete", "config", "-name", "'" + configName + "'"];
        sendRequest(
            args,
            function(reply) { thisSection.handleReply_deleteConfig(reply); },
            function(error) { thisSection.handleError_deleteConfig(error); });
    }

    this.saveConfig = function()
    {
        this.finalize_saveConfig = function()
        {
            setMessage("");
        }

        this.handleReply_saveConfig = function(reply)
        {
            if (reply.error) {
                alert("saveConfig() failed (2): " + reply.error);
            } else {

                select = document.getElementById("ixConfigs");

                pos = -1;
                for (i = 0; i < select.options.length; ++i)
                    if (select.options[i].value == configName)  {
                        pos = i;
                        break;
                    }
                if (pos == -1) {
                    select.appendChild(new Option(configName));
                    sortOptions("ixConfigs");
                }

//                alert("configuration successfully saved");
            }

            this.finalize_saveConfig();
        }

        this.handleError_saveConfig = function(error)
        {
            alert("saveConfig() failed (1): " + error);
            this.finalize_saveConfig();
        }

        var thisSection = this; // ### hack?

        // Get new config name ...
        configName = trim(document.getElementById("ixNewConfig").value);
        if (configName.length == 0) {
            alert("please enter a non-empty name!");
            return;
        }

        if (!confirm("Really save configuration '" + configName + "' ?"))
            return;

        // Save config values ...
        setMessage("saving config values ...");
        var args = [
            "index", "put", "config", "-name", "'" + configName + "'",
            "-basetimestamp", document.getElementById("ixBaseTime").value,
            "-medianwinsize", document.getElementById("ixMedianWinSize").value
        ];

        function filterSelection(tableId, option)
        {
            var sel = [];

            table = document.getElementById(tableId);
            for (i = 0; i < table.rows.length; ++i) {
                if (table.rows[i].cells[0].childNodes[0].checked) {
                    sel[sel.length] = option;
                    sel[sel.length] = table.rows[i].cells[1].innerHTML;

                }
            }

            return sel;
        }

        args = args.concat(filterSelection("ixTestCaseFilter", "-testcase"));
        args = args.concat(filterSelection("ixMetricFilter", "-metric"));
        args = args.concat(filterSelection("ixPlatformFilter", "-platform"));
        args = args.concat(filterSelection("ixHostFilter", "-host"));
        args = args.concat(filterSelection("ixBranchFilter", "-branch"));

        sendRequest(
            args,
            function(reply) { thisSection.handleReply_saveConfig(reply); },
            function(error) { thisSection.handleError_saveConfig(error); });
    }

    this.appendAll = function(section)
    {
        var thisSection = this; // ### hack?

        section.appendChild(document.createElement("br"));
        section.appendChild(document.createElement("br"));


        // Create 'top row' section ...
        table = section.appendChild(document.createElement("table"));
        table.setAttribute("style", "border:0px; padding:0px");
        topRow = table.insertRow(0);

        // ... 'compute index' link ...
        td = topRow.insertCell(topRow.cells.length);
        td.setAttribute("style", "border:0px; padding:2px");
        link = td.appendChild(document.createElement("a"));
        link.id = "ixComputeIndexLink";
        link.appendChild(document.createTextNode("Compute Index"));
        link.setAttribute("class", "ixLink_index");
        link.setAttribute("href", "dummy"); // for correct initial rendering
        link.setAttribute(
            "onmouseover",
            "updateComputeIndexLink("
                + maxTimestamp
                + ", " + defaultMedianWinSize
                + ")"
        );
        link.onclick = confirmComputeLink;

        // ... data quality stats fieldset ...
        td = topRow.insertCell(topRow.cells.length);
        td.setAttribute("style", "border:0px; padding:0px");
        fieldset = td.appendChild(document.createElement("fieldset"));
        legend = fieldset.appendChild(document.createElement("legend"));
        legend.appendChild(document.createTextNode("Data Quality Statistics"));
        legend.innerHTML = "Data Quality Statistics";
        legend.setAttribute("style", "font-size:12");
        table = fieldset.appendChild(document.createElement("table"));
        table.setAttribute("style", "border:0px; padding:0px");

        tr = table.insertRow(table.rows.length);
        td = tr.insertCell(0);
        td.innerHTML = "Enabled: ";
        td.setAttribute("style", "text-align:right");
        td = tr.insertCell(1);
        td.setAttribute("style", "text-align:left");
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "checkbox");
        input.setAttribute("id", "ixDQStatsEnabled");

        tr = table.insertRow(table.rows.length);
        td = tr.insertCell(0);
        td.setAttribute("style", "text-align:right");
        td.innerHTML = "Difference tolerance (%): ";
        td = tr.insertCell(1);
        td.setAttribute("style", "text-align:left");
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "text");
        input.setAttribute("id", "ixDQStatsDiffTol");
        input.setAttribute("Value", "2");

        tr = table.insertRow(table.rows.length);
        td = tr.insertCell(0);
        td.setAttribute("style", "text-align:right");
        td.innerHTML = "Stability tolerance: ";
        td = tr.insertCell(1);
        td.setAttribute("style", "text-align:left");
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "text");
        input.setAttribute("id", "ixDQStatsStabTol");
        input.setAttribute("Value", "3");

        // ... ASF scorecard stats fieldset ...
        td = topRow.insertCell(topRow.cells.length);
        td.setAttribute("style", "border:0px; padding:0px");
        fieldset = td.appendChild(document.createElement("fieldset"));
        legend = fieldset.appendChild(document.createElement("legend"));
        legend.appendChild(document.createTextNode("Data Quality Statistics"));
        legend.innerHTML = "ASF Scorecard Statistics";
        legend.setAttribute("style", "font-size:12");

        // ... 'compute ASF stats' link ...
        link = fieldset.appendChild(document.createElement("a"));
        link.id = "ixComputeASFStatsLink";
        link.appendChild(document.createTextNode("Compute"));
        link.setAttribute("class", "ixLink_index");
        link.setAttribute("href", "dummy"); // for correct initial rendering
        link.setAttribute(
            "onmouseover",
            "updateComputeASFStatsLink("
                + maxTimestamp
                + ", " + defaultMedianWinSize
                + ")"
        );
        link.onclick = confirmComputeLink;

        fieldset.appendChild(document.createElement("br"));

        table = fieldset.appendChild(document.createElement("table"));
        table.setAttribute("style", "border:0px; padding:0px; background-color:#eeeeee");
        tr = table.insertRow(table.rows.length);
        td = tr.insertCell(0);
        td.setAttribute("style", "text-align:right");
        td.innerHTML = "<i>MaxESS (maximum equality subsequence):</i>";
        td = tr.insertCell(1);
        td.innerHTML =
            "One of the subsequences formed by partitioning a result history into the smallest "
            + "number of contiguous sequences so that each sequence consists only of values that "
            + "are considered equal.";
        //
        tr = table.insertRow(table.rows.length);
        td = tr.insertCell(0);
        td.setAttribute("style", "text-align:right");
        td.innerHTML = "<i>Level:</i>";
        td = tr.insertCell(1);
        td.innerHTML = "The representative value of a MaxESS (typically the first value).";
        //
        tr = table.insertRow(table.rows.length);
        td = tr.insertCell(0);
        td.setAttribute("style", "text-align:right");
        td.innerHTML = "<i>Relative difference between positive real numbers v1 and v2:</i>";
        td = tr.insertCell(1);
        td.innerHTML =
            "100&nbsp;*&nbsp;(max(v1,&nbsp;v2)&nbsp;/&nbsp;min(v1,&nbsp;v2)"
            + "&nbsp;&nbsp;-&nbsp;&nbsp;1)";

        fieldset.appendChild(document.createElement("br"));

        table = fieldset.appendChild(document.createElement("table"));
        table.setAttribute("style", "border:0px; padding:0px");

        // ... 'from timestamp' ...
        tr = table.insertRow(table.rows.length);
        td = tr.insertCell(0);
        td.innerHTML = "From timestamp (secs since 1970): ";
        td.setAttribute("style", "text-align:right");
        td = tr.insertCell(1);
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "text");
        input.setAttribute("id", "ixASFStatsFromTime");
        input.setAttribute("value", "-1");
        td = tr.insertCell(2);
        td.innerHTML = "Latest time of first sample.";
        td.setAttribute("style", "background-color:#eeeeee");

        // ... 'to timestamp' ...
        tr = table.insertRow(table.rows.length);
        td = tr.insertCell(0);
        td.innerHTML = "To timestamp (secs since 1970): ";
        td.setAttribute("style", "text-align:right");
        td = tr.insertCell(1);
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "text");
        input.setAttribute("id", "ixASFStatsToTime");
        input.setAttribute("value", "-1");
        td = tr.insertCell(2);
        td.innerHTML = "Latest time of second sample.";
        td.setAttribute("style", "background-color:#eeeeee");

        // ... 'difference tolerance' ...
        tr = table.insertRow(table.rows.length);
        td = tr.insertCell(0);
        td.setAttribute("style", "text-align:right");
        td.innerHTML = "Difference tolerance (%): ";
        td = tr.insertCell(1);
        td.setAttribute("style", "text-align:left");
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "text");
        input.setAttribute("id", "ixASFStatsDiffTol");
        input.setAttribute("Value", "2");
        td = tr.insertCell(2);
        td.innerHTML =
            "The maximum allowed relative difference between two values for them to be "
            + "considered equal.";
        td.setAttribute("style", "background-color:#eeeeee");

        // ... 'stability tolerance' ...
        tr = table.insertRow(table.rows.length);
        td = tr.insertCell(0);
        td.setAttribute("style", "text-align:right");
        td.innerHTML = "Stability tolerance: ";
        td = tr.insertCell(1);
        td.setAttribute("style", "text-align:left");
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "text");
        input.setAttribute("id", "ixASFStatsStabTol");
        input.setAttribute("Value", "3");
        td = tr.insertCell(2);
        td.innerHTML = "The minimum length a MaxESS must have for it to be considered stable.";
        td.setAttribute("style", "background-color:#eeeeee");

        // ... 'stability fraction tolerance' ...
        tr = table.insertRow(table.rows.length);
        td = tr.insertCell(0);
        td.setAttribute("style", "text-align:right");
        td.innerHTML = "Stability fraction tolerance (%): ";
        td = tr.insertCell(1);
        td.setAttribute("style", "text-align:left");
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "text");
        input.setAttribute("id", "ixASFStatsSFTol");
        input.setAttribute("Value", "50");
        td = tr.insertCell(2);
        td.innerHTML =
            "The minimum SF (stability fraction) of a result history must have for it to be "
            + "considered stable, where SF is the percentage of MaxESS'es that are stable.";
        td.setAttribute("style", "background-color:#eeeeee");

        // ... 'level fraction tolerance' ...
        tr = table.insertRow(table.rows.length);
        td = tr.insertCell(0);
        td.setAttribute("style", "text-align:right");
        td.innerHTML = "Unique level fraction tolerance (%): ";
        td = tr.insertCell(1);
        td.setAttribute("style", "text-align:left");
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "text");
        input.setAttribute("id", "ixASFStatsLFTol");
        input.setAttribute("Value", "50");
        td = tr.insertCell(2);
        td.innerHTML =
            "The minimum ULF (unique level fraction) a result history must have for it to be "
            + "considered stable, where ULF is the percentage of levels that are unique.";
        td.setAttribute("style", "background-color:#eeeeee");

        // ... 'max level difference tolerance' ...
        tr = table.insertRow(table.rows.length);
        td = tr.insertCell(0);
        td.setAttribute("style", "text-align:right");
        td.innerHTML = "Max level difference tolerance (%): ";
        td = tr.insertCell(1);
        td.setAttribute("style", "text-align:left");
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "text");
        input.setAttribute("id", "ixASFStatsMaxLDTol");
        input.setAttribute("Value", "200");
        td = tr.insertCell(2);
        td.innerHTML = "The percentage of subsequence levels that are unique.";
        td.innerHTML =
            "The maximum allowed relative difference between any two levels within the "
            + "[from, to] timestamp range in a result history "
            + "for it to be considered stable.";
        td.setAttribute("style", "background-color:#eeeeee");


        // Create 'Configuration' section ...
        section.appendChild(document.createElement("br"));
        section.appendChild(document.createElement("br"));
        table = section.appendChild(document.createElement("table"));
        table.setAttribute("style", "border:0px; padding:0px");
        tr = table.insertRow(0);
        td = tr.insertCell(0);
        td.setAttribute("style", "border:0px; padding:0px");
        configFieldset = td.appendChild(document.createElement("fieldset"));

        legend = configFieldset.appendChild(document.createElement("legend"));
        legend.appendChild(document.createTextNode("Configuration"));

        // ... 'Reset' button ...
        input = configFieldset.appendChild(document.createElement("input"));
        input.setAttribute("type", "button");
        input.setAttribute("id", "ixReset");
        input.setAttribute("value", "Reset");
        input.onclick = function() {
            try { thisSection.reset(); }
            catch (ex) { alert("thisSection.reset() failed: " + ex); }
        }

        configFieldset.appendChild(document.createElement("br"));
        configFieldset.appendChild(document.createElement("br"));

        table = configFieldset.appendChild(document.createElement("table"));
        table.setAttribute("style", "border:0px; padding:0px");
        tr = table.insertRow(0);
        topLeftCell = tr.insertCell(0);
        topLeftCell.setAttribute("style", "border:0px; padding:0px");
        topMidCell = tr.insertCell(1);
        topMidCell.setAttribute("style", "border:0px; padding:10px");
        topRightCell = tr.insertCell(2);
        topRightCell.setAttribute("style", "border:0px; padding:0px");

        // Create "miscellaneous settings" section ...
        table = topLeftCell.appendChild(document.createElement("table"));
        table.setAttribute("style", "border:0px; padding:0px");

        // ... base timestamp ...
        tr = table.insertRow(table.rows.length);
        td = tr.insertCell(0);
        td.innerHTML = "Base timestamp (secs since 1970): ";
        td.setAttribute("style", "text-align:right");
        td = tr.insertCell(1);
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "text");
        input.setAttribute("id", "ixBaseTime");
        input.setAttribute("value", defaultBaseTime);

        // ... median window size ...
        tr = table.insertRow(table.rows.length);
        td = tr.insertCell(0);
        td.innerHTML = "Median window size: ";
        td.setAttribute("style", "text-align:right");
        td = tr.insertCell(1);
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "text");
        input.setAttribute("id", "ixMedianWinSize");
        input.setAttribute("value", defaultMedianWinSize);


        // Create "load/save" section ...
        loadSaveTable = topRightCell.appendChild(document.createElement("table"));

        // ... available configurations + operations 'load' and 'delete' ...
        tr = loadSaveTable.insertRow(loadSaveTable.rows.length);
        td = tr.insertCell(0);
        select = td.appendChild(document.createElement("select"));
        select.setAttribute("id", "ixConfigs");
        select.setAttribute("size", "8");
        select.onchange = function() {
            select = document.getElementById("ixConfigs");
            document.getElementById("ixNewConfig").value =
                select.options[select.selectedIndex].value;
        }

        td = tr.insertCell(1);
        loadDeleteTable = td.appendChild(document.createElement("table"));
        loadDeleteTable.setAttribute("style", "border:0px; padding:0px");

        tr = loadDeleteTable.insertRow(loadDeleteTable.rows.length);
        td = tr.insertCell(0);
        td.setAttribute("style", "border:0px; padding:2px");
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "button");
        input.setAttribute("id", "ixLoadConfig");
        input.setAttribute("value", "Load");
        input.onclick = function() {
            try { thisSection.loadConfig(); }
            catch (ex) { alert("thisSection.loadConfig() failed: " + ex); }
        }

        tr = loadDeleteTable.insertRow(loadDeleteTable.rows.length);
        td = tr.insertCell(0);
        td.setAttribute("style", "border:0px; padding:2px");
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "button");
        input.setAttribute("id", "ixDeleteConfig");
        input.setAttribute("value", "Delete");
        input.onclick = function() {
            try { thisSection.deleteConfig(); }
            catch (ex) { alert("thisSection.deleteConfig() failed: " + ex); }
        }


        // ... new configuration + operation 'save' ...
        tr = loadSaveTable.insertRow(loadSaveTable.rows.length);
        td = tr.insertCell(0);
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "text");
        input.setAttribute("id", "ixNewConfig");
//        input.setAttribute("value", "<enter new name here>");
        td = tr.insertCell(1);
        td.setAttribute("colspan", "2");
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "button");
        input.setAttribute("id", "ixSaveConfig");
        input.setAttribute("value", "Save");
        input.onclick = function() {
            try { thisSection.saveConfig(); }
            catch (ex) { alert("thisSection.saveConfig() failed: " + ex); }
        }


        // Create 'Filters' section ...
        configFieldset.appendChild(document.createElement("br"));
        table = configFieldset.appendChild(document.createElement("table"));
        table.setAttribute("style", "border:0px; padding:0px");
        tr = table.insertRow(0);
        td = tr.insertCell(0);
        td.setAttribute("style", "border:0px; padding:0px");
        filtersFieldset = td.appendChild(document.createElement("fieldset"));

        legend = filtersFieldset.appendChild(document.createElement("legend"));
        legend.appendChild(document.createTextNode("Filters"));

        // ... a bit of documentation ...
        table = filtersFieldset.appendChild(document.createElement("table"));
        table.setAttribute("style", "border:0px; padding:0px");
        tr = table.insertRow(0);
        td = tr.insertCell(0);
        td.setAttribute("style", "background-color:#eeeeee");
        td.innerHTML =
            "<b>Note</b>: The green backgrounds in the table below indicate matching combinations. "
            + "<br /><b>Note</b>: A filter with no explicitly selected values is considered "
            + "disabled/open, i.e. it behaves as if all values were selected.";

        filtersFieldset.appendChild(document.createElement("br"));

        // ... main table ...
        table = filtersFieldset.appendChild(document.createElement("table"));
        table.setAttribute("id", "ixFilters");

        table.insertRow(0); // Header row
        table.insertRow(1); // Row for 'clear' buttons
        table.insertRow(2); // Row for values

        thisSection.addFilter = function(table, title, id)
        {
            headerRow = table.rows[0];
            clearButtonsRow = table.rows[1];
            valuesRow = table.rows[2];
            insertPos = headerRow.cells.length;

            th = headerRow.appendChild(document.createElement("th"));
            th.innerHTML = title;

            td = clearButtonsRow.insertCell(insertPos);
            input = td.appendChild(document.createElement("input"));
            input.setAttribute("type", "button");
            input.setAttribute("value", "Clear");
            input.onclick = function() { thisSection.clearFilter(id); }

            td = valuesRow.insertCell(insertPos);
            table = td.appendChild(document.createElement("table"));
            table.setAttribute("id", id);
            table.setAttribute("style", "border:0px; padding:0px");
        }

        thisSection.addFilter(table, "Test Case", "ixTestCaseFilter");
        thisSection.addFilter(table, "Metric", "ixMetricFilter");
        thisSection.addFilter(table, "Platform", "ixPlatformFilter");
        thisSection.addFilter(table, "Host", "ixHostFilter");
        thisSection.addFilter(table, "Branch", "ixBranchFilter");
    }

    this.create = function()
    {
        var sectionElem = document.createElement("div");
        insertTitle(sectionElem, "Index");
        sectionElem.appendChild(document.createElement("br"));
        appendHelpLink(sectionElem, "help.html#Index");

        this.appendAll(sectionElem);

        return sectionElem;
    }

    this.state = function() {
        var state_ = [];

        // Data Quality Stats parameters ...

        // ... difference tolerance ...
        state_["ixDQStatsDiffTol"] = document.getElementById("ixDQStatsDiffTol").value;

        // ... stability tolerance ...
        state_["ixDQStatsStabTol"] = document.getElementById("ixDQStatsStabTol").value;


        // ASF Scorecard Stats parameters ...

        // ... 'from' timestamp ...
        state_["ixASFStatsFromTime"] = document.getElementById("ixASFStatsFromTime").value;

        // ... 'to' timestamp ...
        state_["ixASFStatsToTime"] = document.getElementById("ixASFStatsToTime").value;

        // ... difference tolerance ...
        state_["ixASFStatsDiffTol"] = document.getElementById("ixASFStatsDiffTol").value;

        // ... stability tolerance ...
        state_["ixASFStatsStabTol"] = document.getElementById("ixASFStatsStabTol").value;

        // ... stability fraction tolerance ...
        state_["ixASFStatsSFTol"] = document.getElementById("ixASFStatsSFTol").value;

        // ... unique level fraction tolerance ...
        state_["ixASFStatsLFTol"] = document.getElementById("ixASFStatsLFTol").value;

        // ... max level difference tolerance ...
        state_["ixASFStatsMaxLDTol"] = document.getElementById("ixASFStatsMaxLDTol").value;

        return state_;
    }

    this.setState = function(state_, handleDone) {

        // Data Quality Stats parameters ...

        // ... difference tolerance ...
        document.getElementById("ixDQStatsDiffTol").value =
            getIntFromText(state_["ixDQStatsDiffTol"], 2);

        // ... stability tolerance ...
        document.getElementById("ixDQStatsStabTol").value =
            getIntFromText(state_["ixDQStatsStabTol"], 3);


        // ASF Scorecard Stats parameters ...

        // ... 'from' timestamp ...
        document.getElementById("ixASFStatsFromTime").value =
            getIntFromText(state_["ixASFStatsFromTime"], -1);

        // ... 'to' timestamp ...
        document.getElementById("ixASFStatsToTime").value =
            getIntFromText(state_["ixASFStatsToTime"], -1);

        // ... difference tolerance ...
        document.getElementById("ixASFStatsDiffTol").value =
            getIntFromText(state_["ixASFStatsDiffTol"], 2);

        // ... stability tolerance ...
        document.getElementById("ixASFStatsStabTol").value =
            getIntFromText(state_["ixASFStatsStabTol"], 3);

        // ... stability fraction tolerance ...
        document.getElementById("ixASFStatsSFTol").value =
            getIntFromText(state_["ixASFStatsSFTol"], 50);

        // ... unique level fraction tolerance ...
        document.getElementById("ixASFStatsLFTol").value =
            getIntFromText(state_["ixASFStatsLFTol"], 50);

        // ... max level level difference tolerance ...
        document.getElementById("ixASFStatsMaxLDTol").value =
            getIntFromText(state_["ixASFStatsMaxLDTol"], 200);


        // complete this ...
        // also convert the stab tol in the data quality stats section to a regular text input
        // (rather than a select input) ...

//        2 B DONE!


        handleDone();
    }

    SectionBase.call(this, "indexButton");
    this.sectionElem = this.create();
}

IndexSection.prototype = new SectionBase();
IndexSection.prototype.constructor = IndexSection;
IndexSection.instance = function()
{
    if (!IndexSection.instance_)
        IndexSection.instance_ = new IndexSection();
    return IndexSection.instance_;
}
function ixSection() { return IndexSection.instance(); }
