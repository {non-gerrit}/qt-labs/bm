/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

function PlatformSummarySection()
{
    this.name = function() { return "psSection"; }

    this.updateUpdateSummaryButtonDisability = function()
    {
        document.getElementById("updatePSSummary").disabled =
            (selectedOptions("psBranches").length == 0);
    }

    this.resetConfig = function(handleFinalize)
    {
        this.finalize_config = function()
        {
            setMessage("");
            thisSection.updateTopSection();

            document.getElementById("resetPSConfig").disabled = false;
            this.updateUpdateSummaryButtonDisability();
            if (handleFinalize)
                handleFinalize();
        }

        this.handleReply_metrics = function(reply)
        {
            if (reply.error) {
                alert("resetConfig() failed in handleReply_metrics(): " + reply.error);

            } else {

                var select = document.getElementById("psPrimaryMetric");
                select.options.length = 0;

                for (var i = 0; i < reply.items.length; ++i) {
                    var option = new Option();
                    select.appendChild(option);
                    option.text = reply.items[i][0];
                    option.value = option.text;
                }
            }

            this.finalize_config();
        }

        this.handleReply_branches = function(reply)
        {
            if (reply.error) {
                alert("resetConfig() failed in handleReply_branches(): " + reply.error);
                this.finalize_config();
            } else {

                var select = document.getElementById("psBranches");
                select.options.length = 0;

                for (var i = 0; i < reply.items.length; ++i) {
                    var option = new Option();
                    select.appendChild(option);
                    option.text = reply.items[i][0] + " " + reply.items[i][1];
                    option.value = option.text;
                }

                // Fetch metrics ...
                var args = ["get", "metrics"];
                sendRequest(
                    args,
                    function(reply) { thisSection.handleReply_metrics(reply); },
                    function(error) { thisSection.handleError_config(error); });
            }
        }

        this.handleError_config = function(error)
        {
            alert("resetConfig() failed: " + error);
            this.finalize_config();
        }

        disableTopSectionButtons();
        setMessage("resetting platform summary configuration ...");

        var thisSection = this; // ### hack?

        document.getElementById("resetPSConfig").disabled = true;

        // Fetch branches ...
        var args = ["get", "branches"];
        sendRequest(
            args,
            function(reply) { thisSection.handleReply_branches(reply); },
            function(error) { thisSection.handleError_config(error); });
    }

    this.setConfig = function(
        branches, primaryMetric, primaryMetricOnly, diffTolerance, stabTolerance, handleDone)
    {
        this.handleFinalize = function()
        {
            // Branches ...
            select = document.getElementById("psBranches");
            options = select.options;
            for (i = 0; i < options.length; ++i)
                options[i].selected = branches[options[i].value];

            // Primary metric ...
            select = document.getElementById("psPrimaryMetric");
            options = select.options;
            for (i = 0; i < options.length; ++i)
                options[i].selected = (options[i].value == primaryMetric);

            // Primary metric only ...
            document.getElementById("psPrimaryMetricOnly").checked = primaryMetricOnly;

            // Diff tolerance ...
            select = document.getElementById("psDiffTolerance");
            options = select.options;
            for (i = 0; i < options.length; ++i)
                options[i].selected = (options[i].value == diffTolerance);

            // Stability tolerance ...
            select = document.getElementById("psStabTolerance");
            options = select.options;
            for (i = 0; i < options.length; ++i)
                options[i].selected = (options[i].value == stabTolerance);

            thisSection.updateUpdateSummaryButtonDisability();

            if (handleDone)
                handleDone();
        }

        var thisSection = this; // ### hack?
        var finalizeHandler = function(constraint) { thisSection.handleFinalize(); }

        this.resetConfig(finalizeHandler);
    }

    this.reset = function(handleDone)
    {
        this.resetConfig(handleDone);
//        this.resetSummary();
    }

    this.dirtyUpdateSummaryButton = function()
    {
        document.getElementById("updatePSSummary").value = "* Update";
    }

    this.openBSSectionFromCell = function(e)
    {
        this.openBSSectionHandleDone = function()
        {
            if (!updateSummaryCalled) {
                // Step 2:
                bsSection().updateSummary(handleDone);
                updateSummaryCalled = true;
            } else {
                // Step 3:
                bsSection().install();
            }
        }

        var thisSection = this; // ### hack?
        var handleDone = function() { thisSection.openBSSectionHandleDone(); }
        var updateSummaryCalled = false;

        // Step 1:
        spans = e.target.getElementsByTagName("span");
        disableTopSectionButtons();
        bsSection().setConfiguration(
            spans[0].title, spans[1].title, spans[2].title, spans[3].title, spans[4].title,
            spans[5].title, null, null, null, null, handleDone);
    }

    this.updateSummary = function()
    {
        function finalize_summary()
        {
            var button = document.getElementById("updatePSSummary");
            if (!errorDetected)
                button.value = "Update";
            button.disabled = false;

            setMessage("");
            thisSection.updateTopSection();

            stopTiming("platform summary update");
        }

        function processNextPlatformBranch()
        {
            if ((++currBranch % branches.length) == 0) {
                currBranch = 0;
                currPlatform++;
                currRow++;
                currCol = 0;
            }
            if (currPlatform == platforms.length) {
                finalize_summary();
            } else {
                // Fetch hosts for next platform/branch combination ...
                fetchHosts();
            }
        }

        this.handleReply_statistics = function(reply)
        {
            if (reply.error) {
                alert("updateSummary() failed in handleReply_statistics(): " + reply.error);
                errorDetected = true;
                finalize_summary();
                return;
            }

//             alert("got stats for " + platforms[currPlatform] + " / " + branches[currBranch]
//                   + " / " + actualHost + " / " + actualMetric);

            tr = table.rows[currRow];

            if (reply.nBenchmarks == 0) {

                td = tr.insertCell(currCol++);
                td.setAttribute("colspan", branchColumns);
                td.innerHTML = "no results matching benchmark filter";
                td.setAttribute("class", "notFound");

            } else {

                td = tr.insertCell(currCol++);
                td.innerHTML = actualHost;
                td = tr.insertCell(currCol++);
                img = td.appendChild(document.createElement("img"));
                img.setAttribute("src", metricImage(actualMetric));
                tn = td.appendChild(document.createTextNode(""));
                td.appendChild(document.createTextNode(actualMetric));
                // In order to include the '&nbsp;': (### can this be done without using innerHTML?)
                td.innerHTML = "<img src=\"" + metricImage(actualMetric)
                    + "\" />&nbsp;" + actualMetric;

                td = tr.insertCell(currCol++);

                // detailsLink = td.appendChild(document.createElement("img"));
                // detailsLink.setAttribute("src", "images/table.png");

                detailsLink = td.appendChild(document.createElement("span"));
                detailsLink.setAttribute("onmouseover", "this.style.cursor='pointer';");
                detailsLink.setAttribute("style", "color:#0000ee"); // ### hardcoded for now
                detailsLink.innerHTML = "Details";

                appendSpans(
                    detailsLink, actualMetric, platforms[currPlatform], actualHost,
                    branches[currBranch], diffTolerance, stabTolerance);
                detailsLink.addEventListener("click", openBSSectionFunc, false);

                if (reply.nStableLastDiffs > 0) {

                    td = tr.insertCell(currCol++);
                    var minStableLastDiff = reply.minStableLastDiff;
                    td.innerHTML = minStableLastDiff.toFixed(2);
                    td.setAttribute("style", "background-color:" + diffColor(minStableLastDiff));
                    td.setAttribute("class", "lastDiff");

                    td = tr.insertCell(currCol++);
                    var avgStableLastDiff = reply.avgStableLastDiff;
                    td.innerHTML = avgStableLastDiff.toFixed(2);
                    td.setAttribute("style", "background-color:" + diffColor(avgStableLastDiff));
                    td.setAttribute("class", "lastDiff");

                    td = tr.insertCell(currCol++);
                    var maxStableLastDiff = reply.maxStableLastDiff;
                    td.innerHTML = maxStableLastDiff.toFixed(2);
                    td.setAttribute("style", "background-color:" + diffColor(maxStableLastDiff));
                    td.setAttribute("class", "lastDiff");
                } else {
                    td = tr.insertCell(currCol++);
                    td.setAttribute("colspan", 3);
                    td.innerHTML = "no stable last differences";
                    td.setAttribute("class", "notFound");
                }
            }

            processNextPlatformBranch();
        }

        this.handleReply_metrics = function(reply)
        {
            if (reply.error) {
                alert("updateSummary() failed in handleReply_metrics(): " + reply.error);
                errorDetected = true;
                finalize_summary();
                return;
            }

//             alert("got metrics for " + platforms[currPlatform] + " / " + branches[currBranch]
//                   + " / " + hosts[currHost] + ": ...");

            if (hosts[currHost] == hosts[0]) {
                backupHost = hosts[currHost];
                backupMetric = reply.items[0][0];
            }

            if (actualMetric) {
                alert("bug: actualMetric should be null at this point");
                return;
            }

            // Try to find a primary metric match ...
            for (var i = 0; i < reply.items.length; ++i) {
                if (reply.items[i][0] == primaryMetric) {
                    actualMetric = primaryMetric;
                    break;
                }
            }

            if (actualMetric) {
                // Fetch statistics for primary metric ...
                actualHost = hosts[currHost];
                fetchStatistics();
            } else {
                // No primary metric match ...

                if (currHost == (hosts.length - 1)) {
                    // Hosts exhausted ...
                    if (!primaryMetricOnly) {
                        // Fetch statistics for backup metric ...
                        actualMetric = backupMetric;
                        actualHost = backupHost;
                        fetchStatistics();
                    } else {
                        // Give up and proceed to next platform/branch combination ...
                        tr = table.rows[currRow];
                        td = tr.insertCell(currCol++);
                        td.setAttribute("colspan", branchColumns);
                        td.innerHTML = "no results for primary metric";
                        td.setAttribute("class", "notFound");

                        processNextPlatformBranch();
                    }
                } else {
                    // Check next host for metric match ...
                    currHost++;
                    fetchMetrics();
                }
            }
        }

        this.handleReply_hosts = function(reply)
        {
            if (reply.error) {
                alert("updateSummary() failed in handleReply_hosts(): " + reply.error);
                errorDetected = true;
                finalize_summary();
                return;
            }

//             alert("# of hosts for " + platforms[currPlatform] + " / " + branches[currBranch]
//                   + " : " + reply.items.length);

            var tr;

            if (currBranch == 0) {
                tr = table.insertRow(currRow);
                td = tr.insertCell(currCol++);

                // Note: &#8209; is the non-breaking hyphen:
                // (### can this be done without using innerHTML?)
                td.innerHTML = "<img src=\"" + platformImage(platforms[currPlatform])
                    + "\" />&nbsp;" + platforms[currPlatform].replace(/-/g, "&#8209;");

            } else {
                tr = table.rows[currRow];
            }

            hosts = [];
            for (var i = 0; i < reply.items.length; ++i)
                hosts = hosts.concat(reply.items[i][0]);

            if (hosts.length == 0) {
                td = tr.insertCell(currCol++);
                td.setAttribute("colspan", branchColumns);
                td.innerHTML = "no results for any metric";
                td.setAttribute("class", "notFound");

                processNextPlatformBranch();
            } else {
                currHost = 0;
                actualHost = null;
                actualMetric = null;
                fetchMetrics();
            }
        }

        this.handleReply_platforms = function(reply)
        {
            if (reply.error) {
                alert("updateSummary() failed in handleReply_platforms(): " + reply.error);
                errorDetected = true;
                finalize_summary();
                return;
            }

            // Merge into set (### optimize this from O(n^2) to O(n log n) ?)
            for (var i = 0; i < reply.items.length; ++i) {
                p = reply.items[i][0];
                if (!arrayContains(platforms, p))
                    platforms = platforms.concat(p);
            }

            currBranch++;

            if (currBranch == branches.length) {
                currPlatform = -1;
                currBranch = -1;
                currRow = 2;
                processNextPlatformBranch();
            } else {
                // Fetch platforms for next branch ...
                fetchPlatforms();
            }
        }

        this.handleError_summary = function(error)
        {
            alert("updateSummary() failed: " + error);
            errorDetected = true;
        }

        function fetchPlatforms()
        {
            var branch = branches[currBranch].split(' ');
            var args = ["get", "platforms", "-branch", branch[0], branch[1]];
            sendRequest(
                args,
                function(reply) { thisSection.handleReply_platforms(reply); },
                function(error) { thisSection.handleError_summary(error); });
        }

        function fetchHosts()
        {
            var branch = branches[currBranch].split(' ');
            var args = ["get", "hosts", "-platform", platforms[currPlatform],
                        "-branch", branch[0], branch[1]];
            sendRequest(
                args,
                function(reply) { thisSection.handleReply_hosts(reply); },
                function(error) { thisSection.handleError_summary(error); });
        }

        function fetchMetrics()
        {
            var branch = branches[currBranch].split(' ');
            var args = ["get", "metrics", "-platform", platforms[currPlatform],
                        "-branch", branch[0], branch[1],
                        "-host", hosts[currHost]];
            sendRequest(
                args,
                function(reply) { thisSection.handleReply_metrics(reply); },
                function(error) { thisSection.handleError_summary(error); });
        }

        function fetchStatistics()
        {
            setMessage("updating platform summary ... (fetching statistics ...)");
            var branch = branches[currBranch].split(' ');
            var args =
                ["get", "stats",
                 actualMetric, platforms[currPlatform], actualHost,
                 branch[0], branch[1], "-timerange", "first", "last",
                 diffTolerance, stabTolerance, testCaseFilter, testFunctionFilter,
                 dataTagFilter];
            sendRequest(
                args,
                function(reply) { thisSection.handleReply_statistics(reply); },
                function(error) { thisSection.handleError_summary(error); });
        }

        disableTopSectionButtons();
        setMessage("updating platform summary ...");

        var thisSection = this; // ### hack?

        var errorDetected = false;
        var currRow;
        var currCol;

        var branches = selectedOptions("psBranches");
        if (branches.length == 0)
            return;
        var primaryMetric = selectedOptions("psPrimaryMetric")[0];
        var primaryMetricOnly = document.getElementById("psPrimaryMetricOnly").checked;
        var diffTolerance = selectedOptions("psDiffTolerance")[0];
        var stabTolerance = selectedOptions("psStabTolerance")[0];
        var testCaseFilter = benchmarkFilter("testCaseFilter");
        var testFunctionFilter = benchmarkFilter("testFunctionFilter");
        var dataTagFilter = benchmarkFilter("dataTagFilter");
        var currDate = new Date();

        var branchColumns = 6;
        // (host, metric, icon, stable last difference (best, avg, worst))

        document.getElementById("psSummaryConfig").innerHTML =
            "<b>Primary&nbsp;metric:</b>&nbsp;" + primaryMetric
            + "&nbsp;&nbsp; <b>Primary&nbsp;metric only:</b>&nbsp;" + primaryMetricOnly
            + "&nbsp;&nbsp; <b>Difference&nbsp;tolerance:</b>&nbsp;" + diffTolerance
            + "&nbsp;&nbsp; <b>Stability&nbsp;tolerance:</b>&nbsp;" + stabTolerance
            + "&nbsp;&nbsp; <b>Snapshot&nbsp;range:</b>&nbsp;&infin;"
            + "&nbsp;&nbsp; <b>Test&nbsp;case&nbsp;filter:</b>&nbsp;" + testCaseFilter
            + "&nbsp;&nbsp; <b>Test&nbsp;function&nbsp;filter:</b>&nbsp;" + testFunctionFilter
            + "&nbsp;&nbsp; <b>Data&nbsp;tag&nbsp;filter:</b>&nbsp;" + dataTagFilter
            + "&nbsp;&nbsp; <b>Date:</b>&nbsp;" + currDate.toDateString().replace(/\s/g, "&nbsp;")

        var table = document.getElementById("psSummary");

        // Reset table ...
        while (table.rows.length > 0)
            table.deleteRow(0);
        tr = table.insertRow(0);
        td = tr.insertCell(0);

        tr = table.insertRow(1);
        td = tr.insertCell(0);

        tr = table.insertRow(2);
        th = tr.appendChild(document.createElement("th"));
        th.appendChild(document.createTextNode("Platform"));

        for (i = 0; i < branches.length; ++i) {
            tr = table.rows[0];
            td = tr.insertCell(1 + i);
            td.setAttribute("colspan", branchColumns);
            td.innerHTML = branches[i];

            tr = table.rows[1];

            td = tr.insertCell(1 + 2 * i);
            td.setAttribute("colspan", 3);

            td = tr.insertCell(1 + 2 * i + 1);
            td.setAttribute("colspan", 3);
            td.innerHTML = "Stable Last Difference";
            td.setAttribute("class", "platform_summary_lastDiff");

            tr = table.rows[2];
            th = tr.appendChild(document.createElement("th"));
            th.appendChild(document.createTextNode("Host"));
            th = tr.appendChild(document.createElement("th"));
            th.appendChild(document.createTextNode("Metric"));
            th = tr.appendChild(document.createElement("th"));
            th = tr.appendChild(document.createElement("th"));
            th.appendChild(document.createTextNode("Best"));
            th = tr.appendChild(document.createElement("th"));
            th.appendChild(document.createTextNode("Average"));
            th = tr.appendChild(document.createElement("th"));
            th.appendChild(document.createTextNode("Worst"));
        }

        document.getElementById("updatePSSummary").disabled = true;

        var openBSSectionFunc =  function(e) { thisSection.openBSSectionFromCell(e); }

        var platforms = [];
        var currBranch = 0;
        var currPlatform;
        var hosts;
        var currHost;
        var actualHost;
        var actualMetric;
        var backupHost;
        var backupMetric;

        // Fetch platforms for first branch ...
        startTiming();
        fetchPlatforms();
    }

    this.appendConfiguration = function(section)
    {
        var thisSection = this; // ### hack?

        section.appendChild(document.createElement("br"));
        section.appendChild(document.createElement("br"));

        fieldset = section.appendChild(document.createElement("fieldset"));
        legend = fieldset.appendChild(document.createElement("legend"));
        legend.appendChild(document.createTextNode("Configuration"));

        input = fieldset.appendChild(document.createElement("input"));
        input.setAttribute("type", "button");
        input.setAttribute("id", "resetPSConfig");
        input.setAttribute("value", "Reset");
        input.onclick = function() {
            try { thisSection.resetConfig(); }
            catch (ex) { alert("thisSection.resetConfig() failed: " + ex); }
        }

        fieldset.appendChild(document.createElement("br"));
        fieldset.appendChild(document.createElement("br"));

        table = fieldset.appendChild(document.createElement("table"));

        tr = table.insertRow(0);
        tr.setAttribute("style", "vertical-align:top");

        td = tr.insertCell(0);

        fieldset = td.appendChild(document.createElement("fieldset"));
        legend = fieldset.appendChild(document.createElement("legend"));
        legend.appendChild(document.createTextNode("Branches"));

        select = fieldset.appendChild(document.createElement("select"));
        select.setAttribute("id", "psBranches");
        select.setAttribute("multiple", "multiple");
        select.setAttribute("size", "6");
        select.onchange = function() {
            thisSection.dirtyUpdateSummaryButton();
            thisSection.updateUpdateSummaryButtonDisability();
        }

        td = tr.insertCell(1);
        table = td.appendChild(document.createElement("table"));
        table.setAttribute("style", "text-align:right");

        tr = table.insertRow(0);
        td = tr.insertCell(0);
        td.innerHTML = "Primary metric: ";
        td = tr.insertCell(1);
        td.setAttribute("style", "text-align:left");
        select = td.appendChild(document.createElement("select"));
        select.setAttribute("id", "psPrimaryMetric");
        select.onchange = function() { thisSection.dirtyUpdateSummaryButton(); }

        tr = table.insertRow(1);
        td = tr.insertCell(0);
        td.innerHTML = "Show primary metric only: ";
        td = tr.insertCell(1);
        td.setAttribute("style", "text-align:left");
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("type", "checkbox");
        input.setAttribute("id", "psPrimaryMetricOnly");
        input.onchange = function() { thisSection.dirtyUpdateSummaryButton(); }

        tr = table.insertRow(2);
        td = tr.insertCell(0);
        td.innerHTML = "Difference tolerance: ";
        td.setAttribute("class", "platform_summary_lastDiff");
        td = tr.insertCell(1);
        td.setAttribute("style", "text-align:left");
        select = td.appendChild(document.createElement("select"));
        select.setAttribute("id", "psDiffTolerance");
        select.onchange = function() { thisSection.dirtyUpdateSummaryButton(); }
        setDiffToleranceOptions(select);
        select.options[0].selected = true;

        tr = table.insertRow(3);
        td = tr.insertCell(0);
        td.innerHTML = "Stability tolerance: ";
        td.setAttribute("class", "platform_summary_lastDiff");
        td = tr.insertCell(1);
        td.setAttribute("style", "text-align:left");
        select = td.appendChild(document.createElement("select"));
        select.setAttribute("id", "psStabTolerance");
        select.onchange = function() { thisSection.dirtyUpdateSummaryButton(); }
        setStabToleranceOptions(select);
        select.options[0].selected = true;
    }

    this.appendSummary = function(section)
    {
        var thisSection = this; // ### hack?

        section.appendChild(document.createElement("br"));
        section.appendChild(document.createElement("br"));

        fieldset = section.appendChild(document.createElement("fieldset"));
        legend = fieldset.appendChild(document.createElement("legend"));
        legend.appendChild(document.createTextNode("Summary"));

        input = fieldset.appendChild(document.createElement("input"));
        input.setAttribute("type", "button");
        input.setAttribute("id", "updatePSSummary");
        input.setAttribute("value", "* Update");
        input.onclick = function() {
            try {
                thisSection.updateSummary();
            } catch (ex) {
                alert("thisSection.updateSummary() failed: " + ex);
            }
        }
        input.setAttribute("disabled", "");

        fieldset.appendChild(document.createElement("br"));
        fieldset.appendChild(document.createElement("br"));

        span = fieldset.appendChild(document.createElement("span"));
        span.setAttribute("id", "psSummaryConfig");
        span.setAttribute("style", "font-size:12px");

        table = fieldset.appendChild(document.createElement("table"));
        table.setAttribute("id", "psSummary");
        table.setAttribute("border", "1");

        tr = table.insertRow(0).insertCell(0).innerHTML = "<i>no data</i>";
    }

    this.create = function()
    {
        var sectionElem = document.createElement("div");
        insertTitle(sectionElem, "Platform Summary");
        sectionElem.appendChild(document.createElement("br"));
        appendHelpLink(sectionElem, "help.html#PlatformSummary");
        this.appendConfiguration(sectionElem);
        this.appendSummary(sectionElem);
        return sectionElem;
    }

    this.state = function() {
        var state_ = [];

        // Selected branches
        var selBranches = selectedOptions("psBranches");
        for (var i = 0; i < selBranches.length; ++i)
            state_["psBranch" + i] = selBranches[i];
        state_["psBranchCount"] = selBranches.length;

        // Primary metric
        state_["psPrimaryMetric"] = selectedOptions("psPrimaryMetric")[0];

        // Primary metric only
        state_["psPrimaryMetricOnly"] =
            document.getElementById("psPrimaryMetricOnly").checked ? "true" : "false";

        // Difference tolerance
        state_["psDiffTolerance"] = selectedOptions("psDiffTolerance")[0];

        // Stability tolerance
        state_["psStabTolerance"] = selectedOptions("psStabTolerance")[0];

        return state_;
    }

    this.setState = function(state_, handleDone) {
        // Selected branches
        var branches = [];
        var psBranchCount = parseInt(state_["psBranchCount"]);
        if (isNaN(psBranchCount)) {
            handleDone();
            return;
        }
        for (var i = 0; i < psBranchCount; ++i) {
            branches[state_["psBranch" + i]] = true;
        }

        // Primary metric
        var primaryMetric = state_["psPrimaryMetric"];

        // Primary metric only
        var primaryMetricOnly = (state_["psPrimaryMetricOnly"] == "true");

        // Difference tolerance
        var diffTolerance = state_["psDiffTolerance"];

        // Stability tolerance
        var stabTolerance = state_["psStabTolerance"];

        // Update configuration ..
        this.setConfig(
            branches, primaryMetric, primaryMetricOnly, diffTolerance, stabTolerance, handleDone);
    }

    SectionBase.call(this, "platformSummaryButton");
    this.sectionElem = this.create();
}

PlatformSummarySection.prototype = new SectionBase();
PlatformSummarySection.prototype.constructor = PlatformSummarySection;
PlatformSummarySection.instance = function()
{
    if (!PlatformSummarySection.instance_)
        PlatformSummarySection.instance_ = new PlatformSummarySection();
    return PlatformSummarySection.instance_;
}
function psSection() { return PlatformSummarySection.instance(); }
