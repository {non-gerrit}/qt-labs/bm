/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

// ### Rename id "metrics" to "bsMetric" etc. ... 2 B DONE!

// Copies options from primary to secondary branch.
function copyBranch1OptionsToBranch2()
{
    var select2 = document.getElementById("branches2");
    removeElementChildren(select2);

    var select = document.getElementById("branches");

    var options = select.options;
    for (var i = 1; i < options.length; ++i)
        select2.appendChild(options[i].cloneNode(true));

    var spans = select.getElementsByTagName("span");
    for (var i = 0; i < spans.length; ++i)
        select2.appendChild(spans[i].cloneNode(true));
}

function BenchmarkSummarySection()
{
    // Note: The 'context' is part of the overall 'configuration'.
    // (The latter also contains difference- and stability tolerance etc.)
    var contextComponentNames = ["metrics", "platforms", "hosts", "branches"];
    var existingContexts; // Available combinations

    this.name = function() { return "bsSection"; }

    this.sortColumn = function(colId, lexical)
    {
        var thisSection = this; // ### hack?

        var table = document.getElementById("bsSummary");
        // // NOTE: 1 is added to colIndex to compensate for colspan=2 in leftmost header cell!
        // var colIndex = table.rows[0].cells.namedItem(colId).cellIndex + 1;
        var colIndex = table.rows[0].cells.namedItem(colId).cellIndex;

        // Copy main table into a temporary table and determine the new sorting order ...
        var newSortOrder = null;
        var invalidDataFound = false;
        var val1 = null;
        var val2 = null;
        var rows = [];

        for (i = 1; i < table.rows.length; ++i) {
            row = table.rows[i].cloneNode(true);

            j = i - 1;
            rows[j] = row;
            if (newSortOrder == null) {
                var cell = rows[j].cells[colIndex];

                var s;
                if (cell == null)
                    s = "";
                else
                    s =  cell.title;

                if (s != "") {

                    if (val1 == null) {
                        val1 = lexical ? s : parseFloat(s);
                    } else {
                        if (val2 == null)
                            val2 = lexical ? s : parseFloat(s);
                        if (val2 != null) {
                            if (val1 > val2) {
                                newSortOrder = "ascending";
                            } else if (val1 < val2) {
                                newSortOrder = "descending";
                            } else {
                                val1 = val2;
                                val2 = null;
                            }
                        }
                    } 

                } else {
                    invalidDataFound = true;
                }

            }
        }

        var skipSorting = false;

        // Sort temporary table in ascending order ...
        if (lexical) {

            rows.sort(function(row1, row2) {
                var s1 = row1.cells[colIndex].title;
                var s2 = row2.cells[colIndex].title;
                return s1 > s2;
            });

        } else {

            if (newSortOrder == null) {
                if (invalidDataFound)
                    newSortOrder = "ascending"; // Sort to bring invalid values to the bottom
                else {
                    skipSorting = true; // All values are valid and identical
                }
            }

            if (!skipSorting) {
                rows.sort(function(row1, row2) {
                    var cell1 = row1.cells[colIndex];
                    var cell2 = row2.cells[colIndex];
                    var s1 = (cell1 != null) ? cell1.title : "";
                    var s2 = (cell2 != null) ? cell2.title : "";

                    // Ensure that rows with invalid data appear at the bottom regardless of
                    // sorting order ...
                    if ((s1 == "") && (s2 != ""))
                        return (newSortOrder == "ascending");
                    if ((s1 != "") && (s2 == ""))
                        return (newSortOrder != "ascending");
                    if ((s1 == "") && (s2 == ""))
                        return (newSortOrder != "ascending");

                    var val1 = parseFloat(s1);
                    var val2 = parseFloat(s2);
                    return val1 - val2;
                });
            }
        }
 
        if (!skipSorting) {

           // Clear main table ...
            while (table.rows.length > 1)
                table.deleteRow(1);

            // Insert temporary table back into main table in final order ...
            if (newSortOrder == "ascending") {
                for (i = 0; i < rows.length; ++i)
                    table.tBodies[0].appendChild(rows[i]);
            } else {
                for (i = rows.length - 1; i >= 0; --i)
                    table.tBodies[0].appendChild(rows[i]);
            }
        }
    }

    this.setColumnIsSortable = function(colId, hasData)
    {
        var id = "hasData_" + colId;
        var elem = document.getElementById(id);
        if (!elem) {
            elem = document.createElement("span");
            elem.id = id;
            document.body.appendChild(elem);
        }
        elem.title = hasData ? "hasData" : "";
    }

    this.columnIsSortable = function(colId)
    {
        var id = "hasData_" + colId;
        var elem = document.getElementById(id);
        return elem ? (elem.title == "hasData") : false;
    }

    this.sortTestCase = function()
    {
        try {
            if (this.columnIsSortable("testCase"))
                this.sortColumn("testCase", true);
        } catch (ex) {
            alert("sortTestCase() failed: " + ex);
        }
    }

    this.sortTrend = function()
    {
        try {
            if (this.columnIsSortable("trend"))
                this.sortColumn("trend");
        } catch (ex) {
            alert("sortTrend() failed: " + ex);
        }
    }

    this.sortLastDiff = function()
    {
        try {
            if (this.columnIsSortable("lastDiff"))
                this.sortColumn("lastDiff");
        } catch (ex) {
            alert("sortLastDiff() failed: " + ex);
        }
    }

    this.sortLastDiffStable = function()
    {
        try {
            if (this.columnIsSortable("lastDiffStable"))
                this.sortColumn("lastDiffStable");
        } catch (ex) {
            alert("sortLastDiffStable() failed: " + ex);
        }
    }

    this.sortBranch2Diff = function()
    {
        try {
            if (this.columnIsSortable("branch2Diff"))
                this.sortColumn("branch2Diff");
        } catch (ex) {
            alert("sortBranch2Diff() failed: " + ex);
        }
    }

    this.sortBranch2DiffStable = function()
    {
        try {
            if (this.columnIsSortable("branch2DiffStable"))
                this.sortColumn("branch2DiffStable");
        } catch (ex) {
            alert("sortBranch2DiffStable() failed: " + ex);
        }
    }

    this.sortFileDiff = function()
    {
        try {
            if (this.columnIsSortable("fileDiff"))
                this.sortColumn("fileDiff");
        } catch (ex) {
            alert("sortFileDiff() failed: " + ex);
        }
    }

    this.sortFileDiffStable = function()
    {
        try {
            if (this.columnIsSortable("fileDiffStable"))
                this.sortColumn("fileDiffStable");
        } catch (ex) {
            alert("sortFileDiffStable() failed: " + ex);
        }
    }

    this.contextLocked = function()
    {
        var allLocked = true;
        for (var i = 0; i < contextComponentNames.length; ++i) {	
            var select = document.getElementById(contextComponentNames[i]);
            var text =
                ((select.selectedIndex >= 0) && (select.selectedIndex < select.options.length))
                ? select.options[select.selectedIndex].text
                : "";

            if ((text == "") || (text == "*")) {
                allLocked = false;
                break;
            }
        }
        return allLocked;
    }

    this.dirtyUpdateSummaryButton = function()
    {
        document.getElementById("updateBSSummary").value = "* Update";
    }

    this.updateSummaryControls = function()
    {
        var cLocked = this.contextLocked();
        var disabled = !cLocked;
        var prevDisabled = document.getElementById("updateBSSummary").disabled;
        document.getElementById("updateBSSummary").disabled = disabled;

        if (!disabled && prevDisabled) {
            copyBranch1OptionsToBranch2();
        } else if (disabled && !prevDisabled) {
            // Reset secondary branch options ...
            var select2 = document.getElementById("branches2");
            removeElementChildren(select2);
        }

        this.dirtyUpdateSummaryButton();
    }

    this.contextMenuValue = function(menuId)
    {
        var select = document.getElementById(menuId);
        if (select == null)
            return [];
        var selIndex = select.selectedIndex;
        if (selIndex == -1)
            return [];
        var offset = 0;
        if (select.options[0].value == "*") {
            if (selIndex == 0)
                return [];
            offset = 1;
        }

        var val = [];
        var subTexts = select.getElementsByTagName("span");
        var subSize = subTexts.length / (select.options.length - offset);
        var subFirst = (selIndex - offset) * subSize;
        for (var j = subFirst; j < (subFirst + subSize); ++j)
            val = val.concat(subTexts[j].title);

        return val;
    }

    this.setConfiguration = function(
        metric, platform, host, branch, diffTolerance, stabTolerance, branch2Enabled,
        branch2, fileEnabled, fileName, handleDone)
    {
        this.handleFetchContextsDone = function()
        {
            // *** Set the context ***
            this.setContext(metric, platform, host, branch);


            // *** Set the rest of the configuration ***

            // Difference tolerance
            if (diffTolerance != null) {
                var select = document.getElementById("bsDiffTolerance");
                var options = select.options;
                for (var i = 0; i < options.length; ++i)
                    options[i].selected = (options[i].value == diffTolerance);
            }

            // Stability tolerance
            if (stabTolerance != null) {
                var select = document.getElementById("bsStabTolerance");
                var options = select.options;
                for (var i = 0; i < options.length; ++i)
                    options[i].selected = (options[i].value == stabTolerance);
            }

            // Branch 2 enabled
            document.getElementById("branch2Enabled").checked = branch2Enabled;

            // Copy branch 2 ...
            copyBranch1OptionsToBranch2();

            // Branch 2
            var select = document.getElementById("branches2");
            var options = select.options;
            for (var i = 0; i < options.length; ++i)
                options[i].selected = (options[i].text == branch2);

            // File enabled
            document.getElementById("fileEnabled").checked = fileEnabled;

            // File

            // ### This is currently not possible (the value property of a "file upload"
            // element is read-only for security reasons):
//                    document.getElementById("fileName").value = file;

            // For now just show the file name so the user can copy/paste into
            // the file dialog:
            document.getElementById("copyableFileName").innerHTML = fileName;

            setMessage("");
            thisSection.updateTopSection(); // ### replace with 'this' ?

            document.getElementById("resetBSConfig").disabled = false;

            this.updateSummaryControls();

            if (handleDone)
                handleDone();
        }

        disableTopSectionButtons();
        setMessage("updating benchmark summary configuration ...");

        var thisSection = this; // ### hack?
        var fetchContextsDoneHandler = function() { thisSection.handleFetchContextsDone(); }
        document.getElementById("resetBSConfig").disabled = true;

        // Start by fetching available contexts ...
        this.fetchContexts(fetchContextsDoneHandler);
    }

    this.updateContextComponentOptions = function(compIndex)
    {
        var select = document.getElementById(contextComponentNames[compIndex]);
        var options = select.options;

        // Loop over options ...
        for (var i = 0; i < (options.length - 1); ++i) {
            // Form the next candidate ...
            var candidate = [];
            for (var j = 0; j < contextComponentNames.length; ++j) {
                if (j == compIndex) {
                    candidate[candidate.length] = i;
                } else {
                    candidate[candidate.length] =
                        document.getElementById(contextComponentNames[j]).selectedIndex - 1;
                }
            }
            // Make option selectable iff the candidate matches an available
            // context (-1 meaning 'any') ...
            options[i + 1].disabled = !matchesContext(this.existingContexts, candidate);
        }
    }

    // Makes the necessary updates when the context component 'compName' is modified.
    this.modifyContextComponent = function(compName)
    {
        for (var i = 0; i < contextComponentNames.length; ++i)
            if (contextComponentNames[i] != compName)
                this.updateContextComponentOptions(i);

        this.updateSummaryControls();
    }

    this.setContext = function(metric, platform, host, branch)
    {
        var values = [metric, platform, host, branch]; // hm ... maybe use "varargs" instead?
        // assert(values.length == contextComponentNames.length);

        var candidate = [];
        for (var i = 0; i < contextComponentNames.length; ++i) {
            var select = document.getElementById(contextComponentNames[i]);
            var options = select.options;
            for (var j = 0; j < options.length; ++j)
                options[j].selected = (options[j].text == values[i]);
            candidate[candidate.length] = select.selectedIndex - 1;
        }

        if (!matchesContext(this.existingContexts, candidate)) {
            alert("ERROR: context mismatch - using first combination");
            for (var i = 0; i < contextComponentNames.length; ++i) {
                document.getElementById(contextComponentNames[i]).selectedIndex = 1;
            }
        }

        for (var i = 0; i < contextComponentNames.length; ++i)
            this.updateContextComponentOptions(i);
    }

    this.setContextComponentOptions = function(contextCompName, options)
    {
        var select = document.getElementById(contextCompName);
        removeElementChildren(select);

        // Append the 'unlocked' option and make it selected ...
        var unlockedOption = new Option("*");
        unlockedOption.selected = true;
        select.appendChild(unlockedOption);

        // Append the normal options ...
        for (var i = 0; i < options.length; ++i) {

            var option = new Option();
            select.appendChild(option);

            var text = "";

            for (var j = 0; j < options[i].length; ++j) {
                var span = document.createElement("span");
                var subText = options[i][j];
                span.title = subText;
                if (j > 0)
                    text += " ";
                text += subText;
                select.appendChild(span);
            }

            option.text = text;
        }
    }

    this.fetchContexts = function(handleDone)
    {
        var thisSection = this; // ### hack?

        this.handleReply_fetchContexts = function(reply)
        {
            this.setContextComponentOptions("metrics", reply.values[0]);
            this.setContextComponentOptions("platforms", reply.values[1]);
            this.setContextComponentOptions("hosts", reply.values[2]);
            this.setContextComponentOptions("branches", reply.values[3]);
            this.existingContexts = reply.contexts;
            this.updateSummaryControls();
            if (handleDone)
                handleDone();
        }

        this.handleError_fetchContexts = function(error)
        {
            alert("fetchContexts() failed: " + error);
        }

        var args = ["get", "contexts"];
        sendRequest(
            args,
            function(reply) { thisSection.handleReply_fetchContexts(reply); },
            function(error) { thisSection.handleError_fetchContexts(error); });
    }

    this.reset = function(handleDone)
    {
        handleDone();
    }

    // Main use case 1 - analyze the primary branch
    function updateSummaryPrimaryBranch(
        thisSection, table, currDate, metric, platform, host, branch, diffTolerance,
        stabTolerance, ranking, scope, maxBenchmarks, testCaseFilter, testFunctionFilter,
        dataTagFilter)
    {

        thisSection.handleReply_rankedBenchmarks = function(reply)
        {
            if (reply.error) {
                alert("updateSummaryPrimaryBranch() failed in handleReply_rankedBenchmarks(): "
                      + reply.error);
                thisSection.errorDetected = true;
                thisSection.finalize_summary();
            } else {

                rowIndex = 1;

                for (var i = 0; i < reply.benchmarks.length; ++i) {

                    testCase = reply.benchmarks[i].name[0];
                    testFunction = reply.benchmarks[i].name[1];
                    dataTag = reply.benchmarks[i].name[2];

                    row = table.insertRow(rowIndex);

                    var currCol = 0;

                    detailsCell = row.insertCell(currCol++);
                    detailsLink = detailsCell.appendChild(document.createElement("a"));
                    var url = "http://" + location.host + "/cgi-bin/bmclientwrapper";
                    url += "?command=";
                    url += "-server " + server();
                    url += " get detailspage";
                    url += " " + testCase;
                    url += " " + testFunction;
                    // The data tag may consist of "anything", so we need to quote it
                    // and escape problematic characters:
                    url += " " + "'" + encodeURIComponent(dataTag) + "'";
                    url += " " + metric;
                    url += " " + platform;
                    url += " " + host;
                    url += " " + branch[0];
                    url += " " + branch[1];
                    url += " -timerange first last";
                    url += " " + selectedOptions("bsDiffTolerance")[0];
                    url += " " + selectedOptions("bsStabTolerance")[0];
                    url += " " + maxDetailsHistory();
                    url += " " + document.styleSheets[0].href;
                    url += " " + currTimestamp();

                    detailsLink.setAttribute("href", url);
                    detailsLink.innerHTML = "Details";
                    detailsLink.setAttribute("id", "detailsLink" + i);
                    detailsLink.setAttribute("onmouseover", "updateDetailsLink(" + i + ")");

                    // Insert benchmark name
                    cell = row.insertCell(currCol++);
                    cell.innerHTML = testCase;
                    cell.setAttribute("title", testCase + testFunction + dataTag); // for sorting
                    cell.setAttribute("class", "bmark");
                    cell = row.insertCell(currCol++);
                    cell.innerHTML = testFunction;
                    cell.setAttribute("class", "bmark");
                    cell = row.insertCell(currCol++);
                    cell.innerHTML = dataTag;
                    cell.setAttribute("class", "bmark");

                    if (reply.benchmarks[i].lastValue != null) {
                        // Insert age of latest result (relative to current time)
                        date_n = new Date(reply.benchmarks[i].lastValue[0] * 1000);
                        age = currDate.getTime() - date_n.getTime();
                        cell = row.insertCell(currCol++);
                        cell.innerHTML = ageText(age);
                        cell.setAttribute("style", "background-color:" + ageColor(age));
                        cell.setAttribute("class", "bmark");

                        var cell_lastDiff = row.insertCell(currCol++);
                        var cell_lastDiffStable = row.insertCell(currCol++);
                        var cell_lastDiffAge = row.insertCell(currCol++);
                        if (reply.benchmarks[i].lastDiff != null) {
                            // Insert last difference
                            r_n = reply.benchmarks[i].lastValue[2];
                            r_d = reply.benchmarks[i].lastDiff[2];
                            diff = normalizedDifference(r_n, r_d);
                            cell_lastDiff.innerHTML = diff.toFixed(4);
                            cell_lastDiff.setAttribute("title", diff); // keep for sorting etc.
                            cell_lastDiff.setAttribute(
                                "style", "background-color:" + diffColor(diff));
                            cell_lastDiff.setAttribute("class", "lastDiff");

                            // Insert last difference stability
                            var stable = reply.benchmarks[i].lastDiff[3] ? 1 : 0;
                            if (stable) {
                                cell_lastDiffStable.innerHTML = "yes";
                                cell_lastDiffStable.setAttribute(
                                    "style", "background-color:#dddddd");
                            } else {
                                cell_lastDiffStable.innerHTML = "no";
                            }
                            cell_lastDiffStable.setAttribute(
                                "title", stable); // keep for sorting etc.

                            // Insert age of last difference (relative to latest result)
                            date_c = new Date(reply.benchmarks[i].lastDiff[0] * 1000);
                            age = date_n.getTime() - date_c.getTime();
                            cell_lastDiffAge.innerHTML = ageText(age);
                            cell_lastDiffAge.setAttribute(
                                "style", "background-color:" + ageColor(age));
                            cell_lastDiffAge.setAttribute("class", "lastDiff");

                        } else {
                            cell_lastDiff.innerHTML = "no last difference";
                            cell_lastDiff.setAttribute("class", "notFound");
                            cell_lastDiffStable.innerHTML = "n/a";
                            cell_lastDiffStable.setAttribute("class", "notFound");
                            cell_lastDiffAge.innerHTML = "n/a";
                            cell_lastDiffAge.setAttribute("class", "notFound");
                        }

                        // Insert trend
                        cell_trend = row.insertCell(currCol++);
                        if (reply.benchmarks[i].regression != null) {
                            trend = reply.benchmarks[i].regression[1]; // Slope of regression line
                            cell_trend.innerHTML = trend.toFixed(4);
                            cell_trend.setAttribute("title", trend); // keep for sorting etc.
                            cell_trend.setAttribute(
                                "style", "background-color:" + trendColor(trend));
                            cell_trend.setAttribute("class", "trend");
                        } else {
                            cell_trend.innerHTML = "n/a";
                            cell_trend.setAttribute("class", "notFound");
                        }
                    }

                    rowIndex++;
                }

                sortable = (reply.benchmarks.length > 1);
                thisSection.setColumnIsSortable("testCase", sortable);
                thisSection.setColumnIsSortable("lastDiff", sortable);
                thisSection.setColumnIsSortable("lastDiffStable", sortable);
                thisSection.setColumnIsSortable("trend", sortable);

                var countCell = table.rows[0].cells[0];
                countCell.setAttribute("class", "count_cell");
                countCell.innerHTML = reply.benchmarks.length;

                thisSection.finalize_summary();
            }
        }

        function fetchRankedBenchmarks()
        {
            setMessage(
                "updating benchmark summary ... (fetching statistics for primary branch ...)");

            var args =
                ["get", "rankedbenchmarks", metric, platform,
                 host, branch[0], branch[1], "-timerange", "first", "last",
                 diffTolerance, stabTolerance, ranking, scope, maxBenchmarks,
                 testCaseFilter, testFunctionFilter, dataTagFilter];
            sendRequest(
                args,
                function(reply) { thisSection.handleReply_rankedBenchmarks(reply); },
                function(error) { thisSection.handleError_summary(error); }
            );
        }

        var tr = table.rows[0];

        // Reset the rest of the table ...
        th = tr.appendChild(document.createElement("th"));
        th.setAttribute("class", "lastDiffHead");
        th.setAttribute("id", "lastDiff");
        th.onclick = function() { thisSection.sortLastDiff(); }
        img = th.appendChild(document.createElement("img"));
        img.setAttribute("src", "images/sortable.png");
        th.appendChild(document.createTextNode("LD"));

        th = tr.appendChild(document.createElement("th"));
        th.setAttribute("class", "lastDiffHead");
        th.setAttribute("id", "lastDiffStable");
        th.onclick = function() { thisSection.sortLastDiffStable(); }
        img = th.appendChild(document.createElement("img"));
        img.setAttribute("src", "images/sortable.png");
        th.appendChild(document.createTextNode("S"));

        th = tr.appendChild(document.createElement("th"));
        th.innerHTML = "LD Age";
        th.setAttribute("class", "lastDiffHead");

        th = tr.appendChild(document.createElement("th"));
        th.setAttribute("class", "trendHead");
        th.setAttribute("id", "trend");
        th.onclick = function() { thisSection.sortTrend(); }
        img = th.appendChild(document.createElement("img"));
        img.setAttribute("src", "images/sortable.png");
        th.appendChild(document.createTextNode("Trend"));

        document.getElementById("bsSummaryConfig").innerHTML =
            "<b>Type:</b>&nbsp;primary&nbsp;branch&nbsp;trend"
            + "&nbsp;&nbsp; <b>Metric:</b>&nbsp;" + metric
            + "&nbsp;&nbsp; <b>Platform:</b>&nbsp;" + platform
            + "&nbsp;&nbsp; <b>Host:</b>&nbsp;" + host
            + "&nbsp;&nbsp; <b>Branch:</b>&nbsp;" + branch[0] + "&nbsp;" + branch[1]
            + "&nbsp;&nbsp; <b>Difference&nbsp;tolerance:</b>&nbsp;" + diffTolerance + "&nbsp;%"
            + "&nbsp;&nbsp; <b>Stability&nbsp;tolerance:</b>&nbsp;" + stabTolerance
            + "&nbsp;&nbsp; <b>Ranking:</b>&nbsp;" + ranking
            + "&nbsp;&nbsp; <b>Scope:</b>&nbsp;" + scope
            + "&nbsp;&nbsp; <b>Max&nbsp;benchmarks&nbsp;per&nbsp;scope:</b>&nbsp;" + maxBenchmarks
            + "&nbsp;&nbsp; <b>Test&nbsp;case&nbsp;filter:</b>&nbsp;" + testCaseFilter
            + "&nbsp;&nbsp; <b>Test&nbsp;function&nbsp;filter:</b>&nbsp;" + testFunctionFilter
            + "&nbsp;&nbsp; <b>Data&nbsp;tag&nbsp;filter:</b>&nbsp;" + dataTagFilter
            + "&nbsp;&nbsp; <b>Date:</b>&nbsp;"
            + currDate.toDateString().replace(/\s/g, "&nbsp;")
            + "&nbsp;"
            + currDate.toTimeString().replace(/\s/g, "&nbsp;")
            + "<br /><br />";

        startTiming();
        fetchRankedBenchmarks();
    }

    // Main use case 2 - compare the latest results of the primary and secondary branch
    function updateSummaryPrimaryVsSecondaryBranch(
        thisSection, table, currDate, metric, platform, host, branch1, branch2, diffTolerance,
        stabTolerance, ranking, scope, maxBenchmarks, testCaseFilter, testFunctionFilter,
        dataTagFilter)
    {

        thisSection.handleReply_rankedBenchmarks2 = function(reply)
        {
            if (reply.error) {
                alert("updateSummaryPrimaryVsSecondaryBranch() failed in " +
                      "handleReply_rankedBenchmarks2(): " + reply.error);
                thisSection.errorDetected = true;
                thisSection.finalize_summary();
            } else {

                rowIndex = 1;

                for (var i = 0; i < reply.benchmarks.length; ++i) {

                    testCase = reply.benchmarks[i].name[0];
                    testFunction = reply.benchmarks[i].name[1];
                    dataTag = reply.benchmarks[i].name[2];

                    row = table.insertRow(rowIndex);

                    var currCol = 0;

                    detailsCell = row.insertCell(currCol++);
                    detailsLink = detailsCell.appendChild(document.createElement("a"));
                    var url = "http://" + location.host + "/cgi-bin/bmclientwrapper";
                    url += "?command=";
                    url += "-server " + server();
                    url += " get detailspage2";
                    url += " " + testCase;
                    url += " " + testFunction;
                    // The data tag may consist of "anything", so we need to quote it
                    // and escape problematic characters:
                    url += " " + "'" + encodeURIComponent(dataTag) + "'";
                    url += " " + metric;
                    url += " " + platform;
                    url += " " + host;
                    url += " " + branch1[0];
                    url += " " + branch1[1];
                    url += " " + branch2[0];
                    url += " " + branch2[1];
                    url += " -timerange first last";
                    url += " " + selectedOptions("bsDiffTolerance")[0];
                    url += " " + selectedOptions("bsStabTolerance")[0];
                    url += " " + maxDetailsHistory();
                    url += " " + sharedTimeScale();
                    url += " " + document.styleSheets[0].href;
                    url += " " + currTimestamp();

                    detailsLink.setAttribute("href", url);
                    detailsLink.innerHTML = "Details";
                    detailsLink.setAttribute("id", "detailsLink" + i);
                    detailsLink.setAttribute("onmouseover", "updateDetailsLink2(" + i + ")");

                    // Insert benchmark name
                    cell = row.insertCell(currCol++);
                    cell.innerHTML = testCase;
                    cell.setAttribute("title", testCase + testFunction + dataTag); // for sorting
                    cell.setAttribute("class", "bmark");
                    cell = row.insertCell(currCol++);
                    cell.innerHTML = testFunction;
                    cell.setAttribute("class", "bmark");
                    cell = row.insertCell(currCol++);
                    cell.innerHTML = dataTag;
                    cell.setAttribute("class", "bmark");

                    // Insert age of latest result in branch 1 (relative to current time)
                    date_n = new Date(reply.benchmarks[i].lastValue1[0] * 1000);
                    age = currDate.getTime() - date_n.getTime();
                    cell = row.insertCell(currCol++);
                    cell.innerHTML = ageText(age);
                    cell.setAttribute("style", "background-color:" + ageColor(age));
                    cell.setAttribute("class", "bmark");

                    // Insert difference between latest results in branch 1 and 2
                    r_n1 = reply.benchmarks[i].lastValue1[2];
                    r_n2 = reply.benchmarks[i].lastValue2[2];
                    diff = normalizedDifference(r_n1, r_n2);
                    cell = row.insertCell(currCol++);
                    cell.innerHTML = diff.toFixed(4);
                    cell.setAttribute("title", diff); // keep for sorting etc.
                    cell.setAttribute("style", "background-color:" + diffColor(diff));
                    cell.setAttribute("class", "branch2");

                    // Insert stability of difference
                    cell = row.insertCell(currCol++);
                    var stable1 = reply.benchmarks[i].lastValue1[4] ? 1 : 0;
                    var stable2 = reply.benchmarks[i].lastValue2[4] ? 1 : 0;
                    var stable = (stable1 && stable2);
                    if (stable) {
                        cell.innerHTML = "yes";
                        cell.setAttribute("style", "background-color:#dddddd");
                    } else {
                        cell.innerHTML = "no";
                    }
                    cell.setAttribute("title", stable); // keep for sorting etc.

                    // Insert age of latest result in branch 2 (relative to current time)
                    date_n = new Date(reply.benchmarks[i].lastValue2[0] * 1000);
                    age = currDate.getTime() - date_n.getTime();
                    cell = row.insertCell(currCol++);
                    cell.innerHTML = ageText(age);
                    cell.setAttribute("style", "background-color:" + ageColor(age));
                    cell.setAttribute("class", "branch2");


                    rowIndex++;
                }

                sortable = (reply.benchmarks.length > 1);
                thisSection.setColumnIsSortable("testCase", sortable);
                thisSection.setColumnIsSortable("branch2Diff", sortable);
                thisSection.setColumnIsSortable("branch2DiffStable", sortable);

                var countCell = table.rows[0].cells[0];
                countCell.setAttribute("class", "count_cell");
                countCell.innerHTML = reply.benchmarks.length;

                thisSection.finalize_summary();
            }
        }

        function fetchRankedBenchmarks2()
        {
            setMessage(
                "updating benchmark summary ... (fetching statistics for two branches ...)");

            var args =
                ["get", "rankedbenchmarks2", metric, platform,
                 host, branch1[0], branch1[1], branch2[0], branch2[1], diffTolerance,
                 stabTolerance, ranking, scope, maxBenchmarks, testCaseFilter,
                 testFunctionFilter, dataTagFilter];
            sendRequest(
                args,
                function(reply) { thisSection.handleReply_rankedBenchmarks2(reply); },
                function(error) { thisSection.handleError_summary(error); }
            );
        }

        var tr = table.rows[0];

        // Reset the rest of the table ...
        th = tr.appendChild(document.createElement("th"));
        th.setAttribute("class", "branch2Head");
        th.setAttribute("id", "branch2Diff");
        th.onclick = function() { thisSection.sortBranch2Diff(); }
        th.innerHTML = "<img src=\"images/sortable.png\"></img>B2 Diff";

        th = tr.appendChild(document.createElement("th"));
        th.setAttribute("class", "branch2Head");
        th.setAttribute("id", "branch2DiffStable");
        th.onclick = function() { thisSection.sortBranch2DiffStable(); }
        img = th.appendChild(document.createElement("img"));
        img.setAttribute("src", "images/sortable.png");
        th.appendChild(document.createTextNode("S"));

        th = tr.appendChild(document.createElement("th"));
        th.setAttribute("class", "branch2Head");
        th.innerHTML = "B2 Age";

        document.getElementById("bsSummaryConfig").innerHTML =
            "<b>Type:</b>&nbsp;primary&nbsp;vs.&nbsp;secondary&nbsp;branch"
            + "&nbsp;&nbsp; <b>Metric:</b>&nbsp;" + metric
            + "&nbsp;&nbsp; <b>Platform:</b>&nbsp;" + platform
            + "&nbsp;&nbsp; <b>Host:</b>&nbsp;" + host
            + "&nbsp;&nbsp; <b>Branch&nbsp;1:</b>&nbsp;" + branch1[0] + "&nbsp;" + branch1[1]
            + "&nbsp;&nbsp; <b>Branch&nbsp;2:</b>&nbsp;" + branch2[0] + "&nbsp;" + branch2[1]
            + "&nbsp;&nbsp; <b>Difference&nbsp;tolerance:</b>&nbsp;" + diffTolerance + "&nbsp;%"
            + "&nbsp;&nbsp; <b>Stability&nbsp;tolerance:</b>&nbsp;" + stabTolerance
            + "&nbsp;&nbsp; <b>Ranking:</b>&nbsp;" + ranking
            + "&nbsp;&nbsp; <b>Scope:</b>&nbsp;" + scope
            + "&nbsp;&nbsp; <b>Max&nbsp;benchmarks&nbsp;per&nbsp;scope:</b>&nbsp;" + maxBenchmarks
            + "&nbsp;&nbsp; <b>Test&nbsp;case&nbsp;filter:</b>&nbsp;" + testCaseFilter
            + "&nbsp;&nbsp; <b>Test&nbsp;function&nbsp;filter:</b>&nbsp;" + testFunctionFilter
            + "&nbsp;&nbsp; <b>Data&nbsp;tag&nbsp;filter:</b>&nbsp;" + dataTagFilter
            + "&nbsp;&nbsp; <b>Date:</b>&nbsp;"
            + currDate.toDateString().replace(/\s/g, "&nbsp;")
            + "&nbsp;"
            + currDate.toTimeString().replace(/\s/g, "&nbsp;")
            + "<br /><br />";

        startTiming();
        fetchRankedBenchmarks2();
    }

    // Main use case 3 - compare file results to the latest results of the primary branch
    function updateSummaryFileVsPrimaryBranch(
        thisSection, table, currDate, file, fileName, metric, platform, host, branch,
        diffTolerance, stabTolerance)
    {

        thisSection.handleReply_fileBenchmarkResult = function(reply)
        {
            if (reply.error) {
                // alert("updateSummaryFileVsPrimaryBranch() failed in " +
                //       "handleReply_fileBenchmarkResult(): " + reply.error);
                // thisSection.errorDetected = true;
                // thisSection.finalize_summary();
                // return;

                // ###
                // For now, just assume that this is because the database contains no such
                // benchmark (which is not an error as such in this case).
                // A cleaner way of handling this is probably to just let reply.results be
                // empty rather than raising an error.

            } else {

                // assert(reply.results.length == 1);

                fileResult = map[keys[currKey]];
                testCase = fileResult.testCase;
                testFunction = fileResult.testFunction;
                dataTag = fileResult.dataTag;

                row = table.insertRow(currRow++);
                var currCol = 0;

                detailsCell = row.insertCell(currCol++);
                detailsLink = detailsCell.appendChild(document.createElement("a"));
                var url = "unimplemented";
                // detailsLink.setAttribute("href", url);
                detailsLink.setAttribute("style", "color:#bbbbbb");
                detailsLink.innerHTML = "Details (unimplemented)";

                // detailsLink.setAttribute("id", "detailsLink" + i);
                // detailsLink.setAttribute("onmouseover", "updateDetailsLink2(" + i + ")");

                // Insert benchmark name
                cell = row.insertCell(currCol++);
                cell.innerHTML = testCase;
                cell.setAttribute("title", testCase + testFunction + dataTag); // for sorting
                cell.setAttribute("class", "bmark");
                cell = row.insertCell(currCol++);
                cell.innerHTML = testFunction;
                cell.setAttribute("class", "bmark");
                cell = row.insertCell(currCol++);
                cell.innerHTML = dataTag;
                cell.setAttribute("class", "bmark");

                // Insert age of latest result
                date_n = new Date(reply.results[0][0] * 1000);
                age = currDate.getTime() - date_n.getTime();
                cell = row.insertCell(currCol++);
                cell.innerHTML = ageText(age);
                cell.setAttribute("style", "background-color:" + ageColor(age));
                cell.setAttribute("class", "bmark");

                // Insert difference between the file result and the latest result
                r_f = fileResult.value;
                r_n = reply.results[0][2];
                diff = normalizedDifference(r_f, r_n);
                cell = row.insertCell(currCol++);
                cell.innerHTML = diff.toFixed(4);
                cell.setAttribute("title", diff); // keep for sorting etc.
                cell.setAttribute("style", "background-color:" + diffColor(diff));
                cell.setAttribute("class", "file");

                // Insert stability of latest result
                cell = row.insertCell(currCol++);
                var stable = reply.lastValue.stable ? 1 : 0;
                if (stable) {
                    cell.innerHTML = "yes";
                    cell.setAttribute("style", "background-color:#dddddd");
                } else {
                    cell.innerHTML = "no";
                }
                cell.setAttribute("title", stable); // keep for sorting etc.
            }

            currKey++;
            fetchNextBenchmarkResult();
        }

        function fetchNextBenchmarkResult()
        {
            if (currKey == keys.length) {
                var countCell = table.rows[0].cells[0];
                countCell.setAttribute("class", "count_cell");
                countCell.innerHTML = keys.length;

                thisSection.finalize_summary();

                thisSection.setColumnIsSortable("testCase", true);
                thisSection.setColumnIsSortable("fileDiff", true);
                thisSection.setColumnIsSortable("fileDiffStable", true);

            } else {

                fileResult = map[keys[currKey]];
                testCase = fileResult.testCase;
                testFunction = fileResult.testFunction;
                dataTag = fileResult.dataTag;

                var args =
                    ["get", "history", testCase, testFunction, "'" + dataTag + "'",
                     metric, platform, host, branch[0], branch[1], "-timerange", "first",
                     "last", diffTolerance, stabTolerance, 1, "n/a", currTimestamp()];
                sendRequest(
                    args,
                    function(reply) { thisSection.handleReply_fileBenchmarkResult(reply); },
                    function(error) { thisSection.handleError_summary(error); }
                );
            }
        }

        thisSection.handleReply_file = function(reply)
        {
            if (reply.error) {
                alert("updateSummaryFileVsPrimaryBranch() failed in handleReply_file(): "
                      + reply.error);
                thisSection.errorDetected = true;
                thisSection.finalize_summary();
            } else {

                testCase = reply.testCase;
                map = [];
                keys = [];

                testFunctions = reply.testFunctions;
                for (i = 0; i < testFunctions.length; ++i) {
                    testFunction = testFunctions[i].testFunction;
                    results = testFunctions[i].results;

                    for (j = 0; j < results.length; ++j) {
                        dataTag = results[j].dataTag;
                        metric_ = results[j].metric;
                        value = results[j].value;
                        iterations = results[j].iterations;

                        if (metric_ == metric) {
                            // ### Warning: The unambiguity of the following associative array
                            // key assumes that at most one component (the data tag in this case)
                            // may contain white space!
                            map[testCase + testFunction + dataTag] = {
                                "testCase": testCase, "testFunction": testFunction,
                                "dataTag": dataTag, "value": value, "iterations": iterations
                            };
                        }
                    }
                }

                for (key in map)
                    keys[keys.length] = key;

                if (keys.length > 0) {
                    currKey = 0;
                    currRow = 1;
                    fetchNextBenchmarkResult();
                } else {
                    thisSection.finalize_summary();
                }
            }
        }

        var tr = table.rows[0];

        // Reset the rest of the table ...
        th = tr.appendChild(document.createElement("th"));
        th.setAttribute("class", "fileHead");
        th.setAttribute("id", "fileDiff");
        th.onclick = function() { thisSection.sortFileDiff(); }
        img = th.appendChild(document.createElement("img"));
        img.setAttribute("src", "images/sortable.png");
        th.appendChild(document.createTextNode("File Diff"));

        th = tr.appendChild(document.createElement("th"));
        th.setAttribute("class", "fileHead");
        th.setAttribute("id", "fileDiffStable");
        th.onclick = function() { thisSection.sortFileDiffStable(); }
        img = th.appendChild(document.createElement("img"));
        img.setAttribute("src", "images/sortable.png");
        th.appendChild(document.createTextNode("S"));

        var map;
        var keys;
        var currKey;
        var currRow;

        thisSection.setColumnIsSortable("testCase", false);
        thisSection.setColumnIsSortable("fileDiff", false);
        thisSection.setColumnIsSortable("fileDiffStable", false);

        document.getElementById("bsSummaryConfig").innerHTML =
            "<b>Type:</b> file vs primary branch"
            + "&nbsp;&nbsp;<b>Metric:</b> " + metric
            + "&nbsp;&nbsp;<b>Platform:</b> " + platform
            + "&nbsp;&nbsp;<b>Host:</b> " + host
            + "&nbsp;&nbsp;<b>Branch:</b> " + branch[0] + "&nbsp;" + branch[1]
            + "&nbsp;&nbsp;<b>Difference&nbsp;tolerance:</b> " + diffTolerance + " %"
            + "&nbsp;&nbsp;<b>Stability&nbsp;tolerance:</b> " + stabTolerance
            + "&nbsp;&nbsp;<b>File:</b> " + fileName
            + "&nbsp;&nbsp;<b>Date:</b> " + currDate.toDateString()
            + " " + currDate.toTimeString() + "<br /><br />";

        startTiming();
        sendFileRequest(
            file,
            function(reply) { thisSection.handleReply_file(reply); },
            function(error) { thisSection.handleError_summary(error); }
        );
    }

    this.updateSummary = function(handleDone)
    {
        this.finalize_summary = function()
        {
            setMessage("");
            thisSection.updateTopSection();

            var button = document.getElementById("updateBSSummary");
            if (!errorDetected)
                button.value = "Update";
            button.disabled = false;

            if (handleDone)
                handleDone();

            stopTiming("benchmark summary update");
        }

        this.handleError_summary = function(error)
        {
            alert("updateSummary() failed: " + error);
            errorDetected = true;
        }

        disableTopSectionButtons();
        var thisSection = this; // ### hack?
        var errorDetected = false;

        var metric = this.contextMenuValue("metrics");
        var platform = this.contextMenuValue("platforms");
        var host = this.contextMenuValue("hosts");
        var branch1 = this.contextMenuValue("branches");
        var diffTolerance = selectedOptions("bsDiffTolerance")[0];
        var stabTolerance = selectedOptions("bsStabTolerance")[0];
        var ranking = selectedOptions("ranking")[0];
        var scope = selectedOptions("scope")[0];
        var maxBenchmarks = maxBenchmarksPerScope();
        var testCaseFilter = benchmarkFilter("testCaseFilter");
        var testFunctionFilter = benchmarkFilter("testFunctionFilter");
        var dataTagFilter = benchmarkFilter("dataTagFilter");
        var currDate = new Date();
        var table = document.getElementById("bsSummary");

        // Reset common part of table ...
        while (table.rows.length > 0)
            table.deleteRow(0);
        var tr = table.insertRow(0);

        th = tr.appendChild(document.createElement("th"));
        th.setAttribute("colspan", 1);

        th = tr.appendChild(document.createElement("th"));
        th.setAttribute("class", "bmarkHead");
        th.setAttribute("id", "testCase");
        th.onclick = function() { thisSection.sortTestCase(); }
        img = th.appendChild(document.createElement("img"));
        img.setAttribute("src", "images/sortable.png");
        th.appendChild(document.createTextNode("Test Case"));

        th = tr.appendChild(document.createElement("th"));
        th.setAttribute("class", "bmarkHead");
        th.appendChild(document.createTextNode("Test Function"));

        th = tr.appendChild(document.createElement("th"));
        th.setAttribute("class", "bmarkHead");
        th.appendChild(document.createTextNode("Data Tag"));

        th = tr.appendChild(document.createElement("th"));
        th.setAttribute("class", "bmarkHead");
        th.innerHTML = "Age";

        // Call the function for the particular main use case ...
        if (document.getElementById("branch2Enabled").checked) {
            // Use case 2
            var branch2 = this.contextMenuValue("branches2");
            updateSummaryPrimaryVsSecondaryBranch(
                thisSection, table, currDate, metric, platform, host, branch1, branch2,
                diffTolerance, stabTolerance, ranking, scope, maxBenchmarks,
                testCaseFilter, testFunctionFilter, dataTagFilter);
        } else if (document.getElementById("fileEnabled").checked) {
            // Use case 3

            var file = null;
            var fileName = null;
            var fileElem = document.getElementById("fileName");
            var files = fileElem.files;
            if (files) {
                file = files.item(0);
                fileName = fileElem.value;
            }

            if (file) {
                updateSummaryFileVsPrimaryBranch(
                    thisSection, table, currDate, file, fileName, metric, platform, host,
                    branch1, diffTolerance, stabTolerance);
            } else {
                alert("Please select a file!");
                table.deleteRow(0);
                table.insertRow(0).insertCell(0).innerHTML = "<i>no data</i>";
            }

        } else {
            // Use case 1
            updateSummaryPrimaryBranch(
                thisSection, table, currDate, metric, platform, host, branch1,
                diffTolerance, stabTolerance, ranking, scope, maxBenchmarks,
                testCaseFilter, testFunctionFilter, dataTagFilter);
        }
    }

    this.appendConfiguration = function(section)
    {
        var thisSection = this; // ### hack?

        section.appendChild(document.createElement("br"));
        section.appendChild(document.createElement("br"));

        fieldset = section.appendChild(document.createElement("fieldset"));
        legend = fieldset.appendChild(document.createElement("legend"));
        legend.appendChild(document.createTextNode("Configuration"));

        // ### Maybe the Reset button should be an Update instead? (Reset = Update + Clear)
        input = fieldset.appendChild(document.createElement("input"));
        input.setAttribute("type", "button");
        input.setAttribute("id", "resetBSConfig");
        input.setAttribute("value", "Reset");
        input.onclick = function() {
            try { thisSection.fetchContexts(); }
            catch (ex) { alert("thisSection.fetchContexts() failed: " + ex); }
        }

        fieldset.appendChild(document.createElement("br"));
        fieldset.appendChild(document.createElement("br"));

        table = fieldset.appendChild(document.createElement("table"));
        table.setAttribute("border", "1");

        tr = table.insertRow(0);
        th = tr.appendChild(document.createElement("th"));
        th.appendChild(document.createTextNode("Metric"));
        th = tr.appendChild(document.createElement("th"));
        th.appendChild(document.createTextNode("Platform"));
        th = tr.appendChild(document.createElement("th"));
        th.appendChild(document.createTextNode("Host"));
        th = tr.appendChild(document.createElement("th"));
        th.appendChild(document.createTextNode("Branch"));
        th = tr.appendChild(document.createElement("th"));
        th.appendChild(document.createTextNode("Difference Tolerance"));
        th.setAttribute("class", "lastDiffHead");
        th = tr.appendChild(document.createElement("th"));
        th.appendChild(document.createTextNode("Stability Tolerance"));
        th.setAttribute("class", "lastDiffHead");
        th = tr.appendChild(document.createElement("th"));
        th.appendChild(document.createTextNode("Branch 2"));
        th.setAttribute("class", "branch2Head");
        th.setAttribute("colspan", "2");
        th = tr.appendChild(document.createElement("th"));
        th.appendChild(document.createTextNode("File ("));
        th.setAttribute("class", "fileHead");
        th.setAttribute("colspan", "2");
        span = th.appendChild(document.createElement("span"));
        span.setAttribute("id", "copyableFileName");
        span.setAttribute("style", "font-weight:normal");
        span.innerHTML = "";
        th.appendChild(document.createTextNode(")"));

        tr = table.insertRow(1);

        td = tr.insertCell(0);
        select = td.appendChild(document.createElement("select"));
        select.setAttribute("id", "metrics");
        select.onchange = function() { thisSection.modifyContextComponent('metrics'); }

        td = tr.insertCell(1);
        select = td.appendChild(document.createElement("select"));
        select.setAttribute("id", "platforms");
        select.onchange = function() { thisSection.modifyContextComponent('platforms'); }

        td = tr.insertCell(2);
        select = td.appendChild(document.createElement("select"));
        select.setAttribute("id", "hosts");
        select.onchange = function() { thisSection.modifyContextComponent('hosts'); }

        td = tr.insertCell(3);
        select = td.appendChild(document.createElement("select"));
        select.setAttribute("id", "branches");
        select.onchange = function() { thisSection.modifyContextComponent('branches'); }

        td = tr.insertCell(4);
        select = td.appendChild(document.createElement("select"));
        select.setAttribute("id", "bsDiffTolerance");
        select.onchange = function() { thisSection.dirtyUpdateSummaryButton(); }
        setDiffToleranceOptions(select);
        select.options[0].selected = true;

        td = tr.insertCell(5);
        select = td.appendChild(document.createElement("select"));
        select.setAttribute("id", "bsStabTolerance");
        select.onchange = function() { thisSection.dirtyUpdateSummaryButton(); }
        setStabToleranceOptions(select);
        select.options[0].selected = true;

        td = tr.insertCell(6);
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("id", "branch2Enabled");
        input.setAttribute("type", "checkbox");
        input.onchange = function() {
            var checked = document.getElementById("branch2Enabled").checked;
            if (checked)
                document.getElementById("fileEnabled").checked = false;
            thisSection.dirtyUpdateSummaryButton();
        }

        td = tr.insertCell(7);
        select = td.appendChild(document.createElement("select"));
        select.setAttribute("id", "branches2");
        select.onchange = function() { thisSection.dirtyUpdateSummaryButton(); }

        td = tr.insertCell(8);
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("id", "fileEnabled");
        input.setAttribute("type", "checkbox");
        input.onchange = function() {
            var checked = document.getElementById("fileEnabled").checked;
            if (checked)
                document.getElementById("branch2Enabled").checked = false;
            thisSection.dirtyUpdateSummaryButton();
        }

        td = tr.insertCell(9);
        input = td.appendChild(document.createElement("input"));
        input.setAttribute("id", "fileName");
        input.setAttribute("type", "file");
        input.onchange = function() { thisSection.dirtyUpdateSummaryButton(); }
        // ### 2 B DONE: Set a style/attribute to ensure that a file name that exceeds
        // the width of the text field is "scroll aligned" to the right by default,
        // so that the rightmost portion is always visible (clipping away the leftmost
        // portion as necessary).
        // Currently, it is the other way around.
    }

    this.appendSummary = function(section)
    {
        var thisSection = this; // ### hack?

        section.appendChild(document.createElement("br"));
        section.appendChild(document.createElement("br"));

        fieldset = section.appendChild(document.createElement("fieldset"));
        legend = fieldset.appendChild(document.createElement("legend"));
        legend.appendChild(document.createTextNode("Summary"));

        input = fieldset.appendChild(document.createElement("input"));
        input.setAttribute("type", "button");
        input.setAttribute("id", "updateBSSummary");
        input.setAttribute("value", "* Update");
        input.onclick = function() {
            try {
                thisSection.updateSummary();
            } catch (ex) {
                alert("thisSection.updateSummary() failed: " + ex);
            }
        }
        input.setAttribute("disabled", "");

        fieldset.appendChild(document.createElement("br"));
        fieldset.appendChild(document.createElement("br"));

        span = fieldset.appendChild(document.createElement("span"));
        span.setAttribute("id", "bsSummaryConfig");
        span.setAttribute("style", "font-size:12px");

        table = fieldset.appendChild(document.createElement("table"));
        table.setAttribute("id", "bsSummary");
        table.setAttribute("border", "1");

        tr = table.insertRow(0).insertCell(0).innerHTML = "<i>no data</i>";
    }

    this.create = function()
    {
        var sectionElem = document.createElement("div");
        insertTitle(sectionElem, "Benchmark Summary");
        sectionElem.appendChild(document.createElement("br"));
        appendHelpLink(sectionElem, "help.html#BenchmarkSummary");
        this.appendConfiguration(sectionElem);
        this.appendSummary(sectionElem);
        return sectionElem;
    }

    this.handleShow = function()
    {
//        var s = server();
//        alert("handleShow for 'Benchmark Summary' section, server: >" + s + "<");
    }

    this.state = function() {
        var state_ = [];

        // Metric
        state_["bsMetric"] = selectedOptions("metrics")[0];

        // Platform
        state_["bsPlatform"] = selectedOptions("platforms")[0];

        // Host
        state_["bsHost"] = selectedOptions("hosts")[0];

        // Branch
        state_["bsBranch"] = selectedOptions("branches")[0];

        // Difference tolerance
        state_["bsDiffTolerance"] = selectedOptions("bsDiffTolerance")[0];

        // Stability tolerance
        state_["bsStabTolerance"] = selectedOptions("bsStabTolerance")[0];

        // Branch 2 enabled
        state_["bsBranch2Enabled"] =
            document.getElementById("branch2Enabled").checked ? "true" : "false";

        // Branch 2
        state_["bsBranch2"] = selectedOptions("branches2")[0];

        // File enabled
        state_["bsFileEnabled"] =
            document.getElementById("fileEnabled").checked ? "true" : "false";

        // File name
        state_["bsFileName"] = document.getElementById("fileName").value;

        return state_;
    }

    this.setState = function(state_, handleDone) {
        // Metric
        var metric = state_["bsMetric"];

        // Platform
        var platform = state_["bsPlatform"];

        // Host
        var host = state_["bsHost"];

        // Branch
        var branch = state_["bsBranch"];

        // Difference tolerance
        var diffTolerance = state_["bsDiffTolerance"];

        // Stability tolerance
        var stabTolerance = state_["bsStabTolerance"];

        // Branch 2 enabled
        var branch2Enabled = (state_["bsBranch2Enabled"] == "true");

        // Branch 2
        var branch2 = state_["bsBranch2"];

        // File enabled
        var fileEnabled = (state_["bsFileEnabled"] == "true");

        // File name
        var fileName = state_["bsFileName"];

        // Update configuration ...
        var thisSection = this; // ### hack?
        this.setStateHandleDone = function()
        {
            this.updateSummaryControls();
            if (handleDone)
                handleDone();
        }
        var doneHandler = function() { thisSection.setStateHandleDone(); }
        this.setConfiguration(
            metric, platform, host, branch, diffTolerance, stabTolerance, branch2Enabled,
            branch2, fileEnabled, fileName, doneHandler);
    }

    SectionBase.call(this, "benchmarkSummaryButton");
    this.sectionElem = this.create();
}

BenchmarkSummarySection.prototype = new SectionBase();
BenchmarkSummarySection.prototype.constructor = BenchmarkSummarySection;
BenchmarkSummarySection.instance = function()
{
    if (!BenchmarkSummarySection.instance_)
        BenchmarkSummarySection.instance_ = new BenchmarkSummarySection();
    return BenchmarkSummarySection.instance_;
}
function bsSection() { return BenchmarkSummarySection.instance(); }
