/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the BM project on Qt Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include <QtCore>
#include <QSqlQuery>
#include "bm.h"

static void printUsage()
{
    qDebug().nospace() << "usage: " << qApp->arguments().first().toAscii().data() <<
        " help | initdb <new SQLite database file> | <port> <existing SQLite database file>";
}

static bool initDatabase(const QString &dbfile, QString *error)
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(dbfile);
    if (!db.open()) {
        *error = QString("failed to open database: \"%1\"").arg(dbfile);
        return false;
    }

    QSqlQuery query;

    bool ok;


    // *** Create tables ***

    // metric
    ok = query.exec(
        "CREATE TABLE metric(id INTEGER PRIMARY KEY AUTOINCREMENT"
        ", name TEXT NOT NULL"
        ", UNIQUE(name));");
    Q_ASSERT(ok);

    // platform
    ok = query.exec(
        "CREATE TABLE platform(id INTEGER PRIMARY KEY AUTOINCREMENT"
        ", name TEXT NOT NULL"
        ", UNIQUE(name));");
    Q_ASSERT(ok);

    // host
    ok = query.exec(
        "CREATE TABLE host(id INTEGER PRIMARY KEY AUTOINCREMENT"
        ", name TEXT NOT NULL, details TEXT"
        ", UNIQUE(name));");
    Q_ASSERT(ok);

    // branch
    ok = query.exec(
        "CREATE TABLE branch(id INTEGER PRIMARY KEY AUTOINCREMENT"
        ", gitRepo TEXT NOT NULL, gitBranch TEXT NOT NULL"
        ", UNIQUE(gitRepo, gitBranch));");
    Q_ASSERT(ok);

    // context
    ok = query.exec(
        "CREATE TABLE context(id INTEGER PRIMARY KEY AUTOINCREMENT"
        ", metricId INTEGER REFERENCES metric(id)"
        ", platformId INTEGER REFERENCES platform(id)"
        ", hostId INTEGER REFERENCES host(id)"
        ", branchId INTEGER REFERENCES branch(id)"
        ", UNIQUE(metricId, platformId, hostId, branchId));");
    Q_ASSERT(ok);

    // benchmark
    ok = query.exec(
        "CREATE TABLE benchmark(id INTEGER PRIMARY KEY AUTOINCREMENT"
        ", testCase TEXT NOT NULL, testFunction TEXT NOT NULL, dataTag TEXT NOT NULL"
        ", UNIQUE(testCase, testFunction, dataTag));");
    Q_ASSERT(ok);

    // bmcontext
    ok = query.exec(
        "CREATE TABLE bmcontext(id INTEGER PRIMARY KEY AUTOINCREMENT"
        ", contextId INTEGER REFERENCES context(id)"
        ", benchmarkId INTEGER REFERENCES benchmark(id)"
        ", UNIQUE(contextId, benchmarkId));");
    Q_ASSERT(ok);

    // snapshot
    ok = query.exec(
        "CREATE TABLE snapshot(id INTEGER PRIMARY KEY AUTOINCREMENT"
        ", sha1 TEXT NOT NULL"
        ", UNIQUE(sha1));");
    Q_ASSERT(ok);

    // result
    ok = query.exec(
        "CREATE TABLE result(id INTEGER PRIMARY KEY AUTOINCREMENT"
        ", bmcontextId INTEGER REFERENCES bmcontext(id)"
        ", snapshotId INTEGER REFERENCES snapshot(id)"
        ", timestamp INTEGER NOT NULL"
        ", value REAL NOT NULL"
        ", UNIQUE(bmcontextId, snapshotId));");
    Q_ASSERT(ok);

    // indexConfig
    ok = query.exec(
        "CREATE TABLE indexConfig(id INTEGER PRIMARY KEY AUTOINCREMENT"
        ", name TEXT NOT NULL"
        ", baseTimestamp INTEGER NOT NULL"
        ", medianWinSize INTEGER NOT NULL"
        ", UNIQUE(name));");
    Q_ASSERT(ok);

    // icTestCase
    ok = query.exec(
        "CREATE TABLE icTestCase(id INTEGER PRIMARY KEY AUTOINCREMENT"
        ", indexConfigId INTEGER REFERENCES indexConfig(id)"
        ", name TEXT NOT NULL"
        ", UNIQUE(indexConfigId, name));");
    Q_ASSERT(ok);

    // icMetric
    ok = query.exec(
        "CREATE TABLE icMetric(id INTEGER PRIMARY KEY AUTOINCREMENT"
        ", indexConfigId INTEGER REFERENCES indexConfig(id)"
        ", name TEXT NOT NULL"
        ", UNIQUE(indexConfigId, name));");
    Q_ASSERT(ok);

    // icPlatform
    ok = query.exec(
        "CREATE TABLE icPlatform(id INTEGER PRIMARY KEY AUTOINCREMENT"
        ", indexConfigId INTEGER REFERENCES indexConfig(id)"
        ", name TEXT NOT NULL"
        ", UNIQUE(indexConfigId, name));");
    Q_ASSERT(ok);

    // icHost
    ok = query.exec(
        "CREATE TABLE icHost(id INTEGER PRIMARY KEY AUTOINCREMENT"
        ", indexConfigId INTEGER REFERENCES indexConfig(id)"
        ", name TEXT NOT NULL"
        ", UNIQUE(indexConfigId, name));");
    Q_ASSERT(ok);

    // icBranch
    ok = query.exec(
        "CREATE TABLE icBranch(id INTEGER PRIMARY KEY AUTOINCREMENT"
        ", indexConfigId INTEGER REFERENCES indexConfig(id)"
        ", name TEXT NOT NULL"
        ", UNIQUE(indexConfigId, name));");
    Q_ASSERT(ok);


    // *** Create indexes ***
    ok = query.exec("CREATE INDEX index_metric_name ON metric(name);");
    Q_ASSERT(ok);

    ok = query.exec("CREATE INDEX index_platform_name ON platform(name);");
    Q_ASSERT(ok);

    ok = query.exec("CREATE INDEX index_host_name ON host(name);");
    Q_ASSERT(ok);

    ok = query.exec("CREATE INDEX index_branch_name ON branch(gitRepo, gitBranch);");
    Q_ASSERT(ok);

    ok = query.exec(
        "CREATE INDEX index_context_all ON context(metricId, platformId, hostId, branchId);");
    Q_ASSERT(ok);

    ok = query.exec(
        "CREATE INDEX index_benchmark_name ON benchmark(testCase, testFunction, dataTag);");
    Q_ASSERT(ok);

    ok = query.exec("CREATE INDEX index_bmcontext_contextId ON bmcontext(contextId);");
    Q_ASSERT(ok);

    ok = query.exec("CREATE INDEX index_bmcontext_benchmarkId ON bmcontext(benchmarkId);");
    Q_ASSERT(ok);

    ok = query.exec("CREATE INDEX index_bmcontext_all ON bmcontext(contextId, benchmarkId);");
    Q_ASSERT(ok);

    ok = query.exec("CREATE INDEX index_result_snapshotId ON result(snapshotId);");
    Q_ASSERT(ok);

    ok = query.exec("CREATE INDEX index_result_bmcontextId ON result(bmcontextId);");
    Q_ASSERT(ok);

    ok = query.exec("CREATE INDEX index_result_timestamp ON result(timestamp);");
    Q_ASSERT(ok);

    ok = query.exec(
        "CREATE INDEX index_result_bmcontextId_timestamp ON result(bmcontextId, timestamp);");
    Q_ASSERT(ok);

    ok = query.exec(
        "CREATE INDEX index_result_bmcontextId_snapshotId ON result(bmcontextId, snapshotId);");
    Q_ASSERT(ok);

    ok = query.exec("CREATE INDEX index_indexConfig_name ON indexConfig(name);");
    Q_ASSERT(ok);

    ok = query.exec(
        "CREATE INDEX index_icTestCase_indexConfigId_name ON icTestCase(indexConfigId, name);");
    Q_ASSERT(ok);

    ok = query.exec(
        "CREATE INDEX index_icMetric_indexConfigId_name ON icMetric(indexConfigId, name);");
    Q_ASSERT(ok);

    ok = query.exec(
        "CREATE INDEX index_icPlatform_indexConfigId_name ON icPlatform(indexConfigId, name);");
    Q_ASSERT(ok);

    ok = query.exec(
        "CREATE INDEX index_icHost_indexConfigId_name ON icHost(indexConfigId, name);");
    Q_ASSERT(ok);

    ok = query.exec(
        "CREATE INDEX index_icBranch_indexConfigId_name ON icBranch(indexConfigId, name);");
    Q_ASSERT(ok);

    return true;
}

static QString envValue(const QString &name)
{
    QStringList sysenv = QProcess::systemEnvironment();
    QRegExp rx;
    int pos;
    rx = QRegExp(QString("^%1=(\\S+)$").arg(name));
    if ((pos = sysenv.indexOf(rx)) != -1) {
        rx.indexIn(sysenv.at(pos));
        return rx.cap(1);
    }
    return QString();

// ### The body of this function could be simplified to this in 4.6:
//    return QProcessEnvironment::systemEnvironment().value(name);
}

static void prepare(QSqlDatabase &db)
{
    if (envValue("BMDISKWRITESYNC") == "off") {
        // Disable synchrounous disk writes ...
        QSqlQuery query(db);
        bool ok;
        ok = query.exec("PRAGMA synchronous=off;");
        Q_ASSERT(ok);
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    if ((argc == 1 || QString(argv[1]).endsWith("help"))) {
        printUsage();
        return 0;
    }

    const QString dbfile = argv[2];

    if (QString(argv[1]) == "initdb") {
        if (argc != 3) {
            printUsage();
            return 1;
        }
        if (QFile::exists(dbfile)) {
            qDebug() << "database file already exists:" << dbfile;
            return 1;
        }
        QString error;
        if (!initDatabase(dbfile, &error)) {
            qDebug() << "failed to initialize new database:" << error.toLatin1().data();
            return 1;
        }
        return 0;
    }

    bool ok;
    const quint16 serverPort = QString(argv[1]).toInt(&ok);
    if (!ok) {
        qDebug() << "invalid port number";
        printUsage();
        return 1;
    }

    if (!QFile::exists(dbfile)) {
        qDebug() << "database file doesn't exist:" << dbfile;
        return 1;
    }

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(dbfile);
    if (!db.open()) {
        qDebug() << "failed to open database:" << dbfile;
        return 1;
    }

    prepare(db);

    Q_ASSERT(db.isValid());
    BMServer server (serverPort, &db);

    return app.exec();
}
